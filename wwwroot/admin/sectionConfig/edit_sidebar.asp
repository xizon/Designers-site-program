<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<!--#include file="pluginsPlace.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
if pluginsPlace=0 then
   pluginsPlaceTxt="展示在侧边栏集合后面"
else
   pluginsPlaceTxt="展示在侧边栏集合前面"
end if
%>

<script>
//表单判断
function checkForm(){
	if(document.formM.columnName.value==""){
		ChkFormTip_page("插件标签不能为空",0);
		return false;
	}
	if(document.formM.intro.value==""){
		ChkFormTip_page("标签名称不能为空",0);
		return false;
	}
	
	//ok
	loadingCreate();
	return true;
		
}


function editPlace(){
    floatWin('修改位置','<div align=left><form class="ui-element"  action="action.asp?action=editPlace" method="post" target="actionPage" onSubmit="$(\'#setState\').html(\'&nbsp;设置成功！\')"><select name="placeNow"><option value="<%=pluginsPlace%>" selected="selected"><%=pluginsPlaceTxt%></option><option  value="1">展示在侧边栏集合前面</option><option  value="0" ><br/>展示在侧边栏集合后面</option></select>&nbsp;<br/><br/><input name="Submit" type="submit" value="确认" class="ui-btn"  /><span id="setState"></span></form></div>',300,100,100,0,0,0);
}


$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=侧边栏设置", "link1=<%=MY_sitePath%>admin/sectionConfig/edit_sidebar.asp", "title2=SEO设置", "link2=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-seo", "title3=上传设置", "link3=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-upload", "title4=标签tags设置", "link4=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-tags", "title5=自动生成设置", "link5=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-autoCreate", "title6=评论留言设置", "link6=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-msg", "title7=分页设置", "link7=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-pages",  "title8=缩略图和剪裁设置", "link8=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-img", "title9=列表生成参数设置", "link9=<%=MY_sitePath%>admin/website/setcreateList.asp" , "title10=API数据接口设置", "link10=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-apiset", "title11=其它设置", "link11=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-other");
	   }
	);
	
});

function editFrame(ID){
	ShowIFrameEdit('侧边栏管理','sectionConfig/editFrame.asp?action=sidebar&ID=' + ID,350,500,520,350,100,0);
	
}




</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="edit_sidebar_home.asp" class="link">首页侧边栏</a>
                
                <!-- 当前位置 -->
                当前位置： 网站设置 | 侧边栏
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
                <!-- 后台提示 -->
                
                <div class="notification information png_bg content-alert">
                    <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        1.不同的模板的设计，是否调用侧边栏信息也是由设计者自由安排<br>
                        2.点击上面的导航设置<strong>首页侧边栏</strong><br>
                        3.您可以自定义添加侧边栏模块,它们将与侧边栏集合一同管理和展示，比如新浪微博小挂件等等<br>
                        4.在下面的列表中直接管理模板中的侧边栏集合标签：<strong>${MySidebar}(有分类模块,非首页)、${MySidebar_NonCate}(无分类模块,非首页)</strong><br>
                        5.您也可以在模板中插入<strong>侧边栏模块通用标签${listcontent_comm_???}</strong>到模板任意位置,标签不受后台面板的屏蔽效果影响。详细模板标签，请查看<a href="<%=MY_sitePath%>皮肤主题自定义API教程文档.html" target="_blank">网站标签集</a><br>
                        6.您可以调整增加的自定义模块与侧边栏集合的前后位置<a href="javascript:" onclick="editPlace()">点这里设置</a>
                       
                    </div>
                </div>
          
                
                <!-- 表单域  -->
                <div id="contentShow">
                
					 <%
                     if callCommInfoChk("${MySidebar") = false then response.Write "<script>floatWin('温馨提示','<div class=tipsFloatWin>目前模板里暂时没有<strong>侧边栏</strong>标签，无法看到效果<br>(其它原因：此主题并没有设计侧边栏)</div>',350,0,100,0,0,0);</script>"
                     %>
                
                    <div class="addInfo"><a href="javascript:" onClick="switchInfo('#addInfo','关闭添加','添加模块');"><span class="io">添加模块</span></a><a href="edit_sidebar_home.asp">设置首页侧边栏</a></div>
                    
                                    
                                  <div id="addInfo" data-open="0" style="display:none">
                                  
                                    <form name="formM" action="action.asp?action=addSidebar" method="post" onSubmit="javascript:return checkForm();">
                                           
                                                <table width="100%" class="setting" cellspacing="0">
                                                    
                                                    <!-- 标题 -->                        
                                                    <thead class="setHead-class">
                                                        <tr>
                                                               <th  class="setTitle-class"></th>
                                                               <th colspan="2"></th>
                                                        </tr>
                                                        
                                                    </thead>
                                                    
                                                 
                                                    <!-- 底部 -->
                                                    <tfoot>
                                                    
                                                        <tr>
                                                            <td colspan="3"  class="ui-element">         
                                                                <div class="btnBox-iframe">
                                                                    <input type="submit" value="添加模块" class="ui-btn" id="needCreateLoadingBtn">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                    
                                                    
                                                    <tbody>
                                                       <!-- +++++++  内容   begin +++++++  -->
                            
                                                        <tr>
                                                            <td class="title">名称<a href="javascript:void(0)" class="help" data-info="侧边栏模块,使用标签${MySidebar}和${MySidebar_NonCate},自定义模块与侧边栏一同出现"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：<span class="backStage_star">*</span></td>
                                                            <td colspan="2">
                                                            
                                                               <input name="columnName" value="newPlugins" type="hidden" />
                                                               <input name="sort" value="1" type="hidden" />
                                                                                            
                                                               <input name="intro" type="text" size="15" />
   
                                                              
                                                            </td>
                                                        </tr>  
                                                        
                                                        <tr>
                                                            <td class="title">内容<a href="javascript:void(0)" class="help" data-info="支持HTM、JS代码"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                                            <td colspan="2">
                                                            
                                                               <textarea name="content" cols="80" rows="5" ><%=rs("content")%></textarea>
   
                                                              
                                                            </td>
                                                        </tr>  

                                                  
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                                                
                                                    </tbody>
                                                    
                                                </table>
                   
                   
                                    </form>  
 
                                   
                                </div>    
                                
                                
                                
                          <!-- 列表 -->
                        
           
                        
                                                <table width="100%" class="list" cellspacing="1">
                                                    
                                                    <!-- 标题 -->
                                                    <thead class="setTitle">
                                                        <tr>
                                                           <!-- 有多少竖列 -->
                                                           <th>模块名称</th>
                                                           <th>内容可修改</th>
                                                           <th>内容(点击相关条目查看全部)</th> 
                                                           <th>排序</th>
                                                           <th>显示</th>
                                                           <th>操作</th>
                                                        </tr>
                                                        
                                                   </thead>
                                                    
                                                    
                                                    <tbody>
                                                    
                                                       <!-- +++++++  内容   begin +++++++  -->
                                                                                                            
														<%
                                                        '----------显示记录模块
                                                        dim cat,j
                                                        j=0
             
                                                        set rs =server.createobject("adodb.recordset")
                                                        sql="select * from [plugins] where columnName <> 'imgshow' and columnName <> 'columnImgSwitch' order by index desc"
                                                        rs.open sql,db,1,1
          
                                                        do while not rs.eof and not rs.bof
                                                        j=j+1
														
														if rs("sort")<>1 then coShow = "style=""display:none""" else  coShow = ""
														

														if rs("columnName") = "newPlugins" then 
														
															newPluginsChk = "[自定义]"
														
														else
														
															newPluginsChk = ""
														
														end if
														
														if rs("columnName") = "newColumn1" or rs("columnName") = "newColumn2" or rs("columnName") = "newColumn3" or  rs("columnName") = "newColumn4" or rs("columnName") = "sideTags" then 
														
															newPluginsChk2 = "[预定义]"
														
														else
														
															newPluginsChk2 = ""
														
														end if
														
														
														if rs("columnName") <> "newPlugins" and rs("columnName") <>  "newColumn1" and rs("columnName") <>  "newColumn2" and rs("columnName") <>  "newColumn3" and  rs("columnName") <>  "newColumn4" then 
														
	
															newPluginsChk3 = "[系统]"
														
														else
														
															newPluginsChk3 = ""
														
														end if
														

														
                                                        %>
                                                            
                                                    
                                                        <!--////////////////循环开始 ////////////////-->
                                                        <tr <%=coShow%>>
                                                            
                                                            <td class="title">
                                                            <%=rs("intro")%>
  
                                                            
                                                            </td>
                                                            
                                                            <td>
                                                            
																<%
                                                                if newPluginsChk3 = "[系统]" then viewShow="(系统自动更新)"
                                                                if newPluginsChk2 = "[预定义]" then viewShow="<img src=../../plugins/d-s-management-ui/img/status-y.GIF>"
																if newPluginsChk = "[自定义]" then viewShow="<img src=../../plugins/d-s-management-ui/img/status-y.GIF>"
                                                                response.Write viewShow
                                                                %>
                                  
                                                            </td>
                                                            <td onClick="switchInfo('#msg-<%=j%>','','');">
                                                            <img src="../../plugins/d-s-management-ui/img/view.gif">
                                                            
                                                            <div id="msg-<%=j%>" data-open="0" style="display:none" class="msgShowBox">
                                                                 <%=stripHTML(rs("content"))%>
                                                            </div>
 
                                                            
                                                            
                                                            </td>

                                                            <td>
     
														    	<%=rs("index")%>
                    
                      
                                                            </td>
                                                            <td>
																<%
                                                                if rs("view")=0 then viewShow="<img src=../../plugins/d-s-management-ui/img/status-y.GIF>"
                                                                if rs("view")=1 then viewShow="<img src=../../plugins/d-s-management-ui/img/status-n.GIF>"
                                                                response.Write viewShow
                                                                %>
                                                            
                                                            </td>
                                                            
                                                            <td>
                                                                <!-- Icons -->
                                                                 <a href="javascript:" data-open="0" onclick="editFrame('<%=rs("ID")%>');" title="编辑"><img src="../../plugins/d-s-management-ui/img/hammer_screwdriver.png"></a> 
                                                                 
                                                                 <input type="hidden" value="<%=rs("ID")%>" name="ID<%=j%>" />
                                                            </td>
                                                        </tr>
                                                        
                                                        <!--////////////////循环结束 ////////////////-->
                                                        
														<%
                                                         rs.movenext
                                                         loop
                                                         %>   
                                                         
                                                         <iframe name="delFrame" style="display:none"></iframe>
                                                         <input type="hidden" value="<%=j%>" name="total" />
                                                         
                                                         <!-- 无数据 -->
                                                         <%if rs.bof and rs.eof then %>
                                                         
                                                            <tr>
                                                                <td class="title" colspan="6">
                                                                 暂时还没有数据！
                                                                </td>
                                                            </tr>
                                                         
                                                         <%end if%>
														 
                                                
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                        
                                                
                                                    </tbody>
                                                    
                                                </table>
                                            
                 
            
                               
                     </div> 
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


