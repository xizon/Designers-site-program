<!--#include file="root_temp-edit/_commInfo.asp" -->
<%
'=========================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=========================================

'载入模板配置文档
set xmlDom = server.CreateObject("MSXML2.DOMDocument")
xmlDom.async = false
setXmlPath = ""&MY_sitePath&"template/tempConfig.xml"


'载入独立插件配置文档
if FileExistsStatus(""&MY_sitePath&"template/pluginsInstallConfig.xml") = 1 then 
    '////////////////////////////有独立插件配置文件状态

		'载入插件配置文档
		setXmlPath_plugins = ""&MY_sitePath&"template/pluginsInstallConfig.xml"
		if not xmlDom.Load(Server.MapPath(setXmlPath_plugins))  then 
			'response.write("Load wrong!")
		else
		
  
			'读取XML信息
			set tempInfo_plugins = xmlDom.getElementsByTagName("DSPluginsSetting")  
			pluginsConfigOk = tempInfo_plugins(0).selectSingleNode("pluginsInstallCompleted").Text
		
		
			if pluginsConfigOk = 0 then
		
				'插件及其自定义标签
				pluginsNumMax_single = 30
				
				'////////////////移动文件或者文件夹
				for f = 1 to pluginsNumMax_single
				
						'============读取插件节点
						thisPluginPath = tempInfo_plugins(0).selectSingleNode("plugins_"&f).Text
						thisPluginPathTo = tempInfo_plugins(0).selectSingleNode("plugins_"&f).GetAttribute("pathTo")
						thisPluginType = tempInfo_plugins(0).selectSingleNode("plugins_"&f).GetAttribute("type")
						thisPluginTypeAppName = tempInfo_plugins(0).selectSingleNode("plugins_"&f).GetAttribute("appName")
						thisPluginTypeAppURL = replace(tempInfo_plugins(0).selectSingleNode("plugins_"&f).GetAttribute("appURL"),"{rootPath}",""&MY_sitePath&"")
						thisPluginTypeAppCode = tempInfo_plugins(0).selectSingleNode("plugins_"&f).GetAttribute("appCode")
							
							  
					    thisPluginPath = clearRightString(thisPluginPath,"/")
						thisPluginPathTo = clearRightString(thisPluginPathTo,"/")

				
						'============退出循环
						if instr(showFile(setXmlPath_plugins),"plugins_"&f)=0  then exit for
						
						
						
						'============开始移动插件
						
						if thisPluginType = 1 then 
							MoveFolder MY_sitePath&"template/"&thisPluginPath,MY_sitePath&thisPluginPathTo
						else
							CopyFile MY_sitePath&"template/"&thisPluginPath,MY_sitePath&thisPluginPathTo
			
						end if
						
						'============创建插件app数据	
						if thisPluginTypeAppName <> "none" then	
							
								set rsChk =server.createobject("adodb.recordset")
								sqlChk="select * from App where appName='"&thisPluginTypeAppName&"'"
								rsChk.open sqlChk,db,1,1 
								
								'存在性验证
								if not rsChk.EOF then
									'已存在
							
								else
									'执行添加操作
									GetNum_A = db.execute("select count(1) from App")(0)
			
									set rs =server.createobject("adodb.recordset")
									sql="select * from App"
									rs.open sql,db,1,2
									rs.addnew
									
									rs("appName")=thisPluginTypeAppName
									rs("appCode")=thisPluginTypeAppCode
									rs("appID")=createBase64ID(URLDecode(thisPluginTypeAppName))
									rs("appURL")=thisPluginTypeAppURL
									rs("index")=GetNum_A + 1
								
									rs.update
									rs.close
									set rs=nothing
									
									
								end if
							
								
								rsChk.close
								set rsChk=nothing
								
								
						end if
						
					
				
		
				next
				
			
			
				'配置文件读取完毕判断
				CreateFile setXmlPath_plugins,replace(showFile(setXmlPath_plugins),"<pluginsInstallCompleted>0</pluginsInstallCompleted>","<pluginsInstallCompleted>1</pluginsInstallCompleted>")
				
				response.Write "<script>parent.floatWin('温馨提示','<div class=tipsFloatWin>&nbsp;&nbsp;插件安装成功...</div>',350,0,225,0,1,1);</script>"&vbcrlf
				
			
			end if
				
		
		
		end if

end if


if not xmlDom.Load(Server.MapPath(setXmlPath))  then 
    'response.write("Load wrong!")
else

    '读取节点
	set setTempPara = xmlDom.getElementsByTagName("config")  
	set setnode1 = setTempPara(0).selectSingleNode("setting_article")
	set setnode2 = setTempPara(0).selectSingleNode("setting_digitalPage")  
	set setnode3 = setTempPara(0).selectSingleNode("setting_tagsList") 
	set setnode4 = setTempPara(0).selectSingleNode("setting_rightNav")

	setTempValue1 = setnode1.GetAttribute("ImgClippingWidth")
	setTempValue2 = setnode1.GetAttribute("ImgClippingHeight")
	setTempValue3 = setnode2.GetAttribute("use")
	setTempValue4 = setnode3.GetAttribute("maxNum")
	setTempValue5 = setnode4.GetAttribute("use")
	

	
	'读取模板信息
	set tempInfo = xmlDom.getElementsByTagName("DSsetting")  
	tempConfigOk = tempInfo(0).selectSingleNode("configCompleted").Text
	updateMenuOk = tempInfo(0).selectSingleNode("updateMenu").Text
	
	
	if tempConfigOk = 0 then
	    
		
		'发放类型 
		tempVersion = tempInfo(0).selectSingleNode("version").Text
		if tempVersion = "free" then 
		    tempVersionShow = "免费共享版"
		elseif tempVersion = "customize" then 
		    tempVersionShow = "定制版"
		elseif tempVersion = "charge" then 
		    tempVersionShow = "收费版"
		else
		    tempVersionShow = "未指定"
		end if
		
		
		
		
		tempInfoCode = tempInfoCode &"<div class=""tempConfigInfo"">"&vbcrlf
		tempInfoCode = tempInfoCode &"    <p><b>模板名称</b>:&nbsp;&nbsp;"&tempInfo(0).selectSingleNode("name").Text&"</p>"&vbcrlf
		tempInfoCode = tempInfoCode &"    <p><b>发放类型</b>:&nbsp;&nbsp;"&tempVersionShow&"</p>"&vbcrlf
		tempInfoCode = tempInfoCode &"    <p><b>模板作者</b>:&nbsp;&nbsp;"&tempInfo(0).selectSingleNode("author").Text&"</p>"&vbcrlf
		tempInfoCode = tempInfoCode &"   <p><b>作者邮箱</b>:&nbsp;&nbsp;"&tempInfo(0).selectSingleNode("email").Text&"</p>"&vbcrlf
		tempInfoCode = tempInfoCode &"    <p class=""i"">[1] 初次使用产品或者更换模板皮肤<strong>(template文件夹)</strong>，会出现一次模板配置信息，更换模板后配置文件自动屏蔽。</p>"&vbcrlf
		tempInfoCode = tempInfoCode &"    <p class=""i"">[2] 欢迎您设计皮肤共享，如果其它用户使用您的模板，同样会弹出此信息。</p>"&vbcrlf
		tempInfoCode = tempInfoCode &"    <p class=""i"">[3] 只需要您懂HTML,CSS语言,参看皮肤API文档,不妨试一试设计DIY皮肤,并发布到官方邮箱xizon@c945.com，审核通过，将署名共享到互联网。</p>"&vbcrlf
		tempInfoCode = tempInfoCode &"    <a class=""closeT"" href=""javascript:"" onClick=""$('.tempConfigInfo').fadeOut(500);"" title=""我知道了，关闭窗口"">×</a>"&vbcrlf
		tempInfoCode = tempInfoCode &"</div>"&vbcrlf

		'插件及其自定义标签
		pluginsNumMax = 30
		
		'////////////////移动文件或者文件夹
		for u = 1 to pluginsNumMax
		
		        '============读取插件节点
				thisPluginPath = tempInfo(0).selectSingleNode("plugins_"&u).Text
				thisPluginPathTo = tempInfo(0).selectSingleNode("plugins_"&u).GetAttribute("pathTo")
				thisPluginType = tempInfo(0).selectSingleNode("plugins_"&u).GetAttribute("type")
				thisPluginTypeAppName = tempInfo(0).selectSingleNode("plugins_"&u).GetAttribute("appName")
				thisPluginTypeAppURL = replace(tempInfo(0).selectSingleNode("plugins_"&u).GetAttribute("appURL"),"{rootPath}",""&MY_sitePath&"")
				thisPluginTypeAppCode = tempInfo(0).selectSingleNode("plugins_"&u).GetAttribute("appCode")
					
				thisPluginPath = clearRightString(thisPluginPath,"/")
				thisPluginPathTo = clearRightString(thisPluginPathTo,"/")  
		
				'============退出循环
				if instr(showFile(setXmlPath),"plugins_"&u)=0  then exit for
				
				
				
				'============开始移动插件
				
				if thisPluginType = 1 then 
					MoveFolder MY_sitePath&"template/"&thisPluginPath,MY_sitePath&thisPluginPathTo
				else
					CopyFile MY_sitePath&"template/"&thisPluginPath,MY_sitePath&thisPluginPathTo
				end if
				
				'============创建插件app数据	
				if thisPluginTypeAppName <> "none" then	
					
						set rsChk =server.createobject("adodb.recordset")
						sqlChk="select * from App where appName='"&thisPluginTypeAppName&"'"
						rsChk.open sqlChk,db,1,1 
						
						'存在性验证
						if not rsChk.EOF then
							'已存在
					
						else
							'执行添加操作
							GetNum_A = db.execute("select count(1) from App")(0)

							set rs =server.createobject("adodb.recordset")
							sql="select * from App"
							rs.open sql,db,1,2
							rs.addnew
							
							rs("appName")=thisPluginTypeAppName
							rs("appCode")=thisPluginTypeAppCode
							rs("appID")=createBase64ID(URLDecode(thisPluginTypeAppName))
							rs("appURL")=thisPluginTypeAppURL
							rs("index")=GetNum_A + 1
						
							rs.update
							rs.close
							set rs=nothing
							
							
						end if
					
						
						rsChk.close
						set rsChk=nothing
						
						
				end if
				
			
		

		next
		
		
		'/////////////////创建自定义标签
		for k = 1 to pluginsNumMax

				'============读取自定义标签节点
				thisLabelContent = Replace(Replace(Replace(Replace(tempInfo(0).selectSingleNode("define_"&k).Text,"{article}",MY_createFolder_art),"{works}",MY_createFolder_work),"{mood}",MY_createHtml_mood),"{rootPath}",""&MY_sitePath&"")
				thisLabelSymbol = tempInfo(0).selectSingleNode("define_"&k).GetAttribute("symbol")
				thisLabelTitle = tempInfo(0).selectSingleNode("define_"&k).GetAttribute("title")
				
				'============退出循环
				if instr(showFile(setXmlPath),"define_"&k)=0  then exit for

	            '============开始创建标签
				
				labelSymbol = Replace(Replace(Replace(Replace(HTMLEncode(Trim(thisLabelSymbol)),"&#39;",""),"&quot;",""),"&lt;",""),"&gt;","")
				lableName = Replace(Replace(Replace(Replace(HTMLEncode(Trim(thisLabelTitle)),"&#39;",""),"&quot;",""),"&lt;",""),"&gt;","")
				lableContent = replace(replace(replace(replace(replace(replace(replace(replace(thisLabelContent,"width=""auto""",""),"height=""auto""",""),"width='auto'",""),"height='auto'",""),"{article}",MY_createFolder_art),"{works}",MY_createFolder_work),"{mood}",MY_createHtml_mood),"{rootPath}",""&MY_sitePath&"")
				
				
				'添加自定义标签
				set rsChk =server.createobject("adodb.recordset")
				sqlChk="select * from [define_label] where labelSymbol='"&labelSymbol&"'"
				rsChk.open sqlChk,db,1,1 
				
				'存在性验证
				if not rsChk.EOF then
					'已存在

				else
					'执行添加操作
			
					
					set rs =server.createobject("adodb.recordset")
					sql="select * from [define_label]"
					rs.open sql,db,1,2
					rs.addnew
					
					rs("labelSymbol")=labelSymbol
					rs("lableName")=lableName
					rs("lableContent")=lableContent
					
			
					rs.update
					rs.close
					set rs=nothing
					
					
				end if
			
				
				rsChk.close
				set rsChk=nothing


		next
		
		
		
		websetConfigPath = ""&MY_sitePath&"config/setValue.asp"
		
		websetConfigCode = showFile(""&MY_sitePath&"config/setValue.asp")
		
		
		'/////////////////Banner更新
		for b = 1 to 12

				'============读取信息节点
				B_ID=HTMLEncode(tempInfo(0).selectSingleNode("banner_"&b).GetAttribute("id"))
				B_title=HTMLEncode(tempInfo(0).selectSingleNode("banner_"&b).Text)
				B_src=HTMLEncode(replace(tempInfo(0).selectSingleNode("banner_"&b).GetAttribute("src"),"{rootPath}",""&MY_sitePath&""))
				B_window=HTMLEncode(tempInfo(0).selectSingleNode("banner_"&b).GetAttribute("window"))
				B_imgURL=HTMLEncode(replace(tempInfo(0).selectSingleNode("banner_"&b).GetAttribute("img"),"{rootPath}",""&MY_sitePath&""))
				B_imgSmallURL=HTMLEncode(replace(tempInfo(0).selectSingleNode("banner_"&b).GetAttribute("imgSmall"),"{rootPath}",""&MY_sitePath&""))

				
				'============更新banner数据
				strsql_b="update [banner] set [title]='"& B_title &"',[src]='"& B_src &"',[window]='"& B_window &"',imgURL='"& B_imgURL &"',imgSmallURL='"& B_imgSmallURL &"' where bannerID="&B_ID
				db.execute(strsql_b)					
			

		next

	
	
	   '-----剪裁图片
		websetConfigCode = replace(replace(websetConfigCode,"MySiteValue60 = "&CHR(34)&""&MY_AbbreviativeImg_list_W&""&CHR(34)&"","MySiteValue60 = "&CHR(34)&""&setTempValue1&""&CHR(34)&""),"MySiteValue60 = "&CHR(34)&""&CHR(34)&"","MySiteValue60 = "&CHR(34)&""&setTempValue1&""&CHR(34)&"")
		websetConfigCode = replace(replace(websetConfigCode,"MySiteValue69 = "&CHR(34)&""&MY_cutImgSelectDefaultHeight&""&CHR(34)&"","MySiteValue69 = "&CHR(34)&""&setTempValue2&""&CHR(34)&""),"MySiteValue69 = "&CHR(34)&""&CHR(34)&"","MySiteValue69 = "&CHR(34)&""&setTempValue2&""&CHR(34)&"")
	
	   '-----数字分页
		websetConfigCode = replace(replace(websetConfigCode,"MySiteValue26 = "&CHR(34)&""&MY_createHtml_usePageNum&""&CHR(34)&"","MySiteValue26 = "&CHR(34)&""&setTempValue3&""&CHR(34)&""),"MySiteValue26 = "&CHR(34)&""&CHR(34)&"","MySiteValue26 = "&CHR(34)&""&setTempValue3&""&CHR(34)&"")
	
	   '-----tags输出
		websetConfigCode = replace(replace(websetConfigCode,"MySiteValue71 = "&CHR(34)&""&MY_newTagsCloudNum_side&""&CHR(34)&"","MySiteValue71 = "&CHR(34)&""&setTempValue4&""&CHR(34)&""),"MySiteValue71 = "&CHR(34)&""&CHR(34)&"","MySiteValue71 = "&CHR(34)&""&setTempValue4&""&CHR(34)&"")
	
	   '-----右键导航
	   websetConfigCode = replace(replace(websetConfigCode,"MySiteValue44 = "&CHR(34)&""&MY_rightNavFun&""&CHR(34)&"","MySiteValue44 = "&CHR(34)&""&setTempValue5&""&CHR(34)&""),"MySiteValue44 = "&CHR(34)&""&CHR(34)&"","MySiteValue44 = "&CHR(34)&""&setTempValue5&""&CHR(34)&"")
		'鼠标右键导航功能
	
		rightNavChkJsPath = ""&MY_sitePath&"cat_js/shortCut.js"
		rightNavChkJs = showFile(rightNavChkJsPath)
		if setTempValue5 = 1 then rightNavChkJs_now = replace(replace(rightNavChkJs,"////document.onmousedown=getButton;","document.onmousedown=getButton;"),"//document.onmousedown=getButton;","document.onmousedown=getButton;")
		if setTempValue5 = 0 then rightNavChkJs_now = replace(rightNavChkJs,"document.onmousedown=getButton;","//document.onmousedown=getButton;")
		CreateFile rightNavChkJsPath,rightNavChkJs_now
	
	
		CreateFile websetConfigPath,websetConfigCode
	
		'配置文件读取完毕判断
		CreateFile setXmlPath,replace(showFile(setXmlPath),"<configCompleted>0</configCompleted>","<configCompleted>1</configCompleted>")
		
		'网站跟目录重新配置
		shortCutCon=showFile(""&MY_sitePath&"cat_js/shortCut.js")
		
		CreateFile ""&MY_sitePath&"cat_js/shortCut.js",replace(shortCutCon,"MyrootPath_S=""/"";var MY_ManageHomeIndexUrl="""";var siteURL="""";var siteName=""""","MyrootPath_S="""&MY_sitePath&""";var MY_ManageHomeIndexUrl="""";var siteURL="""&MY_secCheckSiteLink&""";var siteName="""&siteName_Now&"""")
		
		'-----------
		shortCutCon2=showFile(""&MY_sitePath&"cat_js/template.js")
		
		CreateFile ""&MY_sitePath&"cat_js/template.js",replace(shortCutCon2,"MyrootPath=""/"";","MyrootPath="""&MY_sitePath&""";")
		
		'-----------
		tempCommNavCon=showFile(""&MY_sitePath&"template/comm.nav.js")
		
		CreateFile ""&MY_sitePath&"template/comm.nav.js",replace(tempCommNavCon,"MyrootPath_Temp=""/"";","MyrootPath_Temp="""&MY_sitePath&""";")
		
		
		
	
	end if
		


end if

%>
