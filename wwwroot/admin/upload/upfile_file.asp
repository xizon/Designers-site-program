<!--#include file="upload.inc"-->
<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../function/chk_upfile_danger.asp" --> 
<!--#include file="../checkFun_mem.asp" -->
<!--#include file="../function/imgAction.asp" -->
<!--#include file="../function/imgGetWH.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<% 

'////////////////////////////////////////////////////
'文件性质：后台上传配置 
'////////////////////////////////////////////////////
MaxFileSize=cint(MY_uploadSize_man)                                        '上传文件大小限制单位k 
UpFileType=MY_uploadFileType_man                          '允许的上传文件类型 
secure_url=""&MY_sitePath&""&MY_sec_uploadFilePath&"/?src="       '防盗链路径
secure_urlimg=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="
mypath=""&MY_sitePath&""&MY_uploadFilePath&"/"                            '上传路径
mypathImg=""&MY_sitePath&""&MY_uploadImgPath&"/"

createFolder(mypath)
createFolder(mypathImg)
createFolder(MY_uploadSmallImgPath)


fileID=Request.QueryString("fileID")
imgcut=Request.QueryString("imgcut")
cAArticle=request.Cookies("cAArticle")

%> 
<!--#include file="../_comm/upload_head.asp" -->
<% 
call upload_0()  '使用无组件上传类 
%> 
</body> 
</html> 
<% 
sub upload_0()   
    set upload=new upload_file    '建立上传对象 
    for each formName in upload.file '列出所有上传了的文件 
        set file=upload.file(formName)  '生成一个文件对象
		
		'文件空集警告窗口 ----头 
        if file.filesize<50 then 
			 response.Write "<script>parent.floatWin('出现错误','<div class=tipsFloatWin>×&nbsp;您的文件太小!</div>',350,120,100,0,1,0);</script>"
			 response.write "<a href='upload_file.html?fileID="&fileID&"'>重新上传</a>"
            msg="" 
            founderr=true 
        end if 
		'文件空集警告窗口 ----尾 
		
		'大小警告窗口 ----头
        if file.filesize>(MaxFileSize*1024) then 
			   response.Write "<script>parent.floatWin('出现错误','<div class=tipsFloatWin>×&nbsp;文件大小超过了限制，最大只能上传" & CStr(MaxFileSize) & "K的文件!</div>',350,120,100,0,1,0);</script>"
			   response.write "<a href='upload_file.html?fileID="&fileID&"'>重新上传</a>"
            founderr=true 
        end if 
        '大小警告窗口 ----尾
		
        fileExt=lcase(file.FileExt) 
        Forumupload=split(UpFileType,"|") 
        for i=0 to ubound(Forumupload) 
            if fileEXT=trim(Forumupload(i)) then 
                EnableUpload=true 
                exit for 
            end if 
        next 
        if fileEXT="asp" or fileEXT="asa" or fileEXT="aspx" or fileEXT="php" then 
            EnableUpload=false 
        end if     
		
		'类型警告窗口----头
        if EnableUpload=false then 
			   response.Write "<script>parent.floatWin('出现错误','<div class=tipsFloatWin>×&nbsp;这种文件类型不允许上传！<br/>只允许上传这几种文件类型：" & UpFileType & "!</div>',350,120,100,0,1,0);</script>"
			   response.write "<a href='upload_file.html?fileID="&fileID&"'>重新上传</a>"
         founderr=true 
        end if 
        '类型警告窗口----尾          			   	
        strJS="<script language=javascript>" & vbcrlf 
        if founderr<>true then 

            filename=MD5(year(now)&month(now)&day(now)&hour(now)&minute(now)&second(now),1)
            name_ok=replace(CheckFileType(filename),"False",MY_uploadFirstName)

			
			'---------------判断图片是否为木马或者是不正常图片
			If fileEXT="jpg" or fileEXT="jpeg" or fileEXT="gif" or fileEXT="png" or fileEXT="bmp"  then
						ultimate_name=mypathImg&name_ok&filename&"."&fileEXT
						obj_name=replace(ultimate_name,""&MY_sitePath&""&MY_uploadImgPath&"/","")         '防盗链名称
						file.SaveToFile Server.mappath(ultimate_name)   '保存文件
						

			
						'添加图片宽高信息
						newImgName = replace(ultimate_name,"."&fileEXT,"-"&getImgWH(ultimate_name,"w")&"x"&getImgWH(ultimate_name,"h")&"."&fileEXT)
						newObjImgName = replace(obj_name,"."&fileEXT,"-"&getImgWH(ultimate_name,"w")&"x"&getImgWH(ultimate_name,"h")&"."&fileEXT)
						EditFileName ultimate_name,replace(newImgName,mypathImg,"")
						
						response.write "<span class=""backStage_uploadTxt2"">上传成功</span>&nbsp;&nbsp;&nbsp;"
						response.write "<a href='upload_file.html?fileID="&fileID&"'>重新上传</a>"
						FileType=right(fileExt,3) 
						strJS=strJS & "parent.document.getElementById('obj"&fileID&"').value='"&secure_urlimg&""&newObjImgName&"';if(existImgshowID('objdefault_imgShow')==true){parent.document.getElementById('objdefault_imgShow').innerHTML='<img src="""&secure_urlimg&""&newObjImgName&""" width=""220"" style=""border:#E3E3E3 solid 2px;margin-bottom:10px;""/>';};" & vbcrlf  
					 
						If not CheckFileType(Server.mappath(newImgName)) then 
							Set fso = CreateObject("Scripting.FileSystemObject") 
							Set ficn = fso.GetFile(Server.mappath(newImgName)) 
							ficn.delete 
							set ficn=nothing 
							set fso=nothing 
							
							response.Write "<script>parent.floatWin('出现错误','<div class=tipsFloatWin>×&nbsp;您上传的图片不合法，禁止上传！</div>',350,120,100,0,1,0);</script>"
							response.write "<a href='upload_file.html?fileID="&fileID&"'>重新上传</a>"
							response.end 
						end if 
			
						'----------------加水印			
						if request.Cookies("upwater")<>"no" then		
						 imgWaterMark(newImgName)
						end if
			   
			Else   
			 
						ultimate_name=mypath&name_ok&filename&"."&fileEXT
						obj_name=replace(ultimate_name,""&MY_sitePath&""&MY_uploadFilePath&"/","")         '防盗链名称
						file.SaveToFile Server.mappath(ultimate_name)   '保存文件 
						response.write "<span class=""backStage_uploadTxt2"">上传成功</span>&nbsp;&nbsp;&nbsp;"
						response.write "<a href='upload_file.html?fileID="&fileID&"'>重新上传</a>"
						FileType=right(fileExt,3) 
						strJS=strJS & "parent.document.getElementById('obj"&fileID&"').value='"&secure_url&""&obj_name&"';parent.document.getElementById('obj"&fileID&"').style.background='none';" & vbcrlf 
						 
			End If    
			
        end if 
        strJS=strJS & "</script>" 
        response.write strJS 
        set file=nothing 
    next 
	
	
	if MY_imgCropFun = 1 then
	
		'if cAArticle = 1 then
			'剪裁图片----begin
		
				if founderr<>true then 
				
				   If fileEXT="jpg" or fileEXT="jpeg" or fileEXT="gif" or fileEXT="png" or fileEXT="bmp"  then
						'获取图片资料
						Dim AspJpegUp 
						Set AspJpegUp = Server.Createobject("Persits.Jpeg") 
						UpOriginalPath = Server.MapPath(newImgName) 
						AspJpegUp.Open UpOriginalPath '打开原图片 
						
						Response.Cookies("newImgW") = AspJpegUp.Width
						Response.Cookies("newImgH") = AspJpegUp.Height
						Response.Cookies("newImgW").Expires=DateAdd("m",12,now()) 
						Response.Cookies("newImgH").Expires=DateAdd("m",12,now()) 
						
						Response.Cookies("orginImgW") = AspJpegUp.Width
						Response.Cookies("orginImgH") = AspJpegUp.Height
						Response.Cookies("orginImgW").Expires=DateAdd("m",12,now()) 
						Response.Cookies("orginImgH").Expires=DateAdd("m",12,now()) 
					
						
						cutImg=""
						cutImg="<script>ShowIFrameEdit('图片剪裁','"&MY_sitePath&"admin/function/ImgCut/ImgCut.asp?PhotoUrl="&newImgName&"&mode=article',355,650,650,350,30,0);</script>"
						
						if MY_checkJpegComponent=0 then response.write cutImg& vbcrlf 
						
						AspJpegUp.close 
						
					End If   
						
				end if
				'剪草图片----end
			
			'end if
		end if

	
    set upload=nothing 
end sub 
%> 
