<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
worksEditOk = 0
%>
<%
'载入模板配置文档
set xmlDom = server.CreateObject("MSXML2.DOMDocument")
xmlDom.async = false
setXmlPath = ""&MY_sitePath&"template/tempConfig.xml"
if not xmlDom.Load(Server.MapPath(setXmlPath))  then 
    'response.write("Load wrong!")
else

    '读取节点
	set setTempPara = xmlDom.getElementsByTagName("config")  
	set setnode = setTempPara(0).selectSingleNode("setting_works")

	setTempValue1 = setnode.GetAttribute("ImgClippingWidth")
	setTempValue2 = setnode.GetAttribute("ImgClippingHeight")
end if

'剪裁激活
response.Cookies("cAWorks") = 1

'取出最新ID
set rs=server.CreateObject("adodb.recordset")
strsql="select * from [SuccessWork]"
rs.open strsql,db,1,1

if rs.bof and rs.eof then 
    TopID = 1

else

    GetNum_suc = db.execute("select max(ID) from [SuccessWork]")(0)
	TopID = GetNum_suc + 1
	

end if

rs.close
set rs=nothing


%>

<style>
.jqTransformSelectWrapper ul{height:350px; overflow:hidden; overflow-y:auto}
.artRadio{ padding:0; height:15px; width:15px;}
</style>
<script>

function addTagFrame(){
floatWin('选择标签','<iframe src="../_comm/selectTags.asp" allowTransparency="true" name="iframe_tags"  frameborder="0" scrolling="no" height="200" width="470"></iframe>',480,210,20,0,0,1);
	//for IE6
	if ($.browser.msie && $.browser.version < 7) {
		setTimeout('document.frames("iframe_tags").location.reload();',500);
	}


}


$(document).ready(function() {

	var limitNum2 = 80;
	$('#title').inputlimiter({
		limit: limitNum2,
		boxId: 'limitingtext2',
		boxAttach: false
	});	
	
	document.getElementById("limitingtext2").innerHTML = "0/" + limitNum2;

	var limitNumKeyWords = 100;
	$('#KeyWord').inputlimiter({
		limit: limitNumKeyWords,
		boxId: 'limitingtext3',
		boxAttach: false
	});	
	
	document.getElementById("limitingtext3").innerHTML = "0/" + limitNumKeyWords;
	
	
	//外链地址判断
	imgInputChkSrc("#objimg","#objimg_imgShow");
	imgInputChkSrc("#objShowImg","#objShowImg_imgShow");
	imgInputChkSrc("#obj1","#obj1_imgShow");
	imgInputChkSrc("#obj2","#obj2_imgShow");
	imgInputChkSrc("#obj3","#obj3_imgShow");
	imgInputChkSrc("#obj4","#obj4_imgShow");
	imgInputChkSrc("#obj5","#obj5_imgShow");
	imgInputChkSrc("#obj6","#obj6_imgShow");
	imgInputChkSrc("#obj7","#obj7_imgShow");
	imgInputChkSrc("#obj8","#obj8_imgShow");
	imgInputChkSrc("#obj9","#obj9_imgShow");
	imgInputChkSrc("#obj10","#obj10_imgShow");
	


	
	
});

//自动保存
$(function () {
	$('#title,#ClassName,#objimg,#content,#objShowImg,#defineFileName,#KeyWord,#siteURL,#obj1,#obj2,#obj3,#obj4,#obj5,#obj6,#obj7,#obj8,#obj9,#obj10').autosave();
});




//表单判断
function checkForm(){
	if(document.formM.title.value==""){
		ChkFormTip_page("标题不能为空",0);
		return false;
	}
	if(document.formM.ClassName.value==""){
		ChkFormTip_page("类别不能为空",0);
		return false;
	}
	if(document.formM.obj.value==""){
		ChkFormTip_page("预览图不能为空",0);
		return false;
	}
	
	if(document.formM.objShowImg.value==""){
		ChkFormTip_page("请上传至少一张高清图",0);
		return false;
	}
	
	//ok
	ChkFormTip_page("正在处理中",2);
	$('#needCreateLoadingBtn').fadeOut(500);
	return true;
		
}


</script>

<!-- //=====================================================HTML编辑器  begin  -->
<script>
var Path_uploadFUnFile = "../../plugins/HTMLEdit/_ds_fun/upload.asp";
var Path_saveDraft = "../../plugins/HTMLEdit/_ds_fun/AutoSave_show.asp";  
var Path_saveFunFile = "../../plugins/HTMLEdit/_ds_fun/AutoSaver.asp";
var Path_chkDangerImgFunFile = "../../plugins/HTMLEdit/_ds_fun/chk_dangerImg.asp";
var Path_saveFormItemID = "#content";
</script>
<script type="text/javascript" charset="utf-8" src="../../plugins/HTMLEdit/xheditor.js"></script>
<script type="text/javascript">
$(pageInit);
function pageInit(){
	$.extend(xheditor.settings,{shortcuts:{'ctrl+enter':submitForm}});
	$(Path_saveFormItemID).xheditor({upImgUrl:Path_uploadFUnFile,upImgExt:"jpg,jpeg,gif,png",upFlashUrl:Path_uploadFUnFile,upFlashExt:"swf",upMediaUrl:Path_uploadFUnFile,upMediaExt:"wmv,avi,wma,mp3,mid"});
}
function submitForm(){$('#formM').submit();}


</script>
<!-- //=====================================================HTML编辑器  end  -->

<script>
<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=文章日志", "link1=<%=MY_sitePath%>admin/article/article_add.asp", "title2=站内微博", "link2=<%=MY_sitePath%>admin/mood/mood_add.asp","title3=作品案例/产品", "link3=<%=MY_sitePath%>admin/SuccessWork/SuccessWork_add.asp","title4=通知公告", "link4=<%=MY_sitePath%>admin/notice/notice_add.asp");

	   }
	);
	
});

<!-- form ui[js]  end -->

</script>



<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="SuccessWork_list.asp" class="link">列表管理</a>  
                <a href="SuccessWork_class.asp" class="link">分类管理</a>
                <!-- 当前位置 -->
                当前位置： 内容发布 | 作品案例/产品
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
                
                <!-- 表单域  -->
                <div id="contentShow">
                
                    <form name="formM" action="action.asp?action=add" method="post" onSubmit="javascript:return checkForm();">
                     <input type="hidden" name="addID" value="<%=TopID%>">
  
                                <table width="100%" class="setting" cellspacing="0">
                                    
                                    <!-- 标题 -->                        
                                    <thead class="setHead">
                                        <tr>
                                               <th  class="setTitle"></th>
                                               <th colspan="2"></th>
                                        </tr>
                                        
                                    </thead>
                                    
                                 
                                    <!-- 底部 -->
                                    <tfoot>
                                    
                                        <tr>
                                            <td colspan="3">         
                                                <div class="btnBox">
                                                    <img class="autosave" align="absmiddle"  src="../../plugins/d-s-management-ui/img/manage/autosave-btn1.GIF"> <img class="autosave_restore" src="../../plugins/d-s-management-ui/img/manage/autosave-btn2.GIF" align="absmiddle"> <a href="javascript:" class="autosave_removecookies"><img src="../../plugins/d-s-management-ui/img/manage/autosave-btn3.GIF" align="absmiddle"></a> <span class="autosave_saving">Saving&#8230;</span>&nbsp;&nbsp;<input type="submit" value="发布" class="sub" id="needCreateLoadingBtn">
                                                    <!--<input type="reset" value="重置" class="reset">-->
                                              </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                    
                                    <!-- +++++++  内容   begin +++++++  -->
                                    <tbody>
            
                                        <tr>
                                            <td class="title">案例标题：<span class="backStage_star">*</span></td>
                                            <td colspan="2" class="ui-element">
                                                <input type="text" name="title" id="title"  size="40" >&nbsp;&nbsp;<span id="limitingtext2"></span>
                                            </td>
                                        </tr>  
                                        
                                        <tr>
                                            <td class="title">类别：<span class="backStage_star">*</span></td>
                                            <td colspan="2" class="ui-element">
                                               <select name="ClassName" id="ClassName">
                                                   <option value="" selected="selected">请选择类别</option>
                                                    <%	
                                                    set rs=server.CreateObject("adodb.recordset")
                                                    strsql="select * from [SuccessWorkClass]"
                                                    rs.open strsql,db,1,1
                                                    do while not rs.eof and not rs.bof
                                                    %>
                                                    <option value="<%=rs("ClassName")%>"><%=rs("ClassName")%></option>
                                                    <%
                                                  rs.movenext
                                                  loop
												  
													  '无分类
													if rs.bof and rs.eof then response.Write "<script>floatWin('温馨提示','<div class=tipsFloatWin>请先至少添加一个类别再继续操作！&nbsp;&nbsp;<a href=SuccessWork_class.asp?action=add>添加分类</a></div>',350,0,100,1,0,0);</script>"
												  
												  rs.close
												  set rs=nothing
                                                  %>
                                               </select>
                                              &nbsp;&nbsp;<a href="SuccessWork_class.asp?action=add">添加分类</a>
                                            </td>
                                        </tr>
                                        
                                        

                                        
                                        <tr onMouseOver="tipShow('#listImg',0)" onMouseOut="tipShow('#listImg',1)">
                                            <td class="title">列表预览图<img src="../../plugins/d-s-management-ui/img/pic.gif">：<span class="backStage_star">*</span></td>
                                            <td colspan="2" class="ui-element">
                                                <input type="text" name="obj" id="objimg" size="55" class="help" data-info="您可以本地上传图片，或者直接插入http外链图片网址">
                                                
                                                <br clear="all">
                                                <div class="tipsFloatWin-upload-min" id="listImg"><iframe src=../upload/upload_imgID.html?imgID=img&imgcut=true class=backStage_uploadFrame allowTransparency=true  frameborder=0 scrolling=No></iframe><br>推荐<span class=backStage_star><%=setTempValue1%>×<%=setTempValue2%> </span>大小作为列表缩略图,支持本地上传和http外链图片&nbsp;<a href=../img_editor.html target=_blank>在线做图</a></div>
                                                
                                                
                                                <span id="objimg_imgShow"></span>

                                            </td>
                                        </tr>        
  
                                                   
                                                                                                      
                                        <tr>
                                            <td class="title">描述/介绍<a href="javascript:void(0)" class="help" data-info="不同模板的设计,案例作品描述可以在列表中显示，也可以在内容页中显示"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                            <td colspan="2">

                                                 <!-- //=====================================================HTML编辑器  begin -->
                                                    <textarea class="xheditor {tools:'Bold,Italic,Underline,FontColor,Link,Unlink,Align,List,Outdent,Indent,Hr,Source,Preview,Fullscreen',skin:'default'}" name="content" id="content" rows="25" cols="80" style="width: 100%; height:200px;"></textarea>
                                                   <span id="frameImgChk"></span>
                                                <!-- //=====================================================HTML编辑器  end -->
                                                
                                                <br clear="all">
                                                    
                                                
                                                    <!--   作品描述模板调用  -->
                                                    <!--#include file="introTempLoader.asp" -->

                                            </td>
                                        </tr>   
                                         


                                        
                                        <tr onMouseOver="tipShow('#hdimg',0)" onMouseOut="tipShow('#hdimg',1)">
                                            <td class="title">插入高清展示图<img src="../../plugins/d-s-management-ui/img/pic.gif">：<span class="backStage_star">*</span></td>
                                            <td colspan="2" class="ui-element">
                                            
                                              <input type="text" name="objShowImg" id="objShowImg" size="55" class="help" data-info="您可以本地上传图片，或者直接插入http外链图片网址">
                                                <br clear="all">
                                                <div class="tipsFloatWin-upload-min" id="hdimg"><iframe src=../upload/upload_imgID.html?imgID=ShowImg&imgcut=false class=backStage_uploadFrame allowTransparency=true  frameborder=0 scrolling=No></iframe><br>建议宽度大于800像素,支持本地上传和http外链图片</div>
                                                
                                                <span id="objShowImg_imgShow"></span>
                                                <div style="height:50px"></div>

                                            </td>
                                        </tr>  
                                        
                                        <tr>
                                            <td class="title"></td>
                                            <td colspan="2" class="ui-element">
                                                <%
												hdImg_1 = ""
												hdImg_2 = ""
												hdImg_3 = ""
												hdImg_4 = ""
												hdImg_5 = ""
												hdImg_6 = ""
												hdImg_7 = ""
												hdImg_8 = ""
												hdImg_9 = ""
												hdImg_10 = ""
												%>
                                                <!-- 插入更多展示图 --> 
                                                <!--#include file="insertMoreImg.asp" -->
                                                


                                            </td>
                                        </tr> 
                                        
                                        
                                        <tr id="moreShow" onClick="$('.more-item').slideDown(500);$('#moreShow').slideUp(500);">
                                            <td colspan="3"  class="ui-element">
                                               <div class="btnBox-iframe"><input type="button" value="更多设置" class="ui-btn"></div>
                                            </td>
                                        </tr> 
                                        
                                        <tr class="more-item" style="display:none">
                                            <td class="title">自定义文件名<a href="javascript:void(0)" class="help" data-info="生成页面的名字,不需要写后缀"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                            <td colspan="2" class="ui-element">
                                              <input type="text" name="defineFileName" id="defineFileName"  onclick="select()"  value="<%=MD5(year(now)&month(now)&day(now)&hour(now)&minute(now)&second(now),1)%>" size="40" >

                                            </td>
                                        </tr>   
                                        
                                        
                                        <tr class="more-item" style="display:none">
                                            <td class="title">关键词<a href="javascript:void(0)" class="help" data-info="<a href='javascript:' onclick='addTagFrame()'>快速选择关键词</a><br>使用空格隔开,最多10个"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                            <td colspan="2" class="ui-element">
                                                <input type="text" name="KeyWord" id="KeyWord"  size="40" >&nbsp;&nbsp;<span id="limitingtext3"></span>

                                            </td>
                                        </tr>   
                                        

                                        
                                        <tr class="more-item" style="display:none">
                                            <td class="title">展示URL<a href="javascript:void(0)" class="help" data-info="案例作品的URL外链地址,用于[视频]、[swf动画]、[上线网站]的展示,可不填"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                            <td colspan="2" class="ui-element">
                                              <input type="text" name="siteURL" id="siteURL" class="focusJS" data-value="http://" value="" size="60" >

                                            </td>
                                        </tr>  
                                        
                                        
                                        <tr class="more-item" style="display:none">
                                            <td class="title">正文显示<a href="javascript:void(0)" class="help" data-info="是否在前台列表页面中显示,如果关闭此项,可以记录地址发送给朋友"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                            <td colspan="2">
                                               <div class="on_off-box">
                                                    <span  class="on_off"><input type="checkbox" name="private"  value="yes" checked="checked" /></span>
                                               </div>
                                            </td>
                                        </tr>
                                         
                                        
                                        <tr onMouseOver="tipShow('#tip-a',0);" onMouseOut="tipShow('#tip-a',1);" class="more-item" style="display:none">
                                            <td class="title">同步日志<a href="javascript:void(0)" class="help" data-info="同时发布一篇文章来展示此案例作品"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                            <td width="150">
                                               <div class="on_off-box">
                                                    <span  class="on_off"><input type="checkbox"  name="cogradient-diary"  value="yes" /></span>
                                               </div>
                                            </td>
                                            <td>
                                               <div style="margin-left:-50px; visibility:hidden" id="tip-a">
                                               
                                               
                                     
												<%
                                                
							
                                                sql_D="select * from [articleClass]"
                                                
                                                set rs_D=server.createObject("ADODB.Recordset")
                                                rs_D.open sql_D,db,1,1
                                                if not rs_D.bof and not rs_D.eof then
                                                
        
                                                do while not rs_D.eof and not rs_D.bof
                                                
                                                classname=rs_D("classname")
												classID = rs_D("articleCLassID")
												
												if classname <> "" then response.Write "<label for=""classID"&classID&"""><input type=""radio"" id=""classID"&classID&""" class=""artRadio"" name=""ClassName_diary"" value="""&classname&""">"&classname&"&nbsp;</label>"&vbcrlf
                                                
                                                
                                                rs_D.movenext
                                                loop
             
                                                
                                                else
                                                response.Write "暂无分类"

                                                end if

                                                rs_D.close
                                                set rs_D=nothing
                                                
                                                %>
                                                    
                                               </div>
                                            </td>
                                        </tr>             
                                                                     
                                               
                                         <input type="hidden" name="Subdate" value="<%=year(now)&"-"&month(now)&"-"&day(now)%>" />
                                         <!-- 动画/视频 -->
                                         <input type="hidden" name="objswf1" />
                                         <input type="hidden" name="objswfW1" />
                                         <input type="hidden" name="objswfH1" />
                                         <input type="hidden" name="objswf2" />
                                         <input type="hidden" name="objswfW2" />
                                         <input type="hidden" name="objswfH2" />
                                         <input type="hidden" name="objswf3" />
                                         <input type="hidden" name="objswfW3" />
                                         <input type="hidden" name="objswfH3" />              
          
          
          
           
                                                    
                                                    
                                          
                                       <!-- +++++++  内容   end +++++++  -->
                                
                                
                                    </tbody>
                                    
                                </table>
   
                    </form>  
                                
            
            
                               
                     </div> 
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  



<!--#include file="../_comm/page_bottom.asp" -->

