<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../function/imgAction.asp" -->
<!--#include file="../function/sec_pwd.inc" -->
<!--#include file="../_chk_secNow_inc.asp" -->
<!--#include file="../function/Md5.asp" -->
<!--#include file="../checkFun_action.asp" -->
<%secPassNow=chk_sec_pwd%>
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
HTMLSERVE_HEAD()
chk_transfer()
thisFolder=MY_createFolder_work

Template_Code=showFile(DesignerSite_tempPath_works)


'////获取并判断动态模板 begin
thisTempCodePath=""&MY_sitePath&"admin/_temp/content_tempA1_"&tempModifiedDate&".html"
if FileExistsStatus(thisTempCodePath) = -1 then '不存在
'开始执行依赖标签功能
'==================
		
		Template_Code_diary=showFile(DesignerSite_tempPath_common)
		'=============================模板依赖标签不存在判断   begin
		'模板主注释，参数不用变更，默认全部清除
		Template_Code_trip = dependentLabelFun(Template_Code_diary,"note:template",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"note:commCreateInfo",0,"","","")
		
		
		'【必选其一】各栏目标题依赖标签,必选其一设置为参数 1
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:commonUse.title",0,"","","") '通用标题
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:content.article",1,"","","")	 '内容页-文章 
		  

		'列表页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.article",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.works",0,"","","")
		
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.about",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.mood",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.search",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.links",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.message",0,"","","")
		'【必选其一】各栏目meta描述依赖标签,必选其一设置为参数 1 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:comm.desc",0,"","","") '通用网站描述
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:content.articleDesc",1,"","","")'内容页-文章 
		
		
		'【必选其一】各栏目meta关键词依赖标签,必选其一设置为参数 1 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:commonUse.keywords",0,"","","")'其他通用页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:home",0,"","","")'首页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:content",1,"","","")'内容页关键词
		
		
		'RSS订阅，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"rss:commonUse.article",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"rss:commonUse.article.class",1,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"atom:commonUse.article",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"atom:commonUse.article.class",1,"","","")
		
		'banner轮换，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.banner",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"head:commonUse.banner",0,"","","")
		
		'搜索框，订阅图标，置顶推荐文章，最新案例作品，规定数量随机tags输出，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.searchBoard",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.articleFeed",1,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.article.top",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.works.list",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.article.tags.random",0,"","","")
		
		
		'内容页SEO可选link
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"head:content.linkRel",1,"","","")
		
		
		
		'侧边栏
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"sideBar:article,search,about,message",0,"","","")'列表页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"sideBar:home",0,"","","")  '首页
		Template_Code_trip = getTempLablePara_siderBar_content("文章内容页侧边栏",1,"body:content.show.article")
		Template_Code_trip = dependentLabelFun_para(Template_Code_trip,"文章内容页侧边栏",0,"body:content.show.article")
		
		'各栏目的列表页面
		
		'文章
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.article",0,"","","//") '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article.class",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article.tags",0,"","","")  
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.article",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.article.class",0,"/*","*/","")
		
		
		'作品案例
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.worksIe6Js",0,"","","") '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.works",0,"","","//")   '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.works",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.works.class",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.works",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.works.class",0,"/*","*/","")
		
		
		'微博
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.mood",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:content.js.mood",0,"/*","*/","")
		
		
		'关于我们
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.about",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.about",0,"/*","*/","")
		
		
		'搜索
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.search",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.searchResult",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.search",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.searchResult",0,"/*","*/","")
		
		'友情链接
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.links",0,"","","")	  
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.links",0,"/*","*/","")
		
		'留言
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.message",0,"","","//")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.message",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.message",0,"/*","*/","")
		
		'首页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.home",0,"","","//")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.home",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.home",0,"/*","*/","")
		
		
		'内容页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.js.comm.form",1,"","","")	 '栏目必选  	
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.show.commJs",1,"","","")'栏目必选   	
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.show.article",1,"","","")	 					 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:content.js.article",1,"/*","*/","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:content.js.comm",1,"","","//")		 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:content.article",1,"","","//")
		
		
		%>
		<!--#include file="../root_temp-edit/_clearLabel_comm.asp" -->
		<%
		

		'==================
		Template_Code_Now = Template_Code_trip		
		CreateFile thisTempCodePath,Template_Code_Now
		
 
end if
'=============================模板依赖标签不存在判断   end

'这里获取依赖标签清除后模板的值
Template_Code_diary=showFile(thisTempCodePath)
'只调用栏目独立HTML代码判断
callPageAllHTML "body:content.show.article","完全独立页面代码.不调用其它任何依赖标签的内容"


	GetNum_suc = db.execute("select count(1) from [SuccessWork] where private=0 ")(0)

	
	dim action,ID,delRs,cat
	set cat=new myclass
	action=Request.QueryString("action")
	ID=request.querystring("ID")
	Classid=request.querystring("Classid")
	searchGoEdit = request.QueryString("searchGoEdit")
	


	'-----------编辑模块
if action="editWork" then

	folderName=replace(request.form("url"),"/default.html","")
	createFolder("../../"&MY_createFolder_work&"/")
	createFolder(folderName)

	workID=request.querystring("workID")
	
	filepath=session("filepath")'获取路径
	
	set addRs=server.createobject("adodb.recordset")
	strsql="select * from [SuccessWork] where ID="&workID
	addRs.open strsql,db,1,3

	'获取表单内容
	title=request.form("title")
	ClassName=request.form("ClassName")
	content=request.form("content")
	preURL=request.form("obj")
	hits=request.form("hits")
	Subdate=request.form("Subdate")
	privateNow=request.form("private")
	cogradientState=request.form("cogradient-diary")
	ClassName_diary=request.form("ClassName_diary")
	objShowImg=request.form("objShowImg")
	URL1=request.form("obj1")
	URL2=request.form("obj2")
	URL3=request.form("obj3")
	URL4=request.form("obj4")
	URL5=request.form("obj5")
	URL6=request.form("obj6")	
	URL7=request.form("obj7")	
	URL8=request.form("obj8")	
	URL9=request.form("obj9")	
	URL10=request.form("obj10")	
	
	if apiStrNull(URL1)=0 then  URL1=""
	if apiStrNull(URL2)=0 then  URL2=""
	if apiStrNull(URL3)=0 then  URL3=""
	if apiStrNull(URL4)=0 then  URL4=""
	if apiStrNull(URL5)=0 then  URL5=""
	if apiStrNull(URL6)=0 then  URL6=""
	if apiStrNull(URL7)=0 then  URL7=""
	if apiStrNull(URL8)=0 then  URL8=""
	if apiStrNull(URL9)=0 then  URL9=""
	if apiStrNull(URL10)=0 then  URL10=""	
	
	
	objswf1=request.form("objswf1")
	objswfW1=request.form("objswfW1")
	objswfH1=request.form("objswfH1")
	
	objswf2=request.form("objswf2")
	objswfW2=request.form("objswfW2")
	objswfH2=request.form("objswfH2")
	
	objswf3=request.form("objswf3")
	objswfW3=request.form("objswfW3")
	objswfH3=request.form("objswfH3")	
	
	siteURL=request.form("siteURL")	
	siteURLNow=siteURL
	
	if siteURL= "" or siteURL="http://" then siteURLNow = "javascript:void(0)"
	

	
	KeyWord=replace(request.form("KeyWord"),chr(32),",")
	Safe_KeyWord=HTMLEncode(KeyWord)
	
	if instr(objswf1,".swf") > 0 then swfShow1 = "<embed src="""&objswf1&""" type=""application/x-shockwave-flash"" width="""&objswfW1&""" height="""&objswfH1&""" wmode=""transparent"">"
	
	
	if instr(objswf2,".swf") > 0 then swfShow2 = "<embed src="""&objswf2&""" type=""application/x-shockwave-flash"" width="""&objswfW2&""" height="""&objswfH2&""" wmode=""transparent"">"
	
	
	if instr(objswf3,".swf") > 0 then swfShow3 = "<embed src="""&objswf3&""" type=""application/x-shockwave-flash"" width="""&objswfW3&""" height="""&objswfH3&""" wmode=""transparent"">"
	
	swfShow = swfShow1&swfShow2&swfShow3
	
	
	siteURLShow=""&DY_LANG_101&"<a href="""&siteURLNow&""" target=""_blank"">"&siteURLNow&"</a>"
	if siteURL="" or siteURL="http://" then siteURLShow=""
	
	
'--------------更多图片
%>
<!--#include file="action_moreImg.asp" -->
<%

'--------------
	
	
	thisfilePath=filepath
	
	
	O_ImageThis=ShowImageURL(objShowImg) 
	O_parm=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="
	O_ShowImage=""&MY_sitePath&"clintInfo/img.html?parm="&O_parm&"&path="&replace(O_ImageThis,MY_sitePath,"")&""
	
	'外链判断			
	if instr(objShowImg,"http://")>0 then
	   O_preShow="<li class=""slider-large-list index-0""><a href="""&objShowImg&""" target=""_blank""><img title="""&HTMLEncode(title)&""" alt="""&HTMLEncode(title)&""" src="""&objShowImg&""" ></a></li>"
	   
	else
	   O_preShow="<li class=""slider-large-list index-0""><a href="""&O_ShowImage&""" target=""_blank""><img title="""&HTMLEncode(title)&""" alt="""&HTMLEncode(title)&""" src="""&objShowImg&""" ></a></li>"
	   '添加sub_???缩略图
		sub_urlOrigin=replace(replace(replace(objShowImg,"../",""),""&MY_sec_uploadImgPath&"/?src=",""),MY_sitePath,"")
		imgSmallCreate_subject(""&MY_sitePath&""&MY_uploadImgPath&"/"&sub_urlOrigin)
		   
	   
	end if
	
	
	'缩略图地址		
	if instr(objShowImg,"http://")>0 then
	   subImgSrc_d = objShowImg
	   subO_d_preShow="<li class=""slider-thumbs-list index-0""><a href=""javascript:"" data-show="""&objShowImg&"""><img alt="""" src="""&subImgSrc_d&""" ></a></li>"		
	else
	   if MY_checkJpegComponent=0 then 
		   '支持组件
		   subImgSrc_d = ""&MY_sitePath&""&MY_uploadImgPath&"/small/sub_"&replace(replace(replace(objShowImg,"../",""),""&MY_sec_uploadImgPath&"/?src=",""),MY_sitePath,"")	
	   else
		   subImgSrc_d = ""&MY_sitePath&""&MY_uploadImgPath&"/"&replace(replace(replace(objShowImg,"../",""),""&MY_sec_uploadImgPath&"/?src=",""),MY_sitePath,"")	
	   end if

	   subO_d_preShow="<li class=""slider-thumbs-list index-0""><a href=""javascript:"" data-show="""&objShowImg&"""><img alt="""" src="""&subImgSrc_d&""" ></a></li>"		
	end if
		
		
					
	objShowImg_html = ""&O_preShow&""
	subObjShowImg_html = ""&subO_d_preShow&""
	
	if objShowImg = "" then objShowImg_html = "" : subObjShowImg_html = ""
	
	
	
	'---------同步日志路径
	folderNameD = replace(replace(thisfilePath,"/default.html",""),""&MY_sitePath&""&MY_createFolder_work&"/","")
	if MY_createFileMode = 0 then
	createFolder("../../"&MY_createFolder_art&"/"&folderNameD&"/")
	
	fname_diary="default.html"
	folder_diary="../../"&MY_createFolder_art&"/"&folderNameD&"/"

				
	elseif MY_createFileMode = 1 then
	
	fname_diary=folderNameD&".html"
	folder_diary="../../"&MY_createFolder_art&"/"
	
	end if
	
	filePath_diary=folder_diary&fname_diary
	filePath_diary=""&MY_sitePath&""&replace(filePath_diary,"../","")&""
	
	'---------同步日志路径   end
	
	
	if privateNow <> "yes" then 
	   privateNow = "1"
	   content = "none"
	elseif privateNow = "yes" then
	   privateNow = "0"
	else
	   privateNow = "0"
	end if
	
	comment_diary = ""
	 
	 
	
	if preURL="" then preURL="nonpreview2.gif"

	Safe_Title=HTMLEncode(title)	
	Safe_content=content
	
	
	
  if title="" or ClassName="" or  preURL="" then
    response.Write "<script>floatWin('出现错误','<div class=tipsFloatWin>×&nbsp;&nbsp;标题、类别、预览图均不能为空！<br/><br/><button class=""backBtn"" onclick=""javascript:history.go(-1)"">返回</button></div>',350,120,100,1,0,0);</script>"
	

  elseif  cogradientState="yes" and ClassName_diary="" then 
  
     response.Write "<script>floatWin('出现错误','<div class=tipsFloatWin>×&nbsp;请选择同步日志的分类！<br/><br/><button class=""backBtn"" onclick=""javascript:history.go(-1)"">返回</button></div>',350,120,100,1,0,0);</script>"

  else
  
	if cogradientState="yes" and ClassName_diary<>"" then  
	
	
			createFolder("../../"&MY_createFolder_art&"/")
			
			set addRs_D=server.createobject("adodb.recordset")
			strsql_D="select * from [article]"
			addRs_D.open strsql_D,db,1,3
			
			'判断是否重复数据
			
			set rs =server.createobject("adodb.recordset")
			sql="select * from [article] where FilePath='"&filePath_diary&"'" 
			rs.open sql,db,1,1 
			
			'=============================存在性验证
			if not rs.EOF then
				
			else
				addRs_D.addnew  '以下是添加进数据库的内容	
			end if
			
			rs.close
			set rs=nothing
			
			
			
            

			C_summary=LCase(content)
			CutContent = SplitLines(C_summary,MY_getSplitLinesSummary)
			showSummary=stripHTML(replace(replace(replace(replace(replace(replace(replace(replace(CutContent,"<br>",CHR(32)),CHR(9),CHR(32)),"<br/>",CHR(32)),"<br />",CHR(32)),CHR(32)," "),"</p>",CHR(32)),"<p>",""),CHR(10),""))

				
				
			addRs_D("content")=content&siteURLShow&objShowImg_html&O_1_objShowImg_html&O_2_objShowImg_html&O_3_objShowImg_html&O_4_objShowImg_html&O_5_objShowImg_html&O_6_objShowImg_html&O_7_objShowImg_html&O_8_objShowImg_html&O_9_objShowImg_html&O_10_objShowImg_html
			addRs_D("summary")=HTMLEncode(showSummary)
			addRs_D("className")=ClassName_diary
			addRs_D("ArticleTitle")=HTMLEncode(title)
			addRs_D("files")=""
			addRs_D("mainImg")=preURL
			addRs_D("top")=0
			addRs_D("private")="No"
			addRs_D("KeyWord")=HTMLEncode(Safe_KeyWord)
			addRs_D("subdate")=now()
			
			articleid=addRs_D("articleid")
            
			'========================
			'模板路径添加块，顺序不能错	
			'↓↓↓↓↓↓↓↓↓↓↓↓
					
			addRs_D("FilePath")=filePath_diary
			'↑↑↑↑↑↑↑↑↑↑↑↑		
			
			'=======================	
			Template_Code_diary=replace(Template_Code_diary,"${ArticleTitle}",HTMLEncode(title))
			Template_Code_diary=replace(Template_Code_diary,"${KeyWord}",Safe_KeyWord)
			Template_Code_diary=replace(Template_Code_diary,"${Summary}",HTMLDecode(showSummary))
			Template_Code_diary=replace(Template_Code_diary,"${Content}",content&siteURLShow&objShowImg_html&O_1_objShowImg_html&O_2_objShowImg_html&O_3_objShowImg_html&O_4_objShowImg_html&O_5_objShowImg_html&O_6_objShowImg_html&O_7_objShowImg_html&O_8_objShowImg_html&O_9_objShowImg_html&O_10_objShowImg_html)
			Template_Code_diary=replace(Template_Code_diary,"${ClassName}",ClassName_diary)
			Template_Code_diary=replace(Template_Code_diary,"${SubDate}",Now())	
			Template_Code_diary=replace(Template_Code_diary,"${articleid}",articleid)	
			Template_Code_diary=replace(Template_Code_diary,"${meta_keywords}",MY_Meta_keywords)
			CreateFile filePath_diary,Template_Code_diary
			addRs_D.update
			addRs_D.close
			set addRs_D=nothing
	
	
			updateFrame_diary="<iframe src="""&filePath_diary&""" style=""display:none"" ></iframe><iframe src=""../root_temp-edit/Article_show.asp"" style=""display:none"" ></iframe>"
			updateFrame_diary_tip="(已同步日志)"
			
			comment_diary = "<div id=""workComment""><a href="""&filePath_diary&"#secFrame_formArea"" target=""_blank""><img src="""&MY_sitePath&"images/skins/temp_style/work_commentBtn.png"" ></a></div>"
	
	end if
  

			
	thisHits = hits + 1
	addRs("title")=Safe_Title
	addRs("hits")=thisHits
	addRs("worksIntro")=Safe_content
	addRs("show")=objShowImg_html&O_1_objShowImg_html&O_2_objShowImg_html&O_3_objShowImg_html&O_4_objShowImg_html&O_5_objShowImg_html&O_6_objShowImg_html&O_7_objShowImg_html&O_8_objShowImg_html&O_9_objShowImg_html&O_10_objShowImg_html
	addRs("ClassName")=ClassName
	addRs("siteURL")=siteURLNow
	addRs("preURL")=preURL
	addRs("Subdate")=Subdate
	addRs("private")=privateNow
	addRs("KeyWord")=HTMLEncode(Safe_KeyWord)
	
    addRs("hdImg_d")=objShowImg
    addRs("hdImg_1")=URL1
    addRs("hdImg_2")=URL2
    addRs("hdImg_3")=URL3
    addRs("hdImg_4")=URL4
    addRs("hdImg_5")=URL5
    addRs("hdImg_6")=URL6
    addRs("hdImg_7")=URL7
    addRs("hdImg_8")=URL8
    addRs("hdImg_9")=URL9
    addRs("hdImg_10")=URL10
	
	addRs.update
	
	urlOrigin=replace(replace(replace(addRs("preURL"),"../",""),""&MY_sec_uploadImgPath&"/?src=",""),MY_sitePath,"")
	imgSmallCreate_subject(""&MY_sitePath&""&MY_uploadImgPath&"/"&urlOrigin)
			
	addRs.close
	set addRs=nothing 
	
	update_ID = workID
	   
%>
<!--#include file="../root_temp-edit/comm_getSideData.asp" -->
<!--#include file="../root_temp-edit/_batchTemp_works.asp" -->
<!--#include file="classList.asp" -->
<%


		Template_Code=replace(Template_Code,"${work_recentU}",rsTitle1)
		Template_Code=replace(Template_Code,"${work_recentD}",rsTitle2)
		Template_Code=replace(Template_Code,"${work_classList}",classListCase)
		Template_Code=replace(Template_Code,"${work_title}",Safe_Title)
		Template_Code=replace(Template_Code,"${work_con}",Safe_content)
		Template_Code=replace(Template_Code,"${work_siteURL}",siteURLNow)
		Template_Code=replace(Template_Code,"${work_class}",ClassName)
		Template_Code=replace(Template_Code,"${work_show}",objShowImg_html&O_1_objShowImg_html&O_2_objShowImg_html&O_3_objShowImg_html&O_4_objShowImg_html&O_5_objShowImg_html&O_6_objShowImg_html&O_7_objShowImg_html&O_8_objShowImg_html&O_9_objShowImg_html&O_10_objShowImg_html)
		Template_Code=replace(Template_Code,"${work_date}",Subdate)	
		Template_Code=replace(Template_Code,"${work_id}",workID)	
		Template_Code=replace(Template_Code,"${hits}",thisHits)
		Template_Code=replace(Template_Code,"${work_path}",filepath)
		Template_Code=replace(Template_Code,"${meta_keywords}",MY_Meta_keywords)
		Template_Code=replace(Template_Code,"${KeyWord}",Safe_KeyWord)
		Template_Code=replace(Template_Code,"${MY_defineName_work}",MY_createFolder_work)
		Template_Code=replace(Template_Code,"${work_show_thumbs}",subObjShowImg_html&subO_1_objShowImg_html&subO_2_objShowImg_html&subO_3_objShowImg_html&subO_4_objShowImg_html&subO_5_objShowImg_html&subO_6_objShowImg_html&subO_7_objShowImg_html&subO_8_objShowImg_html&subO_9_objShowImg_html&subO_10_objShowImg_html)


		'Seo
        thisSeoPath=db.execute("select FilePath from [SuccessWork] where ID=" & workID)(0)
		Template_Code=replace(Template_Code,"${thisPageSEOURL}",thisSeoPath)	
		
		
		CreateFile filepath,Template_Code&createData
		
		if searchGoEdit <> "yes" then Response.Write "<script>window.parent.parent.frames['mainPage'].location.href = '../SuccessWork/SuccessWork_list.asp?pageno="&session("pageno")&"&classname="&session("classnameNow")&"'</script>"
	
		
		'-----生成展示页面end

  end if		
end if   
%>
<%
HTMLSERVE_BOTTOM_notClose()
%>
<!--#include file="../checkFun_actionB.asp" -->
