<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
searchGoEdit = request.QueryString("searchGoEdit")
worksEditOk = 1
%>
<style>
#contentShowBox{margin-top:-30px}
.btnBox_editF{width:40px; position:relative;margin-left:7px; margin-top:20px;}
#warpper{padding:33px 10px 0 0;}
#floatFrame{ visibility:hidden}
.artRadio{ padding:0; height:15px; width:15px;}
</style>
<%
'载入模板配置文档
set xmlDom = server.CreateObject("MSXML2.DOMDocument")
xmlDom.async = false
setXmlPath = ""&MY_sitePath&"template/tempConfig.xml"
if not xmlDom.Load(Server.MapPath(setXmlPath))  then 
    'response.write("Load wrong!")
else

    '读取节点
	set setTempPara = xmlDom.getElementsByTagName("config")  
	set setnode = setTempPara(0).selectSingleNode("setting_works")

	setTempValue1 = setnode.GetAttribute("ImgClippingWidth")
	setTempValue2 = setnode.GetAttribute("ImgClippingHeight")
end if

'剪裁激活
response.Cookies("cAWorks") = 1

%>



<script>


function addTagFrame(){
floatWin('选择标签','<iframe src="../_comm/selectTags.asp" allowTransparency="true" name="iframe_tags"  frameborder="0" scrolling="no" height="200" width="470"></iframe>',480,210,20,0,0,1);
	//for IE6
	if ($.browser.msie && $.browser.version < 7) {
		setTimeout('document.frames("iframe_tags").location.reload();',500);
	}


}


$(document).ready(function() {

	
	var limitNum2 = 80;
	$('#title').inputlimiter({
		limit: limitNum2,
		boxId: 'limitingtext2',
		boxAttach: false
	});	
	
	
	var limitNumKeyWords = 100;
	$('#KeyWord').inputlimiter({
		limit: limitNumKeyWords,
		boxId: 'limitingtext3',
		boxAttach: false
	});	
	
	//外链地址判断
	imgInputChkSrc("#objimg","#objimg_imgShow");
	imgInputChkSrc("#objShowImg","#objShowImg_imgShow");
	imgInputChkSrc("#obj1","#obj1_imgShow");
	imgInputChkSrc("#obj2","#obj2_imgShow");
	imgInputChkSrc("#obj3","#obj3_imgShow");
	imgInputChkSrc("#obj4","#obj4_imgShow");
	imgInputChkSrc("#obj5","#obj5_imgShow");
	imgInputChkSrc("#obj6","#obj6_imgShow");
	imgInputChkSrc("#obj7","#obj7_imgShow");
	imgInputChkSrc("#obj8","#obj8_imgShow");
	imgInputChkSrc("#obj9","#obj9_imgShow");
	imgInputChkSrc("#obj10","#obj10_imgShow");
	
	
});


//表单判断
function checkForm(){
	if(document.formM.title.value==""){
		ChkFormTip_page("标题不能为空",0);
		return false;
	}
	if(document.formM.ClassName.value==""){
		ChkFormTip_page("类别不能为空",0);
		return false;
	}
	if(document.formM.obj.value==""){
		ChkFormTip_page("预览图不能为空",0);
		return false;
	}
	
	if(document.formM.objShowImg.value==""){
		ChkFormTip_page("请上传至少一张高清图",0);
		return false;
	}
	
	//ok
	ChkFormTip_page("保存成功",1);
	return true;
		
}


</script>

<!-- //=====================================================HTML编辑器  begin  -->
<script>
var Path_uploadFUnFile = "../../plugins/HTMLEdit/_ds_fun/upload.asp";
var Path_saveDraft = "../../plugins/HTMLEdit/_ds_fun/AutoSave_show.asp";  
var Path_saveFunFile = "../../plugins/HTMLEdit/_ds_fun/AutoSaver.asp";
var Path_chkDangerImgFunFile = "../../plugins/HTMLEdit/_ds_fun/chk_dangerImg.asp";
var Path_saveFormItemID = "#content";
</script>
<script type="text/javascript" charset="utf-8" src="../../plugins/HTMLEdit/xheditor.js"></script>
<script type="text/javascript">
$(pageInit);
function pageInit(){
	$.extend(xheditor.settings,{shortcuts:{'ctrl+enter':submitForm}});
	$(Path_saveFormItemID).xheditor({upImgUrl:Path_uploadFUnFile,upImgExt:"jpg,jpeg,gif,png",upFlashUrl:Path_uploadFUnFile,upFlashExt:"swf",upMediaUrl:Path_uploadFUnFile,upMediaExt:"wmv,avi,wma,mp3,mid"});
}
function submitForm(){$('#formM').submit();}


</script>
<!-- //=====================================================HTML编辑器  end  -->


<!--**************编辑删除板块*************-->

<%
action=request.QueryString("action")
workID=request.QueryString("workID")

if action="edit" then

	set rs2=server.CreateObject("adodb.recordset")
	strsql2="select * from [SuccessWork] where ID="&workID 
	rs2.open strsql2,db,1,1
	session("filepath")=rs2("filepath")
		
%>


<!--========================================主内容区  begin ===================================== -->  
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
                
                <!-- 表单域  -->
                <div id="contentShow">
                
                    <form name="formM" action="action_edit.asp?searchGoEdit=<%=searchGoEdit%>&action=editWork&workID=<%=workID%>"  target="actionPage" method="post" onSubmit="javascript:return checkForm();">
  
                                <table width="100%" class="setting" cellspacing="0">
                                    
                                    <!-- 标题 -->                        
                                    <thead class="setHead">
                                        <tr>
                                               <th  class="setTitle"></th>
                                               <th colspan="2"></th>
                                        </tr>
                                        
                                    </thead>
                                    
                                 
                                    <!-- 底部 -->
                                    <tfoot>
                                    
                                        <tr>
                                            <td colspan="3" class="ui-element">         
                                                <div class="btnBox_editF">
                                                    <input type="submit" value="保存" class="ui-btn">
                                                    <!--<input type="reset" value="重置" class="reset">-->
                                              </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                    
                                    <!-- +++++++  内容   begin +++++++  -->
                                    <input type="hidden"name="url" value="<%=rs2("url")%>">
                                    
                                    <tbody>
            
                                        <tr>
                                            <td class="title">案例标题：<span class="backStage_star">*</span></td>
                                            <td colspan="2" class="ui-element">
                                                <input type="text" name="title" id="title" value="<%=rs2("title")%>"  size="40" >&nbsp;&nbsp;<span id="limitingtext2"></span>
                                            </td>
                                        </tr>  
                                        
                                        <tr>
                                            <td class="title">类别：<span class="backStage_star">*</span></td>
                                            <td colspan="2" class="ui-element">
                                               <select name="ClassName">
                                                   <option value="<%=rs2("ClassName")%>" selected="selected"><%=rs2("ClassName")%></option>
                                                    <%	
                                                    set rs=server.CreateObject("adodb.recordset")
                                                    strsql="select * from [SuccessWorkClass]"
                                                    rs.open strsql,db,1,1
                                                    do while not rs.eof and not rs.bof
                                                    %>
                                                    <option value="<%=rs("ClassName")%>"><%=rs("ClassName")%></option>
                                                    <%
                                                    rs.movenext
                                                    loop
                                                    %>
                                               </select>
                                              
                                              
                                            </td>
                                        </tr>
                                        
                                        <tr style="display:none">
                                            <td class="title">排序：</td>
                                            <td colspan="2" class="ui-element">
													<%
                                                    function GetNumSuc()
                                                    GetNumSuc=db.execute("select count(1) from [SuccessWork] where private=0")(0)
                                                    end function
													num=GetNumSuc()
                                                    %>
                                            
												   <%if rs("private") = "0" then%>
                                                    <select name="index">
                                                                <%
                                                                for p=1 to num
                                                                %>
                                                                            <option <%if p=rs("index") then response.write "selected"  end if%> value="<%=p%>"><%=p%></option>
                                                                            <%
                                                                next%>
                                                          </select>
                                                    <%else%>
                                                    <input name="index" type="hidden" value="0"/>
                                                    无
                                                    <%end if%> 
                                              
                                              
                                            </td>
                                        </tr>
 
                                        
                                        <tr>
                                            <td class="title">正文显示<a href="javascript:void(0)" class="help" data-info="是否在前台列表页面中显示,如果关闭此项,可以记录地址发送给朋友"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                            <td colspan="2">
                                               <div class="on_off-box">
                                                    <span  class="on_off">
                                                    
															<%if rs2("private") = 0 then%>            
                                                                <input type="checkbox" name="private"  value="yes" checked="checked" />
                                                            <%else%>
                                                                <input type="checkbox" name="private"  value="yes"/>
                                                            <%end if%>  
                                                    
                                                    
                                                    </span>
                                               </div>
                                            </td>
                                        </tr>
                                         
                                        
                                        <tr onMouseOver="tipShow('#tip-a',0);" onMouseOut="tipShow('#tip-a',1);">
                                            <td class="title">同步日志<a href="javascript:void(0)" class="help" data-info="同时发布一篇文章来展示此案例作品"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                            <td width="150">
                                               <div class="on_off-box">
                                                    <span  class="on_off">
                                                    <input type="checkbox"  name="cogradient-diary"  value="yes" />
                                                    </span>
                                               </div>
                                            </td>
                                            <td>
                                               <div style="margin-left:-50px;  visibility:hidden" id="tip-a">
                                     
												<%
                                                
                                                sql_D="select * from [articleClass]"
                                                
                                                set rs_D=server.createObject("ADODB.Recordset")
                                                rs_D.open sql_D,db,1,1
                                                if not rs_D.bof and not rs_D.eof then           
                                                              
                                                do while not rs_D.eof and not rs_D.bof
                                                
                                                classname=rs_D("classname")
												classID = rs_D("articleCLassID")
												
												if classname <> "" then response.Write "<label for=""classID"&classID&"""><input type=""radio"" id=""classID"&classID&""" class=""artRadio"" name=""ClassName_diary"" value="""&classname&""">"&classname&"&nbsp;</label>"&vbcrlf
                                                
                                                rs_D.movenext
                                                loop
                                                
                                                classHtml2 = classHtml2 &"</select>"&vbcrlf
                                                
                                                else
                                                response.Write "暂无分类"
                                                end if
 
                            
                                                rs_D.close
                                                set rs_D=nothing
                                                
                                                %>
                                                    
                                               </div>
                                            </td>
                                        </tr>  
                                                             
                                        <tr>
                                            <td class="title">关键词<a href="javascript:void(0)" class="help" data-info="<a href='javascript:' onclick='addTagFrame()'>快速选择关键词</a><br>使用空格隔开,最多10个"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                            <td colspan="2" class="ui-element">
                                                <input type="text" name="KeyWord" id="KeyWord" value="<%=replace(rs2("keyword"),",",chr(32))%>"  size="40" >&nbsp;&nbsp;<span id="limitingtext3"></span>

                                            </td>
                                        </tr>   
                                        
                                        
                                        <tr onMouseOver="tipShow('#listImg',0)" onMouseOut="tipShow('#listImg',1)">
                                            <td class="title">列表预览图<img src="../../plugins/d-s-management-ui/img/pic.gif">：<span class="backStage_star">*</span></td>
                                            <td colspan="2" class="ui-element">
                                                <input type="text" name="obj" id="objimg" size="55" value="<%=rs2("preURL")%>" class="help" data-info="您可以本地上传图片，或者直接插入http外链图片网址">
                                                <br clear="all">
                                                <div class="tipsFloatWin-upload-min" id="listImg"><iframe src=../upload/upload_imgID.html?imgID=img&imgcut=true class=backStage_uploadFrame allowTransparency=true  frameborder=0 scrolling=No></iframe><br>推荐<span class=backStage_star><%=setTempValue1%>×<%=setTempValue2%> </span>大小作为列表缩略图,支持本地上传和http外链图片&nbsp;<a href=../img_editor.html target=_blank>在线做图</a></div>
                                                
                                                <%if instr(rs2("preURL"),"nonpreview") > 0 then %>
                                                <span id="objimg_imgShow"></span>
                                                <%else%>
                                                <span id="objimg_imgShow"><img src="<%=rs2("preURL")%>" style="border:#E3E3E3 solid 2px;margin-bottom:10px;" width="220"></span>
                                                <%end if%>
                                                
                                                

                                            </td>
                                        </tr>        
  
                                                   
                                                                                                      
                                        <tr>
                                            <td class="title">描述/介绍<a href="javascript:void(0)" class="help" data-info="不同模板的设计,案例作品描述可以在列表中显示，也可以在内容页中显示"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                            <td colspan="2">

                                                 <!-- //=====================================================HTML编辑器  begin -->
                                                    <textarea name="content" class="xheditor {tools:'Bold,Italic,Underline,FontColor,Link,Unlink,Align,List,Outdent,Indent,Hr,Source,Preview,Fullscreen',skin:'default'}" id="content" rows="25" cols="80" style="width: 100%; height:200px;"><%=HTMLDecode(rs2("worksIntro"))%></textarea>
    
                                                
                                                <!-- //=====================================================HTML编辑器  end -->
                                                
                                                <br clear="all">
                                                
                                                    <!--   作品描述模板调用  -->
                                                    <!--#include file="introTempLoader.asp" -->

                                            </td>
                                        </tr>   
                                         

                                       
                                        <tr>
                                            <td class="title">展示URL<a href="javascript:void(0)" class="help" data-info="案例作品的URL外链地址,用于[视频]、[swf动画]、[上线网站]的展示,可不填"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                            <td colspan="2" class="ui-element">
                                              <input type="text" name="siteURL" value="<%=rs2("siteURL")%>"  size="60" >

                                            </td>
                                        </tr>  
                                        
                                       
                                        
                                        <tr onMouseOver="tipShow('#hdimg',0)" onMouseOut="tipShow('#hdimg',1)">
                                            <td class="title">插入高清展示图<img src="../../plugins/d-s-management-ui/img/pic.gif">：<span class="backStage_star">*</span></td>
                                            <td colspan="2" class="ui-element">
                                            
                                              <input type="text" name="objShowImg" id="objShowImg" value="<%=rs2("hdImg_d")%>" size="55" class="help" data-info="您可以本地上传图片，或者直接插入http外链图片网址">
                                                <br clear="all">
                                                <div class="tipsFloatWin-upload-min" id="hdimg"><iframe src=../upload/upload_imgID.html?imgID=ShowImg&imgcut=false class=backStage_uploadFrame allowTransparency=true  frameborder=0 scrolling=No></iframe><br>建议宽度大于800像素,支持本地上传和http外链图片</div>
                                                
                                                <%if instr(rs2("hdImg_d"),"nonpreview") > 0 then %>
                                                <span id="objdefault_imgShow"></span>
                                                <%else%>
                                                <span id="objShowImg_imgShow"><img src="<%=rs2("hdImg_d")%>" style="border:#E3E3E3 solid 2px;margin-bottom:10px;" width="220"></span>
                                                <%end if%>
                  
                                                <div style="height:50px"></div>


                                            </td>
                                        </tr>  
                                        
                                        <tr>
                                            <td class="title"></td>
                                            <td colspan="2" class="ui-element">
                                                <%
												hdImg_1 = rs2("hdImg_1")
												hdImg_2 = rs2("hdImg_2")
												hdImg_3 = rs2("hdImg_3")
												hdImg_4 = rs2("hdImg_4")
												hdImg_5 = rs2("hdImg_5")
												hdImg_6 = rs2("hdImg_6")
												hdImg_7 = rs2("hdImg_7")
												hdImg_8 = rs2("hdImg_8")
												hdImg_9 = rs2("hdImg_9")
												hdImg_10 = rs2("hdImg_10")
												%>
                                                <!-- 插入更多展示图 --> 
                                                <!--#include file="insertMoreImg.asp" -->

                                            </td>
                                        </tr> 
                                        
                                                                     
                                               
                                         <input type="hidden" name="Subdate" value="<%=year(now)&"-"&month(now)&"-"&day(now)%>" />
                                         <input type="hidden" value="<%=rs2("hits")%>" name="hits" />
                                         
                                         <!-- 动画/视频 -->
                                         <input type="hidden" name="objswf1" />
                                         <input type="hidden" name="objswfW1" />
                                         <input type="hidden" name="objswfH1" />
                                         <input type="hidden" name="objswf2" />
                                         <input type="hidden" name="objswfW2" />
                                         <input type="hidden" name="objswfH2" />
                                         <input type="hidden" name="objswf3" />
                                         <input type="hidden" name="objswfW3" />
                                         <input type="hidden" name="objswfH3" />              
          
                                                    
                                                    
                                          
                                       <!-- +++++++  内容   end +++++++  -->
                                
                                
                                    </tbody>
                                    
                                </table>
   
                    </form>  
                                
            
            
                               
                     </div> 
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<%
rs2.close
set rs2=nothing
end if
%>



<!--#include file="../_comm/page_bottom.asp" -->


