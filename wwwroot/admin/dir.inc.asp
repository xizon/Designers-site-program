<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
	<%
    '载入模板配置文档
    set xmlDom2 = server.CreateObject("MSXML2.DOMDocument")
    xmlDom2.async = false
    setXmlFilePath = ""&MY_sitePath&"template/tempConfig.xml"
    if not xmlDom2.Load(Server.MapPath(setXmlFilePath))  then 
        'response.write("Load wrong!")
    else
    
		set tempInfo = xmlDom2.getElementsByTagName("DSsetting")  
		verNow = tempInfo(0).selectSingleNode("version").Text
		
	
	
    end if
    
    %>
    <!--menu-->
    <div id="mainNav">
       <ul class="show nav">
           
           <li class="m-1">
              <a class="dir" href="javascript:"></a>
              <div class="sub-nav">
                  <div class="sub-box">
                  
                      <div class="row">
                          <span class="title">博客/文章</span>
                          <a class="im1" target="mainPage" href="<%=MY_sitePath%>admin/article/article_add.asp">发布</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/article/article_list.asp">列表管理</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/article/article_class.asp">分类管理</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/article/article_channel.asp">频道管理</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/article/article_top.asp">置顶管理</a>
                      </div>
                      
                      <div class="row">
                          <span class="title">案例/产品</span>
                          <a class="im1" target="mainPage" href="<%=MY_sitePath%>admin/SuccessWork/SuccessWork_add.asp">发布</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/SuccessWork/SuccessWork_list.asp">列表管理</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/SuccessWork/SuccessWork_class.asp">分类管理</a>
                      </div>
                      
                      
                      <div class="row">
                          <span class="title">微博/动态</span>
                          <a class="im1" target="mainPage" href="<%=MY_sitePath%>admin/mood/mood_add.asp">发布</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/mood/mood_edit.asp">列表管理</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/website/_moodChkCode.asp">校验码设置</a>
                      </div>
                      
                      
                      <div class="row">
                          <span class="title">首页通知</span>
                          <a class="im1" target="mainPage" href="<%=MY_sitePath%>admin/notice/notice_add.asp">发布</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/notice/notice_edit.asp">列表管理</a>
                      </div>
                      
                      
                      <div class="row">
                          <span class="title">自定义标签</span>
                          <a class="im1" target="mainPage" href="<%=MY_sitePath%>admin/self-definition/add_label.asp">新增</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/self-definition/edit_label.asp">标签管理</a>
                      </div>
                      
                      
                      <div class="row">
                          <span class="title">自定义页面</span>
                          <a class="im1" target="mainPage" href="<%=MY_sitePath%>admin/newTmplate/add_temp.asp?step=addLabel">新增</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/newTmplate/edit_temp.asp?edit=no">页面管理</a>
                      </div>
                      
                      
                      <div class="row">
                          <span class="title">幻灯片/轮播</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/fullColumnSet/edit_banner.asp">轮播图片管理</a>
                      </div>
                      
                      
                      <div class="row">
                          <span class="title">文章评论</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/article/article_comment.asp">列表管理</a>
                      </div>
                      
                      
                      <div class="row">
                          <span class="title">访客留言</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/message/message_edit.asp">列表管理</a>
                      </div>
                      
                      
                      <div class="row">
                          <span class="title">友情链接</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/link/links_add.asp">添加</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/link/links_edit.asp">链接管理</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/link/userLinks_list.asp">审核前台提交链接</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/link/links_edit-Shielded.asp">已屏蔽链接管理</a>
                          
                      </div>
                      
                  
                  </div>
                  
                  
              </div>
           </li>
           
           <li class="m-2">
              <a class="dir" href="javascript:"></a>
              <div class="sub-nav">
                  <div class="sub-box">
                      
                      <div class="row">
                          <span class="title">网站设置</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/website/siteSetting.asp">网站标题/版权信息/作者/Logo管理</a>
                          
                      </div>
                      
                      
                      <div class="row">
                          <span class="title">导航菜单</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/edit_file/edit_NavMenu.asp">通用页面导航管理</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/edit_file/edit_NavMenu_phone.asp?type=p">手机版页面导航管理</a>
       
                          
                      </div>
                      
                      
                      <div class="row">
                          <span class="title">管理员帐号</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/website/change_pwd.asp">设置</a>
                          
                      </div>
                      
                      
                      
                      <div class="row">
                          <span class="title">二级密码</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/edit_sec_pwd/edit_sec_pwd.asp">设置</a>
                          
                      </div>
                      
                      
                      <div class="row">
                          <span class="title">通用侧边栏</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/sectionConfig/edit_sidebar_home.asp">首页侧边栏设置</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/sectionConfig/edit_sidebar.asp">其它页面侧边栏设置</a>
                          
                      </div>
                      
                      
                      <div class="row">
                          <span class="title">网站SEO</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/website/siteConfig.asp#setting-seo">设置</a>
                          
                      </div>
                      
                  
                  </div>
                  
                  
              </div>
              
           </li>
           <li class="m-3">
              <a class="dir" href="javascript:"></a>
              <div class="sub-nav">
                  <div class="sub-box">
                      
                      <div class="row">
                          <span class="title">手动生成</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/root_temp-edit/index.asp">栏目/列表页</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/article/edit_batch_temp.asp">文章内容页(批量)</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/SuccessWork/edit_batch_temp.asp">案例产品内容页(批量)</a>

                          
                      </div>
                      
                      <div class="row">
                          <span class="title">自动生成</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/website/siteConfig.asp#setting-autoCreate">自动生成时间设置</a>
                          
                      </div>
                      
              
                  
                  </div>
                  
                  
              </div>
              
           </li>
           
           <li class="m-4">
              <a class="dir" href="javascript:"></a>
              <div class="sub-nav">
                  <div class="sub-box">
                      
                      <div class="row">
                          <span class="title">模板外观</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/edit_file/edit_commStyle.asp">自定义背景/文字颜色</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/cloudSKin/manageSkin.asp">主题管理</a>|
                          <a class="im1" target="mainPage" href="<%=MY_sitePath%>admin/cloudSKin/skinList.asp">云皮肤(在线安装)</a>
                        
                      </div>
                      
                      <div class="row">
                          <span class="title">模板在线修改</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/edit_file/edit_TEMP.asp">编辑模板HTML代码</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/edit_file/edit_css.asp">编辑模板CSS样式代码</a>
                        
                      </div>
                             
                      <div class="row">
                          <span class="title">模板文字包</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/edit_file/edit_lang.asp">设置</a>
                        
                      </div>    
                      
                      <div class="row">
                          <span class="title">插件</span>
                          <a target="mainPage" href="javascript:" id="bp1">已安装插件管理</a>|
                          <a class="im1" target="mainPage" href="<%=MY_sitePath%>admin/plugins/cloud/cloudPlugins-list.asp">云插件(在线安装)</a>
                         
                      </div>                  
                             
                  
                  </div>
                  
                  
              </div>
              
           </li>
           
           <li class="m-5">
              <a class="dir" href="javascript:"></a>
              <div class="sub-nav">
                  <div class="sub-box">
                      
                      <div class="row">
                          <span class="title">关键词设置</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/shield/keywords.asp">留言/评论关键词过滤</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/edit_file/edit_thesaurus.asp">文章标签关键词库</a>
     
                          
                      </div>
                      
                      <div class="row">
                          <span class="title">数据库管理</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/data_manage/restore_backup.asp">备份/还原/删除</a>
                       
                      </div>

                      
                      <div class="row">
                          <span class="title">网站设置</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/website/siteConfig.asp#setting-upload">上传设置</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/website/siteConfig.asp#setting-tags">标签输出设置</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/website/siteConfig.asp#setting-msg">评论留言设置</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/website/siteConfig.asp#setting-pagesp">分页设置</a>|
                          <span class="br-pa">
                              <a target="mainPage" href="<%=MY_sitePath%>admin/website/siteConfig.asp#setting-img">缩略图和剪裁设置</a>|
                              <a target="mainPage" href="<%=MY_sitePath%>admin/website/setcreateList.asp">列表生成参数设置</a>|
                              <a target="mainPage" href="<%=MY_sitePath%>admin/website/siteConfig.asp#setting-apiset">API数据接口设置</a>|
                          </span>
                          <span class="br-pa">
                              <a target="mainPage" href="<%=MY_sitePath%>admin/website/siteConfig.asp#setting-other">其它设置</a>
                          </span>

                          
                      </div>
                      
                      
                      <div class="row">
                          <span class="title">会员中心</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/member_manage/member_edit.asp">注册用户管理</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/member_manage/memResource_list.asp">投稿管理</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/member_manage/msg_list.asp">建议反馈</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/member_manage/config.asp">权限设置</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/member_manage/database_member.asp">数据库</a>
               
                          
                      </div>
                      
                      <div class="row">
                          <span class="title">文件操作</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/data_manage/attachments.asp">上传文件管理</a>|
                          <a target="mainPage" href="<%=MY_sitePath%>admin/edit_file/edit_batchReplaceStr.asp">文件批量字符替换</a>
                       
                      </div>
                      
                      <div class="row">
                          <span class="title">IP操作</span>
                          <a target="mainPage" href="<%=MY_sitePath%>admin/shield/IP_filter.asp">IP权限设置</a>
                       
                          
                      </div>
                      
                      <div class="row">
                          <span class="title">数据转移</span>
                          <a class="im2" target="mainPage" href="<%=MY_sitePath%>admin/edit_file/edit_webRootpath.asp">网站根目录修改(谨慎操作)</a>
                       
                      </div>
                      
                      
                  
                  </div>
                  
                  
              </div>
              
           </li>
           
           
                            
       </ul>
    
    </div>
    
   
   
   
   <a class="nav-open" href="javascript:void(0)" title="Open"></a>
   <a class="nav-close" href="javascript:void(0)" title="Close"></a>
   <a class="customRightmenuBtn" href="javascript:void(0)"></a>
   <div class="usual-btn" style="visibility:hidden"><a href="javascript:void(0)" class="help" data-info="安装新插件后<br>请刷新后台更新此列表"><img src="_manage_img/t/usual-btn.png"></a></div>
   <div class="usualBox" style="visibility:hidden">
       <a class="closeB" href="javascript:" title="关闭">×</a>
       
       <div class="plugins-scroller" style="visibility:hidden">
			<%
            set rs_rpluginS =server.createobject("adodb.recordset")
            sql_rpluginS="select * from App order by ID desc" 
            rs_rpluginS.open sql_rpluginS,db,1,1 
            if rs_rpluginS.bof and rs_rpluginS.eof then
            
                response.Write "暂无..."&vbcrlf
                
                    
            else
                do while not rs_rpluginS.eof and not rs_rpluginS.bof
                
            
                response.Write "<a href='"&rs_rpluginS("appURL")&"'target='mainPage' title='"&rs_rpluginS("appName")&"'>"&rs_rpluginS("appName")&"</a>"&vbcrlf
                
                rs_rpluginS.movenext
                loop	
            
    
            
            end if
    
            rs_rpluginS.close
            set rs_rpluginS=nothing
            
            %>
     
       </div>

   
   </div>
   
   
   <script src="../cat_js/jquery.jscrollpane.min.js"></script>
   <script>
   
		
   $(document).ready(function(){  
		//二级导航
		var nav = $("#mainNav");
		nav.find("li").each(function() {
			if ($(this).find(".sub-nav").length > 0) {
				//show subnav on hover
				$(this).click(function() {
					$(" a.dir",this).addClass("active");
					$(this).find(".sub-nav").css("display","block");
					
				});
				

				//hide submenus on exit
				$(this).mouseleave(function() {
					$(" a.dir",this).removeClass("active");
					$(this).find(".sub-nav").css("display","none");
				});
			}
		});

		
		$("#bp1").click(function () {
			var iH = 200;
			var iW = 500;
			parent.floatWin('插件管理','<iframe name="frameEdit" allowTransparency"=true" frameborder="0"  scrolling="no" height="' + iH + '" width="' + iW + '" hspace="0" vspace="0" src="<%=MY_sitePath%>admin/pluginsManage_list.asp"></iframe>',iW + 20,0,150,0,0,0);
			//for IE6"&vbcrlf
			if ($.browser.msie && $.browser.version < 7) {
				setTimeout('document.frames("frameEdit").location.reload();',500);
			}	

		});
		
		
		//Plugins Show
		setTimeout(function(){
			$(".plugins-scroller").jScrollPane();
		},500);
		
		
		//子导航hover
		$('#mainNav .show .sub-nav .row').hover(function(){
			$(this).css("background","#161412");
				},function(){
					$(this).css("background","none");
			}
		); 
		
		
		

   });
   
	$(function () { 
	    
		//Plugins
		var $j = $(".usual-btn a");
		setTimeout(function(){
			
			if (document.documentElement.clientHeight < 600 ){
				$j.animate({bottom: "-400px"},{queue:false,duration:500});
			}
			if (document.documentElement.clientHeight > 600 ){
				$j.animate({bottom: "2px"},{queue:false,duration:4500});
			}

			//插件框架显示
			$("#columnBtnBox-plugins,#mainNav .sub-item,.plugins-scroller,.usual-btn,.usualBox").css("visibility","visible");	

		},2000);
		
		$(".usual-btn a").click(function(){
			$(".usualBox").animate({bottom: "25px"},{queue:false,duration:500});
			$("#mainNav").animate({opacity: 0.2},{queue:false,duration:500});
			$("#mainNav").slideUp(500);
			
		});
		
		$(".closeB").click(function(){
			$(".usualBox").animate({bottom: "-400px"},{queue:false,duration:500});
			$("#mainNav").animate({opacity: 1},{queue:false,duration:500});
			$("#mainNav").slideDown(500);
		});
		
	
		//help
		$(".help").powerFloat({
			targetMode: "remind",
			targetAttr: "data-info",
			position: "1-4"
		});	
		
		
	
		
		
	})
	   
   
   </script>
    
    
    <![endif]-->
                    
    <!--#include file="_systemUpdate.asp" -->

   
   
