<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================

					
'/////////////////////////////////////模板通用栏目调用

titleLimitLen_sidebar_comm = getTempLablePara_siteConfig("标题字符数截取(侧边栏)")
titleLimitLen_sideListNum_comm = getTempLablePara_siteConfig("内容输出列表数量(侧边栏)")


sql_1=""
sql_1_2=""
sql_1_3=""


sql_2=""
sql_2_2=""
sql_2_3=""

sql_3=""


sql_4=""
sql_4_2=""
sql_4_3=""

columnImgSwitch=""

'-------横向分类导航
if callCommInfoChk("${landScapeCateNav}") = true then
	
	sql="select  * from [plugins] where view = 0 and sort = 2 and sort <> 3 and  columnName<>'newPlugins' and columnName<>'links' "&sql_1_2&""&sql_4_2&""&sql_2_2&""&sql_4_2&" order by Index asc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	do while not rs.eof
	
	'文章分类高亮
	if instr(""&rs("content")&"",classname_go)>0 then 
	
		thislandScapeCateContent=replace(rs("content"),"title="""&classname_go&""""," class=""selected"" title="""&classname_go&"""")
	
	else
	
		thislandScapeCateContent=rs("content")
	
	end if
	
	landScapeCateNav = landScapeCateNav&""&thislandScapeCateContent&""
	
	
	rs.movenext
	loop 
				
	rs.close   
	set rs=nothing 	

end if



'----------无侧边类别导航	
if callCommInfoChk("${MySidebar_NonCate}") = true then

	sql="select  * from [plugins] where view = 0 and sort <> 2 and sort <> 3  and columnName <> 'classarticle' and  columnName<>'newPlugins'  and columnName<>'links' "&sql_3&""&sql_1_3&""&sql_2_3&""&sql_4_3&" order by Index asc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	do while not rs.eof
	
	MySidebar_NonCate = MySidebar_NonCate&""&rs("content")&""
	
	rs.movenext
	loop 
				
	rs.close   
	set rs=nothing 	

end if



'----------完整侧边导航	
if callCommInfoChk("${MySidebar}") = true then

	sql="select  * from [plugins] where view = 0 and sort <> 2 and sort <> 3 and  columnName<>'newPlugins'  and columnName<>'links' "&sql_1&""&sql_4&""&sql_2&""&sql_3&""&sql_1_3&""&sql_2_3&""&sql_4_3&" order by Index asc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	do while not rs.eof
	
	MySidebar = MySidebar&""&rs("content")&""
	
	rs.movenext
	loop 
				
	rs.close   
	set rs=nothing 		

end if


'----------首页侧边导航
if callCommInfoChk("${MySidebar_defaultPage}") = true then

	sql="select  * from [plugins] where sort <> 2 and sort <> 3 and columnName <> 'newPlugins' "&homeColumnConfig&" "&sql_1&""&sql_4&""&sql_2&""&sql_3&""&sql_1_3&""&sql_2_3&""&sql_4_3&" order by Index asc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	do while not rs.eof
	
	MySidebar_defaultPage = MySidebar_defaultPage&""&rs("content")&""
	
	rs.movenext
	loop 
				
	rs.close   
	set rs=nothing 	

end if
	
	
'------------------横向类别导航-文章
if callCommInfoChk("${listcontent_comm_classarticle_landScape}") = true then

	listcontent_comm_classarticle_landScape = ""
	sql="select * from [plugins] where columnName = 'classarticle_landScape'"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
		listcontent_comm_classarticle_landScape=""&DY_LANG_51&""
	else
		
		listcontent_comm_classarticle_landScape=rs("content")
		
		 
	end if
	rs.close   
	set rs=nothing 		

end if


'------------------搜索
if callCommInfoChk("${listcontent_comm_search}") = true then

	listcontent_comm_search = ""
	sql="select * from [plugins] where columnName = 'search_board'"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
		listcontent_comm_search=""&DY_LANG_51&""
	else
		
		listcontent_comm_search=replace(replace(rs("content"),"../../","../"),"../",MY_sitePath)
		
		 
	end if
	rs.close   
	set rs=nothing 	


end if




'------------------网名
if callCommInfoChk("${listcontent_comm_netname}") = true then

	listcontent_comm_netname = ""
	sql="select * from [plugins] where columnName = 'netname'"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
		listcontent_comm_netname=""&DY_LANG_51&""
	else
		
		listcontent_comm_netname=rs("content")
		
		 
	end if
	rs.close   
	set rs=nothing 		


end if



'------------------统计
if callCommInfoChk("${listcontent_comm_siteCounter}") = true then

	listcontent_sideAloneComm_siteCounter = ""
	sql="select * from [plugins] where columnName = 'info_count'"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
		listcontent_sideAloneComm_siteCounter=""&DY_LANG_51&""
	else
		
		listcontent_sideAloneComm_siteCounter=replace(replace(rs("content"),"<div class=""sitedata""><h3 class=""left_title"">"&DY_LANG_32&"</h3>",""),"</div>","")
		
		 
	end if
	rs.close   
	set rs=nothing 	

end if



'------------------------BANNER(通用)
if callCommInfoChk("${bannerImgList}") = true then

    bannerImg = ""
    bannerImgSmall = ""
	listcontent_comm_bannerShow = ""
	listcontent_comm_bannerSmallShow = ""
	
	sql="select * from [banner] order by bannerID desc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	do while not rs.eof
	
	banner_title=rs("title")
	banner_src=rs("src")
	banner_imgURL=rs("imgURL")
	banner_imgSmallURL=rs("imgSmallURL")
	banner_window=rs("window")	
	banner_id=rs("bannerID")
	
	MY_sitePathBanner = MY_sitePath
	MY_sitePathBanner = clearRightString(MY_sitePathBanner,"/")
	
	banner_imgURL = replace(replace(replace(replace(banner_imgURL,"//","/"),""&MY_sitePathBanner&""&MY_sitePathBanner&""&MY_sitePathBanner&"",""&MY_sitePathBanner&""),""&MY_sitePathBanner&""&MY_sitePathBanner&"",""&MY_sitePathBanner&""),"//","/")
	
	banner_imgSmallURL = replace(replace(replace(replace(banner_imgSmallURL,"//","/"),""&MY_sitePathBanner&""&MY_sitePathBanner&""&MY_sitePathBanner&"",""&MY_sitePathBanner&""),""&MY_sitePathBanner&""&MY_sitePathBanner&"",""&MY_sitePathBanner&""),"//","/")
	
	if instr(banner_imgURL,"../") then 
	    banner_imgURL = MY_sitePath&replace(banner_imgURL,"../","")
	end if 		
	
	if instr(banner_imgSmallURL,"../") then 
	    banner_imgSmallURL = MY_sitePath&replace(banner_imgSmallURL,"../","")
	end if 	
		
	
	if banner_title <> "" then bannerImg = "<li data-targetSmallImg="""&banner_imgSmallURL&""" data-targetSmallID=""banner-s-img"&banner_id&""" id=""banner-img"&banner_id&"""><a href="""&banner_src&""" target="""&banner_window&""" title="""&banner_title&"""><img src="""&banner_imgURL&""" alt="""&banner_title&""" /></a></li>"&vbcrlf else bannerImg = ""
	
	if banner_imgSmallURL <> "" then bannerImgSmall = "<li data-targetImg="""&banner_imgURL&""" data-targetID=""banner-img"&banner_id&""" id=""banner-s-img"&banner_id&"""><a href=""javascript:""><img src="""&banner_imgSmallURL&""" alt="""" /></a></li>"&vbcrlf else bannerImgSmall = ""
	

	listcontent_comm_bannerShow=listcontent_comm_bannerShow&bannerImg
	listcontent_comm_bannerSmallShow=listcontent_comm_bannerSmallShow&bannerImgSmall
	

	rs.movenext
	loop
	
	rs.close
	set rs=nothing


end if



'------------------分类栏目地址(通用)
if callCommInfoChk("${pageURL_comm_article") = true or callCommInfoChk("${pageURL_comm_works") = true then

	sql="select * from [ArticleClass]"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if not rs.eof then
	
	
	
		do while not rs.eof
		
		
			urlComm_ID=rs("ArticleClassID")	
			urlComm_ClassName=replace(rs("ClassName"),chr(32),"")
			urlComm_ClassName_show=rs("ClassName")
			
			Template_Code=replace(Template_Code,"${pageURL_comm_article["&urlComm_ClassName&"]}",""&MY_sitePath&"classlist/"&MY_createFolder_art&""&urlComm_ID&"_default.html")
			
			
			rs.movenext
		 loop 
		 
			
	else
		
		
	end if
	rs.close   
	set rs=nothing 
	
	'---
	
	sql="select * from [SuccessWorkClass]"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
		
	else
		
		
		do while not rs.eof
		
		
			urlComm_ID=rs("SuccessWorkClassID")	
			urlComm_ClassName=replace(rs("ClassName"),chr(32),"")
			urlComm_ClassName_show=rs("ClassName")
			
			Template_Code=replace(Template_Code,"${pageURL_comm_works["&urlComm_ClassName_show&"]}",""&MY_sitePath&"classlist/"&MY_createFolder_work&""&urlComm_ID&"_default.html")
			
			rs.movenext
		 loop 
	
		 
	end if
	rs.close   
	set rs=nothing 


end if
	
	

'------------------随机文章
if callCommInfoChk("${listcontent_comm_randomArticle}")  = true then

	randomize
	listcontent_sideAloneComm_randomArt = ""
	listcontent_sideAloneComm_randomArt = listcontent_sideAloneComm_randomArt&loopTempLabelString("loopList:sidebarList.alone_begin","{#loopVar:sidebarList_sideAloneComm.alone_begin.ul.style/}","","{#loopVar:sidebarList_sideAloneComm.alone_begin.DY_LANG.comm/}",""&DY_LANG_15&"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	
	
	sql="select top "&titleLimitLen_sideListNum_comm&" * from [article]  order by Rnd(ArticleID-timer()) desc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if  rs.eof and rs.bof then
	
	listcontent_sideAloneComm_randomArt = listcontent_sideAloneComm_randomArt& ""&DY_LANG_29&""
	else
	
	
	
	
	
	do while not rs.eof
	
	
	ID=rs("ArticleID")
	Subdate=rs("Subdate")
	ClassName=rs("ClassName")
	Title=replace(rs("ArticleTitle"),chr(32),"")
	Title_show=gotTopic(Title,titleLimitLen_sidebar_comm)
	hits=rs("hits")
	renum=rs("renum")
	mypath=replace(rs("FilePath"),"/default.html","")
	SubdateShow_y=year(Subdate)
	SubdateShow_m=month(Subdate)
	SubdateShow_d=day(Subdate)
	
	mainImg=rs("mainImg")
	Summary=replace(replace(HTMLDecode(rs("Summary")),"<p>",""),"</p>","<br />")
	postID=createBase64ID(replace(replace(mypath,MY_sitePath,""),MY_createFolder_art,""))

	if instr(mainImg,"security")>0 or instr(mainImg,"http://")>0 then
		useI_begin = ""
		useI_end = ""
		
	else
	
		useI_begin = "<!--"
		useI_end = "-->"
		
	end if
	
'主题图片
	
	SmallOrginUrl=replace(replace(mainImg,"../",""),""&MY_sec_uploadImgPath&"/?src=","")

	'外链判断			
	if instr(mainImg,"http://")>0 then
	   sSmallPath=mainImg
	   
	else
	   sSmallPath=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")		
	end if
	

	   

	
	
'分类获取
	sql_class="select * from [ArticleClass] where ClassName='"&ClassName&"'"
	set rs_class=server.createObject("ADODB.Recordset")
	rs_class.open sql_class,db,1,1
	ClassID=rs_class("ArticleClassID")
	ClassSrc="<a class=""sort"" href="""&MY_sitePath&"classlist/"&MY_createFolder_art&""&ClassID&"_default.html""  title="""&classname&""">"&classname&"</a>"&vbcrlf'类别链接
	rs_class.close
	set rs_class=nothing
	
									
	'分类ID
	listClassIDShow = "class-id-"&ClassID&""



 '取出被顶踩信息
 	  set rs_Dig=server.createobject("adodb.recordset")
	  sql_Dig="Select * From [vote] Where titleID="&ID&""
      rs_Dig.open sql_Dig,db,1,1
      if rs_Dig.eof then
      my_good="0"
      my_bad="0" 
	  else 
      my_good=rs_Dig("art_good")
      my_bad=rs_Dig("art_bad")
	  end if
	  	  
	  rs_Dig.Close
	  Set rs_Dig = Nothing
	

	 listcontent_sideAloneComm_randomArt = listcontent_sideAloneComm_randomArt&loopTempLabelString("loopList:sidebarList.alone_content","{#loopVar:sidebarList_sideAloneComm.alone_content.mypath.comm/}",""&mypath&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title.comm/}",""&Title&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title_show.comm/}",""&Title_show&"","{#loopVar:sidebarList_sideAloneComm.alone_content.li.a.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.li.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.sSmallPath.comm/}",""&sSmallPath&"","{#loopVar:postID/}",""&postID&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Summary.comm/}",""&Summary&"","{#loopVar:sidebarList_sideAloneComm.alone_content.hits.comm/}",""&hits&"","{#loopVar:sidebarList_sideAloneComm.alone_content.renum.comm/}",""&renum&"","<!--{#imgUse:begin/}-->",""&useI_begin&"","<!--{#imgUse:end/}-->",""&useI_end&"","","","","","{#loopVar:sidebarList_sideAloneComm.alone_content.classname.link.comm/}",""&ClassSrc&"","{#loopVar:sidebarList_sideAloneComm.alone_content.classname.comm/}",""&classname&"","","","{#loopVar:sidebarList_sideAloneComm_content.y_date.comm/}",""&SubdateShow_y&"","{#loopVar:sidebarList_sideAloneComm_content.m_date.comm/}",""&SubdateShow_m&"","{#loopVar:sidebarList_sideAloneComm_content.d_date.comm/}",""&SubdateShow_d&"",0) 
	
	rs.movenext
	loop 
	
	
	
	
	end if
	rs.close   
	set rs=nothing 
	
	   listcontent_sideAloneComm_randomArt = listcontent_sideAloneComm_randomArt&loopTempLabelString("loopList:sidebarList.alone_end","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	

end if



'--------------文章
if callCommInfoChk("${listcontent_comm_art}")  = true then
	
	
	listcontent_sideAloneComm_art = ""
	listcontent_sideAloneComm_art = listcontent_sideAloneComm_art&loopTempLabelString("loopList:sidebarList.alone_begin","{#loopVar:sidebarList_sideAloneComm.alone_begin.ul.style/}","","{#loopVar:sidebarList_sideAloneComm.alone_begin.DY_LANG.comm/}",""&DY_LANG_2&"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	
	
	sql="select top "&titleLimitLen_sideListNum_comm&"  * from [article]  order by ArticleID desc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
	
	listcontent_sideAloneComm_art = listcontent_sideAloneComm_art& ""&DY_LANG_30&""
	else
	
	
	
	
	do while not rs.eof
	
	
	ID=rs("ArticleID")
	Subdate=rs("Subdate")
	ClassName=rs("ClassName")
	Title=replace(rs("ArticleTitle"),chr(32),"")
	Title_show=gotTopic(Title,titleLimitLen_sidebar_comm)
	hits=rs("hits")
	renum=rs("renum")
	mypath=replace(rs("FilePath"),"/default.html","")
	SubdateShow_y=year(Subdate)
	SubdateShow_m=month(Subdate)
	SubdateShow_d=day(Subdate)
	
	mainImg=rs("mainImg")
	Summary=replace(replace(HTMLDecode(rs("Summary")),"<p>",""),"</p>","<br />")
	postID=createBase64ID(replace(replace(mypath,MY_sitePath,""),MY_createFolder_art,""))

	if instr(mainImg,"security")>0 or instr(mainImg,"http://")>0 then
		useI_begin = ""
		useI_end = ""
		
	else
	
		useI_begin = "<!--"
		useI_end = "-->"
		
	end if
	
'主题图片
	
	SmallOrginUrl=replace(replace(mainImg,"../",""),""&MY_sec_uploadImgPath&"/?src=","")

	'外链判断			
	if instr(mainImg,"http://")>0 then
	   sSmallPath=mainImg
	   
	else
	   sSmallPath=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")		
	end if
	
	
'分类获取
	sql_class="select * from [ArticleClass] where ClassName='"&ClassName&"'"
	set rs_class=server.createObject("ADODB.Recordset")
	rs_class.open sql_class,db,1,1
	ClassID=rs_class("ArticleClassID")
	ClassSrc="<a class=""sort"" href="""&MY_sitePath&"classlist/"&MY_createFolder_art&""&ClassID&"_default.html""  title="""&classname&""">"&classname&"</a>"&vbcrlf'类别链接
	rs_class.close
	set rs_class=nothing
	
	'分类ID
	listClassIDShow = "class-id-"&ClassID&""



 '取出被顶踩信息
 	  set rs_Dig=server.createobject("adodb.recordset")
	  sql_Dig="Select * From [vote] Where titleID="&ID&""
      rs_Dig.open sql_Dig,db,1,1
      if rs_Dig.eof then
      my_good="0"
      my_bad="0" 
	  else 
      my_good=rs_Dig("art_good")
      my_bad=rs_Dig("art_bad")
	  end if
	  	  
	  rs_Dig.Close
	  Set rs_Dig = Nothing
	
	
	 listcontent_sideAloneComm_art = listcontent_sideAloneComm_art&loopTempLabelString("loopList:sidebarList.alone_content","{#loopVar:sidebarList_sideAloneComm.alone_content.mypath.comm/}",""&mypath&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title.comm/}",""&Title&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title_show.comm/}",""&Title_show&"","{#loopVar:sidebarList_sideAloneComm.alone_content.li.a.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.li.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.sSmallPath.comm/}",""&sSmallPath&"","{#loopVar:postID/}",""&postID&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Summary.comm/}",""&Summary&"","{#loopVar:sidebarList_sideAloneComm.alone_content.hits.comm/}",""&hits&"","{#loopVar:sidebarList_sideAloneComm.alone_content.renum.comm/}",""&renum&"","<!--{#imgUse:begin/}-->",""&useI_begin&"","<!--{#imgUse:end/}-->",""&useI_end&"","","","","","{#loopVar:sidebarList_sideAloneComm.alone_content.classname.link.comm/}",""&ClassSrc&"","{#loopVar:sidebarList_sideAloneComm.alone_content.classname.comm/}",""&classname&"","","","{#loopVar:sidebarList_sideAloneComm_content.y_date.comm/}",""&SubdateShow_y&"","{#loopVar:sidebarList_sideAloneComm_content.m_date.comm/}",""&SubdateShow_m&"","{#loopVar:sidebarList_sideAloneComm_content.d_date.comm/}",""&SubdateShow_d&"",0) 
	
	rs.movenext
	loop 
	
	
	
	
	end if
	rs.close   
	set rs=nothing 
	
	
	listcontent_sideAloneComm_art = listcontent_sideAloneComm_art&loopTempLabelString("loopList:sidebarList.alone_end","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	listcontent_sideAloneComm_art = listcontent_sideAloneComm_art&loopTempLabelString("loopList:sidebarList.alone_more","{#loopVar:sidebarList_sideAloneComm_more.MY_sitePath.comm/}",""&MY_sitePath&"","{#loopVar:sidebarList_sideAloneComm.alone_more.MY_createFolder.comm/}",""&MY_sitePath&"list/"&MY_createFolder_art&"_default.html","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
		

end if


'--------------最热文章
if callCommInfoChk("${listcontent_comm_hot_art}")  = true then
	
	listcontent_sideAloneComm_Hot_art = ""
	listcontent_sideAloneComm_Hot_art = listcontent_sideAloneComm_Hot_art&loopTempLabelString("loopList:sidebarList.alone_begin","{#loopVar:sidebarList_sideAloneComm.alone_begin.ul.style/}","","{#loopVar:sidebarList_sideAloneComm.alone_begin.DY_LANG.comm/}",""&DY_LANG_3&"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	
	
	sql="select top "&titleLimitLen_sideListNum_comm&"  * from [article]  order by hits desc, Subdate desc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
	
	listcontent_sideAloneComm_Hot_art = listcontent_sideAloneComm_Hot_art& ""&DY_LANG_30&""
	else
	
	
	
	
	do while not rs.eof
	
	
	ID=rs("ArticleID")
	Subdate=rs("Subdate")
	ClassName=rs("ClassName")
	Title=replace(rs("ArticleTitle"),chr(32),"")
	Title_show=gotTopic(Title,titleLimitLen_sidebar_comm)
	hits=rs("hits")
	renum=rs("renum")
	mypath=replace(rs("FilePath"),"/default.html","")
	SubdateShow_y=year(Subdate)
	SubdateShow_m=month(Subdate)
	SubdateShow_d=day(Subdate)
	
	mainImg=rs("mainImg")
	Summary=replace(replace(HTMLDecode(rs("Summary")),"<p>",""),"</p>","<br />")
	postID=createBase64ID(replace(replace(mypath,MY_sitePath,""),MY_createFolder_art,""))

	if instr(mainImg,"security")>0 or instr(mainImg,"http://")>0 then
		useI_begin = ""
		useI_end = ""
		
	else
	
		useI_begin = "<!--"
		useI_end = "-->"
		
	end if
	
'主题图片
	
	SmallOrginUrl=replace(replace(mainImg,"../",""),""&MY_sec_uploadImgPath&"/?src=","")

	'外链判断			
	if instr(mainImg,"http://")>0 then
	   sSmallPath=mainImg
	   
	else
	   sSmallPath=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")		
	end if
	
'分类获取
	sql_class="select * from [ArticleClass] where ClassName='"&ClassName&"'"
	set rs_class=server.createObject("ADODB.Recordset")
	rs_class.open sql_class,db,1,1
	ClassID=rs_class("ArticleClassID")
	ClassSrc="<a class=""sort"" href="""&MY_sitePath&"classlist/"&MY_createFolder_art&""&ClassID&"_default.html""  title="""&classname&""">"&classname&"</a>"&vbcrlf'类别链接
	rs_class.close
	set rs_class=nothing

	'分类ID
	listClassIDShow = "class-id-"&ClassID&""


 '取出被顶踩信息
 	  set rs_Dig=server.createobject("adodb.recordset")
	  sql_Dig="Select * From [vote] Where titleID="&ID&""
      rs_Dig.open sql_Dig,db,1,1
      if rs_Dig.eof then
      my_good="0"
      my_bad="0" 
	  else 
      my_good=rs_Dig("art_good")
      my_bad=rs_Dig("art_bad")
	  end if
	  	  
	  rs_Dig.Close
	  Set rs_Dig = Nothing
	
	
	listcontent_sideAloneComm_Hot_art = listcontent_sideAloneComm_Hot_art&loopTempLabelString("loopList:sidebarList.alone_content","{#loopVar:sidebarList_sideAloneComm.alone_content.mypath.comm/}",""&mypath&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title.comm/}",""&Title&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title_show.comm/}",""&Title_show&"","{#loopVar:sidebarList_sideAloneComm.alone_content.li.a.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.li.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.sSmallPath.comm/}",""&sSmallPath&"","{#loopVar:postID/}",""&postID&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Summary.comm/}",""&Summary&"","{#loopVar:sidebarList_sideAloneComm.alone_content.hits.comm/}",""&hits&"","{#loopVar:sidebarList_sideAloneComm.alone_content.renum.comm/}",""&renum&"","<!--{#imgUse:begin/}-->",""&useI_begin&"","<!--{#imgUse:end/}-->",""&useI_end&"","","","","","{#loopVar:sidebarList_sideAloneComm.alone_content.classname.link.comm/}",""&ClassSrc&"","{#loopVar:sidebarList_sideAloneComm.alone_content.classname.comm/}",""&classname&"","","","{#loopVar:sidebarList_sideAloneComm_content.y_date.comm/}",""&SubdateShow_y&"","{#loopVar:sidebarList_sideAloneComm_content.m_date.comm/}",""&SubdateShow_m&"","{#loopVar:sidebarList_sideAloneComm_content.d_date.comm/}",""&SubdateShow_d&"",0) 
	
	rs.movenext
	loop 
	
	
	
	
	end if
	rs.close   
	set rs=nothing 
	
	
	listcontent_sideAloneComm_Hot_art = listcontent_sideAloneComm_Hot_art&loopTempLabelString("loopList:sidebarList.alone_end","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	listcontent_sideAloneComm_Hot_art = listcontent_sideAloneComm_Hot_art&loopTempLabelString("loopList:sidebarList.alone_more","{#loopVar:sidebarList_sideAloneComm_more.MY_sitePath.comm/}",""&MY_sitePath&"","{#loopVar:sidebarList_sideAloneComm.alone_more.MY_createFolder.comm/}",""&MY_sitePath&"list/"&MY_createFolder_art&"_default.html","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	

end if

'--------------(一个月内)最热文章
if callCommInfoChk("${listcontent_comm_hot_month_art}")  = true then
	
	listcontent_sideAloneComm_Hot_month_art = ""
	listcontent_sideAloneComm_Hot_month_art = listcontent_sideAloneComm_Hot_month_art&loopTempLabelString("loopList:sidebarList.alone_begin","{#loopVar:sidebarList_sideAloneComm.alone_begin.ul.style/}","","{#loopVar:sidebarList_sideAloneComm.alone_begin.DY_LANG.comm/}",""&DY_LANG_4&"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	
	
	sql="select top "&titleLimitLen_sideListNum_comm&"  * from [article] where DateDiff('h',Subdate,now())<=24*30  order by hits desc, Subdate desc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
	
	listcontent_sideAloneComm_Hot_month_art = listcontent_sideAloneComm_Hot_month_art& ""&DY_LANG_30&""
	else
	
	
	
	
	do while not rs.eof
	
	
	ID=rs("ArticleID")
	Subdate=rs("Subdate")
	ClassName=rs("ClassName")
	Title=replace(rs("ArticleTitle"),chr(32),"")
	Title_show=gotTopic(Title,titleLimitLen_sidebar_comm)
	hits=rs("hits")
	renum=rs("renum")
	mypath=replace(rs("FilePath"),"/default.html","")
	SubdateShow_y=year(Subdate)
	SubdateShow_m=month(Subdate)
	SubdateShow_d=day(Subdate)
	
	mainImg=rs("mainImg")
	Summary=replace(replace(HTMLDecode(rs("Summary")),"<p>",""),"</p>","<br />")
	postID=createBase64ID(replace(replace(mypath,MY_sitePath,""),MY_createFolder_art,""))

	if instr(mainImg,"security")>0 or instr(mainImg,"http://")>0 then
		useI_begin = ""
		useI_end = ""
		
	else
	
		useI_begin = "<!--"
		useI_end = "-->"
		
	end if
	
'主题图片
	
	SmallOrginUrl=replace(replace(mainImg,"../",""),""&MY_sec_uploadImgPath&"/?src=","")

	'外链判断			
	if instr(mainImg,"http://")>0 then
	   sSmallPath=mainImg
	   
	else
	   sSmallPath=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")		
	end if
	
'分类获取
	sql_class="select * from [ArticleClass] where ClassName='"&ClassName&"'"
	set rs_class=server.createObject("ADODB.Recordset")
	rs_class.open sql_class,db,1,1
	ClassID=rs_class("ArticleClassID")
	ClassSrc="<a class=""sort"" href="""&MY_sitePath&"classlist/"&MY_createFolder_art&""&ClassID&"_default.html""  title="""&classname&""">"&classname&"</a>"&vbcrlf'类别链接
	rs_class.close
	set rs_class=nothing

	'分类ID
	listClassIDShow = "class-id-"&ClassID&""



 '取出被顶踩信息
 	  set rs_Dig=server.createobject("adodb.recordset")
	  sql_Dig="Select * From [vote] Where titleID="&ID&""
      rs_Dig.open sql_Dig,db,1,1
      if rs_Dig.eof then
      my_good="0"
      my_bad="0" 
	  else 
      my_good=rs_Dig("art_good")
      my_bad=rs_Dig("art_bad")
	  end if
	  	  
	  rs_Dig.Close
	  Set rs_Dig = Nothing
	
	
	listcontent_sideAloneComm_Hot_month_art = listcontent_sideAloneComm_Hot_month_art&loopTempLabelString("loopList:sidebarList.alone_content","{#loopVar:sidebarList_sideAloneComm.alone_content.mypath.comm/}",""&mypath&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title.comm/}",""&Title&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title_show.comm/}",""&Title_show&"","{#loopVar:sidebarList_sideAloneComm.alone_content.li.a.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.li.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.sSmallPath.comm/}",""&sSmallPath&"","{#loopVar:postID/}",""&postID&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Summary.comm/}",""&Summary&"","{#loopVar:sidebarList_sideAloneComm.alone_content.hits.comm/}",""&hits&"","{#loopVar:sidebarList_sideAloneComm.alone_content.renum.comm/}",""&renum&"","<!--{#imgUse:begin/}-->",""&useI_begin&"","<!--{#imgUse:end/}-->",""&useI_end&"","","","","","{#loopVar:sidebarList_sideAloneComm.alone_content.classname.link.comm/}",""&ClassSrc&"","{#loopVar:sidebarList_sideAloneComm.alone_content.classname.comm/}",""&classname&"","","","{#loopVar:sidebarList_sideAloneComm_content.y_date.comm/}",""&SubdateShow_y&"","{#loopVar:sidebarList_sideAloneComm_content.m_date.comm/}",""&SubdateShow_m&"","{#loopVar:sidebarList_sideAloneComm_content.d_date.comm/}",""&SubdateShow_d&"",0) 
	
	rs.movenext
	loop 
	
	
	
	
	end if
	rs.close   
	set rs=nothing 
	
	
	listcontent_sideAloneComm_Hot_month_art = listcontent_sideAloneComm_Hot_month_art&loopTempLabelString("loopList:sidebarList.alone_end","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	listcontent_sideAloneComm_Hot_month_art = listcontent_sideAloneComm_Hot_month_art&loopTempLabelString("loopList:sidebarList.alone_more","{#loopVar:sidebarList_sideAloneComm_more.MY_sitePath.comm/}",""&MY_sitePath&"","{#loopVar:sidebarList_sideAloneComm.alone_more.MY_createFolder.comm/}",""&MY_sitePath&"list/"&MY_createFolder_art&"_default.html","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	

end if


'--------------(一周内)最热文章
if callCommInfoChk("${listcontent_comm_hot_week_art}")  = true then
	
	listcontent_sideAloneComm_Hot_week_art = ""
	listcontent_sideAloneComm_Hot_week_art = listcontent_sideAloneComm_Hot_week_art&loopTempLabelString("loopList:sidebarList.alone_begin","{#loopVar:sidebarList_sideAloneComm.alone_begin.ul.style/}","","{#loopVar:sidebarList_sideAloneComm.alone_begin.DY_LANG.comm/}",""&DY_LANG_5&"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	
	
	sql="select top "&titleLimitLen_sideListNum_comm&"  * from [article] where DateDiff('h',Subdate,now())<=24*7  order by hits desc, Subdate desc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
	
	listcontent_sideAloneComm_Hot_week_art = listcontent_sideAloneComm_Hot_week_art& ""&DY_LANG_30&""
	else
	
	
	
	
	do while not rs.eof
	
	
	ID=rs("ArticleID")
	Subdate=rs("Subdate")
	ClassName=rs("ClassName")
	Title=replace(rs("ArticleTitle"),chr(32),"")
	Title_show=gotTopic(Title,titleLimitLen_sidebar_comm)
	hits=rs("hits")
	renum=rs("renum")
	mypath=replace(rs("FilePath"),"/default.html","")
	SubdateShow_y=year(Subdate)
	SubdateShow_m=month(Subdate)
	SubdateShow_d=day(Subdate)
	
	mainImg=rs("mainImg")
	Summary=replace(replace(HTMLDecode(rs("Summary")),"<p>",""),"</p>","<br />")
	postID=createBase64ID(replace(replace(mypath,MY_sitePath,""),MY_createFolder_art,""))

	if instr(mainImg,"security")>0 or instr(mainImg,"http://")>0 then
		useI_begin = ""
		useI_end = ""
		
	else
	
		useI_begin = "<!--"
		useI_end = "-->"
		
	end if
	
'主题图片
	
	SmallOrginUrl=replace(replace(mainImg,"../",""),""&MY_sec_uploadImgPath&"/?src=","")

	'外链判断			
	if instr(mainImg,"http://")>0 then
	   sSmallPath=mainImg
	   
	else
	   sSmallPath=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")		
	end if
	
	
'分类获取
	sql_class="select * from [ArticleClass] where ClassName='"&ClassName&"'"
	set rs_class=server.createObject("ADODB.Recordset")
	rs_class.open sql_class,db,1,1
	ClassID=rs_class("ArticleClassID")
	ClassSrc="<a class=""sort"" href="""&MY_sitePath&"classlist/"&MY_createFolder_art&""&ClassID&"_default.html""  title="""&classname&""">"&classname&"</a>"&vbcrlf'类别链接
	rs_class.close
	set rs_class=nothing

	'分类ID
	listClassIDShow = "class-id-"&ClassID&""



 '取出被顶踩信息
 	  set rs_Dig=server.createobject("adodb.recordset")
	  sql_Dig="Select * From [vote] Where titleID="&ID&""
      rs_Dig.open sql_Dig,db,1,1
      if rs_Dig.eof then
      my_good="0"
      my_bad="0" 
	  else 
      my_good=rs_Dig("art_good")
      my_bad=rs_Dig("art_bad")
	  end if
	  	  
	  rs_Dig.Close
	  Set rs_Dig = Nothing
	
	
	listcontent_sideAloneComm_Hot_week_art = listcontent_sideAloneComm_Hot_week_art&loopTempLabelString("loopList:sidebarList.alone_content","{#loopVar:sidebarList_sideAloneComm.alone_content.mypath.comm/}",""&mypath&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title.comm/}",""&Title&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title_show.comm/}",""&Title_show&"","{#loopVar:sidebarList_sideAloneComm.alone_content.li.a.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.li.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.sSmallPath.comm/}",""&sSmallPath&"","{#loopVar:postID/}",""&postID&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Summary.comm/}",""&Summary&"","{#loopVar:sidebarList_sideAloneComm.alone_content.hits.comm/}",""&hits&"","{#loopVar:sidebarList_sideAloneComm.alone_content.renum.comm/}",""&renum&"","<!--{#imgUse:begin/}-->",""&useI_begin&"","<!--{#imgUse:end/}-->",""&useI_end&"","","","","","{#loopVar:sidebarList_sideAloneComm.alone_content.classname.link.comm/}",""&ClassSrc&"","{#loopVar:sidebarList_sideAloneComm.alone_content.classname.comm/}",""&classname&"","","","{#loopVar:sidebarList_sideAloneComm_content.y_date.comm/}",""&SubdateShow_y&"","{#loopVar:sidebarList_sideAloneComm_content.m_date.comm/}",""&SubdateShow_m&"","{#loopVar:sidebarList_sideAloneComm_content.d_date.comm/}",""&SubdateShow_d&"",0) 
	
	rs.movenext
	loop 
	
	
	
	
	end if
	rs.close   
	set rs=nothing 
	
	
	listcontent_sideAloneComm_Hot_week_art = listcontent_sideAloneComm_Hot_week_art&loopTempLabelString("loopList:sidebarList.alone_end","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	listcontent_sideAloneComm_Hot_week_art = listcontent_sideAloneComm_Hot_week_art&loopTempLabelString("loopList:sidebarList.alone_more","{#loopVar:sidebarList_sideAloneComm_more.MY_sitePath.comm/}",""&MY_sitePath&"","{#loopVar:sidebarList_sideAloneComm.alone_more.MY_createFolder.comm/}",""&MY_sitePath&"list/"&MY_createFolder_art&"_default.html","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	

end if



'------------作品类别【侧边类别导航】
if callCommInfoChk("${listcontent_comm_classWorks}")  = true then
	
	
	
	listcontent_sideAloneComm_classWorks = ""
	listcontent_sideAloneComm_classWorks = listcontent_sideAloneComm_classWorks&loopTempLabelString("loopList:sidebarList.alone_begin","{#loopVar:sidebarList_sideAloneComm.alone_begin.ul.style/}","","{#loopVar:sidebarList_sideAloneComm.alone_begin.DY_LANG.comm/}",""&DY_LANG_104&"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	
	sql="select * from [SuccessWorkClass] order by Index asc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
	
	listcontent_sideAloneComm_classWorks = listcontent_sideAloneComm_classWorks& ""&DY_LANG_30&""
	else
	
	
	
	do while not rs.eof
	
	ID=rs("SuccessWorkClassID")	
	ClassName=replace(rs("ClassName"),chr(32),"")
	ClassName_show=ClassName
	ClassIntro=rs("ClassIntro")
	'提取相应类别的内容数量
	sql2="select * from [SuccessWork] where ClassName='"& ClassName &"'"
	set rs2=server.createobject("adodb.recordset")
	rs2.open sql2,db,1,1
	quantity=rs2.recordcount
	rs2.close
	set rs2=nothing
	
	postclassID=createBase64ID(ID&ClassName)
	
	
	
	listcontent_sideAloneComm_classWorks = listcontent_sideAloneComm_classWorks&loopTempLabelString("loopList:sidebarList.alone_content","{#loopVar:sidebarList_sideAloneComm.alone_content.mypath.comm/}",""&MY_sitePath&"classlist/"&MY_createFolder_work&""&ID&"_default.html","{#loopVar:sidebarList_sideAloneComm.alone_content.Title.comm/}",""&ClassIntro&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title_show.comm/}",""&ClassName_show&"","{#loopVar:sidebarList_sideAloneComm.alone_content.li.a.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.li.class}","","","","","","","","","","","","","","","","","","","","","","","","{#loopVar:post.classID/}",""&postclassID&"","{#loopVar:sidebarList_sideAloneComm_content.y_date.comm/}",""&SubdateShow_y&"","{#loopVar:sidebarList_sideAloneComm_content.m_date.comm/}",""&SubdateShow_m&"","{#loopVar:sidebarList_sideAloneComm_content.d_date.comm/}",""&SubdateShow_d&"",0) 
	
	rs.movenext
	loop 
	
	
	
	
	end if
	rs.close   
	set rs=nothing 
	
	listcontent_sideAloneComm_classWorks = listcontent_sideAloneComm_classWorks&loopTempLabelString("loopList:sidebarList.alone_end","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)


end if
	   

'------------文章类别【侧边类别导航】
if callCommInfoChk("${listcontent_comm_classarticle}")  = true then
	
	listcontent_sideAloneComm_classArt = ""
	listcontent_sideAloneComm_classArt = listcontent_sideAloneComm_classArt&loopTempLabelString("loopList:sidebarList.alone_begin","{#loopVar:sidebarList_sideAloneComm.alone_begin.ul.style/}","","{#loopVar:sidebarList_sideAloneComm.alone_begin.DY_LANG.comm/}",""&DY_LANG_10&"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	
	sql="select * from [ArticleClass] order by Index asc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
	
	listcontent_sideAloneComm_classArt = listcontent_sideAloneComm_classArt& ""&DY_LANG_30&""
	else
	
	
	
	do while not rs.eof
	
	ID=rs("ArticleClassID")	
	ClassName=replace(rs("ClassName"),chr(32),"")
	ClassName_show=ClassName
	ClassIntro=rs("ClassIntro")
	'提取相应类别的内容数量
	sql2="select * from [Article] where ClassName='"& ClassName &"'"
	set rs2=server.createobject("adodb.recordset")
	rs2.open sql2,db,1,1
	quantity=rs2.recordcount
	rs2.close
	set rs2=nothing
	
	postclassID=createBase64ID(ID&ClassName)
	
	
	rssLink = "<a class=""rssLink"" href="""&MY_sitePath&"rss/?classname="&server.URLEncode(ClassName)&""" target=""_blank""><img src="""&MY_sitePath&"images/Blog_Rss.png""  alt=""""/></a>"
	
	toIDT = createBase64ID(ClassName_show)
	
	
	listcontent_sideAloneComm_classArt = listcontent_sideAloneComm_classArt&loopTempLabelString("loopList:sidebarList.alone_content","{#loopVar:sidebarList_sideAloneComm.alone_content.mypath.comm/}",""&MY_sitePath&"classlist/"&MY_createFolder_art&""&ID&"_default.html","{#loopVar:sidebarList_sideAloneComm.alone_content.Title.comm/}",""&ClassName&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title_show.comm/}",""&ClassName_show&"","{#loopVar:sidebarList_sideAloneComm.alone_content.li.a.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.li.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.toID(Title).comm/}",""&toIDT&"","","","","","","","","","","","","","","","","","","","{#loopVar:post.rssLink/}",""&rssLink&"","{#loopVar:post.classID/}",""&postclassID&"","{#loopVar:sidebarList_sideAloneComm_content.y_date.comm/}",""&SubdateShow_y&"","{#loopVar:sidebarList_sideAloneComm_content.m_date.comm/}",""&SubdateShow_m&"","{#loopVar:sidebarList_sideAloneComm_content.d_date.comm/}",""&SubdateShow_d&"",0) 
	
	rs.movenext
	loop 
	
	
	
	
	end if
	rs.close   
	set rs=nothing 
	
	listcontent_sideAloneComm_classArt = listcontent_sideAloneComm_classArt&loopTempLabelString("loopList:sidebarList.alone_end","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	

end if


'------------------links
if callCommInfoChk("${listcontent_comm_links}")  = true then
	
	listcontent_sideAloneComm_links = ""
	listcontent_sideAloneComm_links = listcontent_sideAloneComm_links&loopTempLabelString("loopList:sidebarList.alone_begin","{#loopVar:sidebarList_sideAloneComm.alone_begin.ul.style/}","","{#loopVar:sidebarList_sideAloneComm.alone_begin.DY_LANG.comm/}",""&DY_LANG_14&"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	
	
	
	sql="select top "&MY_NumLinksSide&"  * from [Link] where view=1 and homeShow=1 order by Index asc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
	
	listcontent_sideAloneComm_links = listcontent_sideAloneComm_links& ""&DY_LANG_30&""
	else
	
	
	
	
	do while not rs.eof
	
	LinkTitle=replace(rs("LinkTitle"),chr(32),"")
	LinkTitle_show=gotTopic(LinkTitle,titleLimitLen_sidebar_comm)
	ClassName=rs("ClassName")
	URL=rs("URL")
	o_logo=rs("logo")
	urlOrigin=replace(replace(replace(o_logo,"../",""),""&MY_sec_uploadImgPath&"/?src=",""),MY_sitePath,"")
	logo=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&urlOrigin&""
	
	
	if instr(o_logo,"http://")>0 then
	logo=o_logo
	else
	urlOrigin=replace(replace(replace(o_logo,"../",""),""&MY_sec_uploadImgPath&"/?src=",""),MY_sitePath,"")
	logo=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&urlOrigin&""
	end if
	
	
	if o_logo="" then '没有LOGO时
	
	LinkTitle_show = LinkTitle_show
	liClass_link = "class=""sideLink"""
	
	
	else  '存在LOGO时
	
	LinkTitle_show = "<img src="""&logo&""" alt="""&LinkTitle&""" />"
	liClass_link = "class=""sideLink_img"""
	
	
	end if
	
	
	
	listcontent_sideAloneComm_links = listcontent_sideAloneComm_links&loopTempLabelString("loopList:sidebarList.alone_content","{#loopVar:sidebarList_sideAloneComm.alone_content.mypath.comm/}",""&URL&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title.comm/}",""&LinkTitle&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title_show.comm/}",""&LinkTitle_show&"","{#loopVar:sidebarList_sideAloneComm.alone_content.li.a.class}","target="&CHR(34)&"_blank"&CHR(34)&"","{#loopVar:sidebarList_sideAloneComm.alone_content.li.class}",""&liClass_link&"","","","","","","","","","","","","","","","","","","","","","","","","","{#loopVar:sidebarList_sideAloneComm_content.y_date.comm/}",""&SubdateShow_y&"","{#loopVar:sidebarList_sideAloneComm_content.m_date.comm/}",""&SubdateShow_m&"","{#loopVar:sidebarList_sideAloneComm_content.d_date.comm/}",""&SubdateShow_d&"",0) 
	
	rs.movenext
	loop 
	
	
	
	end if
	rs.close   
	set rs=nothing 
	
	listcontent_sideAloneComm_links = listcontent_sideAloneComm_links&loopTempLabelString("loopList:sidebarList.alone_end","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	listcontent_sideAloneComm_links = listcontent_sideAloneComm_links&loopTempLabelString("loopList:sidebarList.alone_more","{#loopVar:sidebarList_sideAloneComm_more.MY_sitePath.comm/}",""&MY_sitePath&"","{#loopVar:sidebarList_sideAloneComm.alone_more.MY_createFolder.comm/}",""&MY_sitePath&"catlinks.html","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)

end if



'------------------评论
if callCommInfoChk("${listcontent_comm_comment}")  = true then
	
	listcontent_sideAloneComm_comment = ""
	listcontent_sideAloneComm_comment = listcontent_sideAloneComm_comment&loopTempLabelString("loopList:sidebarList.alone_begin","{#loopVar:sidebarList_sideAloneComm.alone_begin.ul.style/}","","{#loopVar:sidebarList_sideAloneComm.alone_begin.DY_LANG.comm/}",""&DY_LANG_16&"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	
	
	'////文章评论	
	sql="select top "&n&" * from [ArticleComment] order by ArticleCommentid desc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if  rs.eof and rs.bof then
	
	listcontent_sideAloneComm_comment = listcontent_sideAloneComm_comment& ""&DY_LANG_18&""
	else
	
	
	do while not rs.eof
	
	ID=rs("ArticleID")
	comment=stripHTML(rs("comment"))
	comment_show=gotTopic(comment,titleLimitLen_sidebar_comm)
	username=rs("username")
	subdate=rs("subdate")
	view=rs("view")	
	commID=rs("ArticleCommentid")	
	
	'---------
	set rs2=server.CreateObject("adodb.recordset")
	strsql2="select  * from [Article] where articleid="&ID
	rs2.open strsql2,db,1,1
	
	mypath=replace(rs2("FilePath"),"/default.html","")
	
	rs2.close
	set rs2=nothing		
	'----------
	
	
	if view="0" then
	
	listcontent_sideAloneComm_comment = listcontent_sideAloneComm_comment&loopTempLabelString("loopList:sidebarList.alone_content","{#loopVar:sidebarList_sideAloneComm.alone_content.mypath.comm/}",""&mypath&"#"&commID&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title.comm/}",""&username&""&DY_LANG_21&""&subdate&"&#13;"&comment&"&#13;"&DY_LANG_22&""&ID&""&DY_LANG_23&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title_show.comm/}",""&comment_show&"","{#loopVar:sidebarList_sideAloneComm.alone_content.li.a.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.li.class}","","","","","","","","","","","","","","","","","","","","","","","","","","{#loopVar:sidebarList_sideAloneComm_content.y_date.comm/}",""&SubdateShow_y&"","{#loopVar:sidebarList_sideAloneComm_content.m_date.comm/}",""&SubdateShow_m&"","{#loopVar:sidebarList_sideAloneComm_content.d_date.comm/}",""&SubdateShow_d&"",0) 
	
	
	
	else 
	
	listcontent_sideAloneComm_comment = listcontent_sideAloneComm_comment&loopTempLabelString("loopList:sidebarList.alone_content","{#loopVar:sidebarList_sideAloneComm.alone_content.mypath.comm/}",""&mypath&"#"&commID&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title.comm/}",""&username&""&DY_LANG_21&""&subdate&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title_show.comm/}",""&DY_LANG_27&"","{#loopVar:sidebarList_sideAloneComm.alone_content.li.a.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.li.class}","","","","","","","","","","","","","","","","","","","","","","","","","","{#loopVar:sidebarList_sideAloneComm_content.y_date.comm/}",""&SubdateShow_y&"","{#loopVar:sidebarList_sideAloneComm_content.m_date.comm/}",""&SubdateShow_m&"","{#loopVar:sidebarList_sideAloneComm_content.d_date.comm/}",""&SubdateShow_d&"",0) 
	
	
	
	end if	
	
	rs.movenext
	loop 
	  
	end if
	rs.close   
	set rs=nothing 
	
	
	listcontent_sideAloneComm_comment = listcontent_sideAloneComm_comment&loopTempLabelString("loopList:sidebarList.alone_end","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	

end if


'------------------推荐文章-按类别来调用(通用)
if callCommInfoChk("${listcontent_comm_topArticle")  = true then
	
	titleLimitLen_comm_classArticle = getTempLablePara_siteConfig("标题字符数截取(对应类别调用文章-通用)")
	titleLimitLen_comm_classArticle_ListNum = getTempLablePara_siteConfig("内容输出列表数量(对应类别调用文章-通用)")
	
	sql2="select * from [ArticleClass]"
	set rs2=server.createObject("ADODB.Recordset")
	rs2.open sql2,db,1,1
	
	if rs2.EOF or rs2.BOF then
		
		listcontent_comm_classArticleList = ""&DY_LANG_30&""
	
	else
	
	
		do until rs2.eof
		
	
			classname_go_comm=cstr(rs2("ClassName"))
	
			
			sql="select  top "&titleLimitLen_comm_classArticle_ListNum&"  * from [article]  where ClassName='"& classname_go_comm &"' order by Subdate desc"
			set rs=server.createobject("adodb.recordset")
			rs.open sql,db,1,1
			if rs.bof then
				listcontent_comm_classArticleList=""&DY_LANG_51&""
				Template_Code=replace(Template_Code,"${listcontent_comm_topArticle["&classname_go_comm&"]}",listcontent_comm_classArticleList)
			else
				
				
				listcontent_comm_classArticleList = ""
				listcontent_comm_classArticleList = listcontent_comm_classArticleList&loopTempLabelString("loopList:sidebarList.alone_begin","{#loopVar:sidebarList_sideAloneComm.alone_begin.ul.style/}","","{#loopVar:sidebarList_sideAloneComm.alone_begin.DY_LANG.comm/}",""&DY_LANG_2&"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
		
	
				
				
				do while not rs.eof
				
				
				ID=rs("Articleid")
				Subdate=rs("Subdate")
				classname=rs("ClassName")
				Title=replace(rs("ArticleTitle"),chr(32),"")
				Title_show=gotTopic(Title,titleLimitLen_comm_classArticle)
				hits=rs("hits")
				files=replace(rs("files"),"/","")
				mainImg=rs("mainImg")
				renum=rs("renum")
				Summary=replace(replace(HTMLDecode(rs("Summary")),"<p>",""),"</p>","<br />")
				mypath=replace(rs("FilePath"),"/default.html","")
				SubdateShow=year(Subdate)&"-"&month(Subdate)&"-"&day(Subdate)
				SubdateShow_y=year(Subdate)
				SubdateShow_m=month(Subdate)
				SubdateShow_d=day(Subdate)
	
				postID=createBase64ID(replace(replace(mypath,MY_sitePath,""),MY_createFolder_art,""))
			
				if instr(mainImg,"security")>0 or instr(mainImg,"http://")>0 then
					useI_begin = ""
					useI_end = ""
					
				else
				
					useI_begin = "<!--"
					useI_end = "-->"
					
				end if
				
			'主题图片
				
				SmallOrginUrl=replace(replace(mainImg,"../",""),""&MY_sec_uploadImgPath&"/?src=","")

				'外链判断			
				if instr(mainImg,"http://")>0 then
				   sSmallPath=mainImg
				   
				else
				   sSmallPath=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")		
				end if
				
				
			'分类获取
				sql_class="select * from [ArticleClass] where ClassName='"&ClassName&"'"
				set rs_class=server.createObject("ADODB.Recordset")
				rs_class.open sql_class,db,1,1
				ClassID=rs_class("ArticleClassID")
				ClassSrc="<a class=""sort"" href="""&MY_sitePath&"classlist/"&MY_createFolder_art&""&ClassID&"_default.html""  title="""&classname&""">"&classname&"</a>"&vbcrlf'类别链接
				rs_class.close
				set rs_class=nothing
				
				'分类ID
				listClassIDShow = "class-id-"&ClassID&""

			
			
			 '取出被顶踩信息
				  set rs_Dig=server.createobject("adodb.recordset")
				  sql_Dig="Select * From [vote] Where titleID="&ID&""
				  rs_Dig.open sql_Dig,db,1,1
				  if rs_Dig.eof then
				  my_good="0"
				  my_bad="0" 
				  else 
				  my_good=rs_Dig("art_good")
				  my_bad=rs_Dig("art_bad")
				  end if
					  
				  rs_Dig.Close
				  Set rs_Dig = Nothing
				
				
				  '日期
				  t_year = year(Subdate)
				  t_month = month(Subdate)
				  t_day = day(Subdate)
				  
					
					 listcontent_comm_classArticleList = listcontent_comm_classArticleList&loopTempLabelString("loopList:sidebarList.alone_content","{#loopVar:sidebarList_sideAloneComm.alone_content.mypath.comm/}",""&mypath&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title.comm/}",""&Title&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title_show.comm/}",""&Title_show&"","{#loopVar:sidebarList_sideAloneComm.alone_content.li.a.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.li.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.sSmallPath.comm/}",""&sSmallPath&"","{#loopVar:postID/}",""&postID&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Summary.comm/}",""&Summary&"","{#loopVar:sidebarList_sideAloneComm.alone_content.hits.comm/}",""&hits&"","{#loopVar:sidebarList_sideAloneComm.alone_content.renum.comm/}",""&renum&"","<!--{#imgUse:begin/}-->",""&useI_begin&"","<!--{#imgUse:end/}-->",""&useI_end&"","","","","","{#loopVar:sidebarList_sideAloneComm.alone_content.classname.link.comm/}",""&ClassSrc&"","{#loopVar:sidebarList_sideAloneComm.alone_content.classname.comm/}",""&classname&"","","","{#loopVar:sidebarList_sideAloneComm_content.y_date.comm/}",""&SubdateShow_y&"","{#loopVar:sidebarList_sideAloneComm_content.m_date.comm/}",""&SubdateShow_m&"","{#loopVar:sidebarList_sideAloneComm_content.d_date.comm/}",""&SubdateShow_d&"",0) 
					
		
		
		
					rs.movenext
				 loop 
				 
				 
			   listcontent_comm_classArticleList = listcontent_comm_classArticleList&loopTempLabelString("loopList:sidebarList.alone_end","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
			   listcontent_comm_classArticleList = listcontent_comm_classArticleList&loopTempLabelString("loopList:sidebarList.alone_more","{#loopVar:sidebarList_sideAloneComm_more.MY_sitePath.comm/}",""&MY_sitePath&"","{#loopVar:sidebarList_sideAloneComm.alone_more.MY_createFolder.comm/}",""&MY_sitePath&"list/"&MY_createFolder_art&"_default.html","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
					
				 
				 
				 Template_Code=replace(Template_Code,"${listcontent_comm_topArticle["&classname_go_comm&"]}",listcontent_comm_classArticleList)
				 
			end if
			rs.close   
			set rs=nothing 
			
	
					
				
		
		rs2.movenext
		loop
		rs2.close   
		set rs2=nothing 	
	
	end if

end if



'------------------最新案例作品(通用)
if callCommInfoChk("${listcontent_comm_newcase}")  = true or callCommInfoChk("{#loopListContentShow:works.list.comm/}")  = true then
	
	titleLimitLen_comm_case = getTempLablePara_siteConfig("标题字符数截取(最新案例作品-通用)")
	titleLimitLen_comm_case_ListNum = getTempLablePara_siteConfig("内容输出列表数量(最新案例作品-通用)")
	
	sql="select top "&titleLimitLen_comm_case_ListNum&"  * from [SuccessWork] where private=0  order by Subdate desc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
		listcontent_comm_case=""&DY_LANG_51&""
	else
		
		listcontent_comm_case=""
		
		do while not rs.eof
		
		
		ID=rs("ID")
		Title=replace(rs("title"),chr(32),"")
		Title_show=gotTopic(Title,titleLimitLen_comm_case)
		url=rs("url")
		ClassName=rs("ClassName")
		content=replace(replace(HTMLDecode(rs("content")),"<p>",""),"</p>","<br />")
		Image=replace(rs("preURL"),"../","")
		Subdate=rs("Subdate")
		siteURL=rs("siteURL")
		mypath=replace(rs("FilePath"),"/default.html","")
		
		if siteURL = "javascript:" then siteURL = ""
		
		postID=createBase64ID(replace(replace(url,MY_sitePath,""),MY_createFolder_work,""))
	
	
		SmallOrginUrl=replace(replace(rs("preURL"),"../",""),""&MY_sec_uploadImgPath&"/?src=","")
		if FileExistsStatus(""&MY_sitePath&""&MY_sec_uploadSmallImgPath&"/?src=s_"&replace(SmallOrginUrl,MY_sitePath,"")&"") = -1 then
		imgSmallCreate(""&MY_sitePath&""&MY_uploadImgPath&"/"&SmallOrginUrl)
		end if
			
		'列表预览图外链判断
		if instr(rs("preURL"),"http://")>0 then
		   sSmallPath=rs("preURL")
		else
		   sSmallPath=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")
		end if
	
			
		'分类获取
		sql_class="select * from [SuccessWorkClass] where ClassName='"&ClassName&"'"
		set rs_class=server.createObject("ADODB.Recordset")
		rs_class.open sql_class,db,1,1
		ClassID=rs_class("SuccessWorkClassID")
		ClassSrc="<a class=""sort"" href="""&MY_sitePath&"classlist/"&MY_createFolder_work&""&ClassID&"_default.html""  title="""&classname&""">"&classname&"</a>"&vbcrlf'类别链接
		rs_class.close
		set rs_class=nothing
		
		'分类ID
		listClassIDShow = "class-id-"&ClassID&""

		
	
	  '日期
	  t_year = year(Subdate)
	  t_month = month(Subdate)
	  t_day = day(Subdate)
	  
	
	
	  listcontent_comm_case = listcontent_comm_case&loopTempLabelString("loopList:works.list.comm","{#loopVar:works.list.comm.t_year/}",""&t_year&"","{#loopVar:works.list.comm.t_month/}",""&t_month&"","{#loopVar:works.list.comm.t_day/}",""&t_day&"","{#loopVar:works.list.comm.sSmallPath/}",""&sSmallPath&"","{#loopVar:works.list.comm.Title/}",""&Title&"","{#loopVar:works.list.comm.url/}",""&url&"","{#loopVar:works.list.comm.classname/}",""&classname&"","{#loopVar:works.list.comm.Subdate/}",""&Subdate&"","{#loopVar:works.list.comm.content/}",""&content&"","{#loopVar:works.list.comm.mypath/}",""&mypath&"","{#loopVar:works.list.comm.Title_show/}",""&Title_show&"","{#loopVar:works.list.comm.siteURL/}",""&siteURL&"","{#loopVar:works.list.comm.classname.link/}",""&ClassSrc&"","{#loopVar:postID/}",""&postID&"","{#loopVar:works.classID/}",""&listClassIDShow&"","","","","","","","","","","",0)
	
	
			rs.movenext
		 loop 
		 
	end if
	rs.close   
	set rs=nothing 

end if		


	
'------------------置顶推荐文章(通用)
if callCommInfoChk("${listcontent_comm_topArticle}")  = true or callCommInfoChk("{#loopListContentShow:article.top.comm/}")  = true then
	
	titleLimitLen_comm_topArticle = getTempLablePara_siteConfig("标题字符数截取(置顶推荐文章-通用)")
	titleLimitLen_comm_topArticle_ListNum = getTempLablePara_siteConfig("内容输出列表数量(置顶推荐文章-通用)")
	
	
	listcontent_comm_topArticle = ""
	sql="select  top "&titleLimitLen_comm_topArticle_ListNum&"  * from [article] where top=1 order by hits desc, Subdate desc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
		listcontent_comm_topArticle=""&DY_LANG_51&""
	else
		
	
		
		do while not rs.eof
		
		
		ID=rs("Articleid")
		Subdate=rs("Subdate")
		classname=rs("ClassName")
		Title=replace(rs("ArticleTitle"),chr(32),"")
		Title_show=gotTopic(Title,titleLimitLen_comm_topArticle)
		hits=rs("hits")
		files=replace(rs("files"),"/","")
		mainImg=rs("mainImg")
		renum=rs("renum")
		Summary=replace(replace(HTMLDecode(rs("Summary")),"<p>",""),"</p>","<br />")
		mypath=replace(rs("FilePath"),"/default.html","")
		SubdateShow=year(Subdate)&"-"&month(Subdate)&"-"&day(Subdate)
		SubdateShow_y=year(Subdate)
		SubdateShow_m=month(Subdate)
		SubdateShow_d=day(Subdate)
	
		postID=createBase64ID(replace(replace(mypath,MY_sitePath,""),MY_createFolder_art,""))
		
		if instr(mainImg,"security")>0 or instr(mainImg,"http://")>0 then
			useI_begin = ""
			useI_end = ""
			
		else
		
			useI_begin = "<!--"
			useI_end = "-->"
			
		end if
		
		'主题图片
		
		SmallOrginUrl=replace(replace(mainImg,"../",""),""&MY_sec_uploadImgPath&"/?src=","")

		'外链判断			
		if instr(mainImg,"http://")>0 then
		   sSmallPath=mainImg
		   
		else
		   sSmallPath=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")		
		end if
		
		'分类获取
		sql_class="select * from [ArticleClass] where ClassName='"&ClassName&"'"
		set rs_class=server.createObject("ADODB.Recordset")
		rs_class.open sql_class,db,1,1
		ClassID=rs_class("ArticleClassID")
		ClassSrc="<a class=""sort"" href="""&MY_sitePath&"classlist/"&MY_createFolder_art&""&ClassID&"_default.html""  title="""&classname&""">"&classname&"</a>"&vbcrlf'类别链接
		rs_class.close
		set rs_class=nothing
		
		'分类ID
		listClassIDShow = "class-id-"&ClassID&""

		
		'取出被顶踩信息
		  set rs_Dig=server.createobject("adodb.recordset")
		  sql_Dig="Select * From [vote] Where titleID="&ID&""
		  rs_Dig.open sql_Dig,db,1,1
		  if rs_Dig.eof then
		  my_good="0"
		  my_bad="0" 
		  else 
		  my_good=rs_Dig("art_good")
		  my_bad=rs_Dig("art_bad")
		  end if
			  
		  rs_Dig.Close
		  Set rs_Dig = Nothing
		
		  
			
			listcontent_comm_topArticle= listcontent_comm_topArticle&loopTempLabelString("loopList:article.top.comm","{#loopVar:article.top.comm.mypath/}",""&mypath&"","{#loopVar:article.top.comm.Title/}",""&Title&"","{#loopVar:article.top.comm.Title_show/}",""&Title_show&"","","","","","{#loopVar:article.top.comm.sSmallPath/}",""&sSmallPath&"","{#loopVar:postID/}",""&postID&"","{#loopVar:article.top.comm.Summary/}",""&Summary&"","{#loopVar:article.top.comm.hits/}",""&hits&"","{#loopVar:article.top.comm.renum/}",""&renum&"","<!--{#imgUse:begin/}-->",""&useI_begin&"","<!--{#imgUse:end/}-->",""&useI_end&"","{#loopVar:article.top.comm.Subdate/}",""&Subdate&"","","","{#loopVar:article.top.comm.classname.link/}",""&ClassSrc&"","{#loopVar:article.top.comm.classname/}",""&classname&"","","","{#loopVar:article.top.comm.t_year/}",""&SubdateShow_y&"","{#loopVar:article.top.comm.t_month/}",""&SubdateShow_m&"","{#loopVar:article.top.comm.t_day/}",""&SubdateShow_d&"",0) 


	
			rs.movenext
		 loop 
		 
	end if
	rs.close   
	set rs=nothing 

end if


	
'------------------推荐案例-按类别来调用(通用)	
if callCommInfoChk("${listcontent_comm_newcase")  = true then
	
	titleLimitLen_comm_classWorks = getTempLablePara_siteConfig("标题字符数截取(对应类别调用作品-通用)")
	titleLimitLen_comm_classWorks_ListNum = getTempLablePara_siteConfig("内容输出列表数量(对应类别调用作品-通用)")
	
	
	sql2="select * from [SuccessWorkClass]"
	set rs2=server.createObject("ADODB.Recordset")
	rs2.open sql2,db,1,1
	
	if rs2.EOF or rs2.BOF then
		
		listcontent_comm_classWorks = ""&DY_LANG_30&""
	
	else
	
	
		do until rs2.eof
		
	
			classname_go_comm=cstr(rs2("ClassName"))
			listcontent_comm_classWorks = ""
			
			sql="select  top "&titleLimitLen_comm_classWorks_ListNum&"  * from [SuccessWork]  where ClassName='"& classname_go_comm &"' order by Subdate desc"
			set rs=server.createobject("adodb.recordset")
			rs.open sql,db,1,1
			if rs.bof then
				listcontent_comm_classWorks=""&DY_LANG_51&""
				Template_Code=replace(Template_Code,"${listcontent_comm_newcase["&classname_go_comm&"]}",listcontent_comm_classWorks)
			else
				
				
				listcontent_comm_classWorks = ""
				listcontent_comm_classWorks = listcontent_comm_classWorks&loopTempLabelString("loopList:sidebarList.alone_begin","{#loopVar:sidebarList_sideAloneComm.alone_begin.ul.style/}","","{#loopVar:sidebarList_sideAloneComm.alone_begin.DY_LANG.comm/}",""&DY_LANG_1&"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
		

				
				do while not rs.eof
				
						
				ID=rs("ID")
				Title=replace(rs("title"),chr(32),"")
				Title_show=gotTopic(Title,titleLimitLen_comm_classWorks)
				url=rs("url")
				ClassName=rs("ClassName")
				content=replace(replace(HTMLDecode(rs("worksIntro")),"<p>",""),"</p>","<br />")
				Image=replace(rs("preURL"),"../","")
				Subdate=rs("Subdate")
				siteURL=rs("siteURL")
				mypath=replace(rs("FilePath"),"/default.html","")
				
				if siteURL = "javascript:" then siteURL = ""
				
				postID=createBase64ID(replace(replace(url,MY_sitePath,""),MY_createFolder_work,""))
			
			
			
				SmallOrginUrl=replace(replace(rs("preURL"),"../",""),""&MY_sec_uploadImgPath&"/?src=","")
				if FileExistsStatus(""&MY_sitePath&""&MY_sec_uploadSmallImgPath&"/?src=s_"&replace(SmallOrginUrl,MY_sitePath,"")&"") = -1 then
				imgSmallCreate(""&MY_sitePath&""&MY_uploadImgPath&"/"&SmallOrginUrl)
				end if
				
				'列表预览图外链判断
				if instr(rs("preURL"),"http://")>0 then
				   sSmallPath=rs("preURL")
				else
				   sSmallPath=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")
				end if
				
				'分类获取
				sql_class="select * from [SuccessWorkClass] where ClassName='"&ClassName&"'"
				set rs_class=server.createObject("ADODB.Recordset")
				rs_class.open sql_class,db,1,1
				ClassID=rs_class("SuccessWorkClassID")
				ClassSrc="<a class=""sort"" href="""&MY_sitePath&"classlist/"&MY_createFolder_work&""&ClassID&"_default.html""  title="""&classname&""">"&classname&"</a>"&vbcrlf'类别链接
				rs_class.close
				set rs_class=nothing
				
				'分类ID
				listClassIDShow = "class-id-"&ClassID&""

				
				
				  '日期
					SubdateShow=year(Subdate)&"-"&month(Subdate)&"-"&day(Subdate)
					SubdateShow_y=year(Subdate)
					SubdateShow_m=month(Subdate)
					SubdateShow_d=day(Subdate)
				  
				  
					 listcontent_comm_classWorks = listcontent_comm_classWorks&loopTempLabelString("loopList:sidebarList.alone_content","{#loopVar:sidebarList_sideAloneComm.alone_content.mypath.comm/}",""&mypath&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title.comm/}",""&Title&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Title_show.comm/}",""&Title_show&"","{#loopVar:sidebarList_sideAloneComm.alone_content.li.a.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.li.class}","","{#loopVar:sidebarList_sideAloneComm.alone_content.sSmallPath.comm/}",""&sSmallPath&"","{#loopVar:postID/}",""&postID&"","{#loopVar:sidebarList_sideAloneComm.alone_content.Summary.comm/}",""&content&"","{#loopVar:sidebarList_sideAloneComm.alone_content.hits.comm/}",""&hits&"","{#loopVar:sidebarList_sideAloneComm.alone_content.renum.comm/}",""&renum&"","{#loopVar:sidebarList_sideAloneComm.alone_content.siteURL.comm/}",""&siteURL&"","{#loopVar:sidebarList_sideAloneComm_content.classID.comm/}",""&listClassIDShow&"","","","","","{#loopVar:sidebarList_sideAloneComm.alone_content.classname.link.comm/}",""&ClassSrc&"","{#loopVar:sidebarList_sideAloneComm.alone_content.classname.comm/}",""&classname&"","","","{#loopVar:sidebarList_sideAloneComm_content.y_date.comm/}",""&SubdateShow_y&"","{#loopVar:sidebarList_sideAloneComm_content.m_date.comm/}",""&SubdateShow_m&"","{#loopVar:sidebarList_sideAloneComm_content.d_date.comm/}",""&SubdateShow_d&"",0) 
					 
		
		
					rs.movenext
				 loop 
				 
			   listcontent_comm_classWorks = listcontent_comm_classWorks&loopTempLabelString("loopList:sidebarList.alone_end","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
			   listcontent_comm_classWorks = listcontent_comm_classWorks&loopTempLabelString("loopList:sidebarList.alone_more","{#loopVar:sidebarList_sideAloneComm_more.MY_sitePath.comm/}",""&MY_sitePath&"","{#loopVar:sidebarList_sideAloneComm.alone_more.MY_createFolder.comm/}",""&MY_sitePath&"list/"&MY_createFolder_work&"_default.html","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
					
				 
				 Template_Code=replace(Template_Code,"${listcontent_comm_newcase["&classname_go_comm&"]}",listcontent_comm_classWorks)
				 
			end if
			rs.close   
			set rs=nothing 
		
		
		rs2.movenext
		loop
		rs2.close   
		set rs2=nothing 	
	
	end if

end if


'------------------文章分类-按频道来调用(通用)
if callCommInfoChk("${listcontent_comm_ArticleClassList")  = true then
	
	sql2="select * from [channelList]"
	set rs2=server.createObject("ADODB.Recordset")
	rs2.open sql2,db,1,1
	
	if rs2.EOF or rs2.BOF then
		
		listcontent_comm_ArticleClassList = ""&DY_LANG_30&""
		Template_Code=replace(Template_Code,"${listcontent_comm_ArticleClassList["&channelname_go_comm&"]}",listcontent_comm_ArticleClassList)
	
	else
	
	
		do until rs2.eof
		
	
			channelname_go_comm=cstr(rs2("channelName"))
			listcontent_comm_ArticleClassList = ""
			
			sql="select * from [ArticleClass]  where channel='"& channelname_go_comm &"' order by Index asc"
			set rs=server.createobject("adodb.recordset")
			rs.open sql,db,1,1
			if rs.bof then
				listcontent_comm_ArticleClassList=""&DY_LANG_51&""
			else
				
				listcontent_comm_ArticleClassList= listcontent_comm_ArticleClassList&loopTempLabelString("loopList:sidebarList_begin","{#loopVar:sidebarList_begin.ul.style/}","","{#loopVar:sidebarList_begin.DY_LANG.comm/}",""&channelname_go_comm&"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
				
				do while not rs.eof
				
				
					ID=rs("ArticleClassID")	
					ClassName=replace(rs("ClassName"),chr(32),"")
					ClassName_show=ClassName
					ClassIntro=rs("ClassIntro")
					
					
					rssLink = "<a class=""rssLink"" href="""&MY_sitePath&"rss/?classname="&server.URLEncode(ClassName)&""" target=""_blank""><img src="""&MY_sitePath&"images/Blog_Rss.png""  alt=""""/></a>"
					
					toIDT = createBase64ID(ClassName_show)
					
					
							 listcontent_comm_ArticleClassList= listcontent_comm_ArticleClassList&loopTempLabelString("loopList:sidebarList_content","{#loopVar:sidebarList_content.mypath.comm/}",""&MY_sitePath&"classlist/"&MY_createFolder_art&""&ID&"_default.html","{#loopVar:sidebarList_content.Title.comm/}",""&ClassName&"","{#loopVar:sidebarList_content.Title_show.comm/}",""&ClassName_show&"","{#loopVar:sidebarList_content.li.a.class}","","{#loopVar:sidebarList_content.li.class}","","{#loopVar:sidebarList_content.toID(Title).comm/}",""&toIDT&"","","","","","","","","","","","","","","","","","","","","","{#loopVar:post.rssLink/}",""&rssLink&"","{#loopVar:sidebarList_content.y_date.comm/}",""&SubdateShow_y&"","{#loopVar:sidebarList_content.m_date.comm/}",""&SubdateShow_m&"","{#loopVar:sidebarList_content.d_date.comm/}",""&SubdateShow_d&"",0) 
							 

		
					rs.movenext
				 loop 
				 
				 
				   listcontent_comm_ArticleClassList= listcontent_comm_ArticleClassList&loopTempLabelString("loopList:sidebarList_end","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
				 
				 
				 Template_Code=replace(Template_Code,"${listcontent_comm_ArticleClassList["&channelname_go_comm&"]}",listcontent_comm_ArticleClassList)
				 
			end if
			rs.close   
			set rs=nothing 
		
		
		rs2.movenext
		loop
		rs2.close   
		set rs2=nothing 	
	
	end if	

end if




'------------------------关键词输出(通用)
if callCommInfoChk("${listcontent_comm_tags}")  = true or callCommInfoChk("{#loopListContentShow:article.tags.random.comm/}")  = true then
	
	listcontent_comm_tags = ""
	randomize
	sql="select top "&MY_newTagsCloudNum_side&"  * from [article]  order by Rnd(ArticleID-timer()) desc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
		listcontent_comm_tags=""&DY_LANG_51&""
	else
	
		do while not rs.eof
		
			ID=rs("Articleid")
			Title=rs("ArticleTitle")
			classname=rs("ClassName")
			KeyWord=replace(rs("KeyWord"),chr(32),"")
			hits=rs("hits")
			renum=rs("renum")
			
			'==========================
			'分割关键词         
			keywords=split(KeyWord,",") '将输入的字符串根据空格分开，获得一个数组
			max=ubound(keywords) '得出这个数组的维数，即输入的关键字个数
			for k=0 to max 
			KeyWord_show=keywords(k)
			'==========================	
			
			
			if KeyWord_show <> "" then listcontent_comm_tags= listcontent_comm_tags&loopTempLabelString("loopList:article.tags.random.comm","{#loopVar:article.tags.random.comm.MY_sitePath/}",""&MY_sitePath&"","{#loopVar:article.tags.random.comm.MY_createFolder_art/}",""&MY_createFolder_art&"","{#loopVar:article.tags.random.comm.toUTF8(KeyWord_show)/}",""&server.URLEncode(KeyWord_show)&"","{#loopVar:article.tags.random.comm.KeyWord_show/}",""&KeyWord_show&"","{#loopVar:article.tags.random.comm.FontRandom_Weight/}",""&FontRandom_Weight(10)&"","{#loopVar:article.tags.random.comm.FontRandom_Size/}",""&FontRandom_Size(10)&"","","","","","","","","","","","","","","","","","","","","","","","","","","","","",1)
	
				
				
			next
			rs.movenext
		 loop 
	
	 
	end if
	rs.close   
	set rs=nothing 

end if




%>