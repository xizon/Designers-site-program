<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%


'-------------------网站信息(通用)
Set rs=server.CreateObject("adodb.recordset")
strsql="select * from [website]"
rs.open strsql,db,1,1
sitename=rs("siteName")
blogtitle=rs("blogtitle")
myimg=rs("myimg")
realname=rs("realname")
netname=rs("netname")
introduce=rs("introduce")
tel=rs("tel")
qq=rs("qq")
email=rs("email")
msn=rs("msn")
age=rs("age")
copyright=rs("copyright")
detail_motto=rs("detail_motto")
detail_work=rs("detail_work")
detail_conn=rs("detail_conn")
rs.close
set rs=nothing


Template_Code=replace(Template_Code,"${siteName}",sitename)
Template_Code=replace(Template_Code,"${siteNameComm}",sitename)
Template_Code=replace(Template_Code,"${blogtitle}",blogtitle)
Template_Code=replace(Template_Code,"${myimg}",myimg)
Template_Code=replace(Template_Code,"${realname}",realname)
Template_Code=replace(Template_Code,"${netname}",netname)
Template_Code=replace(Template_Code,"${age}",age)
Template_Code=replace(Template_Code,"${msn}",msn)
Template_Code=replace(Template_Code,"${email}",email)
Template_Code=replace(Template_Code,"${qq}",qq)
Template_Code=replace(Template_Code,"${tel}",tel)
Template_Code=replace(Template_Code,"${copyright}",copyright)
Template_Code=replace(Template_Code,"${introduce}",introduce)
Template_Code=replace(Template_Code,"${advertisement}",adv)	
Template_Code=replace(Template_Code,"${detail_motto}",detail_motto)
Template_Code=replace(Template_Code,"${siteSubName}",detail_work)
Template_Code=replace(Template_Code,"${detail_conn}",detail_conn)				

%>
