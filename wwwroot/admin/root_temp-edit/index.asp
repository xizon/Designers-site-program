<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<!--#include file="../tempConfig.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<script>

$(function(){

	<%
	if cint(MY_betweenPageEnd) > cint(getDataPageTotal()) then MY_betweenPageEnd = getDataPageTotal()
	
	%>
	$("#slider-page").slider({ 
	    values: [ <%=MY_betweenPageBegin%>,  <%=MY_betweenPageEnd%> ],//初始值
		step: 1, 
		max: <%=getDataPageTotal()%>,
		min: 1,
		range: true,
		slide: function(event, ui){ 
			$("#beginNum").val(ui.values[0]); 
			$("#endNum").val(ui.values[1]); 
			$(".txtShow").html(ui.values[0]);
			$(".txtShow2").html(ui.values[1]);
		} 
	
	}); 

	

});


<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=栏目页面静态生成", "link1=<%=MY_sitePath%>admin/root_temp-edit/index.asp", "title2=文章内容页批量生成", "link2=<%=MY_sitePath%>admin/article/edit_batch_temp.asp","title3=案例/产品内容页批量生成", "link3=<%=MY_sitePath%>admin/SuccessWork/edit_batch_temp.asp");

	   }
	);
	
	$(".createAT").click(
	   function() {
			ChkFormTip_page('正在生成,请稍候...',2);

	   }
	);	
	
	
	
	
});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="javascript:" class="link" onclick="ManageOpenWin('html_all.asp');"  title="header=[] body=[<br>首页、搜索页、友情链接、关于我们、文章列表、案例列表、自定义页面]">生成全部页面</a>
                
                <!-- 当前位置 -->
                当前位置： 手动静态生成 | 栏目页面静态生成
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
                <!-- 后台提示 -->
                
                <div class="notification information png_bg content-alert">
                    <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        1.<strong class="guideTitle">通常情况下您不需要随时手动生成页面</strong>,[<strong>发布信息、到达自动生成时间(默认3天)并访问页面</strong>] 可以实现系统同步生成<br>
                        2.<strong>以下一些操作您需要手动生成页面才能及时看到效果</strong> (PS:某些浏览器存在缓存，需要重启浏览器才能看到效果)<br>
                        &nbsp;a)修改模板文件的HTML代码 <br>
                        &nbsp;b)编辑自定义标签内容 <br>
                        &nbsp;c)编辑模板文字包 <br>
                        &nbsp;d)云皮肤更换 <br>
                        &nbsp;e)修改导航菜单,Banner幻灯片 <br>
                        &nbsp;f)信息的删除、置顶操作 <br>
                        3.页面生成默认只生成相关栏目最新的第一页，信息较多并更换模板文件时,您需要进行<a href="javascript:void(0)" onclick="ManageOpenWin('html_all.asp');"  title="header=[] body=[首页、搜索页、友情链接、关于我们、文章列表、案例列表、自定义页面]">批量生成</a>,才能及时看到效果,<strong>开启多分页生成状态</strong>后才可以批量生成多个分页<br>
                        
                        <%if MY_batchPageCreateHtmlStatus = 0 then%> 
                        
                        <a class="bS-btn" href="../website/_batchCreateHtmlOpen.asp?action=open" target="htmlControl" id="batchBtn" onClick="$('#bS').html('开启');$('.bS-btn').fadeOut(500);"><span class="io">开启</span>多分页批量生成状态</a>
						
						<%else%>
                        
                         <a  class="bS-btn"  href="../website/_batchCreateHtmlOpen.asp?action=close" target="htmlControl" id="batchBtn" onClick="$('#bS').html('关闭');$('.bS-btn').fadeOut(500);"><span class="io">关闭</span>多分页批量生成状态</a>
                        
                        <%end if%>
                        
                        当前状态：<span id="bS" class="backStage_star"><%if MY_batchPageCreateHtmlStatus = 0 then response.Write "关闭" else response.Write "开启"%></span>
                        <span data-open="0" id="batchBtnStatus"></span>

                        
                        <br>
    
                    </div>
                </div>
          
                <iframe name="htmlControl" style="display:none"></iframe>
                <iframe style="display:none" src="ClearCache.asp"></iframe>


                <!-- 表单域  -->
                <div id="contentShow">

                        <table width="100%" class="setting" cellspacing="0">
                            
                            <!-- 标题 -->                        
                            <thead class="setHead">
                                <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2"></th>
                                </tr>
                                
                            </thead>
                            
                            
                            
                            
                            <!-- +++++++  内容   begin +++++++  -->
                            
                            <tbody>
    
    
                                <tr>
                                    <td class="title">多分页批量生成范围<a href="javascript:void(0)" class="help" data-info="信息量较大造成分页过多时，您需要<strong>分段进行</strong>批量生成，减轻服务器压力<br>PS：范围尽量控制在<strong>50</strong>页以内,<strong>小号数字表示根据发布时间排序,发布最靠后的信息列表分页</strong>,数字并不代表页码"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                   <td width="203" class="ui-element">
                                   <form method="post" action="../website/_batchCreateHtmlBetweenPage.asp?action=update" target="actionPage" onSubmit="javascript:return ChkFormTip_page('保存成功',1);" title="header=[] body=[1) 根据发布时间排序-发布最靠后的信息列表<br>2) 信息量较大造成分页过多时，您需要分段进行批量生成，减轻服务器压力<br>3) 默认不开启批量生成状态，只生成最新的一页列表<br>4) 重新进入后台则自动还原]">
                                        <input type="hidden" id="beginNum" name="beginNum" value="<%=MY_betweenPageBegin%>" />  
                                        <input type="hidden" id="endNum" name="endNum" value="<%=MY_betweenPageEnd%>" />  
                                        最新的<span class="txtShow"><%=MY_betweenPageBegin%></span>-<span class="txtShow txtShow2"><%=MY_betweenPageEnd%></span>页
                                        <br clear="all">
                                        <input type="submit" value="保存设置" class="ui-btn">
                       
                                    </form>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-page"></div></td>
                                </tr>
                                
                                    
                                <thead class="setHead">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">常用页面生成</th>
                                    </tr> 
                                </thead>   
                                
                                
                                
                                <tr>
                                    <td colspan="3">
                                       
                                        <ul class="columnBtn mainBtn-big">
                                            <li><a href="home_show.asp" class="createAT" target="htmlControl">网站首页</a></li>
                                            <li><a href="search_show.asp" class="createAT" target="htmlControl">搜索页面</a></li>
                                            <li><a href="searchresult_show.asp" class="createAT" target="htmlControl">搜索结果显示页</a></li>
                                            <li><a href="links_show.asp" class="createAT" target="htmlControl" >友情链接</a></li>
                                            <li><a href="selfinfo_show.asp" class="createAT" target="htmlControl">关于我们</a></li>
                                        </ul>
                                       
                                    </td>
                                </tr>
                                
                                
                                <thead class="setHead">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">列表页面生成</th>
                                    </tr> 
                                </thead>   
                                
                                
                                
                                <tr>
                                    <td colspan="3">
                                       
                                        <ul class="columnBtn mainBtn-big">
                                            <li><a href="Article_show.asp" class="createAT" target="htmlControl">文章列表页</a></li>
                                            <li><a href="Articleclass_Iframe_action.asp?ID=0" class="createAT" target="htmlControl">分类文章列表页</a></li>
                                            <li><a href="SuccessWork_show.asp" class="createAT" target="htmlControl">案例产品列表页</a></li>
                                            <li><a href="SuccessWorkClass_Iframe_action.asp?ID=0" class="createAT" target="htmlControl">分类案例产品列表页</a></li>
                                            <li><a href="message_show.asp" class="createAT" target="htmlControl">留言列表页</a></li>
                                        </ul>
                                       
                                    </td>
                                </tr>   
                                
                                <thead class="setHead">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">内容页批量生成</th>
                                    </tr> 
                                </thead>   
                                
                                
                                
                                <tr>
                                    <td colspan="3">
                                       
                                        <ul class="columnBtn mainBtn-big">
                                            <li><a href="<%=MY_sitePath%>admin/article/edit_batch_temp.asp">文章内容页批量生成</a></li>
                                            <li><a href="<%=MY_sitePath%>admin/SuccessWork/edit_batch_temp.asp">案例产品内容页批量生成</a></li>
                                        </ul>
                                       
                                    </td>
                                </tr>   
                                

                                <thead class="setHead">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">自定义页面生成</th>
                                    </tr> 
                                </thead>   
                                
                                
                                
                                <tr>
                                    <td colspan="3">
                                       
                                        <ul class="columnBtn mainBtn-big">
                                            <li><a href="javascript:"  class="help" data-info="后台 [<strong>自定义标签/页面</strong>] 功能创建的图文单页" onclick="ManageOpenWin('newTemplate_show.asp');">后台创建的独立页面</a></li>
                                            <li><a href="javascript:" class="help" data-info="template/模板文件中使用依赖标签<br> <strong>{#body:columnShow.CustomPage[?]}...<br>{/body:columnShow.CustomPage[?]}</strong> 创建的页面" onclick="ManageOpenWin('newCustomPage_show.asp?total=10&startNum=1');">模板创建的独立页面</a></li>
                                            <li><a href="javascript:" class="help" data-info="template/模板文件中使用依赖标签<br> <strong>{#body:columnShow.CustomPage[classList?]}...<br>{/body:columnShow.CustomPage[classList?]}</strong> 创建的页面<br>【根据不同的类别输出列表，具体用法参看API文档】" onclick="ManageOpenWin('newCustomPage_classListArt_show.asp?total=10&startNum=1');">模板创建的独立[分类文章]列表页</a></li>
                                            <li><a href="javascript:" class="help" data-info="template/模板文件中使用依赖标签<br> <strong>{#body:columnShow.CustomPage[classListWorks?]}...<br>{/body:columnShow.CustomPage[classListWorks?]}</strong> 创建的页面<br>【根据不同的类别输出列表，具体用法参看API文档】" onclick="ManageOpenWin('newCustomPage_classListWorks_show.asp?total=10&startNum=1');">模板创建的独立[分类案例产品]列表页面</a></li>
                                        </ul>
                                       
                                    </td>
                                </tr>                           
                                
                                  
                               <!-- +++++++  内容   end +++++++  -->
                        
                        
                            </tbody>
                            
                        </table>
 
                                
                </div>
     
                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


