<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="judge_promptWin.asp"-->
<!--#include file="../function/complexFont_convert.asp" -->
<!--#include file="_siteInfo.asp"-->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%	


titleLimitLen_home_Art = getTempLablePara_siteConfig("标题字符数截取(首页文章列表)")
titleLimitLen_home_notice = getTempLablePara_siteConfig("标题字符数截取(通知公告)")
dim tag(10)

'缓存内容页面
createFolder("../_contentCache/")
thisFilePath = "../_contentCache/list_home.txt"

if FileExistsStatus(thisFilePath) = 1 then 


response.Write DY_LANG_103


else

HTMLSERVE_HEAD()


'////获取并判断动态模板 begin
thisTempCodePath=""&MY_sitePath&"admin/_temp/home_tempA_"&tempModifiedDate&".html"
if FileExistsStatus(thisTempCodePath) = -1 then '不存在
'开始执行依赖标签功能
'==================

		
		Template_Code=showFile(DesignerSite_tempPath_common)
		'=============================模板依赖标签不存在判断   begin
		
		'模板主注释，参数不用变更，默认全部清除
		Template_Code_trip = dependentLabelFun(Template_Code,"note:template",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"note:commCreateInfo",0,"","","")
		
		
		'【必选其一】各栏目标题依赖标签,必选其一设置为参数 1
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:commonUse.title",1,"","","") '通用标题
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:content.article",0,"","","")	 '内容页-文章 
		  
		'列表页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.article",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.works",0,"","","")
		
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.about",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.mood",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.search",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.links",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.message",0,"","","")
		
		
		'【必选其一】各栏目meta描述依赖标签,必选其一设置为参数 1 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:comm.desc",1,"","","") '通用网站描述
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:content.articleDesc",0,"","","")'内容页-文章 
		

		
		'【必选其一】各栏目meta关键词依赖标签,必选其一设置为参数 1 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:commonUse.keywords",0,"","","")'其他通用页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:home",1,"","","")'首页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:content",0,"","","")'内容页关键词
		
		
		'RSS订阅，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"rss:commonUse.article",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"rss:commonUse.article.class",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"atom:commonUse.article",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"atom:commonUse.article.class",0,"","","")
		
		'banner轮换，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.banner",1,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"head:commonUse.banner",1,"","","")
		
		'搜索框，订阅图标，置顶推荐文章，最新案例作品，规定数量随机tags输出，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.searchBoard",1,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.articleFeed",1,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.article.top",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.works.list",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.article.tags.random",0,"","","")
		
		'内容页SEO可选link
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"head:content.linkRel",0,"","","")
		
		
		
		'侧边栏
		
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"sideBar:article,search,about,message",0,"","","")'列表页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"sideBar:content.comm",0,"","","") '内容页
		Template_Code_trip = getTempLablePara_siderBar_home("首页侧边栏",1,"body:columnShow.home")
		Template_Code_trip = dependentLabelFun_para(Template_Code_trip,"首页侧边栏",0,"body:columnShow.home")
		
		
		'各栏目的列表页面
		
		'文章
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.article",0,"","","//") '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article.class",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article.tags",0,"","","")  
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.article",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.article.class",0,"/*","*/","")
		
		
		'作品案例
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.worksIe6Js",0,"","","") '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.works",0,"","","//")   '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.works",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.works.class",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.works",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.works.class",0,"/*","*/","")
		
		
		
		'微博
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.mood",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:content.js.mood",0,"/*","*/","")
		

		'关于我们
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.about",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.about",0,"/*","*/","")
		
		
		'搜索
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.search",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.searchResult",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.search",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.searchResult",0,"/*","*/","")
		
		'友情链接
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.links",0,"","","")	  
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.links",0,"/*","*/","")
		
		'留言
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.message",0,"","","//")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.message",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.message",0,"/*","*/","")
		
		'首页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.home",1,"","","//")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.home",1,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.home",1,"/*","*/","")
		
		
		'内容页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.js.comm.form",0,"","","")	 '栏目必选  	
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.show.commJs",0,"","","")'栏目必选   	
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.show.article",0,"","","")	 						 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:content.js.article",0,"/*","*/","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:content.js.comm",0,"","","//")		 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:content.article",0,"","","//")
		
		
		%>
		<!--#include file="_clearLabel_comm.asp" -->
		<%
  
  

		'==================
		Template_Code_Now = Template_Code_trip		
		CreateFile thisTempCodePath,Template_Code_Now
		
 
end if
'=============================模板依赖标签不存在判断   end

'这里获取依赖标签清除后模板的值
Template_Code=showFile(thisTempCodePath)
'只调用栏目独立HTML代码判断
callPageAllHTML "body:columnShow.home","完全独立页面代码.不调用其它任何依赖标签的内容"

	
%>
<!--#include file="sideColumn.asp" -->
<%

randomize


'------------------首页友情链接	
if callCommInfoChk("${homeLinks}") = true or callCommInfoChk("${homeLinks_txt}") = true then	

	sql="select top "&MY_NumLinksSide&"  * from [Link] where view=1 and homeShow=1 order by Index asc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
		listcontent_links=""&DY_LANG_102&""
		listcontent_links_txt=""&DY_LANG_102&""
	else
	    
		listcontent_links=""
		listcontent_links_txt=""
		
		do while not rs.eof
		
		
		LinkTitle=replace(rs("LinkTitle"),chr(32),"")
		LinkTitle_show=LinkTitle
		ClassName=rs("ClassName")
		URL=rs("URL")
		o_logo=rs("logo")
		urlOrigin=replace(replace(replace(o_logo,"../",""),""&MY_sec_uploadImgPath&"/?src=",""),MY_sitePath,"")
		logo=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&urlOrigin&""
		
		if instr(o_logo,"http://")>0 then
		   logo=o_logo
		else
		   urlOrigin=replace(replace(replace(o_logo,"../",""),""&MY_sec_uploadImgPath&"/?src=",""),MY_sitePath,"")
		   logo=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&urlOrigin&""
		end if
		
	

				
			'首页限定数量友情链接输出标签
			
			if o_logo="" then '没有LOGO时
			
				listcontent_links= listcontent_links& "<a href="""&URL&""" target=""_blank"" title="""&LinkTitle&""">"&LinkTitle_show&"</a>"&vbcrlf	
			else  '存在LOGO时
			
				listcontent_links= listcontent_links& "<a href="""&URL&""" target=""_blank""  title="""&LinkTitle&"""><img src="""&logo&""" alt="""&LinkTitle&""" /></a>"&vbcrlf		
			
			end if
			
			'纯文字链接
			listcontent_links_txt = listcontent_links_txt& "<a href="""&URL&""" target=""_blank"" title="""&LinkTitle&""">"&LinkTitle_show&"</a>"&vbcrlf	
			

			rs.movenext
		 loop 
		 
	end if
	rs.close   
	set rs=nothing 
	
end if	
	
	
	
'------------------文章

if callCommInfoChk("{#loopListContentShow:article.top/}") = true then
'置顶
	titleLimitLen_art_top_ListNum = getTempLablePara_siteConfig("内容输出列表数量(首页置顶文章)")
	
	tt = 0
	
	sql="select top "&titleLimitLen_art_top_ListNum&"  * from [article] where top=1 order by hits desc, Subdate desc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
		listcontent_art_top=""
	else
		
		listcontent_art_top=""
	
		
		do while not rs.eof
		
	keywords_show = ""
	ID=rs("Articleid")
	Subdate=rs("Subdate")
	classname=rs("ClassName")
	Title=replace(rs("ArticleTitle"),chr(32),"")
	Title_show=gotTopic(Title,titleLimitLen_home_Art)
	hits=rs("hits")
	files=replace(rs("files"),"../","")
	mainImg=rs("mainImg")
	renum=rs("renum")
	Summary=replace(replace(HTMLDecode(rs("Summary")),"<p>",""),"</p>","<br />")
	mypath=replace(rs("FilePath"),"/default.html","")
	SubdateShow=year(Subdate)&"-"&month(Subdate)&"-"&day(Subdate)
	
	if callCommInfoChk("{#loopVar:article.Summary[200]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[200]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),200)
	if callCommInfoChk("{#loopVar:article.Summary[250]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[250]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),250)
	if callCommInfoChk("{#loopVar:article.Summary[300]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[300]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),300)
	if callCommInfoChk("{#loopVar:article.Summary[350]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[350]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),350)
	if callCommInfoChk("{#loopVar:article.Summary[400]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[400]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),400)
	if callCommInfoChk("{#loopVar:article.Summary[450]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[450]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),450)
	if callCommInfoChk("{#loopVar:article.Summary[500]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[500]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),500)
	if callCommInfoChk("{#loopVar:article.Summary[550]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[550]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),550)
	if callCommInfoChk("{#loopVar:article.Summary[600]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[600]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),600)
	if callCommInfoChk("{#loopVar:article.Summary[650]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[650]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),650)
	if callCommInfoChk("{#loopVar:article.Summary[700]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[700]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),700)
	if callCommInfoChk("{#loopVar:article.Summary[750]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[750]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),750)
	if callCommInfoChk("{#loopVar:article.Summary[800]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[800]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),800)
	if callCommInfoChk("{#loopVar:article.Summary[850]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[850]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),850)
	if callCommInfoChk("{#loopVar:article.Summary[900]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[900]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),900)
	if callCommInfoChk("{#loopVar:article.Summary[950]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[950]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),950)
	if callCommInfoChk("{#loopVar:article.Summary[1000]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[1000]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),1000)
	
	postID=createBase64ID(replace(replace(mypath,MY_sitePath,""),MY_createFolder_art,""))
	
	if instr(mainImg,"security")>0 or instr(mainImg,"http://")>0 then
		useI_begin = ""
		useI_end = ""
		usePreImgClass = "use-preImg"
		
	else
		useI_begin = "<!--"
		useI_end = "-->"
		usePreImgClass = "use-no-preImg"
		
	end if
	
	'分类获取
	sql_class="select * from [ArticleClass] where ClassName='"&ClassName&"'"
	set rs_class=server.createObject("ADODB.Recordset")
	rs_class.open sql_class,db,1,1
	ClassID=rs_class("ArticleClassID")
	ClassSrc="<a class=""sort"" href="""&MY_sitePath&"classlist/"&MY_createFolder_art&""&ClassID&"_default.html""  title="""&classname&""">"&classname&"</a>"&vbcrlf'类别链接
	rs_class.close
	set rs_class=nothing
	
	'分类ID
	listClassIDShow = "class-id-"&ClassID&""
	
	
	'取出被顶踩信息
	  set rs_Dig=server.createobject("adodb.recordset")
	  sql_Dig="Select * From [vote] Where titleID="&ID&""
	  rs_Dig.open sql_Dig,db,1,1
	  if rs_Dig.eof then
	  my_good="0"
	  my_bad="0" 
	  else 
	  my_good=rs_Dig("art_good")
	  my_bad=rs_Dig("art_bad")
	  end if
		  
	  rs_Dig.Close
	  Set rs_Dig = Nothing
	  
	'主题图片
	
	SmallOrginUrl=replace(replace(mainImg,"../",""),""&MY_sec_uploadImgPath&"/?src=","")
	

	'外链判断			
	if instr(mainImg,"http://")>0 then
	   sSmallPath=mainImg
	   
	else
	   sSmallPath=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")		
	end if
	
	
	top="<span class=""fontIstop"">"&DY_LANG_52&"</span>"
	
	  '日期
	  t_year = year(Subdate)
	  t_month = dateMonthGoEnglish(month(Subdate))
	  t_day = day(Subdate)
	  
	  '奇偶class + 是否使用主题图片class
	  if (tt+1) mod 2 = 0 then partClass = "post-even "&usePreImgClass&"" else partClass = "post-odd "&usePreImgClass&""	
	  
	   '标签
	   if callCommInfoChk("{#loopVar:article.tags/}") = true then
	   
			keywords_show = ""
			KeyWord=replace(rs("KeyWord"),chr(32),"")
			
			'==========================
			'分割关键词         
			tag=split(KeyWord,",") '将输入的字符串根据空格分开，获得一个数组
			max=ubound(tag) '得出这个数组的维数，即输入的关键字个数
			'==========================	
			for thht = 0 to 10
			
				tagHref = ""&MY_sitePath&"plugins/keywords/?"&MY_createFolder_art&"_tag=" & server.URLEncode(tag(thht)) & ".html"
				tagTitle = tag(thht)
				tagShowLoop = loopTempLabelString("loopList:article.tagsList","{#loopVar:article.tagsList.MY_sitePath/}",""&MY_sitePath&"","{#loopVar:article.tagsList.MY_createFolder_art/}",""&MY_createFolder_art&"","{#loopVar:article.tagsList.toUTF8(title)/}",""&server.URLEncode(tag(thht))&"","{#loopVar:article.tagsList.link/}",""&tagHref&"","{#loopVar:article.tagsList.title/}",""&tagTitle&"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0) 
				if tagTitle = "" then tagShowLoop = ""
				
				keywords_show = keywords_show&tagShowLoop
				
				if thht >= max then exit for
				
			next
		
		end if
        
      listcontent_art_top = listcontent_art_top&loopTempLabelString("loopList:article.home","{#loopVar:article.t_year/}",""&t_year&"","{#loopVar:article.t_month/}",""&t_month&"","{#loopVar:article.t_day/}",""&t_day&"","{#loopVar:article.sSmallPath/}",""&sSmallPath&"","{#loopVar:article.Title/}",""&Title&"","{#loopVar:article.mypath/}",""&mypath&"","{#loopVar:article.classname/}",""&classname&"","{#loopVar:article.Subdate/}",""&Subdate&"","{#loopVar:article.Summary/}",""&Summary&"","{#loopVar:article.hits/}",""&hits&"","{#loopVar:article.renum/}",""&renum&"","{#loopVar:postClassType/}",""&partClass&"","<!--{#imgUse:begin/}-->",""&useI_begin&"","<!--{#imgUse:end/}-->",""&useI_end&"","{#loopVar:article.tags/}",""&keywords_show&"","{#loopVar:article.id/}",""&ID&"","{#loopVar:postID/}",""&postID&"","{#loopVar:article.n_month/}",""&month(Subdate)&"","{#loopVar:article.classname.link/}",""&ClassSrc&"",""&thisSumLabel&"",""&thisSumLabel_rep&"",0)
	  
	  listcontent_art_top = replace(listcontent_art_top,"1.如果想文章未发布主题图片,列表中同时又不想显示,只留下文字效果,就把下面的首尾注释放置到主题图片的html代码首尾即可","")
	  listcontent_art_top = replace(listcontent_art_top,"2.如果未发布图片依然在列表页中显示没有图片的样子，还是图文并茂，那就不要把他们包含在主题图片html代码首尾","")
	  listcontent_art_top = replace(listcontent_art_top,"${realname}",realname)
	  
	  
	
			rs.movenext
			tt=tt+1
		 loop 
		 
	end if
	rs.close   
	set rs=nothing 
	
end if	





'////////
if callCommInfoChk("{#loopListContentShow:article.home/}") = true then
	titleLimitLen_art_home_ListNum = getTempLablePara_siteConfig("内容输出列表数量(首页文章)")
	
	tta = 0
	sql="select top "&titleLimitLen_art_home_ListNum&"  * from [article]  order by Subdate desc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
		listcontent_art=""&DY_LANG_102&""
	else
		listcontent_art=""
	
		
		do while not rs.eof
		
	keywords_show = ""
	ID=rs("Articleid")
	Subdate=rs("Subdate")
	classname=rs("ClassName")
	Title=replace(rs("ArticleTitle"),chr(32),"")
	Title_show=gotTopic(Title,titleLimitLen_home_Art)
	hits=rs("hits")
	files=replace(rs("files"),"../","")
	mainImg=rs("mainImg")
	renum=rs("renum")
	Summary=replace(replace(HTMLDecode(rs("Summary")),"<p>",""),"</p>","<br />")
	mypath=replace(rs("FilePath"),"/default.html","")
	SubdateShow=year(Subdate)&"-"&month(Subdate)&"-"&day(Subdate)
	
	if callCommInfoChk("{#loopVar:article.Summary[200]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[200]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),200)
	if callCommInfoChk("{#loopVar:article.Summary[250]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[250]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),250)
	if callCommInfoChk("{#loopVar:article.Summary[300]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[300]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),300)
	if callCommInfoChk("{#loopVar:article.Summary[350]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[350]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),350)
	if callCommInfoChk("{#loopVar:article.Summary[400]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[400]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),400)
	if callCommInfoChk("{#loopVar:article.Summary[450]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[450]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),450)
	if callCommInfoChk("{#loopVar:article.Summary[500]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[500]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),500)
	if callCommInfoChk("{#loopVar:article.Summary[550]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[550]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),550)
	if callCommInfoChk("{#loopVar:article.Summary[600]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[600]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),600)
	if callCommInfoChk("{#loopVar:article.Summary[650]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[650]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),650)
	if callCommInfoChk("{#loopVar:article.Summary[700]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[700]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),700)
	if callCommInfoChk("{#loopVar:article.Summary[750]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[750]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),750)
	if callCommInfoChk("{#loopVar:article.Summary[800]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[800]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),800)
	if callCommInfoChk("{#loopVar:article.Summary[850]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[850]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),850)
	if callCommInfoChk("{#loopVar:article.Summary[900]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[900]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),900)
	if callCommInfoChk("{#loopVar:article.Summary[950]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[950]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),950)
	if callCommInfoChk("{#loopVar:article.Summary[1000]/}") = true then  thisSumLabel = "{#loopVar:article.Summary[1000]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("content")),1000)
	
	postID=createBase64ID(replace(replace(mypath,MY_sitePath,""),MY_createFolder_art,""))
	
	if instr(mainImg,"security")>0 or instr(mainImg,"http://")>0 then
		useI_begin = ""
		useI_end = ""
		usePreImgClass = "use-preImg"
		
	else
		useI_begin = "<!--"
		useI_end = "-->"
		usePreImgClass = "use-no-preImg"
		
	end if
	
	
	'主题图片
	
	SmallOrginUrl=replace(replace(mainImg,"../",""),""&MY_sec_uploadImgPath&"/?src=","")
	
	'外链判断			
	if instr(mainImg,"http://")>0 then
	   sSmallPath=mainImg
	   
	else
	   sSmallPath=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")		
	end if
	
	
	'分类获取
	sql_class="select * from [ArticleClass] where ClassName='"&ClassName&"'"
	set rs_class=server.createObject("ADODB.Recordset")
	rs_class.open sql_class,db,1,1
	ClassID=rs_class("ArticleClassID")
	ClassSrc="<a class=""sort"" href="""&MY_sitePath&"classlist/"&MY_createFolder_art&""&ClassID&"_default.html""  title="""&classname&""">"&classname&"</a>"&vbcrlf'类别链接
	rs_class.close
	set rs_class=nothing
	
	'分类ID
	listClassIDShow = "class-id-"&ClassID&""
	
	
	'取出被顶踩信息
	  set rs_Dig=server.createobject("adodb.recordset")
	  sql_Dig="Select * From [vote] Where titleID="&ID&""
	  rs_Dig.open sql_Dig,db,1,1
	  if rs_Dig.eof then
	  my_good="0"
	  my_bad="0" 
	  else 
	  my_good=rs_Dig("art_good")
	  my_bad=rs_Dig("art_bad")
	  end if
		  
	  rs_Dig.Close
	  Set rs_Dig = Nothing
	  
	  '日期
	  t_year = year(Subdate)
	  t_month = dateMonthGoEnglish(month(Subdate))
	  t_day = day(Subdate)
	  
	  '奇偶class + 是否使用主题图片class
	  if (tta+1) mod 2 = 0 then partClass = "post-even "&usePreImgClass&"" else partClass = "post-odd "&usePreImgClass&""	
	  
	   '标签
	   if callCommInfoChk("{#loopVar:article.tags/}") = true then
	   
			keywords_show2 = ""
			KeyWord2=replace(rs("KeyWord"),chr(32),"")
			
			'==========================
			'分割关键词         
			tag2=split(KeyWord2,",") '将输入的字符串根据空格分开，获得一个数组
			max2=ubound(tag2) '得出这个数组的维数，即输入的关键字个数
			'==========================	
			for tkh = 0 to 10
			
				tagHref2 = ""&MY_sitePath&"plugins/keywords/?"&MY_createFolder_art&"_tag=" & server.URLEncode(tag2(tkh)) & ".html"
				tagTitle2 = tag2(tkh)
				tagShowLoop2 = loopTempLabelString("loopList:article.tagsList","{#loopVar:article.tagsList.MY_sitePath/}",""&MY_sitePath&"","{#loopVar:article.tagsList.MY_createFolder_art/}",""&MY_createFolder_art&"","{#loopVar:article.tagsList.toUTF8(title)/}",""&server.URLEncode(tag2(tkh))&"","{#loopVar:article.tagsList.link/}",""&tagHref2&"","{#loopVar:article.tagsList.title/}",""&tagTitle2&"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0) 
				
				if tagTitle2 = "" then tagShowLoop2 = ""
				
				keywords_show2 = keywords_show2&tagShowLoop2
				
				if tkh >= max2 then exit for
				
			next
		
		end if
        
      listcontent_art = listcontent_art&loopTempLabelString("loopList:article.home","{#loopVar:article.t_year/}",""&t_year&"","{#loopVar:article.t_month/}",""&t_month&"","{#loopVar:article.t_day/}",""&t_day&"","{#loopVar:article.sSmallPath/}",""&sSmallPath&"","{#loopVar:article.Title/}",""&Title&"","{#loopVar:article.mypath/}",""&mypath&"","{#loopVar:article.classname/}",""&classname&"","{#loopVar:article.Subdate/}",""&Subdate&"","{#loopVar:article.Summary/}",""&Summary&"","{#loopVar:article.hits/}",""&hits&"","{#loopVar:article.renum/}",""&renum&"","{#loopVar:postClassType/}",""&partClass&"","<!--{#imgUse:begin/}-->",""&useI_begin&"","<!--{#imgUse:end/}-->",""&useI_end&"","{#loopVar:article.tags/}",""&keywords_show2&"","{#loopVar:article.id/}",""&ID&"","{#loopVar:postID/}",""&postID&"","{#loopVar:article.n_month/}",""&month(Subdate)&"","{#loopVar:article.classname.link/}",""&ClassSrc&"",""&thisSumLabel&"",""&thisSumLabel_rep&"",0)
	  
	  listcontent_art = replace(listcontent_art,"1.如果想文章未发布主题图片,列表中同时又不想显示,只留下文字效果,就把下面的首尾注释放置到主题图片的html代码首尾即可","")
	  listcontent_art = replace(listcontent_art,"2.如果未发布图片依然在列表页中显示没有图片的样子，还是图文并茂，那就不要把他们包含在主题图片html代码首尾","")
	  listcontent_art = replace(listcontent_art,"${realname}",realname)
	
	
	
			rs.movenext
			tta=tta+1
		 loop 
	
	end if
	rs.close   
	set rs=nothing 
	
end if	
	
	
'------------------案例作品
if callCommInfoChk("{#loopListContentShow:works.home/}") = true then

	titleLimitLen_worksHome_top_ListNum = getTempLablePara_siteConfig("内容输出列表数量(首页案例作品)")
	
	tw=0
	sql="select top "&titleLimitLen_worksHome_top_ListNum&"  * from [SuccessWork] where private=0 order by  Index desc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
		listcontent_works_top= ""&DY_LANG_102&""
	else
		
		listcontent_works_top=""
	
		
		do while not rs.eof
		
		
	ID=rs("ID")
	Title=replace(rs("title"),chr(32),"")
	Title_show=gotTopic(Title,titleLimitLen)
	url=rs("url")
	ClassName=rs("ClassName")
	content=replace(replace(HTMLDecode(rs("worksIntro")),"<p>",""),"</p>","<br />")
	Image=replace(rs("preURL"),"../","")
	Subdate=rs("Subdate")
	siteURL=rs("siteURL")
	theBigImage=rs("hdImg_d")
	
	if callCommInfoChk("{#loopVar:works.Summary[200]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[200]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),200)
	if callCommInfoChk("{#loopVar:works.Summary[250]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[250]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),250)
	if callCommInfoChk("{#loopVar:works.Summary[300]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[300]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),300)
	if callCommInfoChk("{#loopVar:works.Summary[350]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[350]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),350)

	if callCommInfoChk("{#loopVar:works.Summary[400]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[400]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),400)
	if callCommInfoChk("{#loopVar:works.Summary[450]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[450]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),450)
	if callCommInfoChk("{#loopVar:works.Summary[500]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[500]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),500)
	if callCommInfoChk("{#loopVar:works.Summary[550]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[550]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),550)
	if callCommInfoChk("{#loopVar:works.Summary[600]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[600]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),600)
	if callCommInfoChk("{#loopVar:works.Summary[650]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[650]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),650)
	if callCommInfoChk("{#loopVar:works.Summary[700]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[700]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),700)
	if callCommInfoChk("{#loopVar:works.Summary[750]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[750]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),750)
	if callCommInfoChk("{#loopVar:works.Summary[800]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[800]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),800)
	if callCommInfoChk("{#loopVar:works.Summary[850]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[850]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),850)
	if callCommInfoChk("{#loopVar:works.Summary[900]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[900]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),900)
	if callCommInfoChk("{#loopVar:works.Summary[950]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[950]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),950)
	if callCommInfoChk("{#loopVar:works.Summary[1000]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[1000]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),1000)
	
	postID=createBase64ID(replace(replace(url,MY_sitePath,""),classlistName,""))
	
	if instr(siteURL,"javascript:")>0 then siteURL = ""
	
	
	
	SmallOrginUrl=replace(replace(rs("preURL"),"../",""),""&MY_sec_uploadImgPath&"/?src=","")
	if FileExistsStatus(""&MY_sitePath&""&MY_sec_uploadSmallImgPath&"/?src=s_"&replace(SmallOrginUrl,MY_sitePath,"")&"") = -1 then
	imgSmallCreate(""&MY_sitePath&""&MY_uploadImgPath&"/"&SmallOrginUrl)
	end if
	

	'外链判断			
	if instr(rs("preURL"),"http://")>0 then
	   sSmallPath=rs("preURL")
	   
	else
	   sSmallPath=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")		
	end if
	
	
'分类获取
	sql_class="select * from [SuccessWorkClass] where ClassName='"&ClassName&"'"
	set rs_class=server.createObject("ADODB.Recordset")
	rs_class.open sql_class,db,1,1
	ClassID=rs_class("SuccessWorkClassID")
	ClassSrc="<a class=""sort"" href="""&MY_sitePath&"classlist/"&MY_createFolder_work&""&ClassID&"_default.html""  title="""&classname&""">"&classname&"</a>"&vbcrlf'类别链接
	rs_class.close
	set rs_class=nothing
	
	'分类ID
	listClassIDShow = "class-id-"&ClassID&""
	
	if url<>"" then urlS="<a href="""&url&""" target=""_blank"">"
	if url="" then urlS="<a href=""?thiswork=none"">"
	
	  '日期
	  t_year = year(Subdate)
	  t_month = dateMonthGoEnglish(month(Subdate))
	  t_day = day(Subdate)
		
	  '奇偶class
	  if (tw+1) mod 2 = 0 then partClass = "post-even" else partClass = "post-odd"
	
	  listcontent_works_top = listcontent_works_top&loopTempLabelString("loopList:works.home","{#loopVar:works.t_year/}",""&t_year&"","{#loopVar:works.t_month/}",""&t_month&"","{#loopVar:works.t_day/}",""&t_day&"","{#loopVar:works.sSmallPath/}",""&sSmallPath&"","{#loopVar:works.Title/}",""&Title&"","{#loopVar:works.url/}",""&url&"","{#loopVar:works.classname/}",""&classname&"","{#loopVar:works.Subdate/}",""&Subdate&"","{#loopVar:works.content/}",""&content&"","{#loopVar:works.siteURL/}",""&siteURL&"","{#loopVar:works.bigImgSrc/}",""&theBigImage&"","{#loopVar:postID/}",""&postID&"","{#loopVar:works.n_month/}",""&month(Subdate)&"","{#loopVar:works.classname.link/}",""&ClassSrc&"",""&thisSumLabel&"",""&thisSumLabel_rep&"","{#loopVar:postClassType/}",""&partClass&"","{#loopVar:works.id/}",""&ID&"","{#loopVar:works.classID/}",""&listClassIDShow&"","","","","",0)
	
			rs.movenext
			tw=tw+1
		 loop 
		 
	end if
	rs.close   
	set rs=nothing 
	
end if

'------------公告	
if callCommInfoChk("{#loopListContentShow:notice/}") = true then
	titleLimitLen_notice_ListNum = getTempLablePara_siteConfig("内容输出列表数量(通知公告)")
	
	
	sql="select top "&titleLimitLen_notice_ListNum&" * from [notice] order by Index desc"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,db,1,1
	if rs.bof then
		listcontent_notice= ""&DY_LANG_102&""
	else
		listcontent_notice= ""
	 
		'XML文件
		FlashXML_T1 = "<?xml version=""1.0"" encoding=""utf-8""?>"&vbcrlf
		FlashXML_T2 = "<items>"&vbcrlf
		FlashXML_T3 = "	<item>"&vbcrlf
		FlashXML_T4 = "		<content><![CDATA[<span class=""title"">"&DY_LANG_Office_4&"</span>"&vbcrlf	
		
	dim j
	j=1
		do while not rs.eof
		
		
	ID=rs("ID")
	Subdate=rs("Subdate")
	time_=year(Subdate)&"-"&month(Subdate)&"-"&day(Subdate)
	Title=replace(rs("Title"),chr(32),"")
	Title_show=gotTopic(Title,titleLimitLen_home_notice)
	content=replace(replace(replace(rs("content"),CHR(9),"&#160;"),CHR(13),"<br />"),Chr(10),"")
	url=rs("url")
	if url <> "" then url_go = "<a href="&url&" target=_blank><span class=NoticeTitle>"&DY_LANG_50&"</span></a>"
	if url = "" then url_go = ""
	
	if url <> "" then url_goXML = "<a href="&url&" target=_blank>"&DY_LANG_50&"</a>"
	if url = "" then url_goXML = ""
	
	
	FlashXML = FlashXML&"<p align=""justify"" class=""news"">"&j&"、"&stripHTML(Title)&"</p>"&vbcrlf
	FlashXML = FlashXML&"<p align=""justify""><span class=""note"">"&stripHTML(content)&"</span></p>"&vbcrlf
	
	listcontent_notice = listcontent_notice&loopTempLabelString("loopList:notice","{#loopVar:notice.Title/}",""&Title&"","{#loopVar:notice.content/}",""&replace(HTMLEncode(content),"&#39;","")&"","{#loopVar:notice.url_go/}",""&url_go&"","{#loopVar:notice.Title_show/}",""&Title_show&"","{#loopVar:notice.time_/}",""&time_&"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0)
	
		j=j+1
			rs.movenext
		 loop 
		 
		
		 
		FlashXML_B1 = "]]></content>"&vbcrlf
		FlashXML_B2 = "   </item>"&vbcrlf
		FlashXML_B3 = "</items>"&vbcrlf
		
		'生成XML文件
		CreateFile "../../flashWebStyle1/xml/news.xml",FlashXML_T1&FlashXML_T2&FlashXML_T3&FlashXML_T4&FlashXML&FlashXML_B1&FlashXML_B2&FlashXML_B3
		 
		 
	end if
	rs.close   
	set rs=nothing 
	
end if    


'------------------------最新微博
if callCommInfoChk("${newMood}") = true then

	sql="select top 1 * from [mood] order by id desc"
	set rs=db.execute(sql)

	 moodContent=rs("content")

	rs.close
	set rs=nothing
	
	moodContentShow = "<a href="""&MY_sitePath&"mood/"&MY_createHtml_mood&"_default.html"" title="""&stripHTML(moodContent)&""">"&moodContent&"</a>"

end if





		Template_Code=replace(Template_Code,"${newMood}",moodContentShow)
		Template_Code=replace(Template_Code,"${homeLinks}",listcontent_links)
		Template_Code=replace(Template_Code,"${homeLinks_txt}",listcontent_links_txt)

		
		Template_Code=replace(Template_Code,"{#loopListContentShow:notice/}",listcontent_notice)
		Template_Code=replace(Template_Code,"{#loopListContentShow:article.home/}",listcontent_art)
		Template_Code=replace(Template_Code,"{#loopListContentShow:works.home/}",listcontent_works_top)
		Template_Code=replace(Template_Code,"{#loopListContentShow:article.top/}",listcontent_art_top)
		

		'Seo
		Template_Code=replace(Template_Code,"${thisPageSEOURL}",MY_sitePath&"default.html")	
		Template_Code=replace(Template_Code,"<h2 id=""logo"">","<h1 id=""logo"">")	
		Template_Code=replace(Template_Code,"</a></h2>","</a></h1>")	
		
		webtipsCode="<script src="""&MY_sitePath&"cat_js/commCall/webTips.js""></script>"

        CreateFile "../../default.html",toshort(Template_Code&createData&DS_CopyrightVerificationInfo)

		indexHTML = ""
		indexHTML = indexHTML&"<!DOCTYPE HTML>"&vbcrlf
		indexHTML = indexHTML&"<html>"&vbcrlf
		indexHTML = indexHTML&"<head>"&vbcrlf
		indexHTML = indexHTML&"<meta http-equiv=""Content-Type"" content=""text/html; charset=gb2312"">"&vbcrlf
		indexHTML = indexHTML&"<title>"&sitename&"</title>"&vbcrlf
		indexHTML = indexHTML&"<meta name=""keywords"" content="""&MY_Meta_keywords&""" />"&vbcrlf
		indexHTML = indexHTML&"<meta name=""description"" content="""&detail_motto&""" />"&vbcrlf
		indexHTML = indexHTML&"</head>"&vbcrlf
		indexHTML = indexHTML&"<body>"&vbcrlf
		indexHTML = indexHTML&"<script>"&vbcrlf
		indexHTML = indexHTML&"window.location.href=""default.html"";"&vbcrlf
		indexHTML = indexHTML&"</script>"&vbcrlf
		indexHTML = indexHTML&"</body>"&vbcrlf
		indexHTML = indexHTML&"</html>"&vbcrlf
		
		CreateFile "../../index.html",indexHTML&createData



		%>

<%
db.close
set db=nothing		
'页面缓存
CreateFile thisFilePath,now()

		
HTMLSERVE_BOTTOM()


end if


%>
