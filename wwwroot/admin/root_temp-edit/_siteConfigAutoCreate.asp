<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
DSTempCommConfigFile = MY_sitePath&"admin/root_temp-edit/_createConfig.asp"
if instr(showFile(DSTempCommConfigFile),"DSSetting.tmpProperty") = 0 then
	
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty1 pid=""config1"" value=""50"" title=""标题字符数截取(侧边栏)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty2 pid=""config2"" value=""24"" title=""标题字符数截取(友情链接内页)"" />"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty3 pid=""config3"" value=""40"" title=""标题字符数截取(最新案例作品-通用)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty4 pid=""config4"" value=""40"" title=""标题字符数截取(置顶推荐文章-通用)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty5 pid=""config5"" value=""40"" title=""标题字符数截取(最新推荐文章-通用)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty6 pid=""config6"" value=""40"" title=""标题字符数截取(对应类别调用文章-通用)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty7 pid=""config7"" value=""40"" title=""标题字符数截取(对应类别调用作品-通用)"" />"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty8 pid=""config8"" value=""40"" title=""标题字符数截取(首页文章列表)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty9 pid=""config9"" value=""50"" title=""标题字符数截取(通知公告)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty10 pid=""config10"" value=""24"" title=""标题字符数截取(留言者姓名)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty11 pid=""config11"" value=""40"" title=""标题字符数截取(文章列表)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty12 pid=""config12"" value=""40"" title=""标题字符数截取(案例作品列表)""/>"&vbcrlf
	
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty13 pid=""config13"" value=""10"" title=""内容输出列表数量(侧边栏)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty14 pid=""config14"" value=""8"" title=""内容输出列表数量(侧边栏评论)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty15 pid=""config15"" value=""100"" title=""内容输出列表数量(友情链接内页)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty16 pid=""config16"" value=""5"" title=""内容输出列表数量(首页置顶文章)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty17 pid=""config17"" value=""5"" title=""内容输出列表数量(首页文章)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty18 pid=""config18"" value=""6"" title=""内容输出列表数量(首页案例作品)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty19 pid=""config19"" value=""10"" title=""内容输出列表数量(通知公告)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty20 pid=""config20"" value=""10"" title=""内容输出列表数量(最新案例作品-通用)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty21 pid=""config21"" value=""10"" title=""内容输出列表数量(置顶推荐文章-通用)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty22 pid=""config22"" value=""10"" title=""内容输出列表数量(最新推荐文章-通用)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty23 pid=""config23"" value=""10"" title=""内容输出列表数量(对应类别调用文章-通用)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty24 pid=""config24"" value=""10"" title=""内容输出列表数量(对应类别调用作品-通用)"" />"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty25 pid=""config25"" value=""12"" title=""内容输出列表数量(留言)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty26 pid=""config26"" value=""18"" title=""内容输出列表数量(微博)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty27 pid=""config27"" value=""12"" title=""内容输出列表数量(文章列表)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty28 pid=""config28"" value=""12"" title=""内容输出列表数量(案例作品列表)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty29 pid=""config29"" value=""8"" title=""内容输出列表数量(相关文章列表)""/>"&vbcrlf
	configCodeDefault = configCodeDefault & "<DSSetting.tmpProperty30 pid=""config30"" value=""10"" title=""内容输出列表数量(手机版栏目列表)""/>"&vbcrlf

    CreateFile DSTempCommConfigFile,configCodeDefault

end if

createConfigCode = makeTempLabelString(showFile(DesignerSite_tempPath_common),"{#note:template}","{/note:template}")
CreateFile DSTempCommConfigFile,createConfigCode

%>