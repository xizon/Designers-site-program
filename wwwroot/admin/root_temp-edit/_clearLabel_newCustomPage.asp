<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
		'【必选其一】各栏目标题依赖标签,必选其一设置为参数 1
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:commonUse.title",1,"","","") '通用标题
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:content.article",0,"","","")	 '内容页-文章 
		  

		'列表页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.article",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.works",0,"","","")
		
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.about",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.mood",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.search",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.links",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.message",0,"","","")
		
		'【必选其一】各栏目meta描述依赖标签,必选其一设置为参数 1 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:comm.desc",1,"","","") '通用网站描述
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:content.articleDesc",0,"","","")'内容页-文章 
		

		
		'【必选其一】各栏目meta关键词依赖标签,必选其一设置为参数 1 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:commonUse.keywords",1,"","","")'其他通用页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:home",0,"","","")'首页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:content",0,"","","")'内容页关键词
		
		
		'RSS订阅，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"rss:commonUse.article",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"rss:commonUse.article.class",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"atom:commonUse.article",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"atom:commonUse.article.class",0,"","","")
		
		'banner轮换，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.banner",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"head:commonUse.banner",0,"","","")
		
		'搜索框，订阅图标，置顶推荐文章，最新案例作品，规定数量随机tags输出，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.searchBoard",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.articleFeed",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.article.top",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.works.list",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.article.tags.random",0,"","","")
		
		
		'内容页SEO可选link
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"head:content.linkRel",0,"","","")
		
		
		
		'侧边栏
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"sideBar:home",0,"","","")  '首页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"sideBar:content.comm",0,"","","") '内容页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"sideBar:article,search,about,message",0,"","","") '列表页
		
		
		'各栏目的列表页面
		
		'文章
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.article",0,"","","//") '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article.class",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article.tags",0,"","","")  
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.article",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.article.class",0,"/*","*/","")
		
		
		'作品案例
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.worksIe6Js",0,"","","") '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.works",0,"","","//")   '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.works",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.works.class",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.works",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.works.class",0,"/*","*/","")
		
		
		'微博
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.mood",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:content.js.mood",0,"/*","*/","")
		
		'关于我们
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.about",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.about",0,"/*","*/","")
		
		
		'搜索
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.search",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.searchResult",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.search",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.searchResult",0,"/*","*/","")
		
		'友情链接
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.links",0,"","","")	  
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.links",0,"/*","*/","")
		
		'留言
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.message",0,"","","//")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.message",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.message",0,"/*","*/","")
		
		'首页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.home",0,"","","//")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.home",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.home",0,"/*","*/","")
		
		
		'内容页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.js.comm.form",0,"","","")	 '栏目必选  	
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.show.commJs",0,"","","")'栏目必选   	
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.show.article",0,"","","")	 						 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:content.js.article",0,"/*","*/","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:content.js.comm",0,"","","//")		 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:content.article",0,"","","//")
		
		
		'---------------
		'循环体参数不用设置
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:article",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:article.home",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:article.tagsList",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:works.home",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:works.classList",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:works",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:mood",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:notice",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:message",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:comment",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:article.top.comm",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:relative.article",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:sidebarList.tabs.title",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:sidebarList.tabs.title.class",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:sidebarList_begin",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:sidebarList_content",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:sidebarList_end",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:sidebarList.class_begin",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:sidebarList.class_content",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:sidebarList.class_end",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:sidebarList_more",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:sidebarList.alone_begin",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:sidebarList.alone_content",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:sidebarList.alone_end",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:sidebarList.alone_more",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:sidebarList.catLandscape_begin",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:sidebarList.catLandscape_content",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:sidebarList.catLandscape_end",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:article.tags.random.comm",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:works.list.comm",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:article.tags.random.comm",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"loopList:works.list.comm",0,"","","")
		
		'---------------
		'内容页独立代码输出，参数不用设置
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"labelEcho:relative.article",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"labelEcho:recent.article",0,"","","") 
%>
