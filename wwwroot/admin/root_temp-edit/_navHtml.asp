<!--#include file="_siteInfo.asp" -->
<!--#include file="../sectionConfig/pluginsPlace.asp" -->
<!--#include file="../otherColumnChk/_otherColumnChk.asp" -->
<!--#include file="../sectionConfig/homeColumnConfig.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================

'是否调用通用信息判断
%>
<!--#include file="_batchMoreInfoCreate.asp" -->
<%

'---------------自定义插件begin(请不要修改默认变量)

set rs=server.CreateObject("adodb.recordset")
strsql="select * from [plugins] where view = 0 and  columnName='newPlugins'  and columnName<>'links' "&sql_1&""&sql_2&""&sql_1_2&""&sql_2_2&""&sql_3&""&sql_1_3&""&sql_2_3&"  order by Index asc"
rs.open strsql,db,1,1	
customPlugins_sidebar=""
customPlugins_lateral=""
customPlugins_showPlugin=""
if not rs.eof and not rs.bof then

	do while not rs.eof
	
	
	newPluginsNow_content=rs("content")
	newPluginsNow_sort=rs("sort")

	

	if newPluginsNow_sort=1 then 	  
	  customPlugins_sidebar=customPlugins_sidebar& ""&newPluginsNow_content&""
	end if
	
	if newPluginsNow_sort=2 then
	  customPlugins_lateral=customPlugins_lateral& ""&newPluginsNow_content&""
	end if
	
	if newPluginsNow_sort=3 then
	  customPlugins_showPlugin=customPlugins_showPlugin& ""&newPluginsNow_content&""
	end if
		
		
	rs.movenext
	loop
	
rs.close
set rs=nothing		
	
end if


'数据整合	
if pluginsPlace=0 then
	landScapeCateNav_ok = landScapeCateNav&customPlugins_lateral
	MySidebar_NonCate_ok = MySidebar_NonCate&customPlugins_sidebar
	MySidebar_ok = MySidebar&customPlugins_sidebar
	
	'首页
	if homeColumnConfig_plugins = 1 then MySidebar_ok_defaultPage = MySidebar_defaultPage
	if homeColumnConfig_plugins = 0 then MySidebar_ok_defaultPage = MySidebar_defaultPage&customPlugins_sidebar
	
	MySidebar_ok_defaultPage = MySidebar_defaultPage&customPlugins_sidebar
	
	columnImgSwitch_ok = columnImgSwitch&customPlugins_showPlugin
elseif pluginsPlace=1 then
	landScapeCateNav_ok = customPlugins_lateral&landScapeCateNav
	MySidebar_NonCate_ok = customPlugins_sidebar&MySidebar_NonCate
	MySidebar_ok = customPlugins_sidebar&MySidebar
	
	'首页
	if homeColumnConfig_plugins = 1 then MySidebar_ok_defaultPage = MySidebar_defaultPage
	if homeColumnConfig_plugins = 0 then MySidebar_ok_defaultPage = customPlugins_sidebar&MySidebar_defaultPage
		
	
	columnImgSwitch_ok = customPlugins_showPlugin&columnImgSwitch

end if

'路径转化

MySidebar_ok = replace(replace(MySidebar_ok,"../../",MY_sitePath),"../",MY_sitePath)	
MySidebar_NonCate_ok = 	replace(replace(MySidebar_NonCate_ok,"../../",MY_sitePath),"../",MY_sitePath)
MySidebar_ok_defaultPage = replace(replace(MySidebar_ok_defaultPage,"../../",MY_sitePath),"../",MY_sitePath)	
landScapeCateNav_ok = replace(replace(landScapeCateNav_ok,"../../",MY_sitePath),"../",MY_sitePath)
	
'--------js生成	
jsCall1 = jsConversWrite(toUTF8(MySidebar_ok),"jsOutput_20")
jsCall2 = jsConversWrite(toUTF8(MySidebar_NonCate_ok),"jsOutput_21")
jsCall3 = jsConversWrite(toUTF8(MySidebar_ok_defaultPage),"jsOutput_22")
jsCall4 = jsConversWrite(toUTF8(landScapeCateNav_ok),"jsOutput_23")

createFolder(""&MY_sitePath&"cat_js/commCall")
CreateFile ""&MY_sitePath&"cat_js/commCall/MySidebar.js",jsCall1
CreateFile ""&MY_sitePath&"cat_js/commCall/MySidebar_NonCate.js",jsCall2
CreateFile ""&MY_sitePath&"cat_js/commCall/MySidebar_defaultPage.js",jsCall3
CreateFile ""&MY_sitePath&"cat_js/commCall/landScapeCateNav.js",jsCall4


js1 = "<span id=""jsOutput_20""></span><script src="""&MY_sitePath&"cat_js/commCall/MySidebar.js""></script> "
js2 = "<span id=""jsOutput_21""></span><script src="""&MY_sitePath&"cat_js/commCall/MySidebar_NonCate.js""></script> "	
js3 = "<span id=""jsOutput_22""></span><script src="""&MY_sitePath&"cat_js/commCall/MySidebar_defaultPage.js""></script> "			
js4 = "<span id=""jsOutput_23""></span><script src="""&MY_sitePath&"cat_js/commCall/landScapeCateNav.js""></script> "			

if MY_commCodeJsCall=1 then


	
'侧边栏目和横向导航采用js调用		
	Template_Code=replace(Template_Code,"${MySidebar}",js1)
	Template_Code=replace(Template_Code,"${MySidebar_NonCate}",js2)
	Template_Code=replace(Template_Code,"${MySidebar_defaultPage}",js3)	
	
		
				
else

    if sideBarCommInfoChk = 1 then
	
		Template_Code=replace(Template_Code,"${MySidebar}",js1)
		Template_Code=replace(Template_Code,"${MySidebar_NonCate}",js2)
		Template_Code=replace(Template_Code,"${MySidebar_defaultPage}",js3)	
		
	
	else
	
		'侧边栏目和横向导航采用纯HTML静态
		Template_Code=replace(Template_Code,"${MySidebar}",MySidebar_ok)
		Template_Code=replace(Template_Code,"${MySidebar_NonCate}",MySidebar_NonCate_ok)
		Template_Code=replace(Template_Code,"${MySidebar_defaultPage}",MySidebar_ok_defaultPage)	
		
	
	end if

		
end if	
	
	
Template_Code=replace(Template_Code,"${landScapeCateNav}",landScapeCateNav_ok)	


Template_Code=replace(Template_Code,"${columnImgSwitch}",columnImgSwitch_ok)

'网址链接
Template_Code=replace(Template_Code,"${MY_secCheckSiteLink}",MY_secCheckSiteLink)
Template_Code=replace(Template_Code,"${MY_ProgramCharacter}",MY_ProgramCharacter)
Template_Code=replace(Template_Code,"${webLink}",MY_secCheckSiteLink&"/")

'自定义文件夹
Template_Code=replace(Template_Code,"${MY_defineName_art}",MY_createFolder_art)
Template_Code=replace(Template_Code,"${MY_defineName_work}",MY_createFolder_work)
Template_Code=replace(Template_Code,"${meta_keywords}",MY_Meta_keywords)
Template_Code=replace(Template_Code,"${meta_author}",MY_Meta_author)
Template_Code=replace(Template_Code,"${meta_copyright}",MY_Meta_Copyright)



'通用栏目列表
Template_Code=replace(Template_Code,"${listcontent_comm_siteCounter}",listcontent_sideAloneComm_siteCounter)
Template_Code=replace(Template_Code,"${listcontent_comm_search}",listcontent_comm_search)


'--------js生成	
jsCallCommContent_1 = jsConversWrite(toUTF8(listcontent_sideAloneComm_classWorks),"jsOutput_1")
CreateFile ""&MY_sitePath&"cat_js/commCall/jsCallCommContent_1.js",jsCallCommContent_1

jsCallCommContent_2 = jsConversWrite(toUTF8(listcontent_sideAloneComm_classArt),"jsOutput_2")
CreateFile ""&MY_sitePath&"cat_js/commCall/jsCallCommContent_2.js",jsCallCommContent_2

jsCallCommContent_4 = jsConversWrite(toUTF8(listcontent_sideAloneComm_randomArt),"jsOutput_4")
CreateFile ""&MY_sitePath&"cat_js/commCall/jsCallCommContent_4.js",jsCallCommContent_4

jsCallCommContent_6 = jsConversWrite(toUTF8(listcontent_sideAloneComm_art),"jsOutput_6")
CreateFile ""&MY_sitePath&"cat_js/commCall/jsCallCommContent_6.js",jsCallCommContent_6


jsCallCommContent_8 = jsConversWrite(toUTF8(listcontent_sideAloneComm_Hot_art),"jsOutput_8")
CreateFile ""&MY_sitePath&"cat_js/commCall/jsCallCommContent_8.js",jsCallCommContent_8

jsCallCommContent_9 = jsConversWrite(toUTF8(listcontent_sideAloneComm_links),"jsOutput_9")
CreateFile ""&MY_sitePath&"cat_js/commCall/jsCallCommContent_9.js",jsCallCommContent_9

jsCallCommContent_10 = jsConversWrite(toUTF8(listcontent_sideAloneComm_comment),"jsOutput_10")
CreateFile ""&MY_sitePath&"cat_js/commCall/jsCallCommContent_10.js",jsCallCommContent_10

jsCallCommContent_13 = jsConversWrite(toUTF8(listcontent_comm_netname),"jsOutput_13")
CreateFile ""&MY_sitePath&"cat_js/commCall/jsCallCommContent_13.js",jsCallCommContent_13

jsCallCommContent_14 = jsConversWrite(toUTF8(listcontent_comm_classarticle_landScape),"jsOutput_14")
CreateFile ""&MY_sitePath&"cat_js/commCall/jsCallCommContent_14.js",jsCallCommContent_14


jsCallCommContent_16 = jsConversWrite(toUTF8(listcontent_comm_case),"jsOutput_16")
CreateFile ""&MY_sitePath&"cat_js/commCall/jsCallCommContent_16.js",jsCallCommContent_16

jsCallCommContent_17 = jsConversWrite(toUTF8(listcontent_comm_topArticle),"jsOutput_17")
CreateFile ""&MY_sitePath&"cat_js/commCall/jsCallCommContent_17.js",jsCallCommContent_17

jsCallCommContent_18 = jsConversWrite(toUTF8(listcontent_comm_tags),"jsOutput_18")
CreateFile ""&MY_sitePath&"cat_js/commCall/jsCallCommContent_18.js",jsCallCommContent_18

jsCallCommContent_19 = jsConversWrite(toUTF8(listcontent_sideAloneComm_Hot_month_art),"jsOutput_19")
CreateFile ""&MY_sitePath&"cat_js/commCall/jsCallCommContent_19.js",jsCallCommContent_19


jsCallCommContent_20 = jsConversWrite(toUTF8(listcontent_sideAloneComm_Hot_week_art),"jsOutput_20")
CreateFile ""&MY_sitePath&"cat_js/commCall/jsCallCommContent_20.js",jsCallCommContent_20


createFolder(""&MY_sitePath&"cat_js/commCall")
jsShow_1 = "<span id=""jsOutput_1""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_1.js""></script> "
jsShow_2 = "<span id=""jsOutput_2""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_2.js""></script> "
jsShow_3 = "<span id=""jsOutput_3""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_3.js""></script> "
jsShow_4 = "<span id=""jsOutput_4""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_4.js""></script> "
jsShow_5 = "<span id=""jsOutput_5""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_5.js""></script> "
jsShow_6 = "<span id=""jsOutput_6""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_6.js""></script> "
jsShow_7 = "<span id=""jsOutput_7""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_7.js""></script> "
jsShow_8 = "<span id=""jsOutput_8""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_8.js""></script> "
jsShow_9 = "<span id=""jsOutput_9""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_9.js""></script> "
jsShow_10 = "<span id=""jsOutput_10""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_10.js""></script> "
jsShow_11 = "<span id=""jsOutput_11""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_11.js""></script> "
jsShow_12 = "<span id=""jsOutput_12""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_12.js""></script> "
jsShow_13 = "<span id=""jsOutput_13""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_13.js""></script> "
jsShow_14 = "<span id=""jsOutput_14""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_14.js""></script> "
jsShow_15 = "<span id=""jsOutput_15""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_15.js""></script> "
jsShow_16 = "<span id=""jsOutput_16""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_16.js""></script> "
jsShow_17 = "<span id=""jsOutput_17""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_17.js""></script> "
jsShow_18 = "<span id=""jsOutput_18""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_18.js""></script> "
jsShow_19 = "<span id=""jsOutput_19""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_19.js""></script> "
jsShow_20 = "<span id=""jsOutput_20""></span><script src="""&MY_sitePath&"cat_js/commCall/jsCallCommContent_20.js""></script> "

if MY_commCodeJsCall=1 then

	Template_Code=replace(Template_Code,"${listcontent_comm_classWorks}",jsShow_1)
	Template_Code=replace(Template_Code,"${listcontent_comm_classarticle}",jsShow_2)
	Template_Code=replace(Template_Code,"${listcontent_comm_randomArticle}",jsShow_4)
	Template_Code=replace(Template_Code,"${listcontent_comm_art}",jsShow_6)
	Template_Code=replace(Template_Code,"${listcontent_comm_hot_art}",jsShow_8)
	Template_Code=replace(Template_Code,"${listcontent_comm_links}",jsShow_9)
	Template_Code=replace(Template_Code,"${listcontent_comm_comment}",jsShow_10)
	'----
	Template_Code=replace(Template_Code,"${listcontent_comm_netname}",jsShow_13)
	Template_Code=replace(Template_Code,"${listcontent_comm_classarticle_landScape}",jsShow_14)
	'----
	Template_Code=replace(Template_Code,"${listcontent_comm_newcase}",jsShow_16)	
	Template_Code=replace(Template_Code,"${listcontent_comm_topArticle}",jsShow_17)
	Template_Code=replace(Template_Code,"${listcontent_comm_tags}",jsShow_18)	
	'-----
	Template_Code=replace(Template_Code,"${listcontent_comm_hot_month_art}",jsShow_19)	
	Template_Code=replace(Template_Code,"${listcontent_comm_hot_week_art}",jsShow_20)	
	
	
			
else

    if sideBarCommInfoChk = 1 then
	    
		'判断为列表页，调用js数据
		Template_Code=replace(Template_Code,"${listcontent_comm_classWorks}",jsShow_1)
		Template_Code=replace(Template_Code,"${listcontent_comm_classarticle}",jsShow_2)
		Template_Code=replace(Template_Code,"${listcontent_comm_randomArticle}",jsShow_4)
		Template_Code=replace(Template_Code,"${listcontent_comm_art}",jsShow_6)
		Template_Code=replace(Template_Code,"${listcontent_comm_hot_art}",jsShow_8)
		Template_Code=replace(Template_Code,"${listcontent_comm_links}",jsShow_9)
		Template_Code=replace(Template_Code,"${listcontent_comm_comment}",jsShow_10)
		'----
		Template_Code=replace(Template_Code,"${listcontent_comm_netname}",jsShow_13)
		Template_Code=replace(Template_Code,"${listcontent_comm_classarticle_landScape}",jsShow_14)
		'----
		Template_Code=replace(Template_Code,"${listcontent_comm_newcase}",jsShow_16)	
		Template_Code=replace(Template_Code,"${listcontent_comm_topArticle}",jsShow_17)
		Template_Code=replace(Template_Code,"${listcontent_comm_tags}",jsShow_18)	
		'----
		Template_Code=replace(Template_Code,"${listcontent_comm_hot_month_art}",jsShow_19)	
		Template_Code=replace(Template_Code,"${listcontent_comm_hot_week_art}",jsShow_20)	
			
		
	else
	   
	   '全静态调用
		Template_Code=replace(Template_Code,"${listcontent_comm_classWorks}",listcontent_sideAloneComm_classWorks)
		Template_Code=replace(Template_Code,"${listcontent_comm_classarticle}",listcontent_sideAloneComm_classArt)
		Template_Code=replace(Template_Code,"${listcontent_comm_randomArticle}",listcontent_sideAloneComm_randomArt)
		Template_Code=replace(Template_Code,"${listcontent_comm_art}",listcontent_sideAloneComm_art)
		Template_Code=replace(Template_Code,"${listcontent_comm_hot_art}",listcontent_sideAloneComm_Hot_art)
		Template_Code=replace(Template_Code,"${listcontent_comm_links}",listcontent_sideAloneComm_links)
		Template_Code=replace(Template_Code,"${listcontent_comm_comment}",listcontent_sideAloneComm_comment)
		'----
		Template_Code=replace(Template_Code,"${listcontent_comm_netname}",listcontent_comm_netname)
		Template_Code=replace(Template_Code,"${listcontent_comm_classarticle_landScape}",listcontent_comm_classarticle_landScape)
		'----
		Template_Code=replace(Template_Code,"${listcontent_comm_newcase}",listcontent_comm_case)	
		Template_Code=replace(Template_Code,"${listcontent_comm_topArticle}",listcontent_comm_topArticle)
		Template_Code=replace(Template_Code,"${listcontent_comm_tags}",listcontent_comm_tags)	
		'----
		Template_Code=replace(Template_Code,"${listcontent_comm_hot_month_art}",listcontent_sideAloneComm_Hot_month_art)	
		Template_Code=replace(Template_Code,"${listcontent_comm_hot_week_art}",listcontent_sideAloneComm_Hot_week_art)
		
		

	end if

	

		
end if	

'通用替换
Template_Code=replace(Template_Code,"{#loopListContentShow:works.list.comm/}",listcontent_comm_case)	
Template_Code=replace(Template_Code,"{#loopListContentShow:article.top.comm/}",listcontent_comm_topArticle)	
Template_Code=replace(Template_Code,"{#loopListContentShow:article.tags.random.comm/}",listcontent_comm_tags)	
Template_Code=replace(Template_Code,"${bannerImgList}",listcontent_comm_bannerShow)
Template_Code=replace(Template_Code,"${bannerImgSmallList}",listcontent_comm_bannerSmallShow)
Template_Code=replace(Template_Code,"${bannerImgList_autoplayTime}",MY_banner_picSwitch_time)
Template_Code=replace(Template_Code,"${pageURL_comm_home}",""&MY_sitePath&"default.html")
Template_Code=replace(Template_Code,"${pageURL_comm_links}",""&MY_sitePath&"catlinks.html")
Template_Code=replace(Template_Code,"${pageURL_comm_about}",""&MY_sitePath&"selfinfo.html")
Template_Code=replace(Template_Code,"${pageURL_comm_mood}",""&MY_sitePath&"mood/mood_default.html")
Template_Code=replace(Template_Code,"${pageURL_comm_search}",""&MY_sitePath&"search.html")
Template_Code=replace(Template_Code,"${pageURL_comm_contact}",""&MY_sitePath&"message/message_default.html")
Template_Code=replace(Template_Code,"${pageURL_comm_article}",""&MY_sitePath&"list/"&MY_createFolder_art&"_default.html")
Template_Code=replace(Template_Code,"${pageURL_comm_works}",""&MY_sitePath&"list/"&MY_createFolder_work&"_default.html")

'---------------自定义标签
set rs=server.createobject("adodb.recordset")
sql="select * from [define_label]"
rs.open sql,db,1,1
do while not rs.eof and not rs.bof

thislabelSymbol = rs("labelSymbol")
thislableContent = rs("lableContent")
			
Template_Code=replace(Template_Code,thislabelSymbol,thislableContent)

rs.movenext
loop
rs.close
set rs=nothing

'CreatePage BottomInfo
Execute(UnEncode(dataCreate))

'-----------导航菜单
%>
<!--#include file="_navValue.asp" -->
