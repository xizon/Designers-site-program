<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file = "links_config.asp"-->
<!--#include file="../function/complexFont_convert.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%	
titleLimitLen = getTempLablePara_siteConfig("标题字符数截取(友情链接内页)")
titleLimitLen_linksListNum = getTempLablePara_siteConfig("内容输出列表数量(友情链接内页)")


'缓存内容页面
createFolder("../_contentCache/")
thisFilePath = "../_contentCache/list_links.txt"

if FileExistsStatus(thisFilePath) = 1 then 


response.Write DY_LANG_103


else


HTMLSERVE_HEAD()

'////获取并判断动态模板 begin
thisTempCodePath=""&MY_sitePath&"admin/_temp/link_tempA_"&tempModifiedDate&".html"
if FileExistsStatus(thisTempCodePath) = -1 then '不存在
'开始执行依赖标签功能
'==================

		
		
		Template_Code=showFile(DesignerSite_tempPath_common)
		'=============================模板依赖标签不存在判断   begin
		
		'模板主注释，参数不用变更，默认全部清除
		Template_Code_trip = dependentLabelFun(Template_Code,"note:template",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"note:commCreateInfo",0,"","","")
		
		'【必选其一】各栏目标题依赖标签,必选其一设置为参数 1
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:commonUse.title",0,"","","") '通用标题
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:content.article",0,"","","")	 '内容页-文章 
		  
		'列表页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.article",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.works",0,"","","")
		
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.about",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.mood",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.search",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.links",1,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.message",0,"","","")
		
		'【必选其一】各栏目meta描述依赖标签,必选其一设置为参数 1 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:comm.desc",1,"","","") '通用网站描述
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:content.articleDesc",0,"","","")'内容页-文章 
		

		
		'【必选其一】各栏目meta关键词依赖标签,必选其一设置为参数 1 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:commonUse.keywords",1,"","","")'其他通用页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:home",0,"","","")'首页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:content",0,"","","")'内容页关键词
		
		
		'RSS订阅，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"rss:commonUse.article",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"rss:commonUse.article.class",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"atom:commonUse.article",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"atom:commonUse.article.class",0,"","","")
		
		'banner轮换，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.banner",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"head:commonUse.banner",0,"","","")
		
		'搜索框，订阅图标，置顶推荐文章，最新案例作品，规定数量随机tags输出，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.searchBoard",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.articleFeed",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.article.top",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.works.list",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.article.tags.random",0,"","","")
		
		
		'内容页SEO可选link
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"head:content.linkRel",0,"","","")
		
		
		
		'侧边栏
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"sideBar:home",0,"","","")  '首页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"sideBar:content.comm",0,"","","") '内容页
		Template_Code_trip = getTempLablePara_siderBar_list("友情链接侧边栏",0,"body:columnShow.links")
		Template_Code_trip = dependentLabelFun_para(Template_Code_trip,"友情链接侧边栏",0,"body:columnShow.links")
		
		'各栏目的列表页面
		
		'文章
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.article",0,"","","//") '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article.class",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article.tags",0,"","","")  
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.article",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.article.class",0,"/*","*/","")
		
		
		'作品案例
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.worksIe6Js",0,"","","") '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.works",0,"","","//")   '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.works",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.works.class",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.works",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.works.class",0,"/*","*/","")
		

		'微博
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.mood",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:content.js.mood",0,"/*","*/","")
		

		'关于我们
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.about",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.about",0,"/*","*/","")
		
		
		'搜索
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.search",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.searchResult",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.search",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.searchResult",0,"/*","*/","")
		
		'友情链接
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.links",1,"","","")	  
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.links",1,"/*","*/","")
		
		'留言
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.message",0,"","","//")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.message",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.message",0,"/*","*/","")
		
		'首页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.home",0,"","","//")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.home",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.home",0,"/*","*/","")
		
		
		'内容页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.js.comm.form",0,"","","")	 '栏目必选  	
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.show.commJs",0,"","","")'栏目必选   	
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.show.article",0,"","","")	 						 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:content.js.article",0,"/*","*/","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:content.js.comm",0,"","","//")		 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:content.article",0,"","","//")
		
		
		%>
		<!--#include file="_clearLabel_comm.asp" -->
		<%
  
		'==================
		Template_Code_Now = Template_Code_trip		
		CreateFile thisTempCodePath,Template_Code_Now
		
 
end if
'=============================模板依赖标签不存在判断   end

'这里获取依赖标签清除后模板的值
Template_Code=showFile(thisTempCodePath)
'只调用栏目独立HTML代码判断
callPageAllHTML "body:columnShow.links","完全独立页面代码.不调用其它任何依赖标签的内容"


%>
<!--#include file="sideColumn.asp" -->
<%
'■■■■■■■■■■■■■■■■■■■■■■■■■links开始■■■■■■■■■■■■■■■■■■■■■■■■		
linkslist=getdata("list",titleLimitLen_linksListNum,titleLimitLen)		
'////////////循环写入模板

sql2="select * from [Link] where view=1"
set rs2=server.createObject("ADODB.Recordset")
rs2.open sql2,db,1,1

'判断是否有留言
if not rs2.EOF then
	dim aacat()
	redim aacat(titleLimitLen_linksListNum)
			for i=1 to titleLimitLen_linksListNum
			aacat(i)=linkslist
			Template_Code=replace(Template_Code,"${linkslist}",aacat(i))
	next 
else
	Template_Code=replace(Template_Code,"${linkslist}",DY_LANG_102)
	
end if

rs2.close
set rs2=nothing




'////////////循环写入模板结束
		
		'Seo
		Template_Code=replace(Template_Code,"${thisPageSEOURL}",MY_sitePath&"catlinks.html")	
		
        CreateFile "../../catlinks.html",toshort(Template_Code&createData&DS_CopyrightVerificationInfo)

		%>

<%
db.close
set db=nothing		
'页面缓存
CreateFile thisFilePath,now()

		
HTMLSERVE_BOTTOM()


end if


%>
