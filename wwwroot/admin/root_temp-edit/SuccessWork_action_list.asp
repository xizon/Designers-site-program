<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
'列表页通用信息js调用判断
sideBarCommInfoChk = 1
%>
<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="set_config.asp" -->
<!--#include file="../function/imgAction.asp" -->

<%	
titleLimitLen = getTempLablePara_siteConfig("标题字符数截取(案例作品列表)")
titleLimitLen_works_ListNum = getTempLablePara_siteConfig("内容输出列表数量(案例作品列表)")

'缓存内容页面
createFolder("../_contentCache/")
thisFilePath = "../_contentCache/list_works.txt"

if FileExistsStatus(thisFilePath) = 1 then 


response.Write DY_LANG_103


else


HTMLSERVE_HEAD()
createFolder("../../list")
classlistName=MY_createFolder_work
		
'------分页循环
sql="select * from [SuccessWork] where private=0"
set rs=server.createObject("ADODB.Recordset")
rs.open sql,db,1,1

num=titleLimitLen_works_ListNum'每页显示记录数
rs.pagesize=num
totalpage=rs.pagecount
rs.close
set rs=nothing


'指定更新页数
if MY_batchPageCreateHtmlStatus = 0 then 
	UpPageNum = 1
	'分页页面生成采用倒序定位
	thisPageBegin = 1
	if thisPageBegin > totalpage - 1 then thisPageBegin = totalpage - 1
	betweenPageBegin = thisPageBegin : totalpageShowCreate=totalpage - (betweenPageBegin-1)'定位参数

	
else
	UpPageNum = cint(MY_betweenPageEnd) : if UpPageNum > totalpage then UpPageNum = totalpage
	'分页页面生成采用倒序定位
	thisPageBegin = cint(MY_betweenPageBegin)
	if thisPageBegin > totalpage - 1 then thisPageBegin = totalpage - 1
	betweenPageBegin = thisPageBegin : totalpageShowCreate=totalpage - (betweenPageBegin-1)'定位参数

	
end if


'////获取并判断动态模板 begin
thisTempCodePath=""&MY_sitePath&"admin/_temp/works_a_"&tempModifiedDate&".html"
if FileExistsStatus(thisTempCodePath) = -1 then '不存在
'开始执行依赖标签功能
'==================

		
		Template_Code_batch=showFile(DesignerSite_tempPath_common)
		'=============================模板依赖标签不存在判断   begin
		
		'模板主注释，参数不用变更，默认全部清除
		Template_Code_trip = dependentLabelFun(Template_Code_batch,"note:template",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"note:commCreateInfo",0,"","","")
		
		
		'【必选其一】各栏目标题依赖标签,必选其一设置为参数 1
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:commonUse.title",0,"","","") '通用标题
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:content.article",0,"","","")	 '内容页-文章 
		  

		'列表页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.article",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.works",1,"","","")
		
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.about",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.mood",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.search",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.links",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.message",0,"","","")
		
		'【必选其一】各栏目meta描述依赖标签,必选其一设置为参数 1 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:comm.desc",1,"","","") '通用网站描述
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:content.articleDesc",0,"","","")'内容页-文章 
		
		
		'【必选其一】各栏目meta关键词依赖标签,必选其一设置为参数 1 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:commonUse.keywords",1,"","","")'其他通用页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:home",0,"","","")'首页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:content",0,"","","")'内容页关键词
		
		
		'RSS订阅，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"rss:commonUse.article",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"rss:commonUse.article.class",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"atom:commonUse.article",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"atom:commonUse.article.class",0,"","","")
		
		'banner轮换，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.banner",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"head:commonUse.banner",0,"","","")
		
		'搜索框，订阅图标，置顶推荐文章，最新案例作品，规定数量随机tags输出，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.searchBoard",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.articleFeed",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.article.top",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.works.list",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.article.tags.random",0,"","","")
		
		
		'内容页SEO可选link
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"head:content.linkRel",0,"","","")
		
		
		
		'侧边栏
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"sideBar:home",0,"","","")  '首页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"sideBar:content.comm",0,"","","") '内容页
		Template_Code_trip = getTempLablePara_siderBar_list("作品列表页侧边栏",0,"body:columnShow.works")
		Template_Code_trip = dependentLabelFun_para(Template_Code_trip,"作品列表页侧边栏",0,"body:columnShow.works")
		
		'各栏目的列表页面
		
		'文章
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.article",0,"","","//") '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article.class",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article.tags",0,"","","")  
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.article",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.article.class",0,"/*","*/","")
		
		
		'作品案例
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.worksIe6Js",1,"","","") '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.works",1,"","","//")   '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.works",1,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.works.class",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.works",1,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.works.class",0,"/*","*/","")
		
		
		'微博
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.mood",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:content.js.mood",0,"/*","*/","")

		'关于我们
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.about",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.about",0,"/*","*/","")
		
		
		'搜索
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.search",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.searchResult",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.search",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.searchResult",0,"/*","*/","")
		
		'友情链接
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.links",0,"","","")	  
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.links",0,"/*","*/","")
		
		'留言
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.message",0,"","","//")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.message",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.message",0,"/*","*/","")
		
		'首页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.home",0,"","","//")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.home",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.home",0,"/*","*/","")
		
		
		'内容页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.js.comm.form",0,"","","")	 '栏目必选  	
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.show.commJs",0,"","","")'栏目必选   	
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.show.article",0,"","","")	 						 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:content.js.article",0,"/*","*/","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:content.js.comm",0,"","","//")		 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:content.article",0,"","","//")
		
		
		%>
		<!--#include file="_clearLabel_comm.asp" -->
		<%
  
		'==================
		Template_Code_Now = Template_Code_trip		
		CreateFile thisTempCodePath,Template_Code_Now
		
 
end if
'=============================模板依赖标签不存在判断   end



sql2="select * from [SuccessWork] where private=0"
set rs2=server.createObject("ADODB.Recordset")
rs2.open sql2,db,1,1

'判断是否有信息
if not rs2.EOF then
	noMsg = 0
else
	noMsg = 1 '不存在
	UpPageNum = 1
	
end if

rs2.close
set rs2=nothing


for j=betweenPageBegin to UpPageNum

Template_Code=showFile(thisTempCodePath)
'只调用栏目独立HTML代码判断
callPageAllHTML "body:columnShow.works","完全独立页面代码.不调用其它任何依赖标签的内容"


'------循环开始
sql="select * from [SuccessWork] where private=0 order by  Index desc"
set rs=server.createObject("ADODB.Recordset")
rs.open sql,db,1,1


whichpage=j 
rs.pagesize=num
listnum=rs.RecordCount
totalpage=rs.pagecount
rs.absolutepage=whichpage
howmanyrecs=0

listcontent_case=""  '这里对下次标签内容的清空，重要！
'分页链接初始化
listcontent=""

%>
<!--#include file="../SuccessWork/classList.asp" -->
<%
		
		
do while not rs.eof and howmanyrecs<rs.pagesize

'++++++++++++++++++++++++++++++++++++++++++++++++内容页
	ID=rs("ID")
	Title=replace(rs("title"),chr(32),"")
	Title_show=gotTopic(Title,titleLimitLen)
	url=rs("url")
	ClassName=rs("ClassName")
    content=replace(replace(HTMLDecode(rs("worksIntro")),"<p>",""),"</p>","<br />")
	Image=replace(rs("preURL"),"../","")
	Subdate=rs("Subdate")
	siteURL=rs("siteURL")
	theBigImage=rs("hdImg_d")
	
	if callCommInfoChk("{#loopVar:works.Summary[200]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[200]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),200)
	if callCommInfoChk("{#loopVar:works.Summary[250]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[250]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),250)
	if callCommInfoChk("{#loopVar:works.Summary[300]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[300]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),300)
	if callCommInfoChk("{#loopVar:works.Summary[350]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[350]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),350)
	if callCommInfoChk("{#loopVar:works.Summary[400]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[400]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),400)
	if callCommInfoChk("{#loopVar:works.Summary[450]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[450]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),450)
	if callCommInfoChk("{#loopVar:works.Summary[500]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[500]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),500)
	if callCommInfoChk("{#loopVar:works.Summary[550]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[550]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),550)
	if callCommInfoChk("{#loopVar:works.Summary[600]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[600]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),600)
	if callCommInfoChk("{#loopVar:works.Summary[650]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[650]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),650)
	if callCommInfoChk("{#loopVar:works.Summary[700]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[700]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),700)
	if callCommInfoChk("{#loopVar:works.Summary[750]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[750]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),750)
	if callCommInfoChk("{#loopVar:works.Summary[800]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[800]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),800)
	if callCommInfoChk("{#loopVar:works.Summary[850]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[850]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),850)
	if callCommInfoChk("{#loopVar:works.Summary[900]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[900]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),900)
	if callCommInfoChk("{#loopVar:works.Summary[950]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[950]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),950)
	if callCommInfoChk("{#loopVar:works.Summary[1000]/}") = true then  thisSumLabel = "{#loopVar:works.Summary[1000]/}" : thisSumLabel_rep = CutStr(stripHTML(rs("worksIntro")),1000)
	
	postID=createBase64ID(replace(replace(url,MY_sitePath,""),classlistName,""))
	
	if instr(siteURL,"javascript:")>0 then siteURL = ""



	SmallOrginUrl=replace(replace(rs("preURL"),"../",""),""&MY_sec_uploadImgPath&"/?src=","")
	if FileExistsStatus(""&MY_sitePath&""&MY_sec_uploadSmallImgPath&"/?src=s_"&replace(SmallOrginUrl,MY_sitePath,"")&"") = -1 then
    imgSmallCreate(""&MY_sitePath&""&MY_uploadImgPath&"/"&SmallOrginUrl)
	end if

    '列表预览图外链判断
	if instr(rs("preURL"),"http://")>0 then
	   sSmallPath=rs("preURL")
	else
	   sSmallPath=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")
	end if
	
		
	
	

	
'分类获取
	sql_class="select * from [SuccessWorkClass] where ClassName='"&ClassName&"'"
	set rs_class=server.createObject("ADODB.Recordset")
	rs_class.open sql_class,db,1,1
	ClassID=rs_class("SuccessWorkClassID")
	ClassSrc="<a class=""sort"" href="""&MY_sitePath&"classlist/"&MY_createFolder_work&""&ClassID&"_default.html""  title="""&classname&""">"&classname&"</a>"&vbcrlf'类别链接
	rs_class.close
	set rs_class=nothing
	
	'分类ID
	listClassIDShow = "class-id-"&ClassID&""
	

	
	if url<>"" then urlS="<a href="""&url&""" target=""_blank"">"
	if url="" then urlS="<a href=""?thiswork=none"">"
	
	  '日期
	  t_year = year(Subdate)
	  t_month = dateMonthGoEnglish(month(Subdate))
	  t_day = day(Subdate)
	  
	  '奇偶class
	  if (howmanyrecs+1) mod 2 = 0 then partClass = "post-even" else partClass = "post-odd"
		
    listcontent_case= listcontent_case&loopTempLabelString("loopList:works","{#loopVar:works.t_year/}",""&t_year&"","{#loopVar:works.t_month/}",""&t_month&"","{#loopVar:works.t_day/}",""&t_day&"","{#loopVar:works.sSmallPath/}",""&sSmallPath&"","{#loopVar:works.Title/}",""&Title&"","{#loopVar:works.url/}",""&url&"","{#loopVar:works.classname/}",""&classname&"","{#loopVar:works.Subdate/}",""&Subdate&"","{#loopVar:works.content/}",""&content&"","{#loopVar:works.siteURL/}",""&siteURL&"","{#loopVar:works.bigImgSrc/}",""&theBigImage&"","{#loopVar:postID/}",""&postID&"","{#loopVar:works.n_month/}",""&month(Subdate)&"","{#loopVar:works.classname.link/}",""&ClassSrc&"",""&thisSumLabel&"",""&thisSumLabel_rep&"","{#loopVar:postClassType/}",""&partClass&"","{#loopVar:works.id/}",""&ID&"","{#loopVar:works.classID/}",""&listClassIDShow&"","","","","",0)



rs.movenext
howmanyrecs=howmanyrecs+1
loop

rs.close
set rs=nothing


'///////////////////////分页输出
AbsPage=""

urlPath="../list/"&classlistName&""
%>
<!--#include file="_list_page.asp" -->
<!--#include file="comm_getSideData.asp" -->
<%
listMode = ""


'生成文件
Template_Code=replace(Template_Code,"{#loopListContentShow:works.classList/}",classListCase)
Template_Code=replace(Template_Code,"{#loopListContentShow:works/}",listMode&listcontent_case&listcontent)
Template_Code=replace(Template_Code,"${classname}","")

filename="../../list/"&classlistName&"_"&totalpageShowCreate&".html"
'Seo
thisSeoPath=replace(replace(filename,"../../",MY_sitePath),"../",MY_sitePath)
Template_Code=replace(Template_Code,"${thisPageSEOURL}",thisSeoPath)	

totalpageShowCreate=totalpageShowCreate-1

'----------有数据
if noMsg = 0 then
	
	if totalpageShowCreate > 0 or totalpageShowCreate = 0 then  CreateFile filename,Template_Code&createData&DS_CopyrightVerificationInfo

	
end if

'判断有无内容
if totalpage<1 then 
response.Write "<span class=""OKinfo"">×出现错误→此栏目没有内容...</span><br>"
end if


next


'----------有数据
if noMsg = 0 then
	
	
CopyFile "../../list/"&classlistName&"_"&totalpage&".html","../../list/"&classlistName&"_default.html"
Template_Code_default=replace(showFile("../../list/"&classlistName&"_default.html"),classlistName&"_"&totalpage&".html",classlistName&"_default.html")
CreateFile "../../list/"&classlistName&"_default.html",Template_Code_default
		
	
end if


'----------没有数据
if noMsg = 1 then

	Template_Code_no=replace(Template_Code,"<span id=""pageShow""></span>", DY_LANG_102)
	
	CreateFile "../../list/"&classlistName&"_default.html",Template_Code_no&createData

end if



'页面缓存
CreateFile thisFilePath,now()

		
HTMLSERVE_BOTTOM()


end if


%>
