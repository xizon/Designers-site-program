<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<script>

$(function(){


	
	
	$("#slider-myvalue34").slider({ 
	    value: <%=MySiteValue34%>,
		step: 1, 
		max: 7,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#myvalue34").val(ui.value); 
			$(".txtShow-myvalue34").html(ui.value + "天");
		} 
	
	}); 
	
	$("#slider-myvalue58").slider({ 
	    value: <%=MySiteValue58%>,
		step: 1, 
		max: 20,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#myvalue58").val(ui.value); 
			$(".txtShow-myvalue58").html(ui.value + "字");
		} 
	
	});
	
	$("#slider-myvalue59").slider({ 
	    value: <%=MySiteValue59%>,
		step: 5, 
		max: 2000,
		min: 50,
		range: "min",
		slide: function(event, ui){ 
			$("#myvalue59").val(ui.value); 
			$(".txtShow-myvalue59").html(ui.value + "字");
		} 
	
	});
	
	$("#slider-myvalue3").slider({ 
	    value: <%=MySiteValue3%>,
		step: 5, 
		max: 250,
		min: 5,
		range: "min",
		slide: function(event, ui){ 
			$("#myvalue3").val(ui.value); 
			$(".txtShow-myvalue3").html(ui.value + "条/页");
		} 
	
	});
	
	
	$("#slider-myvalue37").slider({ 
	    value: <%=MySiteValue37%>,
		step: 4, 
		max: 45,
		min: 5,
		range: "min",
		slide: function(event, ui){ 
			$("#myvalue37").val(ui.value); 
			$(".txtShow-myvalue37").html(ui.value + "个");
		} 
	
	});
	
	$("#slider-myvalue39").slider({ 
	    value: <%=MySiteValue39%>,
		step: 25, 
		max: 10000,
		min: 50,
		range: "min",
		slide: function(event, ui){ 
			$("#myvalue39").val(ui.value); 
			$(".txtShow-myvalue39").html(ui.value + "KB");
		} 
	
	});
	
	$("#slider-myvalue13").slider({ 
	    value: <%=MySiteValue13%>,
		step: 25, 
		max: 50000,
		min: 200,
		range: "min",
		slide: function(event, ui){ 
			$("#myvalue13").val(ui.value); 
			$(".txtShow-myvalue13").html(ui.value + "KB");
		} 
	
	});
	
	$("#slider-myvalue71").slider({ 
	    value: <%=MySiteValue71%>,
		step: 10, 
		max: 1000,
		min: 10,
		range: "min",
		slide: function(event, ui){ 
			$("#myvalue71").val(ui.value); 
			$(".txtShow-myvalue71").html(ui.value + "组");
		} 
	
	});
	
	$("#slider-myvalue78").slider({ 
	    value: <%=MySiteValue78%>,
		step: 500, 
		max: 10000,
		min: 1000,
		range: "min",
		slide: function(event, ui){ 
			$("#myvalue78").val(ui.value); 
			$(".txtShow-myvalue78").html(ui.value/1000 + "秒");
		} 
	
	});
	
	
	$("#slider-myvalue5").slider({ 
	    value: <%=MySiteValue5%>,
		step: 1, 
		max: 50,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#myvalue5").val(ui.value); 
			$(".txtShow-myvalue5").html(ui.value + "条");
		} 
	
	});
	
	
	
	$("#slider-myvalue6").slider({ 
	    value: <%=MySiteValue6%>,
		step: 1, 
		max: 30,
		min: 5,
		range: "min",
		slide: function(event, ui){ 
			$("#myvalue6").val(ui.value); 
			$(".txtShow-myvalue6").html(ui.value);
		} 
	
	});
	
	
	$("#slider-myvalue52").slider({ 
	    value: <%=MySiteValue52%>,
		step: 5, 
		max: 100,
		min: 5,
		range: "min",
		slide: function(event, ui){ 
			$("#myvalue52").val(ui.value); 
			$(".txtShow-myvalue52").html(ui.value);
		} 
	
	});
	
	
	$("#slider-myvalue33").slider({ 
	    value: <%=MySiteValue33%>,
		step: 1, 
		max: 500,
		min: 10,
		range: "min",
		slide: function(event, ui){ 
			$("#myvalue33").val(ui.value); 
			$(".txtShow-myvalue33").html(ui.value + "个");
		} 
	
	});
	
	
	
	$("#slider-myvalue81").slider({ 
	    value: <%=MySiteValue81%>,
		step: 10, 
		max: 50,
		min: 10,
		range: "min",
		slide: function(event, ui){ 
			$("#myvalue81").val(ui.value); 
			$(".txtShow-myvalue81").html(ui.value + "个");
		} 
	
	});
	
	$("#slider-myvalue31").slider({ 
	    value: <%=MySiteValue31%>,
		step: 0.05, 
		max: 30,
		min: 0.00001,
		range: "min",
		slide: function(event, ui){ 
			$("#myvalue31").val(ui.value); 
			$(".txtShow-myvalue31").html(ui.value + "天");
		} 
	
	});
	
	
	
	

});


<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=侧边栏设置", "link1=<%=MY_sitePath%>admin/sectionConfig/edit_sidebar.asp", "title2=SEO设置", "link2=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-seo", "title3=上传设置", "link3=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-upload", "title4=标签tags设置", "link4=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-tags", "title5=自动生成设置", "link5=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-autoCreate", "title6=评论留言设置", "link6=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-msg", "title7=分页设置", "link7=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-pages",  "title8=缩略图和剪裁设置", "link8=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-img", "title9=列表生成参数设置", "link9=<%=MY_sitePath%>admin/website/setcreateList.asp" , "title10=API数据接口设置", "link10=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-apiset", "title11=其它设置", "link11=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-other");
			
			 

	   }
	);
	
	
	var limitNum = 100;
	$('#keyWordsIn').inputlimiter({
		limit: limitNum,
		boxId: 'limitingtext',
		boxAttach: false
	});	
		
	document.getElementById("limitingtext").innerHTML =  "0/" + limitNum;

	
	
});

<!-- form ui[js]  end -->


//表单判断
function checkForm(){
	
	exp=/[^0-9()-]/g; 
	exp2=/.*[\u4e00-\u9fa5]+.*$/; 
	exp3=/^[\u4e00-\u9fa5\da-zA-Z\-\_]+$/;
		
	if(exp2.test(document.formM.myvalue1.value) || exp2.test(document.formM.myvalue28.value)){
		ChkFormTip_page("不能使用中文",0);
		return false;
	}

	if(document.formM.myvalue28.value != "" && document.formM.myvalue28.value.search(exp3) == -1){
		ChkFormTip_page("不能包含特殊符号",0);
		return false;
	}
	if(document.formM.myvalue69.value != "" && document.formM.myvalue69.value.search(exp)  !=  -1){
		ChkFormTip_page("必须为数字类型",0);
		return false;
	}
	if(document.formM.myvalue64.value != "" && document.formM.myvalue64.value.search(exp)  !=  -1){
		ChkFormTip_page("必须为数字类型",0);
		return false;
	}
	if(document.formM.myvalue65.value != "" && document.formM.myvalue65.value.search(exp)  !=  -1){
		ChkFormTip_page("必须为数字类型",0);
		return false;
	}
	if(document.formM.myvalue69.value != "" && document.formM.myvalue69.value.search(exp)  !=  -1){
		ChkFormTip_page("必须为数字类型",0);
		return false;
	}
	if(document.formM.myvalue60.value != "" && document.formM.myvalue60.value.search(exp)  !=  -1){
		ChkFormTip_page("必须为数字类型",0);
		return false;
	}
	if(document.formM.myvalue66.value != "" && document.formM.myvalue66.value.search(exp)  !=  -1){
		ChkFormTip_page("必须为数字类型",0);
		return false;
	}
	if(document.formM.myvalue67.value != "" && document.formM.myvalue67.value.search(exp)  !=  -1){
		ChkFormTip_page("必须为数字类型",0);
		return false;
	}
	if(document.formM.myvalue68.value != "" && document.formM.myvalue68.value.search(exp)  !=  -1){
		ChkFormTip_page("必须为数字类型",0);
		return false;
	}
	if(document.formM.myvalue69.value == "" || document.formM.myvalue60.value == "" || document.formM.myvalue66.value == "" || document.formM.myvalue67.value == "" || document.formM.myvalue68.value == "" ){
		ChkFormTip_page("此项不能留空",0);
		return false;
	}
	
	//ok
	ChkFormTip_page("保存成功",1);
	return true;
		
}



</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 网站设置 | 全局参数设置
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
                <!-- 表单域  -->
                <div id="contentShow">
                
                        <form method="post" name="formM"  action="action.asp?action=siteConfig" target="actionPage" onSubmit="javascript:return checkForm();">

                        <table width="100%" class="setting" cellspacing="0">
                            
                            <!-- 标题 -->                        
                            <thead class="setHead">
                                <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2"></th>
                                </tr>
                                
                            </thead>
                            
                            
                          <!-- 底部 -->
                            <tfoot>
                            
                                <tr>
                                    <td colspan="3">
                                       <div class="btnBox">
                                       
                                           <input type="submit" value="保存" class="sub">
                                           
                                       </div>
                        
                                    </td>
                                </tr>
                            </tfoot>
                            
                            
                            
                            <!-- +++++++  内容   begin +++++++  -->
                            
                            <tbody>
                            

                               <input   name="myvalue0" type="hidden"  value="<%=MySiteValue0%>" />
                               <input   name="myvalue2" type="hidden"  value="<%=MySiteValue2%>" />
                               <input   name="myvalue4" type="hidden"  value="<%=MySiteValue4%>" />
                               <input   name="myvalue7" type="hidden"  value="<%=MySiteValue7%>" />
                               <input   name="myvalue8" type="hidden"  value="<%=MySiteValue8%>" />
                               <input   name="myvalue9" type="hidden"  value="<%=MySiteValue9%>" />
                               <input   name="myvalue10" type="hidden"  value="<%=MySiteValue10%>" />
                               <input   name="myvalue11" type="hidden"  value="<%=MySiteValue11%>" />
                               <input   name="myvalue12" type="hidden"  value="<%=MySiteValue12%>" />
                               <input   name="myvalue15" type="hidden"  value="<%=MySiteValue15%>" />
                               <input   name="myvalue16" type="hidden"  value="<%=MySiteValue16%>" />
                               <input   name="myvalue17" type="hidden"  value="<%=MySiteValue17%>" />
                               <input   name="myvalue18" type="hidden"  value="<%=MySiteValue18%>" />
                               <input   name="myvalue19" type="hidden"  value="<%=MySiteValue19%>" />
                               <input   name="myvalue20" type="hidden"  value="<%=MySiteValue20%>" />
                               <input   name="myvalue21" type="hidden"  value="<%=MySiteValue21%>" />
                               <input   name="myvalue22" type="hidden"  value="<%=MySiteValue22%>" />
                               <input   name="myvalue23" type="hidden"  value="<%=MySiteValue23%>" />
                               <input   name="myvalue24" type="hidden"  value="<%=MySiteValue24%>" />
                               <input   name="myvalue27" type="hidden"  value="<%=MySiteValue27%>" />
                               <input   name="myvalue29" type="hidden"  value="<%=MySiteValue29%>" />
                               <input   name="myvalue30" type="hidden"  value="<%=MySiteValue30%>" />
                               <input   name="myvalue36" type="hidden"  value="<%=MySiteValue36%>" />
                               <input   name="myvalue38" type="hidden"  value="<%=MySiteValue38%>" />
                               <input   name="myvalue41" type="hidden"  value="<%=MySiteValue41%>" />
                               <input   name="myvalue42" type="hidden"  value="<%=MySiteValue42%>" />
                               <input   name="myvalue45" type="hidden"  value="<%=MySiteValue45%>" />
                               <input   name="myvalue46" type="hidden"  value="<%=MySiteValue46%>" />
                               <input   name="myvalue48" type="hidden"  value="<%=MySiteValue48%>" />
                               <input   name="myvalue50" type="hidden"  value="<%=MySiteValue50%>" />
                               <input   name="myvalue51" type="hidden"  value="<%=MySiteValue51%>" />
                               <input   name="myvalue61" type="hidden"  value="<%=MySiteValue61%>" />
                               <input   name="myvalue62" type="hidden"  value="<%=MySiteValue62%>" />
                               <input   name="myvalue63" type="hidden"  value="<%=MySiteValue63%>" />
                               <input   name="myvalue72" type="hidden"  value="<%=MySiteValue72%>" />
                               <input   name="myvalue73" type="hidden"  value="<%=MySiteValue73%>" />
                               <input   name="myvalue74" type="hidden"  value="<%=MySiteValue74%>" />
                               <input   name="myvalue75" type="hidden"  value="<%=MySiteValue75%>" />
                               <input   name="myvalue79" type="hidden"  value="<%=MySiteValue79%>" />
                               <input   name="myvalue80" type="hidden"  value="<%=MySiteValue80%>" />
                               
							  <%if CheckObjInstalled("Persits.Jpeg")=false then%>
                              <input   name="myvalue54" type="hidden" value="1" />
                              <%else%>
                              <input   name="myvalue54" type="hidden"  value="0" />
                              <%end if%>
                                                       
  
           
                                <thead class="setHead" id="setting-seo">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">SEO设置</th>
                                    </tr> 
                                </thead> 
                                
                                <tr>
                                    <td class="title">通用信息js调用<a href="javascript:void(0)" class="help" data-info="开启后以下信息将使用js输出,实现各页面即时同步,但会一定程度降低SEO效果"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2">
                                       <div class="on_off-box">
                                           <span  class="on_off">
                                              <%if MySiteValue35="1" then%>
                                              <input type="checkbox" name="myvalue35"  value="1" checked="checked"/>
                                              <%else%>	 
                                              <input type="checkbox" name="myvalue35"  value="1"/>
                                              <%end if%>
                                            </span>
                                       </div>
                                      
                                    </td>
                                </tr>  
                                
                                <tr>
                                    <td class="title">自定义内容页文件名<a href="javascript:void(0)" class="help" data-info="生成文章内容页可自定义html文件名,否则使用文件夹无后缀URL"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2">
                                       <div class="on_off-box">
                                           <span  class="on_off">
                                              <%if MySiteValue47="1" then%>
                                              <input type="checkbox" name="myvalue47"  value="1" checked="checked"/>
                                              <%else%>	 
                                              <input type="checkbox" name="myvalue47"  value="1"/>
                                              <%end if%>
                                            </span>
                                       </div>
                                      
                                    </td>
                                </tr>  
                                
                                <tr>
                                    <td class="title">文章内容关键词高亮：</td>
                                    <td colspan="2">
                                       <div class="on_off-box">
                                           <span  class="on_off">
                                              <%if MySiteValue43="1" then%>
                                              <input type="checkbox" name="myvalue43"  value="1" checked="checked"/>
                                              <%else%>	 
                                              <input type="checkbox" name="myvalue43"  value="1"/>
                                              <%end if%>
                                            </span>
                                       </div>
                                      
                                    </td>
                                </tr>  
                                
                                
                                <tr>
                                    <td class="title">Meta标签-关键词：</td>
                                    <td colspan="2" class="ui-element">
                                    
                                      <textarea name="myvalue55" id="keyWordsIn" cols="70" rows="3" onclick="this.rows='5'" ><%=MySiteValue55%></textarea>&nbsp;&nbsp;<span id="limitingtext"></span>

                                      
                                    </td>
                                </tr> 
                                
                                <tr>
                                    <td class="title">Meta标签-作者：</td>
                                    <td colspan="2" class="ui-element">
                                    
                                      <input name="myvalue56"  type="text"   size="50" value="<%=MySiteValue56%>"/>

                                      
                                    </td>
                                </tr> 
                                
                                <tr>
                                    <td class="title">Meta标签-版权：</td>
                                    <td colspan="2" class="ui-element">
                                    
                                      <input name="myvalue57"  type="text"   size="50" value="<%=MySiteValue57%>"/>

                                      
                                    </td>
                                </tr> 
                                
                                         
                     
                                <thead class="setHead" id="setting-autoCreate">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">自动生成设置</th>
                                    </tr> 
                                </thead>   
                                
                                <tr>
                                    <td class="title">自动生成页面时间<a href="javascript:void(0)" class="help" data-info="超过此时间,系统将自动清除缓存,只要相关页面被访问一次即可更新<br>如果是自动批量更新分页,您需要到<strong>[手动静态生成]</strong>导航里开启批量列表生成状态,否则只自动生成最新的一页列表页"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="myvalue34" name="myvalue34" value="<%=MySiteValue34%>"/>  
                                        <span class="txtShow txtShow-myvalue34"><%=MySiteValue34%>天</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-myvalue34"></div></td>
                                </tr>  
                                
                                
                                
                                <thead class="setHead" id="setting-msg">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">评论留言设置</th>
                                    </tr> 
                                </thead>      
                                
                                
                                
                                
                                <tr>
                                    <td class="title">审核之后才会显示：</td>
                                    <td colspan="2">
                                       <div class="on_off-box">
                                           <span  class="on_off">
                                              <%if MySiteValue32="1" then%>
                                              <input type="checkbox" name="myvalue32"  value="1" checked="checked"/>
                                              <%else%>	 
                                              <input type="checkbox" name="myvalue32"  value="1"/>
                                              <%end if%>
                                            </span>
                                       </div>
                                      
                                    </td>
                                </tr>  
                                
                                
                                <tr>
                                    <td class="title">关闭评论留言功能<a href="javascript:void(0)" class="help" data-info="1.评论功能将被直接关闭<br>2.留言本功能则需要注册才能发表"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2">
                                       <div class="on_off-box">
                                           <span  class="on_off">
                                              <%if MySiteValue49="1" then%>
                                              <input type="checkbox" name="myvalue49"  value="1" checked="checked"/>
                                              <%else%>	 
                                              <input type="checkbox" name="myvalue49"  value="1"/>
                                              <%end if%>
                                            </span>
                                       </div>
                                      
                                    </td>
                                </tr>  
                                
                                
                                <tr>
                                    <td class="title">留言评论最少字数：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="myvalue58" name="myvalue58" value="<%=MySiteValue58%>"/>  
                                        <span class="txtShow txtShow-myvalue58"><%=MySiteValue58%>字</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-myvalue58"></div></td>
                                </tr>  
                                
                                
                                <tr>
                                    <td class="title">留言评论最多字数：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="myvalue59" name="myvalue59" value="<%=MySiteValue59%>"/>  
                                        <span class="txtShow txtShow-myvalue59"><%=MySiteValue59%>字</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-myvalue59"></div></td>
                                </tr>  
                                
                                
                                
                                <thead class="setHead" id="setting-pages">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">分页设置</th>
                                    </tr> 
                                </thead>
                                
                                
                                <tr>
                                    <td class="title">不使用数字分页<a href="javascript:void(0)" class="help" data-info="前HTML页面是否使用数字分页"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2">
                                       <div class="on_off-box">
                                           <span  class="on_off">
                                              <%if MySiteValue26="1" then%>
                                              <input type="checkbox" name="myvalue26"  value="1" checked="checked"/>
                                              <%else%>	 
                                              <input type="checkbox" name="myvalue26"  value="1"/>
                                              <%end if%>
                                            </span>
                                       </div>
                                      
                                    </td>
                                </tr>  
                                
                                
                                <tr>
                                    <td class="title">数字分页页码数量<a href="javascript:void(0)" class="help" data-info="每个页面可以点击多少个页数链接"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="myvalue37" name="myvalue37" value="<%=MySiteValue37%>"/>  
                                        <span class="txtShow txtShow-myvalue37"><%=MySiteValue37%>个</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-myvalue37"></div></td>
                                </tr>          
                                
                                <tr>
                                    <td class="title">后台列表管理数量：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="myvalue3" name="myvalue3" value="<%=MySiteValue3%>"/>  
                                        <span class="txtShow txtShow-myvalue3"><%=MySiteValue3%>条/页</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-myvalue3"></div></td>
                                </tr>  
                                
                                
                                
   
                                
                                <thead class="setHead" id="setting-upload">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">上传设置</th>
                                    </tr> 
                                </thead>   
                                
                                
                                <tr>
                                    <td class="title">上传文件前缀名：</td>
                                    <td colspan="2" class="ui-element">
                                      <input name="myvalue28"  type="text"   size="20" value="<%=MySiteValue28%>"/>

                                    </td>
                                </tr> 
                                
                                
                                <tr>
                                    <td class="title">会员上传限制：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="myvalue39" name="myvalue39" value="<%=MySiteValue39%>"/>  
                                        <span class="txtShow txtShow-myvalue39"><%=MySiteValue39%>KB</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-myvalue39"></div></td>
                                </tr> 
                                
                                
                                <tr>
                                    <td class="title">管理员上传限制：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="myvalue13" name="myvalue13" value="<%=MySiteValue13%>"/>  
                                        <span class="txtShow txtShow-myvalue13"><%=MySiteValue13%>KB</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-myvalue13"></div></td>
                                </tr> 
                                
                                
                                <tr>
                                    <td class="title">会员上传类型<a href="javascript:void(0)" class="help" data-info="使用 | 隔开"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" class="ui-element">
                                      <input name="myvalue40"  type="text"   size="50" value="<%=MySiteValue40%>"/>

                                    </td>
                                </tr> 
                                
                                
                                <tr>
                                    <td class="title">管理员上传类型<a href="javascript:void(0)" class="help" data-info="使用 | 隔开"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" class="ui-element">
                                      <input name="myvalue14"  type="text"   size="50" value="<%=MySiteValue14%>"/>

                                    </td>
                                </tr> 

                                
                                
                                
                                <thead class="setHead" id="setting-img">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">缩略图和剪裁设置</th>
                                    </tr> 
                                </thead>
                                
                                
                                <tr>
                                    <td class="title">列表图片剪裁功能<a href="javascript:void(0)" class="help" data-info="文章、案例作品列表主题图片剪裁<br>注意：如果您使用了<strong>安全宝</strong>云服务，可能剪裁功能会失效"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2">
                                       <div class="on_off-box">
                                           <span  class="on_off">
                                              <%if MySiteValue53="1" then%>
                                              <input type="checkbox" name="myvalue53"  value="1" checked="checked"/>
                                              <%else%>	 
                                              <input type="checkbox" name="myvalue53"  value="1"/>
                                              <%end if%>
                                            </span>
                                       </div>
                                      
                                    </td>
                                </tr>  
                                
                                
                                <tr>
                                    <td class="title">文章图片剪裁高度：</td>
                                    <td colspan="2" class="ui-element">
                                      <input name="myvalue69"  type="text"   size="20" value="<%=MySiteValue69%>"/>

                                    </td>
                                </tr> 
                                
                                <tr>
                                    <td class="title">文章图片剪裁宽度：</td>
                                    <td colspan="2" class="ui-element">
                                      <input name="myvalue60"  type="text"   size="20" value="<%=MySiteValue60%>"/>&nbsp;图片默认剪裁 = 缩略图宽度

                                    </td>
                                </tr> 
                                
                                <tr>
                                    <td class="title">插件缩略图高度<a href="javascript:void(0)" class="help" data-info="使用其他带有<strong>图片展示功能插件</strong>时的生成的缩略图（如：酷站列表图,作品案例/产品内容页缩略图）"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" class="ui-element">
                                      <input name="myvalue65"  type="text"   size="20" value="<%=MySiteValue65%>"/>

                                    </td>
                                </tr> 
                                
                                <tr>
                                    <td class="title">插件缩略图宽度<a href="javascript:void(0)" class="help" data-info="使用其他带有<strong>图片展示功能插件</strong>时的生成的缩略图（如：酷站列表图,作品案例/产品内容页缩略图）"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" class="ui-element">
                                      <input name="myvalue64"  type="text"   size="20" value="<%=MySiteValue64%>"/>

                                    </td>
                                </tr> 
                                  
                                
                                <thead class="setHead" id="setting-tags">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">标签tags设置</th>
                                    </tr> 
                                </thead>  
                                
                                
                                <tr>
                                    <td class="title">最新标签云显示数<a href="javascript:void(0)" class="help" data-info="标签云插件输出的数量"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="myvalue71" name="myvalue71" value="<%=MySiteValue71%>"/>  
                                        <span class="txtShow txtShow-myvalue71"><%=MySiteValue71%>组</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-myvalue71"></div></td>
                                </tr>  
                                

                                
                                <thead class="setHead" id="setting-apiset">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">API数据接口设置</th>
                                    </tr> 
                                </thead>  
                                
                                  
                                <tr>
                                    <td class="title">API接口信息查询密匙<a href="javascript:void(0)" class="help" data-info="API应用接口时的信息查询密匙，由网站主指定"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" class="ui-element">
                                      <input name="myvalue76"  type="text"   size="30" value="<%=MySiteValue76%>"/>

                                    </td>
                                </tr> 
                                  
                                  
                                <tr>
                                    <td class="title">API接口信息添加密匙<a href="javascript:void(0)" class="help" data-info="API应用接口时的信息添加密匙，由网站主指定"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" class="ui-element">
                                      <input name="myvalue77"  type="text"   size="30" value="<%=MySiteValue77%>"/>

                                    </td>
                                </tr>   
                                
                                <tr>
                                    <td class="title">数据接入API_KEY<a href="javascript:void(0)" class="help" data-info="当您使用社会化登录，社会化评论接入到网站同步网站时，就需要API-KEY<br>API_KEY由<strong>API接口信息密匙</strong>对应加密生成"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" class="ui-element">
                                      <%=MD5(MySiteValue76,5)&MD5(MySiteValue77,5)%>

                                    </td>
                                </tr>                             
                                  
                                
                                
                                <thead class="setHead" id="setting-other">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">其它设置</th>
                                    </tr> 
                                </thead>

                                <tr>
                                    <td class="title">网站认证链接<a href="javascript:void(0)" class="help" data-info="<strong>【重要】</strong>填写您的网站地址,需要包含http://的前缀,如果安装在文件夹下请写上文件夹名字，如http://www.c945.com/blog ,最后不能打斜杠"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" class="ui-element">
                                      <input name="myvalue25"  type="text"   size="50" value="<%=MySiteValue25%>"/>&nbsp;&nbsp;<span class="backStage_star">重要! 系统判断您应该填写：<span style="color:#333"><%=clearRightString("http://"&Request.ServerVariables("Server_Name")&""&MY_sitePath&"","/")%></span></span>

                                    </td>
                                </tr> 


                                <tr>
                                    <td class="title">鼠标右键导航功能<a href="javascript:void(0)" class="help" data-info="前台页面中的快捷导航"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2">
                                       <div class="on_off-box">
                                           <span  class="on_off">
                                              <%if MySiteValue44="1" then%>
                                              <input type="checkbox" name="myvalue44"  value="1" checked="checked"/>
                                              <%else%>	 
                                              <input type="checkbox" name="myvalue44"  value="1"/>
                                              <%end if%>
                                            </span>
                                       </div>
                                      
                                    </td>
                                </tr>         
                                
                                
                                <tr>
                                    <td class="title">水印文字：</td>
                                    <td colspan="2" class="ui-element">
                                      <input name="myvalue1"  type="text"   size="30" value="<%=MySiteValue1%>"/>

                                    </td>
                                </tr> 
                                
                                
                                
                                <tr>
                                    <td class="title">水印文字大小：</td>
                                    <td colspan="2" class="ui-element">
                                      <input name="myvalue66"  type="text"   size="30" value="<%=MySiteValue66%>"/>

                                    </td>
                                </tr> 
                                
                                
                                <tr>
                                    <td class="title">水印X坐标<a href="javascript:void(0)" class="help" data-info="X方向随机数"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" class="ui-element">
                                      <input name="myvalue67"  type="text"   size="30" value="<%=MySiteValue67%>"/>

                                    </td>
                                </tr> 
                                
                                
                                <tr>
                                    <td class="title">水印Y坐标<a href="javascript:void(0)" class="help" data-info="Y方向随机数"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" class="ui-element">
                                      <input name="myvalue68"  type="text"   size="30" value="<%=MySiteValue68%>"/>

                                    </td>
                                </tr> 
                                          
                                          
                                          
                                <tr id="bannerTime">
                                    <td class="title">Banner切换时间：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="myvalue78" name="myvalue78" value="<%=MySiteValue78%>"/>  
                                        <span class="txtShow txtShow-myvalue78"><%=MySiteValue78/1000%>秒</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-myvalue78"></div></td>
                                </tr>        
                                
                                
                                
                                
                                <tr id="linkCompositor">
                                    <td class="title">友情链接自定排序<a href="javascript:void(0)" class="help" data-info="关闭则为随机排序"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2">
                                       <div class="on_off-box">
                                           <span  class="on_off">
                                              <%if MySiteValue70="1" then%>
                                              <input type="checkbox" name="myvalue70"  value="1" checked="checked"/>
                                              <%else%>	 
                                              <input type="checkbox" name="myvalue70"  value="1"/>
                                              <%end if%>
                                            </span>
                                       </div>
                                      
                                    </td>
                                </tr>      
                                
                                <tr>
                                    <td class="title">友情链接显示数量<a href="javascript:void(0)" class="help" data-info="<strong>首页的侧边栏或者独立开来的内容区域</strong>链接的最多显示数量<br>管理友情链接时同时指定[首页显示]后生效"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="myvalue33" name="myvalue33" value="<%=MySiteValue33%>"/>  
                                        <span class="txtShow txtShow-myvalue33"><%=MySiteValue33%>个</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-myvalue33"></div></td>
                                </tr>  
                                
                                 
                                                      
                                <tr>
                                    <td class="title">公告输出数量限制：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="myvalue5" name="myvalue5" value="<%=MySiteValue5%>"/>  
                                        <span class="txtShow txtShow-myvalue5"><%=MySiteValue5%>条</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-myvalue5"></div></td>
                                </tr> 
                                
                                
                                  
                                  
                                <tr>
                                    <td class="title">验证码清晰度：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="myvalue6" name="myvalue6" value="<%=MySiteValue6%>"/>  
                                        <span class="txtShow txtShow-myvalue6"><%=MySiteValue6%></span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-myvalue6"></div></td>
                                </tr>  
                                  
                                  
                                  
                                <tr>
                                    <td class="title">验证码大小：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="myvalue52" name="myvalue52" value="<%=MySiteValue52%>"/>  
                                        <span class="txtShow txtShow-myvalue52"><%=MySiteValue52%></span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-myvalue52"></div></td>
                                </tr>  
                                  
                                  
                                <tr>
                                    <td class="title">自定义最多页面数<a href="javascript:void(0)" class="help" data-info="使用<strong>模板依赖标签</strong>自定义页面时的最多页面数"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="myvalue81" name="myvalue81" value="<%=MySiteValue81%>"/>  
                                        <span class="txtShow txtShow-myvalue81"><%=MySiteValue81%>个</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-myvalue81"></div></td>
                                </tr>  
                                  
                                  
                                  
                                <tr>
                                    <td class="title">动态提示窗口定时<a href="javascript:void(0)" class="help" data-info="首页及其后台右下角的动态提示窗口,保留最新动态的时间周期"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="myvalue31" name="myvalue31" value="<%=MySiteValue31%>"/>  
                                        <span class="txtShow txtShow-myvalue31"><%=MySiteValue31%>天</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-myvalue31"></div></td>
                                </tr>  
                                
                                  
                               <!-- +++++++  内容   end +++++++  -->
                        
                        
                            </tbody>
                            
                        </table>
                        
                        
                      </form>
 
                                
                </div>
     
                
              
              </div>      
         </div>       



<!--========================================主内容区  end ===================================== -->  

    
<!--#include file="../_comm/page_bottom.asp" -->



