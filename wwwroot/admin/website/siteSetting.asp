<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<!--#include file="../function/imgAction.asp" -->
<!--#include file="../function/imgGetWH.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
set rs =server.createobject("adodb.recordset")
sql="select * from [website]"
rs.open sql,db,1,1

if rs("age") = "-" or rs("age") = "0" then thisAge = "1" else thisAge = rs("age")

%>

<style>
.logoBox a{color:#333}
.imgs img{ border:1px #999 solid}
</style>

<script>

$(function(){

	
	$("#slider").slider({ 
	    value: <%=thisAge%>,
		step: 1, 
		max: 120,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#age").val(ui.value); 
			$(".txtShow").html(ui.value);
		} 
	
	}); 

	

});


<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=网站信息/Logo设置", "link1=<%=MY_sitePath%>admin/website/siteSetting.asp", "title2=管理员帐号设置", "link2=<%=MY_sitePath%>admin/website/change_pwd.asp");

	   }
	);
	

	
	var limitNum2 = 80;
	$('#SiteName').inputlimiter({
		limit: limitNum2,
		boxId: 'limitingtext2',
		boxAttach: false
	});	
	
	var limitNum = 200;
	$('#detail_motto').inputlimiter({
		limit: limitNum,
		boxId: 'limitingtext',
		boxAttach: false
	});	
		
		
	document.getElementById("limitingtext").innerHTML =  "0/" + limitNum;
	document.getElementById("limitingtext2").innerHTML =  "0/" + limitNum2;	
	
	
	
});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 网站设置 | 网站信息/Logo设置
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
                <!-- 表单域  -->
                <div id="contentShow">
                
                        <form method="post" action="action.asp?action=edit" target="actionPage" onSubmit="javascript:return ChkFormTip_page('保存成功',1);">

                        <table width="100%" class="setting" cellspacing="0">
                            
                            <!-- 标题 -->                        
                            <thead class="setHead">
                                <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2"></th>
                                </tr>
                                
                            </thead>
                            
                            
                          <!-- 底部 -->
                            <tfoot>
                            
                                <tr>
                                    <td colspan="3">
                                       <div class="btnBox">
                                       
                                           <input type="submit" value="保存" class="sub">
                                           
                                       </div>
                        
                                    </td>
                                </tr>
                            </tfoot>
                            
                            
                            
                            <!-- +++++++  内容   begin +++++++  -->
                            
                            <tbody>
                                
                                <textarea name="blogTitle" style="display:none"><%=HTMLDecode(rs("blogTitle"))%></textarea>
                                <input name="netname"  type="hidden" value="<%=rs("netname")%>"/>
           
                                <thead class="setHead">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">网站信息设置</th>
                                    </tr> 
                                </thead> 
                                
                                
                                <tr>
                                    <td class="title">网站名称/主标题<a href="javascript:void(0)" class="help" data-info="用于部分页面的<strong>title标题</strong><br>用于各页面的通用信息标签<strong>${siteName}</strong>"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" >
                                    
                                        <input name="SiteName" id="SiteName" type="text" size="75"  value="<%=rs("SiteName")%>"/>&nbsp;&nbsp;<a href="<%=MY_sitePath%>admin/website/siteConfig.asp#setting-seo" class="help" data-info="设置网站的关键词,静态化调用等数据">【SEO设置】</a>&nbsp;&nbsp;<span id="limitingtext2"></span>

                                      
                                    </td>
                                </tr>  
                            
                                <tr>
                                    <td class="title">网站副标题<a href="javascript:void(0)" class="help" data-info="用于各页面的通用信息标签<strong>${siteSubName}</strong>"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" >
                                    
                                        <input name="detail_work" type="text" size="75"  value="<%=rs("detail_work")%>"/>

                                      
                                    </td>
                                </tr>  
                            
                            
                                <tr>
                                    <td class="title">网站描述<a href="javascript:void(0)" class="help" data-info="用于部分页面的<strong>meta标签的description属性</strong><br>用于各页面的通用信息标签<strong>${detail_motto}</strong>"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2">
                                    
                                        <textarea name="detail_motto" id="detail_motto" cols="70" rows="2"  ><%=HTMLDecode(rs("detail_motto"))%></textarea>&nbsp;&nbsp;<span id="limitingtext"></span>

                                      
                                    </td>
                                </tr>  
                                
                                <tr>
                                    <td class="title">网站底部信息<a href="javascript:void(0)" class="help" data-info="用于各页面的通用信息标签<strong>${copyright}</strong><br>您也可以把外站的统计代码复制到这里,对网站统计做一个统计接口<br>支持HTML和JS代码"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2">
                                    
                                        <textarea name="copyright" cols="70" rows="5" onclick="this.rows='8'" ><%=rs("copyright")%></textarea>

                                      
                                    </td>
                                </tr>  
                                
                                <tr onMouseOver="tipShow('.tipsFloatWin-upload-logo1',0)" onMouseOut="tipShow('.tipsFloatWin-upload-logo',1)">
                                    <td class="title">网站Logo<a href="javascript:void(0)" class="help" data-info="如果您使用jpg,gif格式的图片，则需要相应修改<strong>template/</strong>模板文件夹下的通用html模板页文件的logo图片地址"><img align="absmiddle" src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" class="logoBox">
                                    
                                        <a href="<%=MY_sitePath%>template/img/logo.png" target="_blank"><span id="objlogoComm_imgShow"><img align="absmiddle" src="<%=MY_sitePath%>template/img/logo.png" height="60" style="border:#C6C6C6 solid 1px;margin-bottom:10px;">&nbsp;&nbsp;(默认logo只支持<strong> png </strong>格式)</span></a>
<div class="tipsFloatWin-upload-logo tipsFloatWin-upload-logo1 tipsFloatWin-upload-min"><iframe src=../upload/upload_sameNameFile.html?fileID=logoComm&upPath=<%=MY_sitePath%>template/img/&fileSaveName=logo class=backStage_uploadFrame allowTransparency=true  frameborder=0 scrolling=No></iframe><br>原图尺寸：<span class=backStage_star><%=getImgWH(""&MY_sitePath&"template/img/logo.png","w")%>宽×<%=getImgWH(""&MY_sitePath&"template/img/logo.png","h")%>高</span>,&nbsp;&nbsp;<strong>注意：在线上传更新只支持.png格式,名字不限</strong></div>
                                      
                                    </td>
                                </tr>
                                
                                
                                <tr onMouseOver="tipShow('.tipsFloatWin-upload-logo2',0)" onMouseOut="tipShow('.tipsFloatWin-upload-logo',1)">
                                    <td class="title">手机版Logo：</td>
                                    <td colspan="2" class="logoBox">
                                    
                                        <a href="<%=MY_sitePath%>phone/_style/img/phone_logo.png" target="_blank"><span id="objlogoPhone_imgShow"><img align="absmiddle" src="<%=MY_sitePath%>phone/_style/img/phone_logo.png" height="42" style="border:#C6C6C6 solid 1px;margin-bottom:10px;">&nbsp;&nbsp;(手机版logo只支持<strong> png </strong>格式)</span></a>
<div class="tipsFloatWin-upload-logo tipsFloatWin-upload-logo2 tipsFloatWin-upload-min"><iframe src=../upload/upload_sameNameFile.html?fileID=logoPhone&upPath=<%=MY_sitePath%>phone/_style/img/&fileSaveName=phone_logo class=backStage_uploadFrame allowTransparency=true  frameborder=0 scrolling=No></iframe><br>原图尺寸：<span class=backStage_star><%=getImgWH(""&MY_sitePath&"phone/_style/img/phone_logo.png","w")%>宽×<%=getImgWH(""&MY_sitePath&"phone/_style/img/phone_logo.png","h")%>高</span>,&nbsp;&nbsp;<strong>注意：在线上传更新只支持.png格式,名字不限</strong></div>
                                      
                                    </td>
                                </tr>
                
                                
                                <thead class="setHead">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">关于我们信息设置</th>
                                    </tr> 
                                </thead> 

           
                                <tr>
                                    <td class="title">联系方式简述<a href="javascript:void(0)" class="help" data-info="用于各页面的通用信息标签<strong>${detail_conn}</strong>"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2">
                                    
                                      <textarea name="detail_conn" cols="50" rows="5" onclick="this.rows='8'" ><%=HTMLDecode(rs("detail_conn"))%></textarea>

                                      
                                    </td>
                                </tr> 
                                
           
                                <tr>
                                    <td class="title">管理者网名<a href="javascript:void(0)" class="help" data-info="用于发布文章时和留言回复时的姓名<br>用于各页面的通用信息标签<strong>${realname}</strong>"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" >
                                    
                                      <input name="realname" type="text"   size="50"    value="<%=rs("realname")%>"/>

                                      
                                    </td>
                                </tr> 
                                
                                
                                <tr>
                                    <td class="title">管理者头像<a href="javascript:void(0)" class="help" data-info="用于评论、留言的(未注册用户)小头像<br>模板设计时可以设计是否有头像<br>修改后只对后期评论留言的有效<br>头像路径：images/avatar/"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" class="imgs">
                                    
                                    <%
									
									as1 = "" : as2 = "" : as3 = "" : as4 = "" : as5 = "" : as6 = ""

									if instr(rs("avatar"),".jpg")>0 or instr(rs("avatar"),".png")>0  or instr(rs("avatar"),".jpeg")>0  or instr(rs("avatar"),".gif")>0  then as7 = "<span style=""position:absolute; margin-left:2px"" class=backStage_star>(自定义头像使用中)</span>" : cusImg = rs("avatar")
									
									if instr(rs("avatar"),"b1.jpg")>0 then as1 = "checked=""checked""" : as7 = "(自定义头像)" : cusImg = "a.gif"
									if instr(rs("avatar"),"b2.jpg")>0 then as2 = "checked=""checked""" : as7 = "(自定义头像)" : cusImg = "a.gif"
									if instr(rs("avatar"),"b3.jpg")>0 then as3 = "checked=""checked""" : as7 = "(自定义头像)" : cusImg = "a.gif"
									if instr(rs("avatar"),"g1.jpg")>0 then as4 = "checked=""checked""" : as7 = "(自定义头像)" : cusImg = "a.gif"
									if instr(rs("avatar"),"g2.jpg")>0 then as5 = "checked=""checked""" : as7 = "(自定义头像)" : cusImg = "a.gif"
									if instr(rs("avatar"),"g3.jpg")>0 then as6 = "checked=""checked""" : as7 = "(自定义头像)" : cusImg = "a.gif"
	
									%>
                                        
                                            <input type="radio" name="sA" <%=as1%> value="<%=MY_sitePath%>images/avatar/b1.jpg" onClick="$('#objheadimg').val(this.value)"><img src="<%=MY_sitePath%>images/avatar/b1.jpg" width="30">&nbsp;
                                            <input type="radio" name="sA" <%=as2%> value="<%=MY_sitePath%>images/avatar/b2.jpg" onClick="$('#objheadimg').val(this.value)"><img src="<%=MY_sitePath%>images/avatar/b2.jpg" width="30">&nbsp;
                                            <input type="radio" name="sA" <%=as3%> value="<%=MY_sitePath%>images/avatar/b3.jpg" onClick="$('#objheadimg').val(this.value)"><img src="<%=MY_sitePath%>images/avatar/b3.jpg" width="30">&nbsp;
                                            <input type="radio" name="sA" <%=as4%> value="<%=MY_sitePath%>images/avatar/g1.jpg" onClick="$('#objheadimg').val(this.value)"><img src="<%=MY_sitePath%>images/avatar/g1.jpg" width="30">&nbsp;
                                            <input type="radio" name="sA" <%=as5%> value="<%=MY_sitePath%>images/avatar/g2.jpg" onClick="$('#objheadimg').val(this.value)"><img src="<%=MY_sitePath%>images/avatar/g2.jpg" width="30">&nbsp;
                                            <input type="radio" name="sA" <%=as6%> value="<%=MY_sitePath%>images/avatar/g3.jpg" onClick="$('#objheadimg').val(this.value)"><img src="<%=MY_sitePath%>images/avatar/g3.jpg" width="30">&nbsp;
                                            &nbsp;<img src="<%=cusImg%>" width="30" height="30" onClick="tipShow('.tipsFloatWin-upload-head',0)" style="cursor:pointer" onerror="this.src='../../images/nonpreview2.gif'"> <%=as7%>
                                            
                                    
<div class="tipsFloatWin-upload-head tipsFloatWin-upload-min"><iframe src=../upload/upload_imgID.html?imgID=headimg class=backStage_uploadFrame allowTransparency=true  frameborder=0 scrolling=No></iframe><br>推荐<span class=backStage_star>64×64 </span>大小&nbsp;（支持本地上传和外链地址）<a href="javascript:" onClick="tipShow('.tipsFloatWin-upload-head',1)">[关闭]</a></div>

                                    
                                      <input type="hidden" name="avatar" id="objheadimg" value="<%=rs("avatar")%>">
                                      
                                    </td>
                                </tr>  
                                
                                    
                                      
                                <tr>
                                    <td class="title">年龄<a href="javascript:void(0)" class="help" data-info="用于各页面的通用信息标签<strong>${age}</strong>"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td width="203" >
                                        <input type="hidden" id="age" name="age" value="<%=rs("age")%>"/>  
                                        <span class="txtShow"><%=rs("age")%></span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider"></div></td>
                                </tr>    
                                      
                                      
                                
                                
                                <tr>
                                    <td class="title">Email<a href="javascript:void(0)" class="help" data-info="用于各页面的通用信息标签<strong>${email}</strong>"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" >
                                    
                                      <input name="Email"   type="text"   size="50" value="<%=rs("Email")%>"/>

                                    </td>
                                </tr>   
                                
                                <tr>
                                    <td class="title">QQ<a href="javascript:void(0)" class="help" data-info="用于各页面的通用信息标签<strong>${qq}</strong>"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" >
                                    
                                     <input name="QQ" type="text"  value="<%=rs("qq")%>" size="50"/>

                                    </td>
                                </tr> 
                                
                                <tr>
                                    <td class="title">MSN<a href="javascript:void(0)" class="help" data-info="用于各页面的通用信息标签<strong>${msn}</strong>"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" >
                                    
                                     <input   name="msn" type="text"   size="50"  value="<%=rs("msn")%>" />

                                    </td>
                                </tr> 
                                
                                
                                <tr>
                                    <td class="title">联系电话<a href="javascript:void(0)" class="help" data-info="用于各页面的通用信息标签<strong>${tel}</strong>"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" >
                                    
                                     <input   name="tel" type="text"   size="50" value="<%=rs("tel")%>"/>

                                    </td>
                                </tr> 
                                
                                
                                <tr>
                                    <td class="title">站长介绍<a href="javascript:void(0)" class="help" data-info="用于各页面的通用信息标签<strong>${introduce}</strong>"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2">
                                    
                                     <textarea   name="introduce" cols="80" rows="5" onclick="this.rows='8'"><%=HTMLDecode(rs("introduce"))%></textarea>	

                                    </td>
                                </tr>   



                                <tr onMouseOver="tipShow('.tipsFloatWin-upload-min',0)" onMouseOut="tipShow('.tipsFloatWin-upload-min',1)">
                                    <td class="title">照片<a href="javascript:void(0)" class="help" data-info="用于各页面的通用信息标签<strong>${myimg}</strong>,此标签示意图片地址"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                    <td colspan="2" >
                                    
                                               <input type="text" name="obj" id="objdefault" size="55" value="<%=replace(replace(rs("myimg"),"../../",MY_sitePath),"../",MY_sitePath)%>" >
                                                <br clear="all">
                                                <div class="tipsFloatWin-upload-min"><iframe src=../upload/upload_imgID.html?imgID=default class=backStage_uploadFrame allowTransparency=true  frameborder=0 scrolling=No></iframe></div>
                                                
                                                <%if instr(rs("myimg"),"nonpreview") > 0 then %>
                                                <span id="objdefault_imgShow"></span>
                                                <%else%>
                                                <span id="objdefault_imgShow"><img src="<%=replace(replace(rs("myimg"),"../../",MY_sitePath),"../",MY_sitePath)%>"  onload="resizeImage(this,286,350)"/></span>
                                                <%end if%>
                                               
                                                


                                    </td>
                                </tr>                        
                                
                                  
                               <!-- +++++++  内容   end +++++++  -->
                        
                        
                            </tbody>
                            
                        </table>
                        
                        
                      </form>
 
                                
                </div>
     
                
              
              </div>      
         </div>       



<!--========================================主内容区  end ===================================== -->  

<%
rs.close
set rs=nothing
%>
    
    
<!--#include file="../_comm/page_bottom.asp" -->


