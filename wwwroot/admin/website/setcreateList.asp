<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<!--#include file="_getHtmlConfig.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<script>


$(function(){


	
	//==============================字符

	$("#slider-svalue1").slider({ 
	    value: <%=sv1%>,
		step: 2, 
		max: 200,
		min: 10,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue1").val(ui.value); 
			$(".txtShow-svalue1").html(ui.value + "个字符");
		} 
	
	}); 
	
	$("#slider-svalue2").slider({ 
	    value: <%=sv2%>,
		step: 2, 
		max: 100,
		min: 10,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue2").val(ui.value); 
			$(".txtShow-svalue2").html(ui.value + "个字符");
		} 
	
	}); 	
	
	$("#slider-svalue3").slider({ 
	    value: <%=sv3%>,
		step: 2, 
		max: 200,
		min: 10,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue3").val(ui.value); 
			$(".txtShow-svalue3").html(ui.value + "个字符");
		} 
	
	}); 
	
	$("#slider-svalue4").slider({ 
	    value: <%=sv4%>,
		step: 2, 
		max: 200,
		min: 10,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue4").val(ui.value); 
			$(".txtShow-svalue4").html(ui.value + "个字符");
		} 
	
	}); 
	
	$("#slider-svalue5").slider({ 
	    value: <%=sv5%>,
		step: 2, 
		max: 200,
		min: 10,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue5").val(ui.value); 
			$(".txtShow-svalue5").html(ui.value + "个字符");
		} 
	
	}); 
		
	$("#slider-svalue6").slider({ 
	    value: <%=sv6%>,
		step: 2, 
		max: 200,
		min: 10,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue6").val(ui.value); 
			$(".txtShow-svalue6").html(ui.value + "个字符");
		} 
	
	}); 
	
	$("#slider-svalue7").slider({ 
	    value: <%=sv7%>,
		step: 2, 
		max: 200,
		min: 10,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue7").val(ui.value); 
			$(".txtShow-svalue7").html(ui.value + "个字符");
		} 
	
	}); 
	$("#slider-svalue8").slider({ 
	    value: <%=sv8%>,
		step: 2, 
		max: 200,
		min: 10,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue8").val(ui.value); 
			$(".txtShow-svalue8").html(ui.value + "个字符");
		} 
	
	}); 
	$("#slider-svalue9").slider({ 
	    value: <%=sv9%>,
		step: 2, 
		max: 200,
		min: 10,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue9").val(ui.value); 
			$(".txtShow-svalue9").html(ui.value + "个字符");
		} 
	
	}); 
	
	$("#slider-svalue10").slider({ 
	    value: <%=sv10%>,
		step: 2, 
		max: 100,
		min: 10,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue10").val(ui.value); 
			$(".txtShow-svalue10").html(ui.value + "个字符");
		} 
	
	}); 
	
	$("#slider-svalue11").slider({ 
	    value: <%=sv11%>,
		step: 2, 
		max: 200,
		min: 10,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue11").val(ui.value); 
			$(".txtShow-svalue11").html(ui.value + "个字符");
		} 
	
	}); 
	
	$("#slider-svalue12").slider({ 
	    value: <%=sv12%>,
		step: 2, 
		max: 200,
		min: 10,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue12").val(ui.value); 
			$(".txtShow-svalue12").html(ui.value + "个字符");
		} 
	
	}); 
	
	
	//==============================列表数
	
	$("#slider-svalue13").slider({ 
	    value: <%=sv13%>,
		step: 1, 
		max: 25,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue13").val(ui.value); 
			$(".txtShow-svalue13").html(ui.value + "条/页");
		} 
	
	}); 	
	
	$("#slider-svalue14").slider({ 
	    value: <%=sv14%>,
		step: 1, 
		max: 25,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue14").val(ui.value); 
			$(".txtShow-svalue14").html(ui.value + "条/页");
		} 
	
	}); 
	
	$("#slider-svalue15").slider({ 
	    value: <%=sv15%>,
		step: 5, 
		max: 250,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue15").val(ui.value); 
			$(".txtShow-svalue15").html(ui.value + "个");
		} 
	
	}); 
	
	$("#slider-svalue16").slider({ 
	    value: <%=sv16%>,
		step: 1, 
		max: 10,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue16").val(ui.value); 
			$(".txtShow-svalue16").html(ui.value + "条");
		} 
	
	}); 
	
	
	$("#slider-svalue17").slider({ 
	    value: <%=sv17%>,
		step: 1, 
		max: 100,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue17").val(ui.value); 
			$(".txtShow-svalue17").html(ui.value + "条");
		} 
	
	}); 
	
	$("#slider-svalue18").slider({ 
	    value: <%=sv18%>,
		step: 1, 
		max: 100,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue18").val(ui.value); 
			$(".txtShow-svalue18").html(ui.value + "条");
		} 
	
	}); 
	
	$("#slider-svalue19").slider({ 
	    value: <%=sv19%>,
		step: 1, 
		max: 10,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue19").val(ui.value); 
			$(".txtShow-svalue19").html(ui.value + "条");
		} 
	
	}); 
	
	$("#slider-svalue20").slider({ 
	    value: <%=sv20%>,
		step: 1, 
		max: 25,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue20").val(ui.value); 
			$(".txtShow-svalue20").html(ui.value + "条");
		} 
	
	}); 
	
	$("#slider-svalue21").slider({ 
	    value: <%=sv21%>,
		step: 1, 
		max: 25,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue21").val(ui.value); 
			$(".txtShow-svalue21").html(ui.value + "条");
		} 
	
	}); 
	
	$("#slider-svalue22").slider({ 
	    value: <%=sv22%>,
		step: 1, 
		max: 25,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue22").val(ui.value); 
			$(".txtShow-svalue22").html(ui.value + "条");
		} 
	
	}); 
	
	$("#slider-svalue23").slider({ 
	    value: <%=sv23%>,
		step: 1, 
		max: 25,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue23").val(ui.value); 
			$(".txtShow-svalue23").html(ui.value + "条");
		} 
	
	}); 
	
	$("#slider-svalue24").slider({ 
	    value: <%=sv24%>,
		step: 1, 
		max: 25,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue24").val(ui.value); 
			$(".txtShow-svalue24").html(ui.value + "条");
		} 
	
	}); 
	
	$("#slider-svalue25").slider({ 
	    value: <%=sv25%>,
		step: 1, 
		max: 50,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue25").val(ui.value); 
			$(".txtShow-svalue25").html(ui.value + "条/页");
		} 
	
	}); 
	
	$("#slider-svalue26").slider({ 
	    value: <%=sv26%>,
		step: 1, 
		max: 50,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue26").val(ui.value); 
			$(".txtShow-svalue26").html(ui.value + "条/页");
		} 
	
	}); 
	
	$("#slider-svalue27").slider({ 
	    value: <%=sv27%>,
		step: 1, 
		max: 50,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue27").val(ui.value); 
			$(".txtShow-svalue27").html(ui.value + "条/页");
		} 
	
	}); 
	
	$("#slider-svalue28").slider({ 
	    value: <%=sv28%>,
		step: 1, 
		max: 50,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue28").val(ui.value); 
			$(".txtShow-svalue28").html(ui.value + "条/页");
		} 
	
	}); 
	
	$("#slider-svalue29").slider({ 
	    value: <%=sv29%>,
		step: 1, 
		max: 25,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue29").val(ui.value); 
			$(".txtShow-svalue29").html(ui.value + "条");
		} 
	
	}); 
	
	$("#slider-svalue30").slider({ 
	    value: <%=sv30%>,
		step: 1, 
		max: 50,
		min: 1,
		range: "min",
		slide: function(event, ui){ 
			$("#svalue30").val(ui.value); 
			$(".txtShow-svalue30").html(ui.value + "条/页");
		} 
	
	}); 
	
	

});


<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=侧边栏设置", "link1=<%=MY_sitePath%>admin/sectionConfig/edit_sidebar.asp", "title2=SEO设置", "link2=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-seo", "title3=上传设置", "link3=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-upload", "title4=标签tags设置", "link4=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-tags", "title5=自动生成设置", "link5=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-autoCreate", "title6=评论留言设置", "link6=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-msg", "title7=分页设置", "link7=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-pages",  "title8=缩略图和剪裁设置", "link8=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-img", "title9=列表生成参数设置", "link9=<%=MY_sitePath%>admin/website/setcreateList.asp" , "title10=API数据接口设置", "link10=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-apiset", "title11=其它设置", "link11=<%=MY_sitePath%>admin/website/siteConfig.asp#setting-other");

	   }
	);


});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 网站设置 | 列表生成参数设置
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
                    <!-- 后台提示 -->
                    
                    <div class="notification information png_bg content-alert">
                        <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                        <div>
                        
                        您也可以到<strong>跟目录/template/</strong>的通用模板文件里直接修改
                        
                        </div>
                    </div>
                    
                <!-- 表单域  -->
                <div id="contentShow">
                
                        <form method="post" name="formM"  action="action.asp?action=setcreateList" target="actionPage" onSubmit="javascript:ChkFormTip_page('保存成功',1);">

                        <table width="100%" class="setting" cellspacing="0">
                            
                            <!-- 标题 -->                        
                            <thead class="setHead">
                                <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2"></th>
                                </tr>
                                
                            </thead>
                            
                            
                          <!-- 底部 -->
                            <tfoot>
                            
                                <tr>
                                    <td colspan="3">
                                       <div class="btnBox">
                                       
                                           <input type="submit" value="保存" class="sub">
                                           
                                       </div>
                        
                                    </td>
                                </tr>
                            </tfoot>
                            
                            
                            
                            <!-- +++++++  内容   begin +++++++  -->
                            
                            <tbody>
                            
                            
                                <thead class="setHead">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">标题字符数截取设置</th>
                                    </tr> 
                                </thead>      
                                
                                  
                                  
                                <tr>
                                    <td class="title">侧边栏：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue1" name="svalue1" value="<%=sv1%>"/>  
                                        <span class="txtShow txtShow-svalue1"><%=sv1%>个字符</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue1"></div></td>
                                </tr>  
                                
                                
                                <tr>
                                    <td class="title">友情链接内页：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue2" name="svalue2" value="<%=sv2%>"/>  
                                        <span class="txtShow txtShow-svalue2"><%=sv2%>个字符</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue2"></div></td>
                                </tr>  
                                                        
                                  
                                <tr>
                                    <td class="title">最新案例作品-通用：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue3" name="svalue3" value="<%=sv3%>"/>  
                                        <span class="txtShow txtShow-svalue3"><%=sv3%>个字符</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue3"></div></td>
                                </tr>  
                                
                                       
                                <tr>
                                    <td class="title">置顶推荐文章-通用：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue4" name="svalue4" value="<%=sv4%>"/>  
                                        <span class="txtShow txtShow-svalue4"><%=sv4%>个字符</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue4"></div></td>
                                </tr>      
                                
                                                               
                                <tr>
                                    <td class="title">最新推荐文章-通用：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue5" name="svalue5" value="<%=sv5%>"/>  
                                        <span class="txtShow txtShow-svalue5"><%=sv5%>个字符</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue5"></div></td>
                                </tr>   
                                
                                                               
                                <tr>
                                    <td class="title">对应类别调用文章-通用：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue6" name="svalue6" value="<%=sv6%>"/>  
                                        <span class="txtShow txtShow-svalue6"><%=sv6%>个字符</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue6"></div></td>
                                </tr>   
                                
                                <tr>
                                    <td class="title">对应类别调用作品-通用：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue7" name="svalue7" value="<%=sv7%>"/>  
                                        <span class="txtShow txtShow-svalue7"><%=sv7%>个字符</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue7"></div></td>
                                </tr>   
                                
                                <tr>
                                    <td class="title">首页文章列表：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue8" name="svalue8" value="<%=sv8%>"/>  
                                        <span class="txtShow txtShow-svalue8"><%=sv8%>个字符</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue8"></div></td>
                                </tr>  
                                                            
                                <tr>
                                    <td class="title">通知公告：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue9" name="svalue9" value="<%=sv9%>"/>  
                                        <span class="txtShow txtShow-svalue9"><%=sv9%>个字符</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue9"></div></td>
                                </tr>                                                                                      
                                  
                                <tr>
                                    <td class="title">留言者姓名：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue10" name="svalue10" value="<%=sv10%>"/>  
                                        <span class="txtShow txtShow-svalue10"><%=sv10%>个字符</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue10"></div></td>
                                </tr>   
                                
                                <tr>
                                    <td class="title">文章列表：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue11" name="svalue11" value="<%=sv11%>"/>  
                                        <span class="txtShow txtShow-svalue11"><%=sv11%>个字符</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue11"></div></td>
                                </tr>   
                                
                                <tr>
                                    <td class="title">案例作品列表：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue12" name="svalue12" value="<%=sv12%>"/>  
                                        <span class="txtShow txtShow-svalue12"><%=sv12%>个字符</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue12"></div></td>
                                </tr>                
                                  
                                  
                                <thead class="setHead">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">内容输出列表数量设置</th>
                                    </tr> 
                                </thead>    
                                
                                
                                <tr>
                                    <td class="title">侧边栏：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue13" name="svalue13" value="<%=sv13%>"/>  
                                        <span class="txtShow txtShow-svalue13"><%=sv13%>条/页</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue13"></div></td>
                                </tr>    
                                
                                <tr>
                                    <td class="title">侧边栏评论：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue14" name="svalue14" value="<%=sv14%>"/>  
                                        <span class="txtShow txtShow-svalue14"><%=sv14%>条/页</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue14"></div></td>
                                </tr>    
                                
                                <tr>
                                    <td class="title">友情链接内页：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue15" name="svalue15" value="<%=sv15%>"/>  
                                        <span class="txtShow txtShow-svalue15"><%=sv15%>个</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue15"></div></td>
                                </tr>    
                                
                                <tr>
                                    <td class="title">首页置顶文章：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue16" name="svalue16" value="<%=sv16%>"/>  
                                        <span class="txtShow txtShow-svalue16"><%=sv16%>条</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue16"></div></td>
                                </tr>    
                                
                                <tr>
                                    <td class="title">首页文章：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue17" name="svalue17" value="<%=sv17%>"/>  
                                        <span class="txtShow txtShow-svalue17"><%=sv17%>条</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue17"></div></td>
                                </tr>             
                                
                                <tr>
                                    <td class="title">首页案例作品：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue18" name="svalue18" value="<%=sv18%>"/>  
                                        <span class="txtShow txtShow-svalue18"><%=sv18%>条</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue18"></div></td>
                                </tr>    
                                
                                <tr>
                                    <td class="title">通知公告：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue19" name="svalue19" value="<%=sv19%>"/>  
                                        <span class="txtShow txtShow-svalue19"><%=sv19%>条</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue19"></div></td>
                                </tr>    
                                
                                <tr>
                                    <td class="title">最新案例作品-通用：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue20" name="svalue20" value="<%=sv20%>"/>  
                                        <span class="txtShow txtShow-svalue20"><%=sv20%>条</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue20"></div></td>
                                </tr>    
                                
                                <tr>
                                    <td class="title">置顶推荐文章-通用：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue21" name="svalue21" value="<%=sv21%>"/>  
                                        <span class="txtShow txtShow-svalue21"><%=sv21%>条</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue21"></div></td>
                                </tr>    
                                
                                <tr>
                                    <td class="title">最新推荐文章-通用：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue22" name="svalue22" value="<%=sv22%>"/>  
                                        <span class="txtShow txtShow-svalue22"><%=sv22%>条</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue22"></div></td>
                                </tr>    
                                <tr>
                                    <td class="title">对应类别调用文章-通用：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue23" name="svalue23" value="<%=sv23%>"/>  
                                        <span class="txtShow txtShow-svalue23"><%=sv23%>条</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue23"></div></td>
                                </tr>    
                                
                                <tr>
                                    <td class="title">对应类别调用作品-通用：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue24" name="svalue24" value="<%=sv24%>"/>  
                                        <span class="txtShow txtShow-svalue24"><%=sv24%>条</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue24"></div></td>
                                </tr>    
                                <tr>
                                    <td class="title">留言：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue25" name="svalue25" value="<%=sv25%>"/>  
                                        <span class="txtShow txtShow-svalue25"><%=sv25%>条/页</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue25"></div></td>
                                </tr>    
                                <tr>
                                    <td class="title">站内微博：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue26" name="svalue26" value="<%=sv26%>"/>  
                                        <span class="txtShow txtShow-svalue26"><%=sv26%>条/页</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue26"></div></td>
                                </tr>    
                                
                                <tr>
                                    <td class="title">文章列表：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue27" name="svalue27" value="<%=sv27%>"/>  
                                        <span class="txtShow txtShow-svalue27"><%=sv27%>条/页</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue27"></div></td>
                                </tr>    
                                
                                <tr>
                                    <td class="title">案例作品列表：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue28" name="svalue28" value="<%=sv28%>"/>  
                                        <span class="txtShow txtShow-svalue28"><%=sv28%>条/页</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue28"></div></td>
                                </tr>    
                                
                                <tr>
                                    <td class="title">相关文章列表：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue29" name="svalue29" value="<%=sv29%>"/>  
                                        <span class="txtShow txtShow-svalue29"><%=sv29%>条</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue29"></div></td>
                                </tr>    
                                
                                <tr>
                                    <td class="title">手机版栏目列表：</td>
                                    <td width="203" class="ui-element">
                                        <input type="hidden" id="svalue30" name="svalue30" value="<%=sv30%>"/>  
                                        <span class="txtShow txtShow-svalue30"><%=sv30%>条/页</span>
                                        
                                    </td>
                                    <td><div class="sliderStyle" id="slider-svalue30"></div></td>
                                </tr>    
                                         
                                
                                
                                  
                                  
                               <!-- +++++++  内容   end +++++++  -->
                        
                        
                            </tbody>
                            
                        </table>
                        
                        
                      </form>
 
                                
                </div>     
                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


