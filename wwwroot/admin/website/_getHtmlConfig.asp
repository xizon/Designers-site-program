<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
sv1 = getTempLablePara_siteConfig("标题字符数截取(侧边栏)")
sv2 = getTempLablePara_siteConfig("标题字符数截取(友情链接内页)")
sv3 = getTempLablePara_siteConfig("标题字符数截取(最新案例作品-通用)")
sv4 = getTempLablePara_siteConfig("标题字符数截取(置顶推荐文章-通用)")
sv5 = getTempLablePara_siteConfig("标题字符数截取(最新推荐文章-通用)")
sv6 = getTempLablePara_siteConfig("标题字符数截取(对应类别调用文章-通用)")
sv7 = getTempLablePara_siteConfig("标题字符数截取(对应类别调用作品-通用)")
sv8 = getTempLablePara_siteConfig("标题字符数截取(首页文章列表)")
sv9 = getTempLablePara_siteConfig("标题字符数截取(通知公告)")
sv10 = getTempLablePara_siteConfig("标题字符数截取(留言者姓名)")
sv11 = getTempLablePara_siteConfig("标题字符数截取(文章列表)")
sv12 = getTempLablePara_siteConfig("标题字符数截取(案例作品列表)")

sv13 = getTempLablePara_siteConfig("内容输出列表数量(侧边栏)")
sv14 = getTempLablePara_siteConfig("内容输出列表数量(侧边栏评论)")
sv15 = getTempLablePara_siteConfig("内容输出列表数量(友情链接内页)")
sv16 = getTempLablePara_siteConfig("内容输出列表数量(首页置顶文章)")
sv17 = getTempLablePara_siteConfig("内容输出列表数量(首页文章)")
sv18 = getTempLablePara_siteConfig("内容输出列表数量(首页案例作品)")
sv19 = getTempLablePara_siteConfig("内容输出列表数量(通知公告)")
sv20 = getTempLablePara_siteConfig("内容输出列表数量(最新案例作品-通用)")
sv21 = getTempLablePara_siteConfig("内容输出列表数量(置顶推荐文章-通用)")
sv22 = getTempLablePara_siteConfig("内容输出列表数量(最新推荐文章-通用)")
sv23 = getTempLablePara_siteConfig("内容输出列表数量(对应类别调用文章-通用)")
sv24 = getTempLablePara_siteConfig("内容输出列表数量(对应类别调用作品-通用)")
sv25 = getTempLablePara_siteConfig("内容输出列表数量(留言)")
sv26 = getTempLablePara_siteConfig("内容输出列表数量(微博)")
sv27 = getTempLablePara_siteConfig("内容输出列表数量(文章列表)")
sv28 = getTempLablePara_siteConfig("内容输出列表数量(案例作品列表)")
sv29 = getTempLablePara_siteConfig("内容输出列表数量(相关文章列表)")
sv30 = getTempLablePara_siteConfig("内容输出列表数量(手机版栏目列表)")
%>