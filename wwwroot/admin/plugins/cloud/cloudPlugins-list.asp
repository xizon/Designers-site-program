<!--#include file="../../function/dsMainFunction.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<!--#include file="../../_comm/page_head.asp" -->
<!--#include file="../../checkFun_noSup.asp" -->
<!--#include file="../../_comm/page_bodyComm.asp" -->

<%
webIDSkin = Application("CommWebID")
webID = webIDSkin

thisWebURLP = "http://"&Request.ServerVariables("server_name")&Request.ServerVariables("script_name") 

ID = request.QueryString("ID")
action = request.QueryString("action")

ID=request.QueryString("ID")
pluginID = request.form("pluginID"&ID)
appTitle = request.form("appTitle"&ID)
actionURL1 = request.form("actionURL1"&ID)
actionURL2 = request.form("actionURL2"&ID)
pluginsFolder = request.form("pluginsFolder"&ID)'插件文件夹

'载入模板配置文档
set xmlDom = server.CreateObject("MSXML2.DOMDocument")
xmlDom.async = false

setXmlPath = ""&MY_sitePath&"template/tempConfig.xml"
if not xmlDom.Load(Server.MapPath(setXmlPath))  then 
    'response.write("Load wrong!")
else

	'读取当前主题ID
	set tempInfo_plugins = xmlDom.getElementsByTagName("DSsetting")  
	themeIDNow = tempInfo_plugins(0).selectSingleNode("themeID").Text
					
end if

%>

<script>

<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=插件管理", "link1=javascript:pluginShow();", "title2=云插件", "link2=<%=MY_sitePath%>admin/plugins/cloud/cloudPlugins-list.asp?action=list");

	   }
	);
	
	parent.floatWin('温馨提示','<div class=tipsFloatWin>&nbsp;&nbsp;正在加载...</div>',380,50,150,1,1,1);
	$("#wBox",window.parent.document).css("visibility","hidden");
	


});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 扩展及插件 | 云插件 
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
            
                  <%if action = "install" then%>
                      
                    <script>
                        parent.floatWin('温馨提示','<div class=tipsFloatWin>&nbsp;&nbsp;正在安装中,请稍等5~15秒,请勿点击其它链接..<br>(安装完成后请您关闭进度窗口)</div>',350,0,225,0,0,1);
                    </script>
                    


					<%

               
					'创建插件文件夹
					createFolder(MY_sitePath&"template/tempPlugins")
					

					'开始云处理 
					actionURL1_action=split(actionURL1,"|")'官方云皮肤路径
					actionURL2_action=split(actionURL2,"|")'目标路径
					pluginsFolder_action=split(pluginsFolder,"|")'文件夹
					max_a1=ubound(actionURL1_action)
					max_a2=ubound(pluginsFolder_action)
					
					vk = 0 '插件转移数据判断
					
              
					for yo=0 to max_a2
					
						createFolder(MY_sitePath&""&pluginsFolder_action(yo)&"")

					next
					
					
			  
					
					'插件转移到template/tempPlugins下
					
					for p=0 to max_a1

                        
						cloudTempFileAction actionURL1_action(p),""&MY_sitePath&""&actionURL2_action(p)&"","plugins"
						response.flush
						
						vk = vk + 1

					next	
					
					'添加到插件App管理
					'addLocalPlugins ""&rs("manageApp_appID")&"",""&rs("manageApp_appName")&"",""&rs("manageApp_appCode")&"",""&rs("manageApp_appURL")&""
					
                    
                    rs.close
                    set rs=nothing
					
                    
					if vk >= max_a1 then
					
						'载入独立插件配置文档
						if FileExistsStatus(""&MY_sitePath&"template/pluginsInstallConfig.xml") = 1 then 
							'////////////////////////////有独立插件配置文件状态
						
								'载入插件配置文档
								setXmlPath_plugins = ""&MY_sitePath&"template/pluginsInstallConfig.xml"
								if not xmlDom.Load(Server.MapPath(setXmlPath_plugins))  then 
									'response.write("Load wrong!")
								else
								
						  
									'读取XML信息
									set tempInfo_plugins = xmlDom.getElementsByTagName("DSPluginsSetting")  
									pluginsConfigOk = tempInfo_plugins(0).selectSingleNode("pluginsInstallCompleted").Text
								
								
									if pluginsConfigOk = 0 then
								
										'插件及其自定义标签
										pluginsNumMax_single = 30
										
										'////////////////移动文件或者文件夹
										for f = 1 to pluginsNumMax_single
										
												'============读取插件节点
												thisPluginPath = tempInfo_plugins(0).selectSingleNode("plugins_"&f).Text
												thisPluginPathTo = tempInfo_plugins(0).selectSingleNode("plugins_"&f).GetAttribute("pathTo")
												thisPluginType = tempInfo_plugins(0).selectSingleNode("plugins_"&f).GetAttribute("type")
												thisPluginTypeAppName = tempInfo_plugins(0).selectSingleNode("plugins_"&f).GetAttribute("appName")
												thisPluginTypeAppURL = replace(tempInfo_plugins(0).selectSingleNode("plugins_"&f).GetAttribute("appURL"),"{rootPath}",""&MY_sitePath&"")
												thisPluginTypeAppCode = tempInfo_plugins(0).selectSingleNode("plugins_"&f).GetAttribute("appCode")
													
													  
												thisPluginPath = clearRightString(thisPluginPath,"/")
												thisPluginPathTo = clearRightString(thisPluginPathTo,"/")
						
										
												'============退出循环
												if instr(showFile(setXmlPath_plugins),"plugins_"&f)=0  then exit for
												
												
												
												'============开始移动插件
												
												if thisPluginType = 1 then 
													MoveFolder MY_sitePath&"template/"&thisPluginPath,MY_sitePath&thisPluginPathTo
												else
													CopyFile MY_sitePath&"template/"&thisPluginPath,MY_sitePath&thisPluginPathTo
									
												end if
												
												'============创建插件app数据	
												if thisPluginTypeAppName <> "none" then	
													
														set rsChk =server.createobject("adodb.recordset")
														sqlChk="select * from App where appName='"&thisPluginTypeAppName&"'"
														rsChk.open sqlChk,db,1,1 
														
														'存在性验证
														if not rsChk.EOF then
															'已存在
													
														else
															'执行添加操作
															GetNum_A = db.execute("select count(1) from App")(0)
									
															set rs =server.createobject("adodb.recordset")
															sql="select * from App"
															rs.open sql,db,1,2
															rs.addnew
															
															rs("appName")=thisPluginTypeAppName
															rs("appCode")=thisPluginTypeAppCode
															rs("appID")=createBase64ID(URLDecode(thisPluginTypeAppName))
															rs("appURL")=thisPluginTypeAppURL
															rs("index")=GetNum_A + 1
														
															rs.update
															rs.close
															set rs=nothing
															
															
														end if
													
														
														rsChk.close
														set rsChk=nothing
														
														
												end if
												
											
										
								
										next
										
									
									
										'配置文件读取完毕判断
										CreateFile setXmlPath_plugins,replace(showFile(setXmlPath_plugins),"<pluginsInstallCompleted>0</pluginsInstallCompleted>","<pluginsInstallCompleted>1</pluginsInstallCompleted>")
										
										response.Write "<script>floatWin('温馨提示','<div class=tipsFloatWin>&nbsp;&nbsp;插件安装成功...</div>',350,0,225,0,1,1);</script>"&vbcrlf
										
										'add offical data
										response.Write "<iframe style=display:none src=http://www.c945.com/d-s-app/usrStatistics/pluginsStatistics.asp?appTitle="&appTitle&"&pluginID="&pluginID&"&webIDNow="&webIDSkin&"&weburl="&thiswebURL&"></iframe>"
										
									
									end if
										
								
								
								end if
						
						end if



					
					end if
					
                    
                    %>  
                

                     <!-- 独立内容区 -->
                    <div class="singleContentBox">
    
                        <div id="skinBox">
                            <strong>【安装成功后您可以在导航[扩展及插件]的插件管理选项里使用】</strong><br>
                            <span class="sTxt"><img src="../../plugins/d-s-management-ui/img/loading.gif">&nbsp;&nbsp;插件正在安装中,请稍候...</span>
    

                        </div>
                        
                    </div>
                  
                  
                  <%else%>
          
                           <!-- 后台提示 -->
                            
                            <div class="notification information png_bg content-alert">
                                <a href="#" class="close"><img src="../../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                <div>1)本产品的云服务只提供了皮肤和插件的接口,作为开发者统计产品使用数量的一个通道，我们保证绝不会利用非法手段或者任何恶意代码、隐藏代码泄露使用者的隐私，请大家放心下载安装<br>
2)使用云插件后您可以直接在导航[扩展及插件]的<strong>插件管理</strong>选项里使用！<br>
3)云插件分为网站通用插件和主题的对应插件(不同主题拥有对应的特殊插件,您当前使用的主题ID：<strong><%=themeIDNow%></strong>)
                             
                                </div>
                            </div> 
                        
                        
                            
                             <!-- 独立内容区 -->
                            <div class="singleContentBox">
            
                                <div id="skinBox">
                                
                                   <!-- +++++++  内容   begin +++++++  -->
                                               
                                       <!-- 获取在线插件数据 -->
                                       <%
                                       response.Write getHTTPPage("http://www.c945.com/d-s-app/_ds_plugins/?installID="&webIDSkin&"&pageno="&pageno&"&themeIDNow="&themeIDNow&"&webURL="&thisWebURLP&"")
                                       %>  
  
                                   <!-- +++++++  内容   end +++++++  -->
                                   
                           
                                    
                                </div>
                               
                                
            
                            </div>
                    
                    <%end if%>
                    
                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../../_comm/page_bottom.asp" -->


