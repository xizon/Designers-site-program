<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<script>


function editFrame(ID){
	ShowIFrameEdit('修改Banner','fullColumnSet/editFrame.asp?action=banner&ID=' + ID,290,430,450,290,100,0);
	
}


function tipAction(){
	
	MySite.Cookie.set('tempTip_S','tempTip_S');
	$('#tipA').html('设置成功！');
	
}

$(function(){
	
	if (MySite.Cookie.get('tempTip_S') != "tempTip_S" ){
		if (getQueryStringRegExp('thisState')!="Succeed"){
			
			floatWin('温馨提示','<div class=tipsFloatWin>（1）如果您修改了Banner，需进行过以下任一操作，才能及时看到效果<br><strong>&nbsp;&nbsp;a)手动操作静态生成</strong>;<br><strong>&nbsp;&nbsp;b)到达自动生成时间并访问页面</strong>;<br><strong>&nbsp;&nbsp;c)内容发布;</strong><br>（2）浏览器存在缓存，需要刷新浏览器才能看到效果<br clear=all><hr><span class=backStage_star>提示：模板页使用标签<strong>${bannerImgList} </strong>(大图)、<strong>${bannerImgSmallList}</strong> (缩略图) 时有效</span><p style=text-align:right id=tipA><a href=javascript: onclick=tipAction()>不再提示</a>&nbsp;&nbsp;</p></div>',460,0,100,0,0,1);
		}
	}



	
});


<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=Banner幻灯片", "link1=<%=MY_sitePath%>admin/fullColumnSet/edit_banner.asp", "title2=导航菜单", "link2=<%=MY_sitePath%>admin/edit_file/edit_NavMenu.asp");

	   }
	);


});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 幻灯片/导航菜单 | Banner幻灯片
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
            
                <!-- 后台提示 -->
                
                <div class="notification information png_bg content-alert">
                    <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        1. 更新Banner后，您需要手动操作<a href="../root_temp-edit/index.asp">栏目页面静态生成</a>，才能及时看到效果<br>
                        2. 或者等待系统达到<strong>静态自动更新间隔</strong>时间，访问页面时系统即可更新静态页面，您也可以通过导航<strong>网站设置</strong>，设置此时间<br>
                        3. <strong>名称</strong> 留空则当前banner不显示<br>
                        4. Banner的效果和图片切换的时间 <a href="../website/siteConfig.asp#bannerTime">点这里设置</a><br>
                        5. 模板里幻灯片输出的标签为 <strong>${bannerImgList} </strong>(大图)、<strong>${bannerImgSmallList}</strong> (缩略图)

                    </div>
                </div>
                
                
                    <!-- 独立内容区 -->
                    <div class="singleContentBox">


					 <%
                     if callCommInfoChk("${bannerImgList}") = false then response.Write "<span class=backStage_star>目前模板里暂时没有<strong>通用Banner</strong>标签，无法看到效果<br>(其它原因：此主题并没有调用通用Banner标签，而是直接在模板里设计Banner及其图片地址，如果您需要修改Banner，请直接修改html模板文件)</span>"
                     %>
                                
                                    <%
                                        j=0
                                        strsql="select * from [banner] order by bannerID desc"
                                        set rs=db.execute(strsql)
                                        do while not rs.eof and not rs.bof
                                        j=j+1
                                        
                                        thisID = j
                                        bgColor=""
                                        if thisID mod 2<>0 then bgColor="#F2F2F2"
                                        
                                        banner_title=rs("title")
                                        banner_src=rs("src")
                                        banner_imgURL=MY_sitePath&replace(replace(rs("imgURL"),"../",""),""&MY_sitePath&"security","security") 
                                        banner_imgSmallURL=MY_sitePath&replace(replace(rs("imgSmallURL"),"../",""),""&MY_sitePath&"security","security") 
                                        banner_window=rs("window")
										
                                        
                                        if banner_window="_blank" then winShowTxt="新窗口"
                                        if banner_window="_self" then winShowTxt="当前窗口"
                                        if banner_window="_parent" then winShowTxt="父窗口"
                                        
                                        if thisID=5 then response.Write "<div style=""display:none"" id=""more"">"&vbcrlf
                                        if thisID=9 then response.Write "<div style=""display:none"" id=""more2"">"&vbcrlf
                                        
                                   
                                    
										'高清图
										if instr(banner_imgURL,"security")>0 then
											urlOrigin1=replace(replace(replace(banner_imgURL,"../",""),""&MY_sec_uploadImgPath&"/?src=",""),MY_sitePath,"")
											if FileExistsStatus(""&MY_sitePath&""&MY_uploadImgPath&"/"&urlOrigin1) = -1 then  
												banner_imgURL=""
												banner_title=""
											end if
										
										end if
										
										'缩略图
										if instr(banner_imgSmallURL,"security")>0 then
											urlOrigin2=replace(replace(replace(banner_imgSmallURL,"../",""),""&MY_sec_uploadImgPath&"/?src=",""),MY_sitePath,"")
											if FileExistsStatus(""&MY_sitePath&""&MY_uploadImgPath&"/"&urlOrigin2) = -1 then  
												banner_imgSmallURL=""
											end if
										
										end if
										

										if banner_title <> "" then     
										
										else  
										
											banner_imgURL = ""
											banner_imgSmallURL = ""
										
										end if
										
										
									    '判断有无缩略图
										simgShowTxt = ""
										if rs("imgSmallURL") <> "" then simgShowTxt = "<span class=backStage_star>缩略图:<img src="""&banner_imgSmallURL&""" width=""25"" height=""25""></span>"
									    
             
                                    %>
                                    
                                      
                                    <div style="background:<%=bgColor%>" onMouseOver="tipShow('#tipsFloatWin-upload<%=thisID%>',0)" onMouseOut="tipShow('#tipsFloatWin-upload<%=thisID%>',1)">
                                    
                                    
                                    <%if banner_imgURL<>"" then%>
                                    <%
									
									MY_sitePathBanner = MY_sitePath
									MY_sitePathBanner = clearRightString(MY_sitePathBanner,"/")
									
									%>
                                    
                                    <img onclick="editFrame('<%=rs("bannerID")%>');" style="cursor:pointer" src="<%=replace(replace(replace(replace(banner_imgURL,"//","/"),""&MY_sitePathBanner&""&MY_sitePathBanner&""&MY_sitePathBanner&"",""&MY_sitePathBanner&""),""&MY_sitePathBanner&""&MY_sitePathBanner&"",""&MY_sitePathBanner&""),"//","/")%>" alt="" height="75" title="header=[<%=banner_title%>] body=[点击图片即可编辑]"><%=simgShowTxt%>&nbsp;&nbsp;&nbsp;<iframe  src="action_delF.asp?ID=<%=rs("bannerID")%>" allowTransparency="true" frameborder="0" scrolling="no" hspace="0" vspace="0" width="125" height="30" ></iframe>
                                    
                                    <%else%>
                                    
                                    <div style="height:50px; line-height:50px; padding-left:5px">暂无图片...<a href="javascript:" style="padding:5px"  onclick="editFrame('<%=rs("bannerID")%>');"><img src="../../plugins/d-s-management-ui/img/pic.gif">添加图片</a></div>
                                    
                                    <%end if%>
                                    
                                         
                                        
                                         

                                    
                      </div>
                                    
                                    
                                    <%
                                    rs.movenext
                                    loop
                                    
                                    rs.close
                                    set rs=nothing%>
                                    <input type="hidden" value="<%=j%>" name="total" />
                                        
                                        
                                    
                                    <br />
                                    
                                    <%
									response.Write "</div>"&vbcrlf
									response.Write "</div>"&vbcrlf
									%>

                                    
                                    <span  id="preImgBtn"><br />
                                      <a href="javascript:" class="backBtn" onclick="$('#more').slideDown(300);$('#preImgBtn2').slideDown(300);$('#preImgBtn').slideUp(600)" style="text-decoration:none">更多Banner图片</a>
                                      </span>
                                    <span  id="preImgBtn2" style="display:none"><br />
                                      <a href="javascript:" class="backBtn" onclick="$('#more2').slideDown(300);$('#preImgBtn2').slideUp(600)" style="text-decoration:none">更多Banner图片</a>
                                      </span>
                                      
                     
                                                                    
                                </form>	 
                    
                    
                    </div>
 
                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


