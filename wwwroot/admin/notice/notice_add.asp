<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<script>
//表单判断
function checkForm(){
	if(document.formM.title.value==""){
		ChkFormTip_page("标题不能为空",0);
		return false;
	}
	if(document.formM.content.value==""){
		ChkFormTip_page("内容不能为空",0);
		return false;
	}
	if(document.formM.content.value.length > 90){
		ChkFormTip_page("内容不能超过90个字符",0);
		return false;
	}
	
	//ok
	loadingCreate();
	return true;
		
}


$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=文章日志", "link1=<%=MY_sitePath%>admin/article/article_add.asp", "title2=站内微博", "link2=<%=MY_sitePath%>admin/mood/mood_add.asp","title3=作品案例/产品", "link3=<%=MY_sitePath%>admin/SuccessWork/SuccessWork_add.asp","title4=通知公告", "link4=<%=MY_sitePath%>admin/notice/notice_add.asp");
	   }
	);
	
	
	var limitNum = 90;
	$('#content').inputlimiter({
		limit: limitNum,
		boxId: 'limitingtext',
		boxAttach: false
	});	
	
	var limitNum2 = 30;
	$('#title').inputlimiter({
		limit: limitNum2,
		boxId: 'limitingtext2',
		boxAttach: false
	});	
	
	document.getElementById("limitingtext2").innerHTML = "0/" + limitNum2;
	document.getElementById("limitingtext").innerHTML =  "0/" + limitNum;
	
	
	
});

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="notice_edit.asp" class="link">列表管理</a>
                
                <!-- 当前位置 -->
                当前位置： 内容发布 | 通知公告
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
                <div class="notification information png_bg content-alert">
                    <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        
                        模板里<strong>首页</strong>通知公告输出链接的标签为 <strong>{#loopListContentShow:notice/}</strong>
                        &nbsp;<a href="javascript:" onClick="$('#ns-1').fadeIn(500);$(this).fadeOut(500);">循环体修改</a>
                        
                        <br clear="all">
                        
                        <span id="ns-1" style="display:none">
                            <hr>&nbsp;&nbsp;循环体修改：<span class="codeArea">在template/通用模板页.html中找到代码{#loopList:notice}■■■{/loopList:notice}，修改■部分即可</span>
                        </span>
                        
                    </div>
                </div>
          
                
                <!-- 表单域  -->
                <div id="contentShow">
                
					 <%
                     if callCommInfoChk("{#loopListContentShow:notice/}") = false then response.Write "<script>floatWin('温馨提示','<div class=tipsFloatWin>目前模板里暂时没有<strong>通知公告</strong>标签，无法看到效果<br>(其它原因：此主题并没有设计首页通知公告区块)</div>',350,0,100,0,0,0);</script>"
                     %>

                                    <form name="formM"  action="action.asp?action=add" method="post" onSubmit="javascript:return checkForm();">
                                           
                                                <table width="100%" class="setting" cellspacing="0">
                                                    
                                                    <!-- 标题 -->                        
                                                    <thead class="setHead">
                                                        <tr>
                                                               <th  class="setTitle"></th>
                                                               <th colspan="2"></th>
                                                        </tr>
                                                        
                                                    </thead>
                                                    
                                                 
                                                    <!-- 底部 -->
                                                    <tfoot>
                                                    
                                                        <tr>
                                                            <td colspan="3">         
                                                                <div class="btnBox">
                                                                    <input type="submit" value="发布" class="sub" id="needCreateLoadingBtn">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                    
                                                    
                                                    <tbody>
                                                       <!-- +++++++  内容   begin +++++++  -->
                            
                                                        <tr>
                                                            <td class="title">标题：<span class="backStage_star">*</span></td>
                                                            <td colspan="2" class="ui-element">
                                                                                            
                                                                <input name="title" id="title" type="text"   size="40" />&nbsp;&nbsp;<span id="limitingtext2"></span>
   
                                                              
                                                            </td>
                                                        </tr>  
                                                        
                                                        <tr>
                                                            <td class="title">链接网址：</td>
                                                            <td colspan="2" class="ui-element">
                                                                                            
                                                                <input name="url" type="text"size="30" class="focusJS" data-value="http://"   >
   
                                                              
                                                            </td>
                                                        </tr>   
                                                        
                                                        <tr>
                                                            <td class="title">内容：<span class="backStage_star">*</span></td>
                                                            <td colspan="2" class="ui-element">
                                                                                            
                                                                <textarea name="content" id="content" cols="70" rows="5" ></textarea>&nbsp;&nbsp;<span id="limitingtext"></span>
   
                                                              
                                                            </td>
                                                        </tr>  
                                                                                                                
                                                   
                                                        
                                                        
                                                  
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                                                
                                                    </tbody>
                                                    
                                                </table>
                   
                   
                                    </form>  

                     </div> 
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->
