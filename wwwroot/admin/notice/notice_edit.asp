<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<script>

$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=文章列表", "link1=<%=MY_sitePath%>admin/article/article_list.asp", "title2=作品案例/产品列表", "link2=<%=MY_sitePath%>admin/SuccessWork/SuccessWork_list.asp","title3=站内微博列表", "link3=<%=MY_sitePath%>admin/mood/mood_edit.asp","title4=通知公告列表", "link4=<%=MY_sitePath%>admin/notice/notice_edit.asp");
	   }
	);

	
	
});

function editFrame(ID){
	ShowIFrameEdit('修改公告','notice/editFrame.asp?action=notice&ID=' + ID,280,350,370,280,100,0);
	
}

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="notice_add.asp" class="link">发布公告</a>
                
                <!-- 当前位置 -->
                当前位置： 通知公告 | 列表管理
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
            
                <div class="notification information png_bg content-alert">
                    <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        
                        模板里<strong>首页</strong>通知公告输出链接的标签为 <strong>{#loopListContentShow:notice/}</strong>
                        &nbsp;<a href="javascript:" onClick="$('#ns-1').fadeIn(500);$(this).fadeOut(500);">循环体修改</a>
                        
                        <br clear="all">
                        
                        <span id="ns-1" style="display:none">
                            <hr>&nbsp;&nbsp;循环体修改：<span class="codeArea">在template/通用模板页.html中找到代码{#loopList:notice}■■■{/loopList:notice}，修改■部分即可</span>
                        </span>
                        
                    </div>
                </div>
            
          
                
                <!-- 表单域  -->
                <div id="contentShow">
                
					 <%
                     if callCommInfoChk("{#loopListContentShow:notice/}") = false then response.Write "<script>floatWin('温馨提示','<div class=tipsFloatWin>目前模板里暂时没有<strong>通知公告</strong>标签，无法看到效果<br>(其它原因：此主题并没有设计首页通知公告区块)</div>',350,0,100,0,0,0);</script>"
                     %>
                                
                          <!-- 列表 -->
                        
                                    <table width="100%" class="list" cellspacing="1">
                                        
                                        <!-- 标题 -->
                                        <thead class="setTitle">
                                            <tr>
                                               <!-- 有多少竖列 -->
                                               <th>ID</th>
                                               <th>标题</th>
                                               <th>链接网址</th> 
                                               <th>内容(点击相关条目查看全部)</th>
                                               <th>排序</th>
                                               <th>操作</th>
                                            </tr>
                                            
                                       </thead>

                                        <tbody>
                                        
                                           <!-- +++++++  内容   begin +++++++  -->
                                                                                                                                                        
                                            <%
                                            '----------显示记录
                                            dim cat,j
                                            j=0
                                            set cat=new myclass
                                            
                                            set rs =server.createobject("adodb.recordset")
                                            sql="select * from [notice] order by index desc"
                                            rs.open sql,db,1,1
                                            
                                            k=1
                                            num=cat.GetNum("notice")
                                            do while not rs.eof and not rs.bof
                                            j=j+1
                                            
                                            %>

                                        
                                            <!--////////////////循环开始 ////////////////-->
                                            <tr id="del<%=j%>">
                                                <td><%=j%></td>
                                                <td>

                                                    <%=rs("title")%>
                        
                                                </td>
                                                
                                                <td>
                                                
                                                <%
                                                if rs("url")="" or rs("url")<>"http://"  then showURL="<a href=""#"">"
                                                if rs("url")<>"" and rs("url")<>"http://"  then showURL="<a href="""&rs("url")&""" title=""浏览相关网址"" target=""_blank"">"
                                                %>  
           
                                                    <%=showURL%><img src="../../plugins/d-s-management-ui/img/view.gif"></a>

                                                
                                                </td>
                                                
                                                <td onClick="switchInfo('#msg-<%=j%>','','');">

                                                    <%=gotTopic(stripHTML(rs("content")),40)%>
                                                    
                                                
                                                    <div id="msg-<%=j%>" data-open="0" style="display:none" class="msgShowBox">
                                                         <%=HTMLDecode(rs("content"))%>
                                                    </div>
           
    
                                                </td>
                                                
                                                <td>

                                                    <%=rs("index")%>

          
                                                </td>
                                                <td>
                                                    <!-- Icons -->
                                                     <a href="javascript:" data-open="0" onclick="editFrame('<%=rs("ID")%>');" title="编辑"><img src="../../plugins/d-s-management-ui/img/hammer_screwdriver.png"></a> 
                                                     
                                                     <a onclick="javascript:floatWin('温馨提示','<div align=center><span id=del_Btn<%=j%>><a href=action.asp?action=delRecord&amp;ID=<%=rs("ID")%> target=delFrame class=backBtn onclick=javascript:$(\'#del<%=j%>,#del_Btn<%=j%>\').fadeOut(2);$(\'#delOk<%=j%>\').fadeIn(500);>确认删除?</a></span><span id=delOk<%=j%> style=display:none>√删除成功！</span></div>',350,50,150,0,0,1);" target="delFrame" href="javascript:"><img src="../../plugins/d-s-management-ui/img/cross.png" title="删除" /></a>
                                                     
                                                     <input type="hidden" value="<%=rs("ID")%>" name="ID<%=j%>" />
                                                </td>
                                            </tr>
                                            
                                            <!--////////////////循环结束 ////////////////-->
                                            
                                            <%
                                             k = k + 1
                                             rs.movenext
                                             loop
                                             %>   
                                             
                                             <iframe name="delFrame" style="display:none"></iframe>
                                             <input type="hidden" value="<%=j%>" name="total" />
                                             
                                             <!-- 无数据 -->
                                             <%if rs.bof and rs.eof then %>
                                             
                                                <tr>
                                                    <td class="title" colspan="6">
                                                     暂时还没有数据！
                                                    </td>
                                                </tr>
                                             
                                             <%end if%>
                                             
                                    
                                           <!-- +++++++  内容   end +++++++  -->
                                    
            
                                    
                                        </tbody>
                                        
                                    </table>
                                
                                                
               
                               
                     </div> 
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->
