<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<script>

//表单判断
function checkForm(){
	if(document.formM.sec_pwd.value==""){
		ChkFormTip_page("新密码不能为空",0);
		return false;
	}
	if(document.formM.re_pwd.value==""){
		ChkFormTip_page("请重复输入您的密码",0);
		return false;
	}
	if(document.formM.sec_pwd.value != document.formM.re_pwd.value){
		ChkFormTip_page("两次输入的密码不一致",0);
		return false;
	}
	//ok
	ChkFormTip_page("保存成功",1);
	return true;
		
}


<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=网站信息/Logo设置", "link1=<%=MY_sitePath%>admin/website/siteSetting.asp", "title2=管理员帐号设置", "link2=<%=MY_sitePath%>admin/website/change_pwd.asp");

	   }
	);
	
	
});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="../website/change_pwd.asp" class="link" >帐号设置</a>
                
                <!-- 当前位置 -->
                当前位置： 网站设置 | 管理员帐号设置 | 二级密码设置
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
                <!-- 后台提示 -->
                
                <div class="notification information png_bg content-alert">
                    <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        1. 二级密码主要用于网站<strong>数据库操作</strong>和<strong>文件操作</strong><br>
                        2. 二级密码可以由任意字符组成，不限长度，请牢记您的二级密码<br>
                        3. 修改成功后建议您重新登录
                    </div>
                </div>
          
                <!-- 表单域  -->
                <div id="contentShow">
                

                                    <form  action="action.asp?action=ok"  name="formM"   method="post" target="actionPage" onSubmit="javascript:return checkForm();">
                                           
                                                <table width="100%" class="setting" cellspacing="0">
                                                    
                                                    <!-- 标题 -->                        
                                                    <thead class="setHead">
                                                        <tr>
                                                               <th  class="setTitle"></th>
                                                               <th colspan="2"></th>
                                                        </tr>
                                                        
                                                    </thead>
                                                    
                                                 
                                                    <!-- 底部 -->
                                                    <tfoot>
                                                    
                                                        <tr>
                                                            <td colspan="3">         
                                                                <div class="btnBox">
                                                                    <input type="submit" value="保存" class="sub">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                    
                                                    
                                                    <tbody>
                                                    
                                                       <!-- +++++++  内容   begin +++++++  -->
                                   
                                                        <tr>
                                                            <td class="title">请输入新的二级密码：<span class="backStage_star">*</span></td>
                                                            <td colspan="2" >
                                                                <input name="sec_pwd" type="password" size="40" />
                                                              
                                                            </td>
                                                        </tr>  
                                                        
                                                        
                                                        <tr>
                                                            <td class="title">重复输入二级密码：<span class="backStage_star">*</span></td>
                                                            <td colspan="2">
                                                                <input name="re_pwd" type="password" size="40" />
                                                              
                                                            </td>
                                                        </tr> 
                                                  
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                                                
                                                    </tbody>
                                                    
                                                </table>
                   
                   
                                    </form>  


                               
                     </div> 
          
                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


