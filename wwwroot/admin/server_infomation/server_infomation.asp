<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->

<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<%



Function DateToStr(DateTime,ShowType)  
	Dim DateMonth,DateDay,DateHour,DateMinute,DateWeek,DateSecond
	Dim FullWeekday,shortWeekday,Fullmonth,Shortmonth,TimeZone1,TimeZone2
	TimeZone1="+0800"
	TimeZone2="+08:00"
	FullWeekday=Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
	shortWeekday=Array("Sun","Mon","Tue","Wed","Thu","Fri","Sat")
    Fullmonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
    Shortmonth=Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")

	DateMonth=Month(DateTime)
	DateDay=Day(DateTime)
	DateHour=Hour(DateTime)
	DateMinute=Minute(DateTime)
	DateWeek=weekday(DateTime)
	DateSecond=Second(DateTime)
	If Len(DateMonth)<2 Then DateMonth="0"&DateMonth
	If Len(DateDay)<2 Then DateDay="0"&DateDay
	If Len(DateMinute)<2 Then DateMinute="0"&DateMinute
	Select Case ShowType
	Case "Y-m-d"  
		DateToStr=Year(DateTime)&"-"&DateMonth&"-"&DateDay
	Case "Y-m-d H:I A"
		Dim DateAMPM
		If DateHour>12 Then 
			DateHour=DateHour-12
			DateAMPM="PM"
		Else
			DateHour=DateHour
			DateAMPM="AM"
		End If
		If Len(DateHour)<2 Then DateHour="0"&DateHour	
		DateToStr=Year(DateTime)&"-"&DateMonth&"-"&DateDay&" "&DateHour&":"&DateMinute&" "&DateAMPM
	Case "Y-m-d H:I:S"
		If Len(DateHour)<2 Then DateHour="0"&DateHour	
		If Len(DateSecond)<2 Then DateSecond="0"&DateSecond
		DateToStr=Year(DateTime)&"-"&DateMonth&"-"&DateDay&" "&DateHour&":"&DateMinute&":"&DateSecond
	Case "YmdHIS"
		DateSecond=Second(DateTime)
		If Len(DateHour)<2 Then DateHour="0"&DateHour	
		If Len(DateSecond)<2 Then DateSecond="0"&DateSecond
		DateToStr=Year(DateTime)&DateMonth&DateDay&DateHour&DateMinute&DateSecond	
	Case "ym"
		DateToStr=Right(Year(DateTime),2)&DateMonth
	Case "d"
		DateToStr=DateDay
    Case "ymd"
        DateToStr=Right(Year(DateTime),4)&DateMonth&DateDay
    Case "mdy" 
        Dim DayEnd
        select Case DateDay
         Case 1 
          DayEnd="st"
         Case 2
          DayEnd="nd"
         Case 3
          DayEnd="rd"
         Case Else
          DayEnd="th"
        End Select 
        DateToStr=Fullmonth(DateMonth-1)&" "&DateDay&DayEnd&" "&Right(Year(DateTime),4)
    Case "w,d m y H:I:S" 
		DateSecond=Second(DateTime)
		If Len(DateHour)<2 Then DateHour="0"&DateHour	
		If Len(DateSecond)<2 Then DateSecond="0"&DateSecond
        DateToStr=shortWeekday(DateWeek-1)&","&DateDay&" "& Left(Fullmonth(DateMonth-1),3) &" "&Right(Year(DateTime),4)&" "&DateHour&":"&DateMinute&":"&DateSecond&" "&TimeZone1
    Case "y-m-dTH:I:S"
		If Len(DateHour)<2 Then DateHour="0"&DateHour	
		If Len(DateSecond)<2 Then DateSecond="0"&DateSecond
		DateToStr=Year(DateTime)&"-"&DateMonth&"-"&DateDay&"T"&DateHour&":"&DateMinute&":"&DateSecond&TimeZone2
	Case Else
		If Len(DateHour)<2 Then DateHour="0"&DateHour
		DateToStr=Year(DateTime)&"-"&DateMonth&"-"&DateDay&" "&DateHour&":"&DateMinute
	End Select
End Function

function DisI(b)
 if b then
   response.write "<img src=""../../plugins/d-s-management-ui/img/status-y.GIF"">"
  else
   response.write "<img src=""../../plugins/d-s-management-ui/img/status-n.GIF"">"
 end if
end function



Sub GetSQLVersion()
	On Error Resume Next

	Dim Rs
	Set Rs = dy.Execute("SELECT @@version ")
	If Err=0 Then
		Response.Write "<span title="""&Rs(0)&""">"
		Response.Write Split(Rs(0), "-")(0)
	End If
	Set Rs = Nothing
	Set Rs = dy.Execute("SELECT SERVERPROPERTY ('productlevel')")
	If Err Then
		Response.Write "Microsoft SQL Server "
	Else
		Response.Write Rs(0) & "</span>"
	End If
	Set Rs = Nothing
End Sub

Call GetSysInfo()
%>


<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:history.go(-1);" class="link" id="column-btn-back">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 服务器信息
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
                <!-- 表单域  -->
                <div id="contentShow">
                

                                <table width="100%" class="setting" cellspacing="0">
                                    
                                    <!-- 标题 -->                        
                                    <thead class="setHead">
                                        <tr>
                                               <th  class="setTitle"></th>
                                               <th colspan="2"></th>
                                        </tr>
                                        
                                    </thead>
                                    
                                    
                                    <tbody>
                                    
                                       <!-- +++++++  内容   begin +++++++  -->
                                       
           
                                        <thead class="setHead">
                                            <tr>
                                               <th  class="setTitle"></th>
                                               <th colspan="2" class="titleInfo">基本信息</th>
                                            </tr> 
                                        </thead> 
                                       
                                       
                   
                                        <tr>
                                            <td class="title">程序版本：</td>
                                            <td colspan="2"><%=MY_ProgramVersion%></td>
                                        </tr>  
                                        
                                        <tr>
                                            <td class="title">数据库系统：</td>
                                            <td colspan="2">
											   <%If IsSqlDataBase = 1 Then
                                                    Call GetSQLVersion()
                                                Else
                                                    Response.Write "Microsoft Access"
                                                End If%>
                                            </td>
                                        </tr>  
                                        
                                        <tr>
                                            <td class="title">服务器时间：</td>
                                            <td colspan="2"><%=DateToStr(Now(),"Y-m-d H:I A")%></td>
                                        </tr>  
                                        
                                        
                                        <tr>
                                            <td class="title">服务器物理路径：</td>
                                            <td colspan="2"><%=Request.ServerVariables("APPL_PHYSICAL_PATH")%></td>
                                        </tr>  
                                        
                                        
                                        <tr>
                                            <td class="title">服务器IIS版本：</td>
                                            <td colspan="2"><%=Request.ServerVariables("SERVER_SOFTWARE")%></td>
                                        </tr>                                                                          
                                        
                                        
                                        
                                        <tr>
                                            <td class="title">管理认证方式：</td>
                                            <td colspan="2"><%=MY_secCheckMode%></td>
                                        </tr>  
                                        
                                        
                                        <tr>
                                            <td class="title">脚本解释引擎：</td>
                                            <td colspan="2"><%=ScriptEngine & "/"& ScriptEngineMajorVersion &"."&ScriptEngineMinorVersion&"."& ScriptEngineBuildVersion %><%="JScript/" & getjver()%></td>
                                        </tr>  
                                        
                                        <tr>
                                            <td class="title">您目前的分辨率：</td>
                                            <td colspan="2"><script>document.write("屏幕分辨率为   "   +   window.screen.width + " * " + window.screen.height );</script></td>
                                        </tr>       
                                        
                                        
                                        <tr>
                                            <td class="title">您当前的浏览器：</td>
                                            <td colspan="2">
												<%
                                                Dim Agent,Browser,version,tmpstr
                                                Agent=Request.ServerVariables("HTTP_USER_AGENT")
                                                Agent=Split(Agent,";")
                                                If InStr(Agent(1),"MSIE")>0 Then
                                                Browser="MS Internet Explorer "
                                                version=Trim(Left(Replace(Agent(1),"MSIE",""),6))
                                                ElseIf InStr(Agent(4),"Netscape")>0 Then 
                                                Browser="Netscape "
                                                tmpstr=Split(Agent(4),"/")
                                                version=tmpstr(UBound(tmpstr))
                                                ElseIf InStr(Agent(4),"rv:")>0 Then
                                                Browser="Mozilla "
                                                tmpstr=Split(Agent(4),":")
                                                version=tmpstr(UBound(tmpstr))
                                                If InStr(version,")") > 0 Then 
                                                tmpstr=Split(version,")")
                                                version=tmpstr(0)
                                                End If
                                                End If
                                                response.Write(""&Browser&"  "&version&"")
                                                %>		
                                            </td>
                                        </tr>  
                                        
                                        
                                        <tr>
                                            <td class="title">服务器IP地址：</td>
                                            <td colspan="2"><%=Request.ServerVariables("LOCAL_ADDR")%></td>
                                        </tr>  
                                        
                                        
                                        <tr>
                                            <td class="title">客户端IP地址：</td>
                                            <td colspan="2"><%=Request.ServerVariables("REMOTE_ADDR")%></td>
                                        </tr>                                                                          
                                        
                                        <thead class="setHead">
                                            <tr>
                                               <th  class="setTitle"></th>
                                               <th colspan="2" class="titleInfo">组件信息</th>
                                            </tr> 
                                        </thead> 
                                        
                                        
                                        <tr>
                                            <td class="title">本站需使用的组件：</td>
                                            <td>
                                                 
                                                 Scripting.FileSystemObject 组件 <%=DisI(CheckObjInstalled("Scripting.FileSystemObject"))%>  <br>
                                                 Microsoft.XMLHTTP 组件 <%=DisI(CheckObjInstalled("Microsoft.XMLHTTP"))%><br>
                                                 ADODB.Stream 组件 <%=DisI(CheckObjInstalled("ADODB.Stream"))%> <br>

                                            </td>
                                            <td>
                                                 Scripting.Dictionary 组件 <%=DisI(CheckObjInstalled("Scripting.Dictionary"))%> <br>
                                                 AspJpeg 图片控制组件 <%=DisI(CheckObjInstalled("Persits.Jpeg"))%> <br>
                                                 MSXML2.DOMDocument组件 <%=DisI(CheckObjInstalled("MSXML2.DOMDocument"))%> <br>
                                            
                                            </td>
                                        </tr>  
                                        
                                        
                                        <tr>
                                            <td class="title">其他未使用的组件：</td>
                                            <td>
                                                 MSXML2.ServerXMLHTTP 组件 <%=DisI(CheckObjInstalled("MSXML2.ServerXMLHTTP"))%> <br>
                                                 MSWC.BrowserType 访问探针组件 <%=DisI(CheckObjInstalled("MSWC.BrowserType"))%><br> 
                                                 AspUpload 上传组件 <%=DisI(CheckObjInstalled("Persits.Upload"))%><br>
                                                 CreatePreviewImage 生成预览图组件 <%=DisI(CheckObjInstalled("CreatePreviewImage.cGvbox"))%><br>
                                            </td>
                                            <td>
                                                 FileUp.upload 组件 <%=DisI(CheckObjInstalled("FileUp.upload"))%><br>
                                                 JMail.SMTPMail 组件 <%=DisI(CheckObjInstalled("JMail.SMTPMail"))%><br>
                                                 CDONTS 发送邮件组件 <%=DisI(CheckObjInstalled("CDONTS.NewMail"))%><br>
                                                 AspEmail 发送邮件组件 <%=DisI(CheckObjInstalled("Persits.MailSender"))%><br>	 
                                            </td>
                                        </tr>  
                            
                                        
                                        
                                       <!-- +++++++  内容   end +++++++  -->
                                
                                
                                    </tbody>
                                    
                                </table>

                               
                     </div> 
          
                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


