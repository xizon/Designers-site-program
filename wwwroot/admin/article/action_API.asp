<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../function/Md5.asp" -->
<!--#include file="../function/include/fun_api.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%

'////获取并判断动态模板 begin
thisTempCodePath=""&MY_sitePath&"admin/_temp/content_tempA1_"&tempModifiedDate&".html"
if FileExistsStatus(thisTempCodePath) = -1 then '不存在
'开始执行依赖标签功能
'==================

		Template_Code=showFile(DesignerSite_tempPath_common)
			
		'=============================模板依赖标签不存在判断   begin
		
		'模板主注释，参数不用变更，默认全部清除
		Template_Code_trip = dependentLabelFun(Template_Code,"note:template",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"note:commCreateInfo",0,"","","")
		
		
		'【必选其一】各栏目标题依赖标签,必选其一设置为参数 1
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:commonUse.title",0,"","","") '通用标题
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:content.article",1,"","","")	 '内容页-文章 
		  

		'列表页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.article",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.works",0,"","","")
		
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.about",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.mood",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.search",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.links",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.message",0,"","","")
		'【必选其一】各栏目meta描述依赖标签,必选其一设置为参数 1 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:comm.desc",0,"","","") '通用网站描述
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:content.articleDesc",1,"","","")'内容页-文章 
		
		
		'【必选其一】各栏目meta关键词依赖标签,必选其一设置为参数 1 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:commonUse.keywords",0,"","","")'其他通用页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:home",0,"","","")'首页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:content",1,"","","")'内容页关键词
		
		
		'RSS订阅，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"rss:commonUse.article",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"rss:commonUse.article.class",1,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"atom:commonUse.article",0,"","","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"atom:commonUse.article.class",1,"","","")
		
		'banner轮换，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.banner",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"head:commonUse.banner",0,"","","")
		
		'搜索框，订阅图标，置顶推荐文章，最新案例作品，规定数量随机tags输出，可分配在任何页面
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.searchBoard",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.articleFeed",1,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.article.top",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.works.list",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.article.tags.random",0,"","","")
		
		
		'内容页SEO可选link
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"head:content.linkRel",1,"","","")
		
		
		
		'侧边栏
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"sideBar:article,search,about,message",0,"","","")'列表页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"sideBar:home",0,"","","")  '首页
		Template_Code_trip = getTempLablePara_siderBar_content("文章内容页侧边栏",1,"body:content.show.article")
		Template_Code_trip = dependentLabelFun_para(Template_Code_trip,"文章内容页侧边栏",0,"body:content.show.article")
		
		'各栏目的列表页面
		
		'文章
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.article",0,"","","//") '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article.class",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article.tags",0,"","","")  
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.article",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.article.class",0,"/*","*/","")
		
		
		'作品案例
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.worksIe6Js",0,"","","") '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.works",0,"","","//")   '栏目必选
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.works",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.works.class",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.works",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.works.class",0,"/*","*/","")
		
		
		'微博
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.mood",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:content.js.mood",0,"/*","*/","")
		
		
		'关于我们
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.about",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.about",0,"/*","*/","")
		
		
		'搜索
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.search",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.searchResult",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.search",0,"/*","*/","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.searchResult",0,"/*","*/","")
		
		'友情链接
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.links",0,"","","")	  
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.links",0,"/*","*/","")
		
		'留言
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.message",0,"","","//")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.message",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.message",0,"/*","*/","")
		
		'首页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.home",0,"","","//")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.home",0,"","","")
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.home",0,"/*","*/","")
		
		
		'内容页
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.js.comm.form",1,"","","")	 '栏目必选  	
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.show.commJs",1,"","","")'栏目必选   	
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.show.article",1,"","","")	 					 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:content.js.article",1,"/*","*/","") 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:content.js.comm",1,"","","//")		 
		Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:content.article",1,"","","//")
		
		
		%>
		<!--#include file="../root_temp-edit/_clearLabel_comm.asp" -->
		<%
		
		'==================
		Template_Code_Now = Template_Code_trip		
		CreateFile thisTempCodePath,Template_Code_Now
		
 
end if
'=============================模板依赖标签不存在判断   end

'这里获取依赖标签清除后模板的值
Template_Code=showFile(thisTempCodePath)
'只调用栏目独立HTML代码判断
callPageAllHTML "body:content.show.article","完全独立页面代码.不调用其它任何依赖标签的内容"
		
		
		'获取表单内容
		ClassName=request.form("ClassName")
		ArticleTitle=request.form("ArticleTitle") 
		KeyWord=replace(request.form("KeyWord"),chr(32),",")
		Summary=request.form("Summary")
		files=request.form("obj")
		summGetBtn=request.Form("summGetBtn")
		content=request.form("Content")
		top=request.form("top")
		definePath=request.form("definePath")
		mainImg=request.form("mainImg")
		
		'摘要截取
		if summGetBtn = 1 then
			if Summary <> "" then
	
			else
				Summary = CutStr(stripHTML(request.form("Content")),200)
			end if
		end if
		
		
		if top<>"1" then top = "0"
        
        if mainImg="" then mainImg="nonpreview2.gif"

        filename=MD5(year(now)&month(now)&day(now)&hour(now)&minute(now)&second(now),1)
		if definePath="" then definePath=filename
							
		Safe_ArticleTitle=HTMLEncode(ArticleTitle)
		Safe_KeyWord=HTMLEncode(KeyWord)	
		Safe_Summary=HTMLEncode(Summary)	
		
	    Temp_HTMLsum=HTMLDecode(Safe_Summary)
		
'////////////////程序功能   
function addContent()
		
		addRs("content")=content
		addRs("summary")=HTMLEncode(Safe_Summary)
		addRs("className")=className
		addRs("ArticleTitle")=HTMLEncode(Safe_ArticleTitle)
		addRs("files")=files
		addRs("mainImg")=mainImg
		addRs("top")=top
		addRs("private")=Myprivate
		addRs("KeyWord")=HTMLEncode(Safe_KeyWord)
		addRs("subdate")=now()
end function

function addTempContent()

        'urlOrigin=replace(replace(replace(addRs("mainImg"),"../",""),""&MY_sec_uploadImgPath&"/?src=",""),MY_sitePath,"")
	    'imgSmallCreate(""&MY_sitePath&""&MY_uploadImgPath&"/"&urlOrigin)
		'imgSmallCreate_subject(""&MY_sitePath&""&MY_uploadImgPath&"/"&urlOrigin)
		
		
end function

		
action=Request.QueryString("action")
	
'-----------添加内容模块
if action="articleAdd" then	

		
	Myprivate=request.form("private")'私密功能	
	if 	Myprivate="1" then Myprivate="Yes"	 else Myprivate="No"
	If ClassName = "" or ArticleTitle = ""  then 
	  apiError(10)
	Else			
		if request.form("Content")="" then 
		  apiError(10)
		else
		
		createFolder("../../"&MY_createFolder_art&"/")
		
		
		'主题图片同步发布
		mainPicToContent = request.Form("mainPicToContent")
		if mainPicToContent = "1" then 
			mainPicToContentAdd = "<p><img src="""&mainImg&""" alt="""" /></p>"&vbcrlf 
		else
			mainPicToContentAdd = ""
		end if
		
		content = mainPicToContentAdd&content
		
		set addRs=server.createobject("adodb.recordset")
		strsql="select * from [article]"
		addRs.open strsql,db,1,3
		
		addRs.addnew  '以下是添加进数据库的内容		
		addContent()
		'========================
		'模板路径添加块，顺序不能错	
		'↓↓↓↓↓↓↓↓↓↓↓↓
		articleid=addRs("articleid")
		
		
		if MY_createFileMode = 0 then
		createFolder("../../"&MY_createFolder_art&"/"&filename&"/")
		
		fname="default.html"
		folder="../../"&MY_createFolder_art&"/"&filename&"/"

					
		elseif MY_createFileMode = 1 then
		
		fname=definePath&".html"
		folder="../../"&MY_createFolder_art&"/"
		
		end if
		
		filePath=folder&fname
		filePath=""&MY_sitePath&""&replace(filePath,"../","")&""
		
		addRs("FilePath")=filePath	
		
		'↑↑↑↑↑↑↑↑↑↑↑↑		
		
		'=======================	
	
		
		addTempContent()
		

		'------关键字识别
		
		if MY_tagsLight = 1 then
		
			'读取源关键词库
			keyWordsLibrary=showFile(""&MY_sitePath&"config/thesaurus.txt")
		
			'==========================
			'分割关键词         
			keywords_light=split(keyWordsLibrary,"|") '将输入的字符串根据空格分开，获得一个数组
			max=ubound(keywords_light) '得出这个数组的维数，即输入的关键字个数
			
			for k=0 to max 
			'==========================	
				
		
				if instr(content,keywords_light(k)) > 0 then content = replace(content,keywords_light(k),"<a href="""&MY_sitePath&"plugins/keywords/?"&MY_createFolder_art&"_tag=" & server.URLEncode(keywords_light(k)) & ".html"" target=""_blank""><mark class=""tagsLight"">"&keywords_light(k)&"</mark></a>")
		
				
					
			next	
			
		end if
		
		
		Template_Code=replace(Template_Code,"${ArticleTitle}",Safe_ArticleTitle)
		Template_Code=replace(Template_Code,"${KeyWord}",Safe_KeyWord)
		Template_Code=replace(Template_Code,"${Summary}",Temp_HTMLsum)
		Template_Code=replace(Template_Code,"${Content}",content)
		Template_Code=replace(Template_Code,"${ClassName}",ClassName)
		Template_Code=replace(Template_Code,"${SubDate}",Now())	
		Template_Code=replace(Template_Code,"${articleid}",articleid)	
		CreateFile filepath,Template_Code
		addRs.update
		addRs.close
		set addRs=nothing
		
		'更新关键词库////////////////////////begin
		path = "../../config/thesaurus.txt"
		'读取源关键词库
		foreCon=showFile(path)
		
		'获取文章关键词
		dim rs,strsql
		set rs=server.CreateObject("adodb.recordset")
		strsql="select * from [article]"
		rs.open strsql,db,1,1
		do while not rs.eof
		KeyWord=replace(rs("KeyWord"),chr(32),"")
		'==========================
		'分割关键词         
		keywords=split(KeyWord,",") '将输入的字符串根据空格分开，获得一个数组
		max=ubound(keywords) '得出这个数组的维数，即输入的关键字个数
		for i=0 to max 
		KeyWord_show=KeyWord_show&keywords(i)&"|"
		
		'==========================	
		next
		
		
		rs.movenext
		loop
		rs.close
		set rs=nothing

		
		
		nowCon=foreCon&KeyWord_show
		nowCon=replace(Trim(nowCon),vbCrlf,"")
		CreateFile path,MoveR(nowCon,"|")
		'更新关键词库////////////////////////end
		
				
				
'---------------内容页更新
	%>
	<!--#include file="../root_temp-edit/sideColumn.asp" -->
	<%
	update_ID = articleid

	set rs =server.createobject("adodb.recordset")
	sql="select * from [article] where articleid=" & update_ID &" "
	rs.open sql,db,1,1
	
	ClassName=rs("ClassName")
	ArticleTitle=rs("ArticleTitle")
	KeyWord=rs("KeyWord")
	Summary=HTMLDecode(rs("Summary"))
	HTMLinfo=HTMLEncode(rs("Summary"))
	content=rs("content")
	SubDate=rs("SubDate")
	Myprivate=rs("private")
	mypath=replace(rs("FilePath"),"/default.html","")
	mypathHtml=rs("FilePath")
	
	'私密判断
	if Myprivate="Yes" then content=""&DY_LANG_53&""&vbcrlf
	
	'删除缩略图
	SmallOrginUrl_del=replace(replace(replace(rs("mainImg"),"../",""),""&MY_sec_uploadImgPath&"/?src=",""),MY_sitePath,"")
	delfile(""&MY_sitePath&""&MY_uploadImgPath&"/small/s_"&SmallOrginUrl_del&"")
	delfile(""&MY_sitePath&""&MY_uploadImgPath&"/small/sub_"&SmallOrginUrl_del&"")
	
	

%>
<!--#include file="../root_temp-edit/_batchTemp_article.asp" -->
<%		

'-----------------内容页更新结束	
			
	'如果有信息，初始生成一次分类项目
	
	sql_class="select * from [ArticleClass] where ClassName='"& request.form("ClassName") &"'"
	set rs_class=server.createObject("ADODB.Recordset")
	rs_class.open sql_class,db,1,1
	
	updateClassID=rs_class("ArticleClassID")
	
	rs_class.close
	set rs_class=nothing
			
	
	sql_have="select * from [Article] where ClassName='"& request.form("ClassName") &"'"
	set rs_have=server.createObject("ADODB.Recordset")
	rs_have.open sql_have,db,1,1
	
	haveInfoNum = rs_have.recordcount
	
	if rs_have.EOF or rs_have.BOF then
		
	
	else
	
		if haveInfoNum < 1 then create_classPage = "" else create_classPage = "<iframe src=""../root_temp-edit/Articleclass_Iframe_action.asp?ID="&updateClassID&""" style=""display:none"" ></iframe>"
	
		
	
	end if	
	
	rs_have.close
	set rs_have = nothing
			
			
		'自定义分类文章列表页面判断	
		oCode = showFile(DesignerSite_tempPath_common)	
		if instr(oCode,"{#body:columnShow.CustomPage[classList")>0 and instr(oCode,"{/body:columnShow.CustomPage[classList")>0 then
		
			 newCustomPageClassList = "<iframe src=""../root_temp-edit/newCustomPage_classListArt_add_show.asp?class="&cstr(request.form("ClassName"))&""" style=""display:none"" ></iframe>"
			 alertCus = "(分类文章列表使用了模板新建独立页面功能,请多等待几秒)"
		
		
		else 
		
			 newCustomPageClassList = ""
			 alertCus = ""
		
		
		
		end if
		
	
		rs.close
		set rs=nothing

		response.Write "<iframe src=""../root_temp-edit/ClearCache.asp"" style=""display:none"" ></iframe><iframe src=""../root_temp-edit/home_action.asp"" style=""display:none"" ></iframe><iframe src=""../root_temp-edit/Article_show.asp"" style=""display:none"" ></iframe>"&create_classPage&""&newCustomPageClassList&""
        
		resultNow = "{"&chr(34)&"title"&chr(34)&":"&chr(34)&""&Safe_ArticleTitle&""&chr(34)&","&chr(34)&"preURL"&chr(34)&":"&chr(34)&""&replace(mypathHtml,MY_sitePath&MY_createFolder_art,MY_secCheckSiteLink&"/"&MY_createFolder_art&"")&""&chr(34)&","&chr(34)&"Complete"&chr(34)&":"&chr(34)&"true"&chr(34)&"}"
		session("resultNow") = resultNow
		
		response.Redirect "../../api/?appAction=true&appid=a&result=ok"

		
		%>
		
		<%
		db.close
		set db=nothing	
		end if	
	End if
		
end if 
  
%>
