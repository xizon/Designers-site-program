<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<script>

function checkAll(){
	for (var i=0;i<form0.myselect.length;i++)
	{
	var e = form0.myselect[i];
	if (e.name != 'selectAll')
	   e.checked = form0.selectAll.checked;
	}
}

$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=文章评论", "link1=<%=MY_sitePath%>admin/article/article_comment.asp", "title2=访客留言", "link2=<%=MY_sitePath%>admin/message/message_edit.asp","title3=关键词过滤", "link3=<%=MY_sitePath%>admin/shield/keywords.asp");

	   }
	);
	
});

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 评论留言管理 | 文章评论
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
                
                <!-- 表单域  -->
                <div id="contentShow">
                      
                      
                          <!-- 搜索 -->
                          <%
						  searchStable = "评论"
						  %>
                          <!--#include file="../_comm/searchBoard.asp" -->
                      
                          <!-- 列表 -->
                        
                                    <form method="post" name="form0" action="action_comm.asp?action_comm=update" target="actionPage" onSubmit="javascript:return ChkFormTip_page('保存成功',1);">
                        
                                                <table width="100%" class="list" cellspacing="1">
                                                    
                                                    <!-- 标题 -->
                                                    <thead class="setTitle">
                                                        <tr>
                                                           <!-- 有多少竖列 -->
                                                           <th>ID</th>
                                                           <th>留言者</th>
                                                           <th>Email</th> 
                                                           <th>前台显示</th>
                                                           <th>内容(点击相关条目查看全部)</th>
                                                           <th>回复</th>
                                                           <th>地址</th>
                                                           <th>删除</th>
                                                        </tr>
                                                        
                                                   </thead>
                                                 
                                                    
                                                    
                                                    <tbody>
                                                    
                                                        <!-- +++++++  内容   begin +++++++  -->
                                                                                                            
														<%if request.querystring("pageno") ="" then
                                                                    pageno=1
                                                                    else
                                                                    pageno=cint(request.querystring("pageno") )
                                                                    end if
                                                                    '---------------------------一部分
                                                                    dim rs,strsql,j
                                                                    j=0
                                                            set rs=server.CreateObject("adodb.recordset")
                                                            strsql="select  * from [ArticleComment] order by ArticleCommentid desc"	     
                                                            rs.open strsql,db,1,1
                                                                 '--------------------------------开始分页
                                                        %>
                                                        <!--#include file="../function/page_first.asp" -->
                                                        <%j=j+1%>
                                                        <%myrep=rs("reply")
                                                        myID=rs("articleid")
                                                        %>	
                                                    
                                                        <!--////////////////循环开始 ////////////////-->
                                                        <tr>
                                                            <td><%=rs("ArticleCommentid")%><input type="hidden" value="<%=rs("ArticleCommentid")%>" name="ArticleCommentid<%=j%>" /></td>
                                                            <td class="link"><a href="http://<%=replace(replace(replace(HTMLDecode(rs("url")),"http",""),"https",""),"://","")%>" target="_blank"><%=rs("username")%></a></td>
                                                            <td><%=rs("Email")%></td>
                                                            <td>
                                                               <div class="on_off-box">
                                                                   <span  class="on_off">
                                                                       <%if rs("view")="1" then%>
                                                                         <input type="checkbox" name="view<%=j%>"  value="0"/>
                                                                      <%else%>	 
                                                                         <input type="checkbox" name="view<%=j%>"  value="0" checked="checked"/>
                                                                      <%end if%>
                                                                    </span>
                                                               </div>
                                                            </td>
                                                            
                                                            <td class="title" onClick="switchInfo('#msg-<%=j%>','','');">
                                                            
															<%=gotTopic(stripHTML(rs("comment")),40)%>(<%=rs("subdate")%>)
                                                            
                                                        
                                                            <div id="msg-<%=j%>" data-open="0" style="display:none" class="msgShowBox">
                                                                 <%=stripHTML(HTMLDecode(rs("comment")))%>
                                                            </div>
                                                            
                                                           
                                                            </td>
                                                            
                                                            <td>
					
                                                               <a  href="javascript:" onClick="switchInfo('#rp<%=j%>','','');">
                                                                <%if myrep=empty then 
                                                                response.Write "<img src=""../../plugins/editInfo_img/9.gif"" border=""0"" title=""回复"" />"
                                                                elseif  myrep<>empty then
                                                                response.Write "<img src=""../../plugins/editInfo_img/9-2.gif"" border=""0"" title=""已回复"" />"
                                                                else
                                                                response.Write "<img src=""../../plugins/editInfo_img/9.gif"" border=""0"" title=""回复"" />"
                                                                end if%>
                                                                </a>
                                                                <div id="rp<%=j%>" style="display:none;" data-open="0" class="repBox">
                                                                    <textarea name="reply<%=j%>" cols="35" rows="2" onClick="this.rows='5'"><%=myrep%></textarea>&nbsp;
                                                                </div>
                    
                                                            </td>
                                                            
                                                            <td>
															<%
                                                                set rs2=server.CreateObject("adodb.recordset")
                                                                strsql2="select  * from [Article] where articleid="&myID  
                                                                rs2.open strsql2,db,1,1
                                                            
                                                                mypath=replace(rs2("FilePath"),"/default.html","")	
                                                                
                                                                rs2.close
                                                                set rs2=nothing
                                                                
                                                            %>	
                                                            
                                                            <a href="<%=mypath%>" title="浏览相关网址" target="_blank"><img src="../../plugins/d-s-management-ui/img/view.gif"></a>
                                                            
                                                             </td>
                                                            <td>
                                                 
                                                                <input type="checkbox" name="myselect" class="checkBox-Normal" value="<%=rs("ArticleCommentid")%>" />
                                                                 
                                                            </td>
                                                        </tr>
                                                        
                                                        <!--////////////////循环结束 ////////////////-->
                                                        
														<%
                                                         rs.movenext
                                                         loop
                                                         %>  
                                                         
                                                         <input type="hidden" value="<%=j%>" name="total" />
                                                         
                                                         <!-- 无数据 -->
                                                         <%if rs.bof and rs.eof then %>
                                                         
                                                            <tr>
                                                                <td class="title" colspan="8">
                                                                 暂时还没有数据！
                                                                </td>
                                                            </tr>
                                                         
                                                         <%end if%>
														 
                                                
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                        
                                                
                                                    </tbody>
                                                    
                                                    
                                                    <!-- 底部 -->
                                                    <tfoot>
                                                    
                                                        <tr>
                                                            <td colspan="8">
                                                            
                                                               <div class="smallBtnBox">
                                                                   <input type="checkbox" name="selectAll" class="checkBox-Normal" value="checkbox" onClick="javascript:checkAll();"/>全选
                                                                   &nbsp;&nbsp;<input type="submit"  value="删除所选"  class="btnSmall" />
                                                               </div>     
                                                               
                                                               <!--#include file="../function/page_second.asp" --> 
                                                                                      
                                                               <div class="btnBox">
                                                               
                                                                   <input type="submit" value="保存" class="sub">
                                                                   
                                                               </div>
                                                               

                                                
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                    
                                                    
                                                </table>
                                            
                                                
                                    </form>  

                                    
            
            
                               
                     </div> 
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->

