<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
  
<%
'剪裁激活
response.Cookies("cAArticle") = 1
%>

<style>
.jqTransformSelectWrapper ul{height:350px; overflow:hidden; overflow-y:auto}
.autoSaveWrapper{margin-top:0; top:590px; }
</style>

<script>


function addTagFrame(){
floatWin('选择标签','<iframe src="../_comm/selectTags.asp" allowTransparency=true name=iframe_tags  frameborder="0" scrolling="no" height="200" width="470"></iframe>',480,210,20,0,0,1);
	//for IE6
	if ($.browser.msie && $.browser.version < 7) {
		setTimeout('document.frames("iframe_tags").location.reload();',500);
	}


}



$(document).ready(function() {

	//外链地址判断
	imgInputChkSrc("#objimg","#objdefault_imgShow");
	
});
				
</script>

<!-- //=====================================================HTML编辑器  begin  -->
<script>
var Path_uploadFUnFile = "../../plugins/HTMLEdit/_ds_fun/upload.asp";
var Path_saveDraft = "../../plugins/HTMLEdit/_ds_fun/AutoSave_show.asp";  
var Path_saveFunFile = "../../plugins/HTMLEdit/_ds_fun/AutoSaver.asp";
var Path_chkDangerImgFunFile = "../../plugins/HTMLEdit/_ds_fun/chk_dangerImg.asp";
var Path_saveFormItemID = "#Content";
</script>
<script type="text/javascript" charset="utf-8" src="../../plugins/HTMLEdit/xheditor.js"></script>
<script type="text/javascript">
$(pageInit);
function pageInit(){
	$.extend(xheditor.settings,{shortcuts:{'ctrl+enter':submitForm}});
	$(Path_saveFormItemID).xheditor({upImgUrl:Path_uploadFUnFile,upImgExt:"jpg,jpeg,gif,png",upFlashUrl:Path_uploadFUnFile,upFlashExt:"swf",upMediaUrl:Path_uploadFUnFile,upMediaExt:"wmv,avi,wma,mp3,mid"});
}
function submitForm(){$('#formM').submit();}


</script>
<!-- //=====================================================HTML编辑器  end  -->

<script>
<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=文章日志", "link1=<%=MY_sitePath%>admin/article/article_add.asp", "title2=站内微博", "link2=<%=MY_sitePath%>admin/mood/mood_add.asp","title3=作品案例/产品", "link3=<%=MY_sitePath%>admin/SuccessWork/SuccessWork_add.asp","title4=通知公告", "link4=<%=MY_sitePath%>admin/notice/notice_add.asp");

	   }
	);
	
});

<!-- form ui[js]  end -->

</script>


<!--**************添加板块*************-->
<script type="text/javascript">



$(document).ready(function() {
	var limitNum = 200;
	$('#Summary').inputlimiter({
		limit: limitNum,
		boxId: 'limitingtext',
		boxAttach: false
	});	
	
	var limitNum2 = 80;
	$('#ArticleTitle').inputlimiter({
		limit: limitNum2,
		boxId: 'limitingtext2',
		boxAttach: false
	});	
	
	
	var limitNumKeyWords = 100;
	$('#KeyWord').inputlimiter({
		limit: limitNumKeyWords,
		boxId: 'limitingtext3',
		boxAttach: false
	});	
	
	
	document.getElementById("limitingtext3").innerHTML = "0/" + limitNumKeyWords;
	document.getElementById("limitingtext2").innerHTML = "0/" + limitNum2;
	document.getElementById("limitingtext").innerHTML = "0/" + limitNum;
	

	//ajax读取关键词
	$("#ArticleTitle").keyup(function(){
	  var thisData=$("#ArticleTitle").val();
	  $.post("getTags.asp?q=" + thisData,{suggest:thisData},function(result){
		$("#KeyWord").val(result);
		
	  });
	});	
	
	//ajax提取文章内容作为摘要
	$("#summGet,#sub-con").click(function(){
		setInterval(function(){
			//$("#summGet").attr("disabled","disabled");
			//$("#Summary,#limitingtext,#Summ_star").slideUp(500);
	
			  var thisData=$("#Content").val();
			  $.post("summGet.asp?con=" + thisData,{suggest:thisData},function(result){
				$("#Summary").val(result);
			  });
	
		},100);
	});

});

//自动保存
$(function () {
	$('#ArticleTitle,#ClassName,#KeyWord,#objimg,#Content,#definePath,#Summary').autosave();
});

//表单判断
function checkForm(){
	if(document.formM.ArticleTitle.value==""){
		ChkFormTip_page("标题不能为空",0);
		return false;
	}
	if(document.formM.ClassName.value==""){
		ChkFormTip_page("类别不能为空",0);
		return false;
	}
	//ok
	ChkFormTip_page("正在处理中",2);
	$('#needCreateLoadingBtn').fadeOut(500);
	return true;
		
}

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="article_list.asp" class="link">列表管理</a>
                <a href="article_class.asp" class="link">分类管理</a>
                <a href="article_channel.asp" class="link">频道管理</a>
                <a href="article_top.asp" class="link">置顶管理</a>
                
                <!-- 当前位置 -->
                当前位置： 内容发布 | 文章日志
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
                
                <!-- 表单域  -->
                <div id="contentShow">
                
                    <form name="formM"  action="action.asp?action=articleAdd" method="post"  onSubmit="javascript:return checkForm();">
  
                                <table width="100%" class="setting" cellspacing="0">
                                    
                                    <!-- 标题 -->                        
                                    <thead class="setHead">
                                        <tr>
                                               <th  class="setTitle"></th>
                                               <th colspan="2"></th>
                                        </tr>
                                        
                                    </thead>
                                    
                                 
                                    <!-- 底部 -->
                                    <tfoot>
                                    
                                        <tr>
                                            <td colspan="3">         
                                                <div class="btnBox">
                                                    <img class="autosave" align="absmiddle"  src="../../plugins/d-s-management-ui/img/manage/autosave-btn1.GIF"> <img class="autosave_restore" src="../../plugins/d-s-management-ui/img/manage/autosave-btn2.GIF" align="absmiddle"> <a href="javascript:" class="autosave_removecookies"><img src="../../plugins/d-s-management-ui/img/manage/autosave-btn3.GIF" align="absmiddle"></a> <span class="autosave_saving">Saving&#8230;</span>&nbsp;&nbsp;<input type="submit" value="发布" class="sub" id="needCreateLoadingBtn">
                                                    <!--<input type="reset" value="重置" class="reset">-->
                                              </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                    
                                    <!-- +++++++  内容   begin +++++++  -->
                                    <tbody>
            
                                        <tr>
                                            <td class="title">文章标题：<span class="backStage_star">*</span></td>
                                            <td colspan="2" class="ui-element">
                                                <input type="text" name="ArticleTitle" id="ArticleTitle"  size="40" >&nbsp;&nbsp;<span id="limitingtext2"></span>
                                            </td>
                                        </tr>  
                                        
                                        <tr>
                                            <td class="title">类别：<span class="backStage_star">*</span></td>
                                            <td colspan="2" class="ui-element">
                                               <select name="ClassName" id="ClassName">
                                                    <option value="" selected="selected">请选择类别</option>
                                                    <%
													set rs =server.createobject("adodb.recordset")
													sql="select * from [articleClass]"
													rs.open sql,db,1,1
                                                    do while not rs.eof and not rs.bof
                                                  %>
                                                    <option value="<%=rs("ClassName")%>"><%=rs("ClassName")%></option>
                                                    <%
                                                  rs.movenext
                                                  loop
												  
													  '无分类
													if rs.bof and rs.eof then response.Write "<script>floatWin('温馨提示','<div class=tipsFloatWin>请先至少添加一个类别再继续操作！&nbsp;&nbsp;<a href=article_class.asp?action=add>添加分类</a></div>',350,0,100,1,0,0);</script>"

												  
												  rs.close
												  set rs=nothing
                                                  %>
                                                </select>
                                             &nbsp;&nbsp; <a href="article_class.asp?action=add">添加分类</a>
                                            </td>
                                        </tr>
                                        
                                        
                                        <input type="hidden" name="private" value="1" />
                                        
                                        <tr>
                                            <td class="title">关键词<a href="javascript:void(0)" class="help" data-info="<a href='javascript:' onclick='addTagFrame()'>快速选择关键词</a><br>使用空格隔开,最多10个"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                            <td colspan="2" class="ui-element">
                                                <input type="text" name="KeyWord" id="KeyWord"  size="40" >&nbsp;&nbsp;<span id="limitingtext3"></span>

                                            </td>
                                        </tr>   
                                        
                                        <tr onMouseOver="tipShow('.tipsFloatWin-upload',0)" onMouseOut="tipShow('.tipsFloatWin-upload',1)">
                                            <td class="title">主题图片/文件上传<img src="../../plugins/d-s-management-ui/img/pic.gif">：</td>
                                            <td colspan="2" class="ui-element">
                                                <input type="text" name="mainImg" id="objimg" size="55" class="help" data-info="您可以本地上传图片，或者直接插入http外链图片网址<br>(如果您在文章内容中要插入文件，可以从这里上传并Ctrl+C复制地址使用)">
                                                <br clear="all">
                                                <div class="tipsFloatWin-upload"><iframe src=../upload/upload_file.html?fileID=img&imgcut=true class=backStage_uploadFrame allowTransparency=true  frameborder=0 scrolling=No></iframe><br>图片推荐尺寸：<span class=backStage_star><%=MY_AbbreviativeImg_list_W%>×<%=MY_cutImgSelectDefaultHeight%></span>,支持本地上传和http外链图片&nbsp;<a href=../img_editor.html target=_blank>在线做图</a><br>如果未上传图片,根据列表页的模板设计,可以实现<strong>只输出文字效果</strong>,或者依旧保持<strong>图文并茂</strong></div>
                                                <span id="objdefault_imgShow"></span>

                                            </td>
                                        </tr>     
                                        
                            
                                                    
                                        <tr id="conBox">
                                            <td colspan="3">

                                                 <!-- //=====================================================HTML编辑器  begin -->
                                                    <textarea name="Content" id="Content" rows="25" cols="80" style="width: 100%; height:350px;"></textarea>
                                                    
                                                    <span id="frameImgChk"></span>
                                                    <div class="autoSaveWrapper">
                                                      <input onclick="SetAutoSave();" type="checkbox" id="Draft_AutoSave" value="1" checked="true"  />
                                                      自动保存？<span id="saveStateNow"></span><!-- AutoSaveMsg显示返回信息 --><a onclick="AutoSave();" href="javascript:" class="autoSavebutton">暂存</a>&nbsp;<a  href="javascript:"  class="autoSavebutton" onclick="FileView();">查看草稿</a>&nbsp;<a  href="javascript:" class="autoSavebutton"  onclick="FileRestore();">恢复</a>
                                                    </div>	
                                                     
                                                     
                                                    <script type="text/javascript" src="../../plugins/HTMLEdit/_ds_fun/AutoSaver.js"></script>
                                                
                                                
                                                <!-- //=====================================================HTML编辑器  end -->

                                            </td>
                                        </tr>            
                                        
                                        
                                        <tr id="moreShow" onClick="$('.more-item').slideDown(500);$('#moreShow').slideUp(500);">
                                            <td colspan="3"  class="ui-element">
                                               <div class="btnBox-iframe"><input type="button" value="更多设置" class="ui-btn"></div>
                                            </td>
                                        </tr>        
                                                   

                                        
										 <%if MY_createFileMode = 1 then %>
                                            <tr class="more-item" style="display:none">
                                                <td class="title">自定义文件名<a href="javascript:void(0)" class="help" data-info="生成页面的名字,不需要写后缀"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                                <td colspan="2" class="ui-element">
                                                  <input type="text" name="definePath" id="definePath"  onclick="select()"  value="<%=MD5(year(now)&month(now)&day(now)&hour(now)&minute(now)&second(now),1)%>" size="40" >
    
                                                </td>
                                            </tr>  
                                         <%end if%>  
                                         
                                        <tr class="more-item" style="display:none">
                                            <td class="title">文章摘要<a href="javascript:void(0)" class="help" data-info="用于列表页面的文章简介(也可相当于博客截取)和内容页meta描述"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                            <td colspan="2">
                                                <textarea  cols="80" rows="3" name="Summary" id="Summary"></textarea>&nbsp;&nbsp;<span id="limitingtext"></span>
                                            </td>
                                        </tr>       
                                        
                                                   
                                        <tr class="more-item" style="display:none">
                                            <td class="title">从内容中提取摘要：</td>
                                            <td colspan="2">
                                               <div class="on_off-box" id="sub-con">
                                                    <span  class="on_off"><input type="checkbox"  name="summGetBtn" value="1" checked/></span>
                                               </div>
                                            </td>
                                        </tr>     
                                           
                                         
                                        <tr class="more-item" style="display:none">
                                            <td class="title">置顶：</td>
                                            <td colspan="2">
                                               <div class="on_off-box">
                                                    <span  class="on_off"><input type="checkbox" name="top" value="1" /></span>
                                               </div>
                                            </td>
                                        </tr>  
                                                             

                                        <tr class="more-item" style="display:none">
                                            <td class="title">图片同步至文章内容<a href="javascript:void(0)" class="help" data-info="上传的主题图片同步发布到文章"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                            <td colspan="2">
                                               <div class="on_off-box">
                                                    <span  class="on_off"><input type="checkbox" name="mainPicToContent" id="mainPicToContent" value="1" /></span>
                                               </div>
                                            </td>
                                        </tr>     
                                                 
                                                    
                                          
                                       <!-- +++++++  内容   end +++++++  -->
                                
                                
                                    </tbody>
                                    
                                </table>
   
                    </form>  
                                
            
            
                               
                     </div> 
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->
