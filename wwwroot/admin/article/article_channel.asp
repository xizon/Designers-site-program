<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<script>
//表单判断
function checkForm(){
	if(document.formM.channelName.value==""){
		ChkFormTip_page("频道名称不能为空",0);
		return false;
	}
	//ok
	loadingCreate();
	return true;
		
}

$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=文章日志", "link1=<%=MY_sitePath%>admin/article/article_add.asp", "title2=站内微博", "link2=<%=MY_sitePath%>admin/mood/mood_add.asp","title3=作品案例/产品", "link3=<%=MY_sitePath%>admin/SuccessWork/SuccessWork_add.asp","title4=通知公告", "link4=<%=MY_sitePath%>admin/notice/notice_add.asp");

	   }
	);
	
	
});

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="article_add.asp" class="link">发布文章</a>
                <a href="article_list.asp" class="link">列表管理</a>
                <a href="article_class.asp" class="link">分类管理</a>
                <a href="article_top.asp" class="link">置顶管理</a>
                
                <!-- 当前位置 -->
                当前位置： 文章日志 | 频道管理
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
                
                <!-- 后台提示 -->
                
                <div class="notification information png_bg content-alert">
                    <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        <strong>[频道]</strong>就类似二级分类，<strong>[文章类别]</strong>就类似三级分类,二级分类下没有文章,只有三级分类列表,文章都分布在三级分类里。
                    </div>
                </div> 
                
                <!-- 表单域  -->
                <div id="contentShow">
                
                    <div class="addInfo"><a href="javascript:" onClick="switchInfo('#addInfo','关闭添加','添加频道');"><span class="io">添加频道</span></a></div>
                    
                                    
                                  <div id="addInfo" data-open="0" style="display:none">
                                  
                                    <form name="formM" action="action.asp?action=addChannel" method="post" onSubmit="javascript:return checkForm();"> 
                                           
                                                <table width="100%" class="setting" cellspacing="0">
                                                    
                                                    <!-- 标题 -->                        
                                                    <thead class="setHead-class">
                                                        <tr>
                                                               <th  class="setTitle-class"></th>
                                                               <th colspan="2"></th>
                                                        </tr>
                                                        
                                                    </thead>
                                                    
                                                 
                                                    <!-- 底部 -->
                                                    <tfoot>
                                                    
                                                        <tr>
                                                            <td colspan="3"  class="ui-element">         
                                                                <div class="btnBox-iframe">
                                                                    <input type="submit" value="添加信息" class="ui-btn" id="needCreateLoadingBtn">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                    
                                                    <!-- +++++++  内容   begin +++++++  -->
                                                    <tbody>
                            
                                                        <tr>
                                                            <td class="title">频道名称：<span class="backStage_star">*</span></td>
                                                            <td colspan="2" class="ui-element">
                                                            <input name="channelName" type="text" size="25"  />
                                                            <input name="channelClass" type="hidden" value="文章频道"  />	   
                                                              
                                                            </td>
                                                        </tr>  
                                                  
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                                                
                                                    </tbody>
                                                    
                                                </table>
                   
                   
                                    </form>  
 
                                   
                                </div>    
                                
                                
                                
                          <!-- 列表 -->
                        

                                    <table width="100%" class="list" cellspacing="1">
                                        
                                        <!-- 标题 -->
                                        <thead class="setTitle">
                                            <tr>
                                               <!-- 有多少竖列 -->
                                               <th>ID</th>
                                               <th>频道名称</th>
                                               <th>操作</th>
                                            </tr>
                                            
                                       </thead>
                                     

                                        <!-- +++++++  内容   begin +++++++  -->
                                        <tbody>
                                                                                                
                                            <%
                                                '----------显示记录模块
                                                j=0
                                                set rs =server.createobject("adodb.recordset")
                                                sql="select * from [channelList]"
                                                rs.open sql,db,1,1
                                                k = 1
                                                do while not rs.eof and not rs.bof
                                                j=j+1
                                                
                                            %>  	
                                        
                                            <!--////////////////循环开始 ////////////////-->
                                            
                                            <tr id="del<%=j%>">
                                                <td><%=j%></td>
                                                <td class="title">
                                                <%=rs("channelName")%>
                                                <input name="channelClass<%=j%>" type="hidden"  value="文章频道"/>
                                                </td>
                                                <td>
                                                    <!-- Icons -->
                                                     <a onclick="javascript:floatWin('温馨提示','<div align=center><span id=del_Btn<%=j%>><a href=action.asp?action=delRecordChannel&name=<%=rs("channelName")%> target=delFrame class=backBtn onclick=javascript:$(\'#del<%=j%>,#del_Btn<%=j%>\').fadeOut(2);$(\'#delOk<%=j%>\').fadeIn(500);>确认删除?</a></span><span id=delOk<%=j%> style=display:none>√删除成功！</span></div>',350,50,150,0,0,1);" target="delFrame" href="javascript:"><img src="../../plugins/d-s-management-ui/img/cross.png" title="删除" /></a>

                                                </td>
                                            </tr>
                                            
                                            <!--////////////////循环结束 ////////////////-->
                                            
                                            <%
                                             k = k + 1
                                             rs.movenext
                                             loop
                                             %>  
                                             
                                             <iframe name="delFrame" style="display:none"></iframe>
                                             <input type="hidden" value="<%=j%>" name="total" />
                                             
                                             <!-- 无数据 -->
                                             <%if rs.bof and rs.eof then %>
                                             
                                                <tr>
                                                    <td class="title" colspan="3">
                                                    暂时还没有数据！
                                                    </td>
                                                </tr>
                                             
                                             <%end if%>
                                             
                                    
                                           <!-- +++++++  内容   end +++++++  -->
                                    
            
                                    
                                        </tbody>
                                        
                                    </table>


                               
                     </div> 
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  

<!--#include file="../_comm/page_bottom.asp" -->
