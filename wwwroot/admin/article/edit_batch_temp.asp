<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<script>

$(function(){


	
	
	$("#slider").slider({ 
	    value: <%=MY_pageNumMulteUpTemp%>,
		step: 5, 
		max: 500,
		min: 5,
		range: "min",
		slide: function(event, ui){ 
			$("#sNum").val(ui.value); 
			$(".txtShow").html(ui.value + "页");
		} 
	
	}); 

	

});


<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=栏目页面静态生成", "link1=<%=MY_sitePath%>admin/root_temp-edit/index.asp", "title2=文章内容页批量生成", "link2=<%=MY_sitePath%>admin/article/edit_batch_temp.asp","title3=案例/产品内容页批量生成", "link3=<%=MY_sitePath%>admin/SuccessWork/edit_batch_temp.asp");

	   }
	);
	
});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="../root_temp-edit/index.asp" class="link">栏目页生成</a>
                
                <!-- 当前位置 -->
                当前位置： 手动静态生成 | 文章内容页批量生成
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
                
                <!-- 表单域  -->
                <div id="contentShow">
                      
                          <!-- 列表 -->
                                      
                                <form method="post" action="../website/_batchCreateHtmlContentNum.asp?action=update&url=../article/edit_batch_temp.asp" target="actionPage" onSubmit="javascript:return ChkFormTip_page('保存成功',1);">
                                
                                
                                    <table width="100%" class="setting" cellspacing="0">
            
                                        <!-- +++++++  内容   begin +++++++  -->
                                        
                                        <tbody>
                                            <tr title="根据服务器的承受能力调整,推荐150条">
                                               <td class="title" width="150">单次批量生成<span class="txtShow"><%=MY_pageNumMulteUpTemp%></span></td>
                                               <td width="103" class="ui-element">
                                                <input type="hidden" id="sNum" name="sNum" value="<%=MY_pageNumMulteUpTemp%>">
                                               <input type="submit" value="保存设置" class="ui-btn"></td>
                                                <td><div class="sliderStyle" id="slider"></div></td>
                                            </tr>
                                            
            
                                              
                                           <!-- +++++++  内容   end +++++++  -->
                                    
                                    
                                        </tbody>
                                        
                                    </table>  
                                    
                                    </form>
                        
                                    <form method="post" name="form0" action="action_batch_temp.asp?action=update">
                        
                                                <table width="100%" class="list" cellspacing="1">
                                                    
                                                    <!-- 标题 -->
                                                    <thead class="setTitle">
                                                        <tr>
                                                           <!-- 有多少竖列 -->
                                                           <th>ID</th>
                                                           <th>页面地址</th>
                                                        </tr>
                                                        
                                                   </thead>
                                                 
                                                 
                                                    <!-- 底部 -->
                                                    <tfoot>
                                                    
                                                        <tr>
                                                            <td colspan="2">
                  
                                                               <div class="btnBox">
                                                               
                                                                   <input type="submit" value="批量更新" class="sub" onclick="loadingCreate();" id="needCreateLoadingBtn">
                                                                   
                                                               </div>
                                                
                                                            </td>
                                                        </tr>
                                                    </tfoot> 
                                                    
                                                    <tbody>
                                                    
                                                       <!-- +++++++  内容   begin +++++++  -->
														<%if request.querystring("pageno") ="" then
                                                                        pageno=1
                                                                    else
                                                                    pageno=cint(request.querystring("pageno") )
                                                                    end if
                                                                    '---------------------------一部分
                                                                    dim rs,strsql,j
                                                                    j=0
                                                            set rs=server.CreateObject("adodb.recordset")
                                                            strsql="select  * from [Article] order by Articleid desc"
                                                            rs.open strsql,db,1,1
                                                                 '--------------------------------开始分页
                                                        %>
                                                        <%'分页第二部分
                                                                 if not rs.eof and not rs.bof then
                                                                    rs.pagesize=MY_pageNumMulteUpTemp
                                                                    pagetotal=rs.pagecount
                                                                    rs.absolutepage=pageno
                                                                    session("pageno")=pageno
                                                                        if pagetotal<10 then
                                                                            minpage=1
                                                                            maxpage=pagetotal
                                                                        else
                                                                    '///////////////////////////////////////////////////
                                                                        
                                                                            if pageno<5 then'判断当前页
                                                                                minpage=1
                                                                                maxpage=7 '显示的页码链接
                                                                            end if
                                                                        '\\\\\\\\\\\\\\\\\\\\
                                                                            if pageno>=5 then'判断当前页
                                                                                minpage=pageno-3    '向左扩张的页码链接
                                                                                maxpage=pageno+3	'向右扩张的页码链接
                                                                            end if		
                                                                        '\\\\\\\\\\\\\\\\\\\\
                                                                
                                                                                if maxpage>pagetotal then
                                                                                    minpage=pageno-4			
                                                                                    maxpage=pagetotal						
                                                                                end if
                                                                        end if
                                                                        '///////////////////////////////////////////////////
                                                                 else
                                                                    response.write ""
                                                                 end if
                                                                 '--------------------------------------
                                                             dim i
                                                             i=MY_pageNumMulteUpTemp	'页面显示的内容/项目数 与上面的pagesize相等 
                                                        %>	
                                                        
                                                        
                                                        <tr>
                                                            <td colspan="2">
                                                               
                                                               <!--#include file="../function/page_second.asp" --> 
                                                
                                                            </td>
                                                        </tr>
                                                        
														<%	  
                                                            do while not rs.eof and I>0
                                                            i=i-1
                                                        
                                                        %>
                                                        <%j=j+1%>	
                                                        
                                                    
                                                        <!--////////////////循环开始 ////////////////-->
                                                        <tr>
                                                            <td><%=rs("articleid")%><input type="hidden" value="<%=rs("articleid")%>" name="articleid<%=j%>" /></td>
                                                            <td  class="title"><%=rs("filePath")%><input name="filePath<%=j%>"  type="hidden" value="<%=rs("filePath")%>"/></td>

                                                        </tr>
                                                        
                                                        <!--////////////////循环结束 ////////////////-->
                                                        
														<%
                                                         rs.movenext
                                                         loop
                                                         %>  
                                                         
 
                                                         <input type="hidden" name="total" value="<%=j%>" />
                                                         
                                                         <!-- 无数据 -->
                                                         <%if rs.bof and rs.eof then %>
                                                         
                                                            <tr>
                                                                <td class="title" colspan="2">
                                                                 暂时还没有数据！
                                                                </td>
                                                            </tr>
                                                         
                                                         <%end if%>
                                                         
                                                         

                                                        <tr>
                                                            <td colspan="2">
                                                               
                                                               <!--#include file="../function/page_second.asp" --> 
                                                
                                                            </td>
                                                        </tr>
														 
                                                
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                        
                                                
                                                    </tbody>
                                                    

                                                    
                                                </table>
                                            
                                                
                                    </form>  

                                    
            
            
                               
                     </div> 
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->





