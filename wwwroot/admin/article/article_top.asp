<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<script>
<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=文章日志", "link1=<%=MY_sitePath%>admin/article/article_add.asp", "title2=站内微博", "link2=<%=MY_sitePath%>admin/mood/mood_add.asp","title3=作品案例/产品", "link3=<%=MY_sitePath%>admin/SuccessWork/SuccessWork_add.asp","title4=通知公告", "link4=<%=MY_sitePath%>admin/notice/notice_add.asp");

	   }
	);
	
});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="article_add.asp" class="link">发布文章</a>
                <a href="article_list.asp" class="link">列表管理</a>
                <a href="article_class.asp" class="link">分类管理</a>
                <a href="article_channel.asp" class="link">频道管理</a>
                
                <!-- 当前位置 -->
                当前位置： 文章日志 | 置顶管理
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
          
                <!-- 后台提示 -->
                
                <div class="notification information png_bg content-alert">
                    <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        1. 更换置顶状态，您需要手动操作<a href="../root_temp-edit/index.asp">栏目页面静态生成</a>，才能及时看到效果<br>
                        2. 或者等待系统达到<strong>静态自动更新间隔</strong>时间，访问页面时系统即可更新静态页面，您也可以通过导航<strong>网站设置</strong>，设置此时间<br>
                        3.置顶文章需要模板里有相关标签：{#loopListContentShow:article.top/} (首页置顶文章)，${listcontent_comm_topArticle} (列表页置顶文章)
                    </div>
                </div>
                
                <!-- 表单域  -->
                <div id="contentShow">
                
					 <%
					 if callCommInfoChk("{#loopListContentShow:article.top/}") = false then response.Write "<script>floatWin('温馨提示','<div class=tipsFloatWin>目前模板里暂时没有<strong>首页置顶文章</strong>标签，无法看到效果<br>(其它原因：此主题并没有设计首页置顶文章区块或者没有调用首页置顶文章标签)</div>',350,0,100,0,0,0);</script>"
					 if callCommInfoChk("${listcontent_comm_topArticle}") = false then response.Write "<script>floatWin('温馨提示','<div class=tipsFloatWin>目前模板里暂时没有<strong>列表页置顶文章</strong>标签，无法看到效果<br>(其它原因：此主题并没有设计列表页置顶文章区块或者没有调用列表页置顶文章标签)</div>',350,0,100,0,0,0);</script>" 
                     %>
             
                      
                          <!-- 列表 -->
                        
                                    <form method="post" name="form0" action="action.asp?action=top" target="actionPage" onSubmit="javascript:return ChkFormTip_page('保存成功',1);" >
                        
                                                <table width="100%" class="list" cellspacing="1">
                                                    
                                                    <!-- 标题 -->
                                                    <thead class="setTitle">
                                                        <tr>
                                                           <!-- 有多少竖列 -->
                                                           <th>ID</th>
                                                           <th>标题</th>
                                                           <th>所属分类</th> 
                                                           <th>操作</th>
                                                        </tr>
                                                        
                                                   </thead>
                                                 
                                                    
                                                    
                                                    <tbody>
                                                    
                                                       <!-- +++++++  内容   begin +++++++  -->
                                                                                                            
														<%if request.querystring("pageno") ="" then
                                                                        pageno=1
                                                                    else
                                                                    pageno=cint(request.querystring("pageno") )
                                                                    end if
                                                                    '---------------------------一部分
                                                                  dim rs,strsql
                                                                  set rs=server.CreateObject("adodb.recordset")
                                                                  strsql="select * from [article] where top=1 order by articleid desc"
                                                                  rs.open strsql,db,1,1	
                                                                 '--------------------------------开始分页
                                                        %>
                                                        <!--#include file="../function/page_first.asp" -->
                                                        <%j=j+1%>
                                                    
                                                        <!--////////////////循环开始 ////////////////-->
                                                        <tr>
                                                            <td><%=rs("articleid")%><input type="hidden" value="<%=rs("articleid")%>" name="articleid<%=j%>" /></td>
                                                            <td  class="title"><%=rs("ArticleTitle")%></td>
                                                            <td><%=rs("classname")%></td>
                                                            <td>
                                                               <div class="on_off-box">
                                                                    <span  class="on_off">
                                                                          <%if rs("top")="1" then%>
                                                                          <input type="checkbox" name="top<%=j%>" value="1" checked="checked"/>
                                                                          <%else%>	 
                                                                          <input type="checkbox" name="top<%=j%>"  value="1"/>
                                                                          <%end if%>
                                                                    
                                                                    </span>
                                                               </div>
                                                            </td>


                                                        </tr>
                                                        
                                                        <!--////////////////循环结束 ////////////////-->
                                                        
														<%
                                                         rs.movenext
                                                         loop
                                                         %>  
 
                                                         <input type="hidden" name="total" value="<%=j%>" />
                                                         
                                                         <!-- 无数据 -->
                                                         <%if rs.bof and rs.eof then %>
                                                         
                                                            <tr>
                                                                <td class="title" colspan="4">
                                                                 暂时还没有数据！
                                                                </td>
                                                            </tr>
                                                         
                                                         <%end if%>
														 
                                                
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                        
                                                
                                                    </tbody>
                                                    
                                                    
                                                    <!-- 底部 -->
                                                    <tfoot>
                                                    
                                                        <tr>
                                                            <td colspan="4">
                                                               
                                                               <!--#include file="../function/page_second.asp" --> 
                                                               
                                                               <div class="btnBox">
                                                               
                                                                   <input type="submit" value="保存" class="sub">
                                                                   
                                                               </div>
                                                               
                                                
                                                            </td>
                                                        </tr>
                                                    </tfoot> 
                                                    
                                                </table>
                                            
                                                
                                    </form>  

                                    
            
            
                               
                     </div> 
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->



