<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
linkChk="http://"
%>
<style>
.jqTransformSelectWrapper ul{height:175px; overflow:hidden; overflow-y:auto}
</style>

<script>

//表单判断
function checkForm(){
	if(document.formM.content.value==""){
		ChkFormTip_page("内容不能为空",0);
		return false;
	}
	//ok
	loadingCreate();
	return true;
		
}

$(document).ready(function() {
	var limitNum = 140;
	$('#content').inputlimiter({
		limit: limitNum,
		boxId: 'limitingtext',
		boxAttach: false
	});	
	


	document.getElementById("limitingtext").innerHTML =  "0/" + limitNum;
	
});
	

	
function addLink(){
    $("#linkM").fadeIn(600);
	$("#linkClose").fadeIn(600);

}
function closeLink(){
    $("#linkM").fadeOut(600);
    $("#linkClose").fadeOut(600);
}
	


<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=文章日志", "link1=<%=MY_sitePath%>admin/article/article_add.asp", "title2=站内微博", "link2=<%=MY_sitePath%>admin/mood/mood_add.asp","title3=作品案例/产品", "link3=<%=MY_sitePath%>admin/SuccessWork/SuccessWork_add.asp","title4=通知公告", "link4=<%=MY_sitePath%>admin/notice/notice_add.asp");


	   }
	);
	
	
});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="mood_edit.asp" class="link">列表管理</a>
                <a href="../website/_moodChkCode.asp" class="link">校验码设置</a>
                
                <!-- 当前位置 -->
                当前位置： 内容发布 | 站内微博
            
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          

          
                <!-- 表单域  -->
                <div id="contentShow">
                
                                        <form name="formM"  action="action.asp?action=add" method="post" onSubmit="javascript:return checkForm();">
                                               
                                                    <table width="100%" class="setting" cellspacing="0">
                                                        
                                                        <!-- 标题 -->                        
                                                        <thead class="setHead">
                                                            <tr>
                                                                   <th  class="setTitle"></th>
                                                                   <th colspan="2"></th>
                                                            </tr>
                                                            
                                                        </thead>
                                                        
                                                     
                                                        <!-- 底部 -->
                                                        <tfoot>
                                                        
                                                            <tr>
                                                                <td colspan="3">         
                                                                    <div class="btnBox">
                                                                        <input type="submit" value="发布" class="sub" id="needCreateLoadingBtn">
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tfoot>
                                                        
                                                        
                                                        <tbody>
                                                        
                                                           <!-- +++++++  内容   begin +++++++  -->
                                
                                                            <tr>
                                                                <td class="title">心情<a href="javascript:void(0)" class="help" data-info="可不填，前台输出采用非图片类型输出"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                                                <td colspan="2" class="ui-element">
                                                                                                
                                                                        <select name="face">
                                                                          <option value="0" selected="selected">选择心情</option>
                                                                          <option value="1">得意</option>
                                                                          <option value="2">乾杯</option>
                                                                          <option value="3">滿足</option>
                                                                          <option value="4">沒睡醒</option>        
                                                                          <option value="5">狡猾</option>
                                                                          <option value="6">被打</option>
                                                                          <option value="7">無言</option>
                                                                          <option value="8">無奈</option>
                                                                          <option value="9">裝傻</option>
                                                                          <option value="10">驚訝</option>
                                                                          <option value="11">發現</option>
                                                                          <option value="12">驚嚇</option>
                                                                          <option value="13">冷</option>
                                                                          <option value="14">沒辦法</option>
                                                                          <option value="15">貓咪臉</option>
                                                                          <option value="16">疑惑</option>
                                                                          <option value="17">阿達</option>
                                                                          <option value="18">重創</option>
                                                                          <option value="19">不</option>
                                                                          <option value="20">懷疑</option>
                                                                          <option value="21">睏</option>
                                                                          <option value="22">崇拜</option>
                                                                          <option value="23">我想想</option>
                                                                          <option value="24">生氣</option>
                                                                          <option value="25">就是你</option>
                                                                          
                                                                        </select>
                                                                  
                                                                </td>
                                                            </tr>  
                                                     
																 <%
                                                                 if request.Cookies("upImgdefault") = "" or request.Cookies("upImgdefault")=null or request.Cookies("upImgdefault")=empty then
                                                                     thisPicPath = ""
                                                                 else
                                                                     thisPicPath = request.Cookies("upImgdefault")
                                                                 
                                                                 end if
                                                                 %>
                                                                 <input name="obj" id="objdefault"  type="hidden"  value="<%=thisPicPath%>" />   
                                                                       
                                                            <tr>
                                                                <td class="title">内容：<span class="backStage_star">*</span></td>
                                                                <td colspan="2" class="ui-element">
                                                                    <textarea  cols="80" rows="5" name="content" id="content"></textarea>&nbsp;&nbsp;<span id="limitingtext"></span>
                                                                </td>
                                                            </tr>  
                                                     
                                                            <tr onMouseOver="tipShow('.tipsFloatWin-upload-min',0)" onMouseOut="tipShow('.tipsFloatWin-upload-min',1)">
                                                                <td></td>
                                                                <td colspan="2">
                                                                    <a href="javascript:"><img src="../../images/skins/temp_style/mood_image.gif" align="absmiddle" border="0">&nbsp;图片</a><a href="javascript:addLink();"><img src="<%=MY_sitePath%>images/skins/temp_style/mood_link.gif" align="absmiddle" border="0">&nbsp;链接</a>&nbsp;&nbsp;<input name="linkM" style="display:none" id="linkM" type="text" size="12" class="focusJS" data-value="<%=linkChk%>" value=""/>&nbsp;<a href="javascript:closeLink()" id="linkClose" style="display:none;">×</a>
                                                                    
                                                             <br clear="all">
                                                             <span id="objdefault_imgShow"></span>
                                                             <div class="tipsFloatWin-upload-min"><iframe src=../upload/upload_imgID.html?imgID=default class=backStage_uploadFrame allowTransparency=true  frameborder=0 scrolling=No></iframe><br>只支持本地上传&nbsp;<a href=../img_editor.html target=_blank>在线做图</a></div> 
                                                                    
                                                                </td>
                                                            </tr>  
                                                            
           
                                                           <!-- +++++++  内容   end +++++++  -->
                                                    
                                                    
                                                        </tbody>
                                                        
                                                    </table>
                       
                       
                                        </form>  
             


                               
                     </div> 
          
                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->



