<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<script>
<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=数据库备份/还原", "link1=<%=MY_sitePath%>admin/data_manage/restore_backup.asp", "title2=标签库", "link2=<%=MY_sitePath%>admin/edit_file/edit_thesaurus.asp","title3=文件批量字符替换", "link3=<%=MY_sitePath%>admin/edit_file/edit_batchReplaceStr.asp","title4=上传文件管理", "link4=<%=MY_sitePath%>admin/data_manage/attachments.asp","title5=IP权限", "link5=<%=MY_sitePath%>admin/shield/IP_filter.asp");

	   }
	);
	
	$(".sub").click(
	   function() {
			window.location.href="attachments.asp?action=attachment";
	   }
	);
	
	
});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 高级功能 | 上传文件管理
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
                    <!-- 独立内容区 -->
                    <div class="singleContentBox">
    
    
                            <% '路径获取
                               set_path=MY_uploadPathRoot
							   
							   set_path_show="img"
							   set_path2_show="img/small"
							   set_path3_show="file"
							   pageShowNumSetting = 50
                            
                             '**********************删除文件***************************************
                             '函数定义
                            '函数定义结束
                            IF Request.QueryString("type")="DeleFile" Then
                                    IF Request.QueryString("filename")=Empty Then
                                        Response.Write("文件不存在！！！&nbsp;&nbsp;&nbsp;<a href=attachments.asp?action=attachment >返回</a>")
                                    Else
                                        fileNameNow=Request.QueryString("filename")
                                        delfile(fileNameNow)
                                        Response.AddHeader "refresh","0.5;URL=attachments.asp?thisState=Succeed"	
                                    End IF
                                     '**********************删除文件结束***************************************
                                Else
                                    Dim AttachmentFolder
                                    Set FSO=Server.CreateObject("Scripting.FileSystemObject")
                                    If -2147221005=Err then 
                                        Response.Write "<script language='javascript'>floatWin('出现错误','<div class=tipsFloatWin>×&nbsp;您的空间不支持FSO组件！</div>',350,120,100,0,0,0);</script>"
                                    Else
                                     '**********************文件夹显示项目***************************************
                                        If Request.QueryString("FolderName")=Empty Then

											
											Response.Write "<p><img src=""../../images/post_open_new.gif"" /><a href=""attachments.asp?show="&pageShowNumSetting&"&action=attachment&FolderName="&set_path_show&""">浏览文件夹"&set_path_show&"中的文件</a></p>"&vbcrlf
											Response.Write "<p><img src=""../../images/post_open_new.gif"" /><a href=""attachments.asp?show="&pageShowNumSetting&"&action=attachment&FolderName="&set_path2_show&""">浏览文件夹"&set_path2_show&"中的文件</a></p>"&vbcrlf	
											Response.Write "<p><img src=""../../images/post_open_new.gif"" /><a href=""attachments.asp?show="&pageShowNumSetting&"&action=attachment&FolderName="&set_path3_show&""">浏览文件夹"&set_path3_show&"中的文件</a></p>"&vbcrlf	

											
                                        Else
										    folderNow = Request.QueryString("FolderName")
											activeShowNum = Request.QueryString("show")
											
											session("pageFileNum") = activeShowNum '设置初始分页展示文件数
											
											thisPageFileNum = session("pageFileNum") '取得分页暂存
											
											if apiStrNull(thisPageFileNum) = 1 then 
											    pageFileNum = activeShowNum '分页显示文件数
												
											else
											    pageFileNum	= pageShowNumSetting
											end if
											
											
											
											
                                            Set AttachmentFolder=FSO.GetFolder(Server.MapPath(""&set_path&""&folderNow&"")) 
                                            Dim AttachmentFileList
                                            Set AttachmentFileList=AttachmentFolder.Files
                                            Dim AttachmentFileName,AttachmentFileEvery
											
											
											counter=0
											fileTotalNum=AttachmentFileList.count'取得文件总数
											newFileSHowNum = activeShowNum - pageShowNumSetting
											

											
                                            For Each AttachmentFileEvery IN AttachmentFileList
											
											'=========================================FSO读取开始================================================
											
											


											
											    if counter >= newFileSHowNum then  '====读取文件初始点
												
												
												
														AttachmentFileName=AttachmentFileEvery.Name
														FileSize = AttachmentFileEvery.Size 
														
														thisPath= ""&replace(replace(set_path,"wwwroot",""),"../../../","../..")&"/"&Request.QueryString("FolderName")&"/"&AttachmentFileName&""
														
														'图片判断
														imgPreview = ""
														if instr(thisPath,".jpg")>0 or instr(thisPath,".jpeg")>0 or instr(thisPath,".png")>0 or instr(thisPath,".bmp")>0 or instr(thisPath,".gif")>0 then
														
														imgShowAtt = "<span color=""#0c6"">【图片】</span>"
														imgPreview = "<a href="""&thisPath&""" target=""_blank""><img style=""border:none; vertical-align:middle"" src="""&thisPath&""" height=""65"" ></a>"
														
														elseif instr(thisPath,".swf")>0  then
														
														imgShowAtt = "<span color=""#c60"">【动画】</span>"
														
														elseif instr(thisPath,".doc")>0 or instr(thisPath,".docx")>0 or instr(thisPath,".pdf")>0 or instr(thisPath,".txt")>0 then
														
														imgShowAtt = "<span color=""#60c"">【文档】</span>"
														
														elseif instr(thisPath,".rar")>0 or instr(thisPath,".zip")>0  then
														
														imgShowAtt = "<span color=""#f3c"">【压缩包】</span>"
														
														elseif instr(thisPath,".html")>0 or instr(thisPath,".htm")>0 or instr(thisPath,".asp")>0 or instr(thisPath,".php")>0 or instr(thisPath,".jsp")>0 or instr(thisPath,".do")>0 then
														
														imgShowAtt = "<span color=""#60f"">【网页】</span>"
														
														elseif instr(thisPath,".js")>0  then
														
														imgShowAtt = "<span color=""#60c"">【脚本】</span>"
														
														elseif instr(thisPath,".css")>0  then
														
														imgShowAtt = "<span color=""#00c"">【样式表】</span>"
														
														elseif instr(thisPath,".mp3")>0 or instr(thisPath,".wma")>0 or instr(thisPath,".wmv")>0 or instr(thisPath,".avi")>0  or instr(thisPath,".rm")>0 or instr(thisPath,".flv")>0 or instr(thisPath,".mp4")>0 then
														
														imgShowAtt = "<span color=""#f60"">【媒体文件】</span>"
														
														else
														
														imgShowAtt = "<span color=""#f00"">【未知类型】</span>"
														
														
														end if
														
														thisFileId = URLDecode(replace(AttachmentFileName,".",""))
														
														Response.Write "<p id="""&thisFileId&"""><img src=""../../images/post_pinned.gif"" />"&imgShowAtt&"<a href="""&thisPath&""" target=""_blank"">"&AttachmentFileName&"</a>&nbsp;&nbsp;("&FormatNumber(FileSize/1000,2)&"KB)&nbsp;&nbsp;"&imgPreview&""&FileIndex&"<a href=attachments.asp?action=attachment&type=DeleFile&filename="&set_path&""&folderNow&"/"&AttachmentFileName&" target=delFrame onclick=javascript:$('#"&thisFileId&"').fadeOut(500);><img src=""../../plugins/editInfo_img/4.gif"" border=""0"" title=""删除数据"" align=""absmiddle"" /></a></p>"&vbcrlf

												

												
												
												end if
												
												
											
											
												'---------------分页
				
												'跳出循环
												if cint(counter) = cint(pageFileNum) then '====读取文件末尾点

													session("pageFileNum") = pageFileNum '设置暂时的分页展示文件数
													exit for '跳出循环
	
													
												end if
												

												counter = counter + 1
												newShowFileNum = thisPageFileNum + pageShowNumSetting
												
												if cint(newShowFileNum) < cint((fileTotalNum+pageShowNumSetting)) then

												    thisPageNextBtn = "<a href=""attachments.asp?show="&newShowFileNum&"&action=attachment&FolderName="&set_path_show&""" class=""backBtn"">后一页</a>"
												else
												    thisPageNextBtn = ""
												end if
												
												if cint(activeShowNum) > cint(pageShowNumSetting) then

												    thisPagePrevBtn = "<a href=""attachments.asp?show="&activeShowNum-pageShowNumSetting&"&action=attachment&FolderName="&set_path_show&""" class=""backBtn"">前一页</a>"
												else
												    thisPagePrevBtn = ""
												end if
												
												
			
												'---------------分页  end
											
											

												
												
										     '=========================================FSO读取结束================================================

												
                                            Next
                                            Set AttachmentFileList=Nothing
											response.Write "<p><br><a href=""attachments.asp"" class=""backBtn"">[返回并选择其它目录]</a>&nbsp;&nbsp;"&thisPagePrevBtn&""&thisPageNextBtn&"</p>"
											
                                        End If
                                        Set AttachmentFolder=Nothing
                                    End If
                                    Set FSO=Nothing
                                End IF
                            %>
    
                    </div>
                    
                    <iframe name="delFrame" style="display:none"></iframe>
                    

                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


