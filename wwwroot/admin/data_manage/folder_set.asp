<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<script>

//表单判断
function checkForm(){
	if(document.formM.path.value==""){
		ChkFormTip_page("目录路径不能为空",0);
		return false;
	}
	//ok
	loadingCreate();
	return true;
		
}


<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=数据库备份/还原", "link1=<%=MY_sitePath%>admin/data_manage/restore_backup.asp", "title2=标签库", "link2=<%=MY_sitePath%>admin/edit_file/edit_thesaurus.asp","title3=文件批量字符替换", "link3=<%=MY_sitePath%>admin/edit_file/edit_batchReplaceStr.asp","title4=上传文件管理", "link4=<%=MY_sitePath%>admin/data_manage/attachments.asp","title5=IP权限", "link5=<%=MY_sitePath%>admin/shield/IP_filter.asp");

	   }
	);
	
	
});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 高级功能 | 目录设置
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          

          
                <!-- 表单域  -->
                <div id="contentShow">
                
                    <div class="addInfo"><a href="javascript:" onClick="switchInfo('#addInfo','关闭添加','添加目录');"><span class="io">添加目录</span></a></div>

                                  
                                  <div  id="addInfo" data-open="0" style="display:none">
                                  
                                        <form name="formM" action="action.asp?action=add" method="post" onSubmit="javascript:return checkForm();">
                                               
                                                    <table width="100%" class="setting" cellspacing="0">
                                                        
                                                        <!-- 标题 -->                        
                                                        <thead class="setHead-class">
                                                            <tr>
                                                                   <th  class="setTitle-class"></th>
                                                                   <th colspan="2"></th>
                                                            </tr>
                                                            
                                                        </thead>
                                                        
                                                     
                                                        <!-- 底部 -->
                                                        <tfoot>
                                                        
                                                            <tr>
                                                                <td colspan="3"  class="ui-element">         
                                                                    <div class="btnBox-iframe">
                                                                        <input type="submit" value="添加信息" class="ui-btn" id="needCreateLoadingBtn">
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tfoot>
                                                        
                                                        
                                                        <tbody>
                                                        
                                                           <!-- +++++++  内容   begin +++++++  -->
                                
                                                            <tr>
                                                                <td class="title">目录路径：<span class="backStage_star">*</span></td>
                                                                <td colspan="2" class="ui-element">
                                                                                                
                                                                    <input name="path" type="text"  size="40"  />
       
                                                                  
                                                                </td>
                                                            </tr>  
                                                     
           
                                                           <!-- +++++++  内容   end +++++++  -->
                                                    
                                                    
                                                        </tbody>
                                                        
                                                    </table>
                       
                       
                                        </form>  
             
                                        
                                    </div>
   

                                    <form  action="action.asp?action=ok" name="form0"   method="post" target="actionPage" onSubmit="javascript:return ChkFormTip_page('保存成功',1);">
                                           
                                                <table width="100%" class="setting" cellspacing="0">
                                                    
                                                    <!-- 标题 -->                        
                                                    <thead class="setHead">
                                                        <tr>
                                                               <th  class="setTitle"></th>
                                                               <th colspan="2"></th>
                                                        </tr>
                                                        
                                                    </thead>
                                                    
                                                 
                                                    <!-- 底部 -->
                                                    <tfoot>
                                                    
                                                        <tr>
                                                            <td colspan="3">         
                                                                <div class="btnBox">
                                                                    <input type="submit" value="保存" class="sub">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                    
                                                    
                                                    <tbody>
                                                    
                                                       <!-- +++++++  内容   begin +++++++  -->
                                   
                                                        <tr>
                                                            <td class="title">设置目录：</td>
                                                            <td colspan="2" class="ui-element">
                                                                <select name="path" >
                                                                  
                                                                    <option value="" selected="selected">选择路径</option>
                                                                  
                                                                  
                                                                  <%
                                                                    sql="select * from [file_manage]"
                                                                    set rs=db.execute(sql)
                                                                    do while not rs.eof and not rs.bof
                                                                  %>
                                                                  <option value="<%=rs("path")%>"><%=rs("path")%></option>
                                                                  <%
                                                                  rs.movenext
                                                                  loop
                                                                  %>
                                                                </select>
                                                              
                                                            </td>
                                                        </tr>  
                                                        
                                                        
                                                  
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                                                
                                                    </tbody>
                                                    
                                                </table>
                   
                   
                                    </form>  


                               
                     </div> 
          
                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->

