<!-- 自定义右键导航 -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%if memPage <> 1 then %>

    <div id="rightMenuStyle" style="display:none;">
    
        <%
        set rs_rmenu =server.createobject("adodb.recordset")
        sql_rmenu="select * from [rightMenu] where view=1 order by index asc" 
        rs_rmenu.open sql_rmenu,db,1,1 
        do while not rs_rmenu.eof and not rs_rmenu.bof
        %>
        <a href="<%=replace(rs_rmenu("url"),"../",MY_sitePath&"admin/")%>" target="mainPage" title="<%=rs_rmenu("title")%>"><%=rs_rmenu("title")%></a>
        <%
        rs_rmenu.movenext
        loop
		rs_rmenu.close
		set rs_rmenu=nothing
		
        set rs_rmenu =server.createobject("adodb.recordset")
        sql_rmenu="select * from [App] where view=1 order by index asc" 
        rs_rmenu.open sql_rmenu,db,1,1 
        do while not rs_rmenu.eof and not rs_rmenu.bof
        %>
        <a href='<%=rs_rmenu("appURL")%>' target='mainPage' title='插件：<%=rs_rmenu("appName")%>'>[插]<%=rs_rmenu("appName")%></a>
        <%
        rs_rmenu.movenext
        loop
		rs_rmenu.close
		set rs_rmenu=nothing
		
        %>
        <a href="javascript:" onclick="location.reload() ">— 刷新 —</a>
    
    </div>

<%end if%>
