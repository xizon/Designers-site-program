<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../checkFun_action.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
HTMLSERVE_HEAD()
action=request.querystring("action") 
mypath = request.querystring("mypath")
mypath2 = request.querystring("mypath2")

'根目录修改
if action="editRootPath" then
   
   rootPath = request.Form("rootPath")
   
	tempCommNavCon1=showFile("../../cat_js/shortCut.js")
	CreateFile "../../cat_js/shortCut.js",replace(tempCommNavCon1,"MyrootPath_S="""&MY_sitePath&"""","MyrootPath_S="""&rootPath&"""")


	tempCommNavCon2=showFile("../../cat_js/template.js")
	CreateFile "../../cat_js/template.js",replace(tempCommNavCon2,"MyrootPath="""&MY_sitePath&"""","MyrootPath="""&rootPath&"""")
	

	tempCommNavCon3=showFile("../../template/comm.nav.js")
	CreateFile "../../template/comm.nav.js",replace(tempCommNavCon3,"MyrootPath_Temp="""&MY_sitePath&"""","MyrootPath_Temp="""&rootPath&"""")

	tempCommNavCon4=showFile("../../config/setValue.asp")
	CreateFile "../../config/setValue.asp",replace(tempCommNavCon4,"MySiteValue0 = """&MY_sitePath&"""","MySiteValue0 = """&rootPath&"""")
	
	tempCommNavCon5=showFile("../../cat_js/commConfig.js")
	CreateFile "../../cat_js/commConfig.js",replace(tempCommNavCon5,"MyrootPath = """&MY_sitePath&"""","MyrootPath = """&rootPath&"""")


    response.Write "<script>floatWin('温馨提示','<div class=tipsFloatWin><span style=color:red>√操作成功！您已经成功修改根目录"&rootPath&"，当前网站暂时不可用，请您转移到当前服务器或者其它服务器的新目录后，再登录使用</span><br>登录后请您批量生成所有页面，更新页面内的链接网址.<br/></div>',350,120,100,1,0,0);</script>"
	
  

end if


if  action="editLangColumn" then
	
	    path = "../../config/langColumn_value.asp"
		fileContent=request.form("lang")
		fileContent2=HTMLEncode(fileContent)
		fileContent2 = Replace(fileContent2, "％", "%")
		fileContent3=replace(replace(fileContent2,"&lt;%",""),"%&gt;","")
		
		
		ii1 = getCount(fileContent,"langValue") '必须为70
		ii2 = getCount(fileContent2,"&lt;%") '必须为1
		ii3 = getCount(fileContent2,"%&gt;")  '必须为1
		ii4 = getCount(fileContent3,"&quot;") '必须为140

		if ii1 <> 58 or ii2 <> 1 or ii3 <> 1 or ii4 <> 116 then
		    response.Write "<script>floatWin('出现错误','<div class=tipsFloatWin>×&nbsp;请严格按照格式修改(内容中不得使用双引号、不能删除langValue?字符)！<br/><br/><button class=""backBtn"" onclick=""javascript:history.go(-1)"">返回</button></div>',350,120,100,1,0,0);</script>"
		
		else
		
		    CreateFile path,request.form("lang")
			response.Write "<script>floatWin('温馨提示','<div class=tipsFloatWin>进程处理中....<br/></div>',350,120,100,1,0,0);</script>"
			Response.AddHeader "refresh","0.5;URL=edit_lang.asp?thisState=Succeed"
		
		end if

end if 

 
if  action="restoreMenu" then
	    path = "../root_temp-edit/_navValue_menu.asp"
		navValue = showFile("_menuValue_origin.asp")

		CreateFile path,navValue

        response.Write "<script>floatWin('温馨提示','<div class=tipsFloatWin>进程处理中....<br/></div>',350,120,100,1,0,0);</script>"
	    Response.AddHeader "refresh","0.5;URL=edit_NavMenu.asp?thisState=Succeed"	

end if  

if  action="restoreMenu_no" then
	    path = "../root_temp-edit/_navValue_menu.asp"
		navValue = showFile("_menuValue_origin_no.asp")

		CreateFile path,navValue

        response.Write "<script>floatWin('温馨提示','<div class=tipsFloatWin>进程处理中....<br/></div>',350,120,100,1,0,0);</script>"
	    Response.AddHeader "refresh","0.5;URL=edit_NavMenu.asp?thisState=Succeed"	

end if  

if  action="editMenu" then
	    path = "../root_temp-edit/_navValue_menu.asp"


        goNum=13
		goNum_sub=20

'===========================主导航   begin
		navValue=""
		navValue=navValue&"<"
		navValue=navValue&"%"&vbcrlf
		navValue=navValue&"dim mName("&goNum&","&goNum_sub&")"&vbcrlf
		navValue=navValue&"dim mNameUrl("&goNum&","&goNum_sub&")"&vbcrlf
		navValue=navValue&"dim mNameTitle("&goNum&","&goNum_sub&")"&vbcrlf
		

				
		for i=0 to goNum
		


		'---------------子导航
		
				
				navValue=navValue&"'----------------------------------------------------- menu"&i&""&vbcrlf


		
				
				navValue_sub=""

				
				for s=1 to goNum_sub
				
				nameM_sub=HTMLEncode(request.form("name_" & i &"_" & s))
				nameUrl_sub=HTMLEncode(request.form("url_" & i &"_" & s))
				nameTitle_sub=HTMLEncode(request.form("title_" & i &"_" & s))
				navValue_sub=navValue_sub&"    '--------"&vbcrlf
				navValue_sub=navValue_sub&"    mName("&i&","&s&")="&CHR(34)&""&nameM_sub&""&CHR(34)&""&vbcrlf
				navValue_sub=navValue_sub&"    mNameTitle("&i&","&s&")="&CHR(34)&""&nameTitle_sub&""&CHR(34)&""&vbcrlf
				navValue_sub=navValue_sub&"    mNameUrl("&i&","&s&")="&CHR(34)&""&nameUrl_sub&""&CHR(34)&""&vbcrlf
				
				next

				
					
				
		'-------------子导航  end
		
		
		nameM=HTMLEncode(request.form("name" & i))
		nameUrl=HTMLEncode(request.form("url" & i))
		nameTitle=HTMLEncode(request.form("title" & i))
		navValue=navValue&"mName("&i&",0)="&CHR(34)&""&nameM&""&CHR(34)&""&vbcrlf
		navValue=navValue&"mNameTitle("&i&",0)="&CHR(34)&""&nameTitle&""&CHR(34)&""&vbcrlf
		navValue=navValue&"mNameUrl("&i&",0)="&CHR(34)&""&nameUrl&""&CHR(34)&""&vbcrlf
		navValue=navValue&navValue_sub
		
		
			
		
		next
		
		navValue=navValue&"%"
		navValue=navValue&">"&vbcrlf

'============================主导航   end


        CreateFile path,navValue
		


end if  


if  action="editMenuPhone" then
	    path = "../../phone/menu.asp"


        goNum=9
	
		navValue=""
		navValue=navValue&"<"
		navValue=navValue&"%"&vbcrlf
		navValue=navValue&"'导航选择状态"
		navValue=navValue&"dim siteurl "&vbcrlf
		navValue=navValue&"siteurl="&CHR(34)&"http://"&CHR(34)&"&request.ServerVariables("&CHR(34)&"Server_NAME"&CHR(34)&")&request.ServerVariables("&CHR(34)&"SCRIPT_NAME"&CHR(34)&")"&vbcrlf

		navValue=navValue&"dim pName("&goNum&")"&vbcrlf
		navValue=navValue&"dim pNameUrl("&goNum&")"&vbcrlf

		for i=0 to goNum
		
		
		
		nameM=HTMLEncode(request.form("name" & i))
		nameUrl=HTMLEncode(request.form("url" & i))
		navValue=navValue&"pName("&i&")="&CHR(34)&""&nameM&""&CHR(34)&""&vbcrlf
		navValue=navValue&"pNameUrl("&i&")="&CHR(34)&""&nameUrl&""&CHR(34)&""&vbcrlf

			
		
		next
		
		navValue=navValue&"%"
		navValue=navValue&">"&vbcrlf
		
		

		navValue=navValue&"<nav class="&CHR(34)&"navBar"&CHR(34)&"> "&vbcrlf
		navValue=navValue&"    <div class="&CHR(34)&"cf"&CHR(34)&">"&vbcrlf
		navValue=navValue&"<"
		navValue=navValue&"%"&vbcrlf
		navValue=navValue&"            for k=0 to 9  "&vbcrlf          
		navValue=navValue&"                if pName(k) <> "&CHR(34)&""&CHR(34)&" then  	"&vbcrlf			    
		navValue=navValue&"					navselected1 = "&CHR(34)&""&CHR(34)&" : navselected2 = "&CHR(34)&""&CHR(34)&" : navselected3 = "&CHR(34)&""&CHR(34)&" : navselected4 = "&CHR(34)&""&CHR(34)&" : navselected5 = "&CHR(34)&""&CHR(34)&"		"&vbcrlf			
		navValue=navValue&"					if instr(siteurl,"&CHR(34)&"/art"&CHR(34)&") > 0 and instr(pNameUrl(k),"&CHR(34)&"/art"&CHR(34)&") > 0 then navselected1 = "&CHR(34)&"on"&CHR(34)&""&vbcrlf
		navValue=navValue&"					if instr(siteurl,"&CHR(34)&"/works"&CHR(34)&") > 0 and instr(pNameUrl(k),"&CHR(34)&"/works"&CHR(34)&") > 0 then navselected2 = "&CHR(34)&"on"&CHR(34)&""&vbcrlf
		navValue=navValue&"					if instr(siteurl,"&CHR(34)&"/mood"&CHR(34)&") > 0 and instr(pNameUrl(k),"&CHR(34)&"/mood"&CHR(34)&") > 0 then navselected3 = "&CHR(34)&"on"&CHR(34)&""&vbcrlf
		navValue=navValue&"					if instr(siteurl,"&CHR(34)&"/contact"&CHR(34)&") > 0 and instr(pNameUrl(k),"&CHR(34)&"/contact"&CHR(34)&") > 0 then navselected4 = "&CHR(34)&"on"&CHR(34)&""&vbcrlf
		navValue=navValue&"					if instr(siteurl,"&CHR(34)&"/about"&CHR(34)&") > 0 and instr(pNameUrl(k),"&CHR(34)&"/about"&CHR(34)&") > 0 then navselected5 = "&CHR(34)&"on"&CHR(34)&""&vbcrlf
		navValue=navValue&"				    response.Write "&CHR(34)&"<a href="&CHR(34)&""&CHR(34)&""&CHR(34)&"&pNameUrl(k)&"&CHR(34)&""&CHR(34)&""&CHR(34)&" class="&CHR(34)&""&CHR(34)&""&CHR(34)&"&navselected1&navselected2&navselected3&navselected4&navselected5&"&CHR(34)&""&CHR(34)&""&CHR(34)&">"&CHR(34)&"&pName(k)&"&CHR(34)&"</a>"&CHR(34)&"&vbcrlf	"&vbcrlf				
		navValue=navValue&"				end if    "&vbcrlf        
		navValue=navValue&"            next"&vbcrlf
		navValue=navValue&"%"
		navValue=navValue&">"&vbcrlf
		navValue=navValue&"    </div>"&vbcrlf
		navValue=navValue&"</nav>"&vbcrlf
            

        CreateFile path,navValue
		


end if  




if  action="editCss" then

	    path = mypath
		fileContent=request.form("okNameCss")
		CreateFile path,fileContent
		
		response.Write "<script>floatWin('温馨提示','<div class=tipsFloatWin>进程处理中....<br/></div>',350,120,100,1,0,0);</script>"
		Response.AddHeader "refresh","0.5;URL=edit_css.asp?thisState=Succeed"


end if 



if  action="editTemp" then

	    path = mypath
		fileContent=request.form("okName")
		CreateFile path,fileContent
		
%>
<!--#include file="../root_temp-edit/_siteConfigAutoCreate.asp" -->
<%

        response.Write "<iframe src=""../_chk_temp_modifiedDate.asp"" style=""display:none"" ></iframe>"
		
		response.Write "<script>floatWin('温馨提示','<div class=tipsFloatWin>进程处理中....<br/></div>',350,120,100,1,0,0);</script>"
		Response.AddHeader "refresh","0.5;URL=edit_TEMP.asp?thisState=Succeed"


end if 

if  action="editThesaurus_hands" then
	
	    path = "../../config/thesaurus.txt"
		fileContent=request.form("thesaurus")
		CreateFile path,fileContent
		
		response.Write "<script>floatWin('温馨提示','<div class=tipsFloatWin>进程处理中....<br/></div>',350,120,100,1,0,0);</script>"
		Response.AddHeader "refresh","0.5;URL=edit_thesaurus.asp?thisState=Succeed"

end if 


if action="updateRightmenu" then

    'rightMenu
	total=request.Form("total")
	for i=1 to total			
		ID=request.form("ID"& i)
		view=request.form("view"& i)	
		desc=request.form("index"& i)	
		
		if view <> "1" then view = "0"
		
		
        strsql="update [rightMenu] set [view]="& view &",[index]="& desc &" where ID="&ID
		db.execute(strsql)
	next
	
	'App
	total_app=request.Form("total_app")
	for k=1 to total_app			
		ID_app=request.form("ID_app"& k)
		view_app=request.form("view_app"& k)	
		desc_app=request.form("index_app"& k)	
		
		if view_app <> "1" then view_app = "0"
		
		
        strsql2="update [App] set [view]="& view_app &",[index]="& desc_app &" where ID="&ID_app
		db.execute(strsql2)
	next
	
	'----
    Response.AddHeader "refresh","0.5;URL=action.asp?action=updateRightmenu-ok"

end if



if action="updateRightmenu-ok" then
			response.Write "<div class=tipsFloatWin>√&nbsp;设置成功！&nbsp;&nbsp;<a href=edit_shortCutMenu.asp>重新设置</a></div>"
end if 


if action="saveStyle" then

    thisClear = request.QueryString("clear")

	configCon = configCon &"<"
	configCon = configCon &"%"& vbCrLf
	
	for i = 1 to 41
	    
		if thisClear = true then
		    thisV = ""
		else
		    thisV = request.form("dsstyle" & i)
		end if
		
		configCon = configCon &"DS_ustyleV"&i&" = "&CHR(34)&""&thisV&""&CHR(34)&""& vbCrLf
		
	next
	
	configCon = configCon &"%"
	configCon = configCon &">"& vbCrLf

	CreateFile "_userStyleInc.asp",configCon
	response.Redirect "action_userStyle.asp?action=updateUserStyleCss"
	if thisClear = true then Response.Write "<script>window.parent.parent.frames['mainPage'].location.href = '../edit_file/edit_commStyle.asp'</script>"

end if

if action="clearStyle" then

	configCon = configCon &"<"
	configCon = configCon &"%"& vbCrLf
	
	for i = 1 to 41
	    
		thisV = ""
		configCon = configCon &"DS_ustyleV"&i&" = "&CHR(34)&""&thisV&""&CHR(34)&""& vbCrLf
		
	next
	
	configCon = configCon &"%"
	configCon = configCon &">"& vbCrLf

	CreateFile "_userStyleInc.asp",configCon
	response.Redirect "action_userStyle.asp?action=updateUserStyleCss"

end if




HTMLSERVE_BOTTOM()
%>
<!--#include file="../checkFun_actionB.asp" -->
