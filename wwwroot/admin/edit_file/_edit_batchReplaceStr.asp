<!--#include file="admin/function/dsMainFunction.asp" -->
<!--#include file="admin/_comm/page_head.asp" -->
<!--#include file="admin/checkFun.asp" -->
<!--#include file="admin/_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
showT1 = session("replaceStr1")
showT2 = session("replaceStr2")
showT3 = session("replaceStr3")

%>
<script>

function txtRepVal1(){
	var thisVal = "<%=showT1%>";
    $("#replace_1-2_see").html(thisVal);
	$("#replace_1-2_see_box").fadeIn(500);
}
function txtRepVal2(){
	var thisVal = "<%=showT2%>";
    $("#replace_1-2_see").html(thisVal);
	$("#replace_1-2_see_box").fadeIn(500);
}

function txtRepVal3(){
	var thisVal = "<%=showT3%>";
    $("#replace_3_see").html(thisVal);
	$("#replace_3_see_box").fadeIn(500);
	$("#replace_3_tip").fadeOut(200);
	
}
<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=数据库备份/还原", "link1=<%=MY_sitePath%>admin/data_manage/restore_backup.asp", "title2=标签库", "link2=<%=MY_sitePath%>admin/edit_file/edit_thesaurus.asp","title3=文件批量字符替换", "link3=<%=MY_sitePath%>admin/edit_file/edit_batchReplaceStr.asp","title4=上传文件管理", "link4=<%=MY_sitePath%>admin/data_manage/attachments.asp","title5=IP权限", "link5=<%=MY_sitePath%>admin/shield/IP_filter.asp");

	   }
	);

	
	
});

<!-- form ui[js]  end -->

</script>




<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 高级功能 | 文件批量字符替换
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          

				<%if request.QueryString("state")="1" or request.QueryString("state")= "2" then %>  
                
                <%else%>

                    <div id="Tabs1" style="display:none;" >
                    
                        <!-- 后台提示 -->
                        
                        <div class="notification information png_bg content-alert">
                            <a href="javascript:" class="close"><img src="<%=MY_sitePath%>plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                            <div>
                                支持的格式：.html;.htm;.asp;.aspx;.txt;.php;.xhtml;.xml;.jsp;.cgi;.perl;.jhtml;.css;.js;.inc<br>
                                选择某两个字符或者语句作为<strong> [开始] </strong>和 <strong>[结束] </strong>的标志，替换它们之间的代码，选择两个<strong>唯一性的字符</strong>作为开始和结束标志，否则替换的内容可能会不对<br>
                                比如使用这样的可以避免出错：<br>
                                开始字符：<strong>&lt;html dir=&quot;ltr&quot; lang=&quot;zh-CN&quot; xml:lang=&quot;zh-CN&quot;&gt;</strong><br>
                                结束字符：<strong>&lt;meta name=&quot;robots&quot; content=&quot;index,follow&quot; /&gt;</strong>
                                
                            </div>
                        </div>
                        <p class="clear"></p>
                        
						<%if request.QueryString("state")="1" or request.QueryString("state")= "2" then %>  
                        
                        <%else%>
                            <ul class="columnBtn mainBtn">
                                <li><a href="javascript:" id="TabTitle0" onclick="ShowTabs_manageOtherMenu(0)">页面字符精确替换</a></li>
                                <li><a href="javascript:" id="TabTitle1" onclick="ShowTabs_manageOtherMenu(1)">替换页面部分代码</a></li>
                            </ul>
        
                        <%end if%> 
                        <br clear="all">
                        
                        <!-- 独立内容区 -->
                        <div class="singleContentBox">
                            <div id="replace_3_see_box" class="seeRecordBox"><span id="replace_3_see"></span></div>  
                        </div>
                        
                        
                        
                  
                        <!-- 表单域  -->
                        <div id="contentShow">
                        
                                            <form action="?act=startClear&state=2"  method="post">
                                                   
                                                        <table width="100%" class="setting" cellspacing="0">
                                                            
                                                            <!-- 标题 -->                        
                                                            <thead class="setHead">
                                                                <tr>
                                                                       <th  class="setTitle"></th>
                                                                       <th colspan="2"></th>
                                                                </tr>
                                                                
                                                            </thead>
                                                            
                                                         
                                                            <!-- 底部 -->
                                                            <tfoot>
                                                            
                                                                <tr>
                                                                    <td colspan="3">         
                                                                        <div class="btnBox">
                                                                        
																			<%if request.QueryString("act")="startClear" then%>
                                                                        
                                                                            <%else%>
                                                                                <input type="submit" value="代码段替换" class="sub" onclick="loadingCreate();" id="needCreateLoadingBtn">
                                                                            <%end if%>
                                                                            
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tfoot>
                                                            
                                                            
                                                            <tbody>
                                                            
                                                               <!-- +++++++  内容   begin +++++++  -->
                                           
                                           
                                                                <tr>
                                                                    <td class="title">转换的目录源路径<a href="javascript:void(0)" class="help" data-info="例如您要更改的文件是根目录下的某个文件夹，则写成 <strong>folder/ </strong>,如果您要批量更改跟目录下的文件，则不需要填写此项；第一个字符请不要加斜杠"/" "><img src="<%=MY_sitePath%>plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                                                    <td colspan="2" >
                                                                       <input name="txtpath" type="text" id="txtpath" value="" />
                                                                    </td>
                                                                </tr>                                     
                                           
                                           
                                                                <tr>
                                                                    <td class="title">开始字符：</td>
                                                                    <td colspan="2" >
                                                                       <input name="replace_1_2" id="replace_1_2" type="text"  size="90"  value="" />
                                                                      
                                                                    </td>
                                                                </tr>   
                                                                
                                                                
                                           
                                                                <tr>
                                                                    <td class="title">结束字符：</td>
                                                                    <td colspan="2" >
                                                                       <input name="replace_2_2" id="replace_2_2" type="text" size="90"  value="" />
                                                                      
                                                                    </td>
                                                                </tr>   
                                           
                                                                <tr>
                                                                    <td class="title">将替换的代码或字符<a href="javascript:void(0)" class="help" data-info="特别注意：开始字符和结束字符之间包含的代码必须也是<strong>唯一的</strong>，否则会把整个文件相同的部分全部替换掉"><img src="<%=MY_sitePath%>plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                                                    <td colspan="2" >
                                                                       <textarea name="replace_3" id="replace_3" cols="80" rows="3" onclick="this.rows='5'" ></textarea><a href="javascript:" onclick="txtRepVal3();">参考上次记录</a>
                                                                      
                                                                    </td>
                                                                </tr>  
                                                                
                                                                
                                                          
                                                               <!-- +++++++  内容   end +++++++  -->
                                                        
                                                        
                                                            </tbody>
                                                            
                                                        </table>
                           
                           
                                            </form>  
        
        
                                       
                             </div> 

                    
                    
                    </div>
                <%end if%>
                
                <!-- //////////////////////////////////////-->
                
				<%if request.QueryString("state")="1" or request.QueryString("state")= "2" then %>  
                
                <%else%>

                    <div id="Tabs0">
                    
                        <!-- 后台提示 -->
                        
                        <div class="notification information png_bg content-alert">
                            <a href="javascript:" class="close"><img src="plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                            <div>
                                支持的格式：.html;.htm;.asp;.aspx;.txt;.php;.xhtml;.xml;.jsp;.cgi;.perl;.jhtml;.css;.js;.inc
                            </div>
                        </div>
                        <p class="clear"></p>
                         
						<%if request.QueryString("state")="1" or request.QueryString("state")= "2" then %>  
                        
                        <%else%>
                            <ul class="columnBtn mainBtn">
                                <li><a href="javascript:" id="TabTitle0" onclick="ShowTabs_manageOtherMenu(0)">页面字符精确替换</a></li>
                                <li><a href="javascript:" id="TabTitle1" onclick="ShowTabs_manageOtherMenu(1)">替换页面部分代码</a></li>
                            </ul>
        
                        <%end if%> 
                        <br clear="all">

                  
                        <!-- 独立内容区 -->
                        <div class="singleContentBox">
                            <div id="replace_1-2_see_box" class="seeRecordBox"><span id="replace_1-2_see"></span></div>
                        </div>
                  
                        <!-- 表单域  -->
                        <div id="contentShow">
                        
                                            <form action="?act=start&state=1" name="formM"   method="post">
                                                   
                                                        <table width="100%" class="setting" cellspacing="0">
                                                            
                                                            <!-- 标题 -->                        
                                                            <thead class="setHead">
                                                                <tr>
                                                                       <th  class="setTitle"></th>
                                                                       <th colspan="2"></th>
                                                                </tr>
                                                                
                                                            </thead>
                                                            
                                                         
                                                            <!-- 底部 -->
                                                            <tfoot>
                                                            
                                                                <tr>
                                                                    <td colspan="3">         
                                                                        <div class="btnBox">
																			<%if request.QueryString("act")="start" then%>
                                                                        
                                                                            <%else%>
                                                                                <input type="submit" value="精确替换" class="sub" onclick="loadingCreate();" id="needCreateLoadingBtn">
                                                                            <%end if%>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tfoot>
                                                            
                                                            
                                                            <tbody>
                                                            
                                                               <!-- +++++++  内容   begin +++++++  -->
                                           
                                                                <tr>
                                                                    <td class="title">转换的目录源路径<a href="javascript:void(0)" class="help" data-info="例如您要更改的文件是根目录下的某个文件夹，则写成 <strong>folder/ </strong>,如果您要批量更改跟目录下的文件，则不需要填写此项；第一个字符请不要加斜杠"/" "><img src="<%=MY_sitePath%>plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                                                    <td colspan="2" >
                                                                       <input name="txtpath" type="text" id="txtpath" value="" />
                                                                    </td>
                                                                </tr> 
                                                                
                                                                
                                                                <tr>
                                                                    <td class="title">源文件的字符：</td>
                                                                    <td colspan="2" >
                                                                       <textarea name="replace_1" id="replace_1" cols="80" rows="1" onclick="this.rows='3'" ></textarea><a href="javascript:" onclick="txtRepVal1();">参考上次记录</a>
                                                                      
                                                                    </td>
                                                                </tr>  
                                                                
                                                                
                                                                <tr>
                                                                    <td class="title">将替换的字符：</td>
                                                                    <td colspan="2" >
                                                                       <textarea name="replace_2" id="replace_2" cols="80" rows="1" onclick="this.rows='3'" ></textarea><a href="javascript:" onclick="txtRepVal2();">参考上次记录</a>
                                                                      
                                                                    </td>
                                                                </tr>   
                                                                
                                                                
                                                          
                                                               <!-- +++++++  内容   end +++++++  -->
                                                        
                                                        
                                                            </tbody>
                                                            
                                                        </table>
                           
                           
                                            </form>  
        
        
                                       
                             </div> 
       
                    
                    
                    </div>
                <%end if%>
                
                
                <!-- 独立内容区 -->
                <div class="singleContentBox">
               
               
                        <div style="overflow:hidden; overflow-y:auto; height:350px; width:95%">
                        <%
                        if not IsObjInstalled("Scripting.FileSystemObject") Then
                        call showErrmsg("你的服务器不支持文件操作，请确认!")
                        end if
                        
                        if request.QueryString("act")="start" then
                        
                            folderpath=replace(request.Form("txtpath"),"\","/")
                            path=request.Form("path")
                            path2=request.Form("path2")
                            txtsub=request.Form("txtsub")
                            txtsub2=request.Form("txtsub2")
                            replace1=HTMLEncode(request.Form("replace_1"))
                            replace2=HTMLEncode(request.Form("replace_2"))
							
                            '去掉最右边的空格，避免未找到匹配
							replace1 = clearRightString(replace1,"&nbsp;")
							replace2 = clearRightString(replace2,"&nbsp;")

                            
                            session("replaceStr1") = replace1
                            session("replaceStr2") = replace2
							
							folderpath = clearRightString(folderpath,"/")

                            
                            folderpathOk = MY_sitePath + folderpath + "/"
							folderpathOk = replace(folderpathOk,"//","/") '路径修正
                        
                            Set FSO=Server.CreateObject("Scripting.FileSystemObject")
                            Set AttachmentFolder= FSO.GetFolder(Server.MapPath(folderpathOk)) 
                            Set AttachmentFileList=AttachmentFolder.Files
                        
                            
                            if request.QueryString("act")<>"startClear" then
                            
                                response.Write("目标目录：<span color=red>"&folderpathOk&"</span>")
                                
                                response.Write "<br>源字符：<span color=red>"&replace1&"</span><br>"
                                response.Write "替换后的字符：<span color=red>"&replace2&"</span><br><hr>"
                                
                            end if
                            
                             response.Write "<p class=""clear""></p>"
                            response.Write " <a href=""?act=none"">【返回操作后继续操作】</a><p class=""clear""></p>"
                            
                        
                            For Each AttachmentFileEvery IN AttachmentFileList
                                
                                AttachmentFileName=folderpathOk&AttachmentFileEvery.Name
                                
                                fileBody=HTMLEncode(LoadFromFile(AttachmentFileName))
                                
                                if instr(fileBody,replace1)>0 then
                                
                                        
                                    if instr(AttachmentFileName,".html")>0 or instr(AttachmentFileName,".htm")>0 or instr(AttachmentFileName,".asp")>0  or instr(AttachmentFileName,".aspx")>0 or instr(AttachmentFileName,".txt")>0  or instr(AttachmentFileName,".php")>0   or instr(AttachmentFileName,".xhtml")>0 or instr(AttachmentFileName,".xml")>0 or instr(AttachmentFileName,".jsp")>0 or instr(AttachmentFileName,".cgi")>0 or instr(AttachmentFileName,".perl")>0 or instr(AttachmentFileName,".jhtml")>0 or instr(AttachmentFileName,".css")>0 or instr(AttachmentFileName,".js")>0 or instr(AttachmentFileName,".inc")>0 then
                                
                                        response.Write "<span color=red>已替换</span>"&AttachmentFileName&"; "
                                        
                                        
                                        
                                    
                                        fileBody=replace(fileBody,replace1,replace2)
                                        
                                        savetofile HTMLDecode(fileBody),AttachmentFileName
                                        
                                    end if
                                
                                
                                end if
                                
                                
                            
                        
                            Next
                            
                        
                            
                        
                                        
                        
                            
                            
                        else
                            'call showErrmsg("请选择正确的操作!")
                        end if
                        
                        
                        
                        '/////////代码段替换操作
                        if request.QueryString("act")="startClear" then
                        
                            folderpath=replace(request.Form("txtpath"),"\","/")
                            path=request.Form("path")
                            path2=request.Form("path2")
                            txtsub=request.Form("txtsub")
                            txtsub2=request.Form("txtsub2")
                            replace1_2=HTMLEncode(request.Form("replace_1_2"))
                            replace2_2=HTMLEncode(request.Form("replace_2_2"))
                            replace_3=HTMLEncode(request.Form("replace_3"))
							
                            '去掉最右边的空格，避免未找到匹配
							replace1_2 = clearRightString(replace1_2,"&nbsp;")
							replace2_2 = clearRightString(replace2_2,"&nbsp;")
							replace3 = clearRightString(replace3,"&nbsp;")
							

                            session("replaceStr3") = replace_3
                            
                            
                            folderpath = clearRightString(folderpath,"/")
                            
                            folderpathOk = MY_sitePath + folderpath + "/"
							folderpathOk = replace(folderpathOk,"//","/") '路径修正
                        
                            Set FSO=Server.CreateObject("Scripting.FileSystemObject")
                            Set AttachmentFolder= FSO.GetFolder(Server.MapPath(folderpathOk)) 
                            Dim AttachmentFileList
                            Set AttachmentFileList=AttachmentFolder.Files
                            Dim AttachmentFileName,AttachmentFileEvery
                        
                             if request.QueryString("act")="startClear" then 
                             
                                 response.Write("目标目录：<span color=red>"&folderpathOk&"</span>")
                                 response.Write "<br>替换的的字符：<span color=red>"&replace_3&"</span><br><hr>"
                                 
                             end if
                        
                             response.Write "<p class=""clear""></p>"
                            response.Write " <a href=""?act=none"">【返回操作后继续操作】</a><p class=""clear""></p>"
                        
                           
                            For Each AttachmentFileEvery IN AttachmentFileList
                                
                                AttachmentFileName=folderpathOk&AttachmentFileEvery.Name
                                
                                fileBody=HTMLEncode(LoadFromFile(AttachmentFileName))

                                if instr(fileBody,replace1_2)>0 and instr(fileBody,replace2_2)>0 then
                                
                                        
                                    if instr(AttachmentFileName,".html")>0 or instr(AttachmentFileName,".htm")>0 or instr(AttachmentFileName,".asp")>0  or instr(AttachmentFileName,".aspx")>0 or instr(AttachmentFileName,".txt")>0  or instr(AttachmentFileName,".php")>0   or instr(AttachmentFileName,".xhtml")>0 or instr(AttachmentFileName,".xml")>0 or instr(AttachmentFileName,".jsp")>0 or instr(AttachmentFileName,".cgi")>0 or instr(AttachmentFileName,".perl")>0 or instr(AttachmentFileName,".jhtml")>0 or instr(AttachmentFileName,".css")>0 or instr(AttachmentFileName,".js")>0 or instr(AttachmentFileName,".inc")>0 then
                                
                                
                                        thisClearString = makeTempLabelString(fileBody,replace1_2,replace2_2)

                                        
                                        if request.QueryString("act")="startClear" then
                                                    response.Write "<span color=red>文件"&AttachmentFileName&"源字符：</span>"&thisClearString&"<br>"
                                        end if
                                                    
                                        
                        
                                        fileBody=replace(fileBody,thisClearString,replace_3)
                                        fileBody=replace(fileBody,replace1_2,replace1_2&vbcrlf)
                        
                                    
                                        savetofile HTMLDecode(fileBody),AttachmentFileName
                                        
                                    end if
                                
                                
                                end if
                                
                                
                            
                        
                            Next
                            
                        
                        
                                        
                        
                            
                            
                        else
                            'call showErrmsg("请选择正确的操作!")
                        end if
                        
                        
                        
                        '显示错误信息
                        sub showErrmsg(msgstring)
                        response.write("<span color=red>"&msgstring&"</span>")
                        end sub
                        '检查是否支持指定的控件
                        Function IsObjInstalled(strClassString)
                            On Error Resume Next
                            IsObjInstalled = False
                            Err = 0
                            Dim xTestObj
                            Set xTestObj = Server.CreateObject(strClassString)
                            If 0 = Err Then IsObjInstalled = True
                            Set xTestObj = Nothing
                            Err = 0
                        End Function
                        
                        
                        
                        Function LoadFromFile(filePath)
                        
                            set fs=server.CreateObject("Scripting.FileSystemObject") 
                             set ts=fs.opentextfile(server.mappath(filePath))
                                LoadFromFile=ts.readall
                                
                             Set fs = Nothing
                                
                        End Function
                        
                        
                        Function SaveToFile(FsoContent,filePath) 
                                set fs=server.CreateObject("Scripting.fileSystemObject")
                                set ts=fs.createtextfile(server.MapPath(filePath))
                                ts.writeline FsoContent
                                ts.close
                                Set fs = Nothing
                        End Function 
                        
                        %>
                        </div>
                         

               
               
               
                </div>
        
  
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="admin/_comm/page_bottom.asp" -->




