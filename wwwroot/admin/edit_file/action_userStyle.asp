<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../checkFun_action.asp" -->
<!--#include file="_userStyleInc.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
HTMLSERVE_HEAD()
action=request.querystring("action") 
cssPath1="../../template/temp-userStyle.css"
cssPath2="../../images/skins/userStyle.css"

if  action="updateUserStyleCss" then
    
	Template_Code=showFile(cssPath1)
	Template_Code=replace(Template_Code,"<#css=body.bgPath#>",DS_ustyleV1)
	Template_Code=replace(Template_Code,"<#css=body.bgposition#>",DS_ustyleV2)
	Template_Code=replace(Template_Code,"<#css=body.bgrepeat#>",DS_ustyleV3)
	Template_Code=replace(Template_Code,"<#css=body.bgshowType#>",DS_ustyleV4)
	Template_Code=replace(Template_Code,"<#css=body.bgcolor#>",DS_ustyleV5)
	Template_Code=replace(Template_Code,"<#css=comm.linkcolor#>",DS_ustyleV6)
	Template_Code=replace(Template_Code,"<#css=comm.linkHovercolor#>",DS_ustyleV7)
	Template_Code=replace(Template_Code,"<#css=logo.linkcolor#>",DS_ustyleV8)
	Template_Code=replace(Template_Code,"<#css=logo.linkHovercolor#>",DS_ustyleV9)
	Template_Code=replace(Template_Code,"<#css=footer.linkcolor#>",DS_ustyleV10)
	Template_Code=replace(Template_Code,"<#css=footer.linkHovercolor#>",DS_ustyleV11)
	Template_Code=replace(Template_Code,"<#css=nav.linkcolor#>",DS_ustyleV12)
	Template_Code=replace(Template_Code,"<#css=nav.linkHovercolor#>",DS_ustyleV13)
	Template_Code=replace(Template_Code,"<#css=listTitle.linkcolor#>",DS_ustyleV14)
	Template_Code=replace(Template_Code,"<#css=listTitle.linkHovercolor#>",DS_ustyleV15)
	Template_Code=replace(Template_Code,"<#css=sidebarList.linkcolor#>",DS_ustyleV16)
	Template_Code=replace(Template_Code,"<#css=sidebarList.linkHovercolor#>",DS_ustyleV17)
	Template_Code=replace(Template_Code,"<#css=sidebarClass.linkcolor#>",DS_ustyleV18)
	Template_Code=replace(Template_Code,"<#css=sidebarClass.linkHovercolor#>",DS_ustyleV19)
	Template_Code=replace(Template_Code,"<#css=sideMoreBtn.linkcolor#>",DS_ustyleV20)
	Template_Code=replace(Template_Code,"<#css=sideMoreBtn.linkHovercolor#>",DS_ustyleV21)
	Template_Code=replace(Template_Code,"<#css=breadCrumbsNav.linkcolor#>",DS_ustyleV22)
	Template_Code=replace(Template_Code,"<#css=breadCrumbsNav.linkHovercolor#>",DS_ustyleV23)
	Template_Code=replace(Template_Code,"<#css=notice.linkcolor#>",DS_ustyleV24)
	Template_Code=replace(Template_Code,"<#css=notice.linkHovercolor#>",DS_ustyleV25)
	Template_Code=replace(Template_Code,"<#css=links.linkcolor#>",DS_ustyleV26)
	Template_Code=replace(Template_Code,"<#css=links.linkHovercolor#>",DS_ustyleV27)
	Template_Code=replace(Template_Code,"<#css=tags.linkcolor#>",DS_ustyleV28)
	Template_Code=replace(Template_Code,"<#css=tags.linkHovercolor#>",DS_ustyleV29)
	Template_Code=replace(Template_Code,"<#css=comm.textcolor#>",DS_ustyleV30)
	Template_Code=replace(Template_Code,"<#css=sitedata.textcolor#>",DS_ustyleV31)
	Template_Code=replace(Template_Code,"<#css=sideTitle.textcolor#>",DS_ustyleV32)
	Template_Code=replace(Template_Code,"<#css=footer.textcolor#>",DS_ustyleV33)
	Template_Code=replace(Template_Code,"<#css=breadCrumbs.textcolor#>",DS_ustyleV34)
	Template_Code=replace(Template_Code,"<#css=listTitle.textcolor#>",DS_ustyleV35)
	Template_Code=replace(Template_Code,"<#css=artContent.textcolor#>",DS_ustyleV36)
	Template_Code=replace(Template_Code,"<#css=artTitle.textcolor#>",DS_ustyleV37)
	Template_Code=replace(Template_Code,"<#css=information.textcolor#>",DS_ustyleV38)
	Template_Code=replace(Template_Code,"<#css=comment.textcolor#>",DS_ustyleV39)
	Template_Code=replace(Template_Code,"<#css=msg.textcolor#>",DS_ustyleV40)
	Template_Code=replace(Template_Code,"<#css=mood.textcolor#>",DS_ustyleV41)
	
	'清除无效css
	Template_Code=replace(Template_Code,"background-image:url();","")
	Template_Code=replace(Template_Code,"background-position:;","")
	Template_Code=replace(Template_Code,"background-repeat:;","")
	Template_Code=replace(Template_Code,"background-attachment:;","")
	Template_Code=replace(Template_Code,"background-color:;","")
	Template_Code=replace(Template_Code,"color:;","")
	Template_Code=replace(Template_Code,"	","")
	Template_Code=replace(Template_Code,"--","-")

	CreateFile cssPath2,Template_Code
	
	response.Redirect "edit_commStyle.asp"


end if

HTMLSERVE_BOTTOM()
%>
<!--#include file="../checkFun_actionB.asp" -->
