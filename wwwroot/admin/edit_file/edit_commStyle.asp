<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<!--#include file="_userStyleInc.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<style>
.Sradio{ padding:0; height:15px; width:15px;}
.colorSelect{vertical-align:middle;}
.lt{font-weight:normal}
</style>
<script>

<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=自定义背景/文字颜色", "link1=<%=MY_sitePath%>admin/edit_file/edit_commStyle.asp", "title2=模板HTML代码", "link2=<%=MY_sitePath%>admin/edit_file/edit_TEMP.asp", "title3=模板CSS样式表", "link3=<%=MY_sitePath%>admin/edit_file/edit_css.asp", "title4=模板文字包", "link4=<%=MY_sitePath%>admin/edit_file/edit_lang.asp");

	   }
	);
	
	//恢复
	$(".reset").click(
	   function() {
		   $('#mainForm').attr("action", "action.asp?action=clearStyle");
		   $('#mainForm').attr("target", "_self");
			$('#mainForm').submit();

	   }
	);

});

<!-- form ui[js]  end -->

function colorSFun(id){
	$("#color-" + id).bigColorpicker(function(el,color){
		$(el).css("background",color);
		$("#ct-" +  + id).val(color);
	});	
}
$(function(){
    colorSFun(1001);
	colorSFun(6);
	colorSFun(7);
	colorSFun(8);
	colorSFun(9);
	colorSFun(10);
	colorSFun(11);
	colorSFun(12);
	colorSFun(13);
	colorSFun(14);
	colorSFun(15);
	colorSFun(16);
	colorSFun(17);
	colorSFun(18);
	colorSFun(19);
	colorSFun(20);
	colorSFun(21);
	colorSFun(22);
	colorSFun(23);
	colorSFun(24);
	colorSFun(25);
	colorSFun(26);
	colorSFun(27);
	colorSFun(28);
	colorSFun(29);
	colorSFun(30);
	colorSFun(31);
	colorSFun(32);
	colorSFun(33);
	colorSFun(34);
	colorSFun(35);
	colorSFun(36);
	colorSFun(37);
	colorSFun(38);
	colorSFun(39);
	colorSFun(40);
	colorSFun(41);
	
	$('.mainCt tr:even').addClass("alt-row");

});

</script>


<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="edit_TEMP.asp"  class="link">HTML代码</a>
                <a href="edit_css.asp"  class="link">CSS样式表</a>
                <a href="edit_lang.asp"  class="link">模板文字包</a>
                
                <!-- 当前位置 -->
                当前位置： 自定义样式/模板 | 自定义背景/文字颜色
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
            
            
                    <!-- 后台提示 -->
                    
                    <div class="notification information png_bg content-alert">
                        <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                        <div>
   
                         提示：网站列出的背景和字体颜色设置<strong>不保证都能看到效果</strong>，是否能看到效果取决于设计者定义的<a href="../../template/temp-userStyle.css" target="_blank">template/temp-userStyle.css</a></strong>文件，如果效果不理想点击<strong>恢复</strong>按钮即可
                        
                        </div>
                    </div>
                

                    <!-- 表单域  -->
                    <div id="contentShow">
    
                        <form method="post" id="mainForm" action="action.asp?action=saveStyle" target="actionPage"  onSubmit="javascript:return ChkFormTip_page('保存成功',1);">
                        
                              
                        <table width="100%" class="setting" cellspacing="0">
                            
                            <!-- 标题 -->                        
                            <thead class="setHead">
                                <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2"></th>
                                </tr>
                                
                            </thead>
                            
                            
                          <!-- 底部 -->
                            <tfoot>
                            
                                <tr>
                                    <td colspan="3">
                                       <div class="btnBox">
                                       
                                           <input type="submit" value="保存" class="sub"><input type="submit" value="恢复" class="reset">

                                           
                                       </div>
                        
                                    </td>
                                </tr>
                            </tfoot>
                            
                            
                            
                            <!-- +++++++  内容   begin +++++++  -->
                            
                            <tbody class="mainCt">
                            
                                
                                <thead class="setHead">
                                    <tr>
                                       <th  class="setTitle"></th>
                                       <th colspan="2" class="titleInfo">页面背景设置</th>
                                    </tr> 
                                </thead>

                                <tr onMouseOver="tipShow('.tipsFloatWin-upload-min',0)" onMouseOut="tipShow('.tipsFloatWin-upload-min',1)">
                                    <td class="title">图片(支持外链)：</td>
                                    <td colspan="2" >
                                    
                                               <input type="text" name="dsstyle1" id="objdefault" size="55" value="<%=DS_ustyleV1%>" >&nbsp;<a href="javascript:" onClick="$('#objdefault').val('');">清除背景</a>
                                                <br clear="all">
                                                <div class="tipsFloatWin-upload-min"><iframe src=../upload/upload_imgID.html?imgID=default class=backStage_uploadFrame allowTransparency=true  frameborder=0 scrolling=No></iframe></div>
                                                
                                                <%if len(DS_ustyleV1) < 1 then %>
                                                <span id="objdefault_imgShow"></span>
                                                <%else%>
                                                <span id="objdefault_imgShow"><img src="<%=DS_ustyleV1%>"  onload="resizeImage(this,286,350)"/></span>
                                                <%end if%>
                                    </td>
                                </tr>   
                                
                                <tr>
                                    <td class="title">对齐方式：</td>
                                    <td colspan="2">
                                     <%
									 if DS_ustyleV2 = "top left" then d1 = "checked=""checked"""
									 if DS_ustyleV2 = "top center" then d2 = "checked=""checked"""
									 if DS_ustyleV2 = "top right" then d3 = "checked=""checked"""
									 if DS_ustyleV2 = "bottom left" then d4 = "checked=""checked"""
									 if DS_ustyleV2 = "bottom left" then d5 = "checked=""checked"""
									 if DS_ustyleV2 = "bottom left" then d6 = "checked=""checked"""
									 if DS_ustyleV2 = "left" then d7 = "checked=""checked"""
									 if DS_ustyleV2 = "center" then d8 = "checked=""checked"""
									 if DS_ustyleV2 = "right" then d9 = "checked=""checked"""
									 if DS_ustyleV2 = "" then d10 = "checked=""checked"""
									 %>
                                      <label><input type="radio" class="Sradio" name="dsstyle2" value="top left" <%=d1%>>顶部居左</label>&nbsp;
                                      <label><input type="radio" class="Sradio" name="dsstyle2" value="top center" <%=d2%>>顶部居中</label>&nbsp;
                                      <label><input type="radio" class="Sradio" name="dsstyle2" value="top right" <%=d3%>>顶部居右</label>&nbsp;
                                      <br>
                                      <label><input type="radio" class="Sradio" name="dsstyle2" value="bottom left" <%=d4%>>底部居左</label>&nbsp;
                                      <label><input type="radio" class="Sradio" name="dsstyle2" value="bottom center" <%=d5%>>底部居中</label>&nbsp;
                                      <label><input type="radio" class="Sradio" name="dsstyle2" value="bottom right" <%=d6%>>底部居右</label>&nbsp;  
                                      <br>   
                                      <label><input type="radio" class="Sradio" name="dsstyle2" value="left" <%=d7%>>整体居左</label>&nbsp;
                                      <label><input type="radio" class="Sradio" name="dsstyle2" value="center" <%=d8%>>整体居中</label>&nbsp;
                                      <label><input type="radio" class="Sradio" name="dsstyle2" value="right" <%=d9%>>整体居右</label>&nbsp;  
                                      
                                      <input style="display:none" type="radio" class="Sradio" name="dsstyle2" value="" <%=d10%>>    
                                                                     
                                    </td>
                                </tr> 
                                
                                <tr>
                                    <td class="title">平铺：</td>
                                    <td colspan="2">
                                     <%
									 if DS_ustyleV3 = "no-repeat" then dr1 = "checked=""checked"""
									 if DS_ustyleV3 = "repeat" then dr2 = "checked=""checked"""
									 if DS_ustyleV3 = "" then dr3 = "checked=""checked"""
									 %>
                                    
                                      <label><input type="radio" class="Sradio" name="dsstyle3" value="no-repeat" <%=dr1%>>单张</label>&nbsp;
                                      <label><input type="radio" class="Sradio" name="dsstyle3" value="repeat" <%=dr2%>>平铺</label>&nbsp; 
                                      <input style="display:none" type="radio" class="Sradio" name="dsstyle3" value="" <%=dr3%>>                
                                    </td>
                                </tr> 
                                
                                <tr>
                                    <td class="title">固定：</td>
                                    <td colspan="2">
                                     <%
									 if DS_ustyleV4 = "scroll" then df1 = "checked=""checked"""
									 if DS_ustyleV4 = "fixed" then df2 = "checked=""checked"""
									 if DS_ustyleV4 = "" then df3 = "checked=""checked"""
									 %>
                                      <label><input type="radio" class="Sradio" name="dsstyle4" value="scroll" <%=df1%>>自动</label>&nbsp;
                                      <label><input type="radio" class="Sradio" name="dsstyle4" value="fixed" <%=df2%>>固定</label>&nbsp;   
                                      <input style="display:none" type="radio" class="Sradio" name="dsstyle4" value="" <%=df3%>>                            
                                    </td>
                                </tr> 
                                
                                
                                <tr>
                                    <td class="title">背景色：</td>
                                    <td colspan="2">
                                      <a href="javascript:void(0)" class="colorSelect" id="color-1001" style="background:<%=DS_ustyleV5%>"></a>
                                      <input type="hidden" id="ct-1001" name="dsstyle5" value="<%=DS_ustyleV5%>" />
                                    </td>
                                </tr> 
                                

                               <!-- +++++++  内容   end +++++++  -->
                        
                        
                            </tbody>
                            
                        </table>
                        
                        
                          <table width="100%" class="setting" cellspacing="0">
                          
                                <thead>
                                    <tr>
                                           <th width="50%">
                                           
                                                <table width="100%" cellspacing="0" cellpadding="0">
                        
                                                    
                                                    <!-- +++++++  内容   begin +++++++  -->
                                                    
                                                    <tbody class="mainCt">
                                                    
                        
                                                        <thead class="setHead">
                                                            <tr>
                                                               <th  class="setTitle"></th>
                                                               <th colspan="2" class="titleInfo">文字颜色设置</th>
                                                            </tr> 
                                                        </thead>
                                  
                                                        <tr>
                                                            <td class="title">页面通用文字：</td>
                                                            <td colspan="2">
                                                              <a href="javascript:void(0)" class="colorSelect" id="color-30" style="background:<%=DS_ustyleV30%>"></a>
                                                              <input type="hidden" id="ct-30" name="dsstyle30" value="<%=DS_ustyleV30%>" />
                                                            </td>
                                                        </tr>   
                                                        
                                                        <tr>
                                                            <td class="title">侧边栏站内统计文字：</td>
                                                            <td colspan="2">
                                                              <a href="javascript:void(0)" class="colorSelect" id="color-31" style="background:<%=DS_ustyleV31%>"></a>
                                                              <input type="hidden" id="ct-31" name="dsstyle31" value="<%=DS_ustyleV31%>" />
                                                            </td>
                                                        </tr>   
                                                        
                                                        <tr>
                                                            <td class="title">侧边栏标题文字：</td>
                                                            <td colspan="2">
                                                              <a href="javascript:void(0)" class="colorSelect" id="color-32" style="background:<%=DS_ustyleV32%>"></a>
                                                              <input type="hidden" id="ct-32" name="dsstyle32" value="<%=DS_ustyleV32%>" />
                                                            </td>
                                                        </tr>   
                                                         
                                                        <tr>
                                                            <td class="title">底部文字：</td>
                                                            <td colspan="2">
                                                              <a href="javascript:void(0)" class="colorSelect" id="color-33" style="background:<%=DS_ustyleV33%>"></a>
                                                              <input type="hidden" id="ct-33" name="dsstyle33" value="<%=DS_ustyleV33%>" />
                                                            </td>
                                                        </tr>   
                                                        <tr>
                                                            <td class="title">面包屑导航文字：</td>
                                                            <td colspan="2">
                                                              <a href="javascript:void(0)" class="colorSelect" id="color-34" style="background:<%=DS_ustyleV34%>"></a>
                                                              <input type="hidden" id="ct-34" name="dsstyle34" value="<%=DS_ustyleV34%>" />
                                                            </td>
                                                        </tr>   
                                                        <tr>
                                                            <td class="title">栏目列表标题文字：</td>
                                                            <td colspan="2">
                                                              <a href="javascript:void(0)" class="colorSelect" id="color-35" style="background:<%=DS_ustyleV35%>"></a>
                                                              <input type="hidden" id="ct-35" name="dsstyle35" value="<%=DS_ustyleV35%>" />
                                                            </td>
                                                        </tr>   
                                                        <tr>
                                                            <td class="title">文章内容页展示文字：</td>
                                                            <td colspan="2">
                                                              <a href="javascript:void(0)" class="colorSelect" id="color-36" style="background:<%=DS_ustyleV36%>"></a>
                                                              <input type="hidden" id="ct-36" name="dsstyle36" value="<%=DS_ustyleV36%>" />
                                                            </td>
                                                        </tr>   
                                                        <tr>
                                                            <td class="title">文章内容页标题文字：</td>
                                                            <td colspan="2">
                                                              <a href="javascript:void(0)" class="colorSelect" id="color-37" style="background:<%=DS_ustyleV37%>"></a>
                                                              <input type="hidden" id="ct-37" name="dsstyle37" value="<%=DS_ustyleV37%>" />
                                                            </td>
                                                        </tr>   
                                                        <tr>
                                                            <td class="title">文章内容页子信息文字：</td>
                                                            <td colspan="2">
                                                              <a href="javascript:void(0)" class="colorSelect" id="color-38" style="background:<%=DS_ustyleV38%>"></a>
                                                              <input type="hidden" id="ct-38" name="dsstyle38" value="<%=DS_ustyleV38%>" />
                                                            </td>
                                                        </tr>   
                                                        <tr>
                                                            <td class="title">评论列表文字：</td>
                                                            <td colspan="2">
                                                              <a href="javascript:void(0)" class="colorSelect" id="color-39" style="background:<%=DS_ustyleV39%>"></a>
                                                              <input type="hidden" id="ct-39" name="dsstyle39" value="<%=DS_ustyleV39%>" />
                                                            </td>
                                                        </tr>   
                                                        <tr>
                                                            <td class="title">留言列表文字：</td>
                                                            <td colspan="2">
                                                              <a href="javascript:void(0)" class="colorSelect" id="color-40" style="background:<%=DS_ustyleV40%>"></a>
                                                              <input type="hidden" id="ct-40" name="dsstyle40" value="<%=DS_ustyleV40%>" />
                                                            </td>
                                                        </tr>   
                                                        
                                                        <tr>
                                                            <td class="title">站内微博列表文字：</td>
                                                            <td colspan="2">
                                                              <a href="javascript:void(0)" class="colorSelect" id="color-41" style="background:<%=DS_ustyleV41%>"></a>
                                                              <input type="hidden" id="ct-41" name="dsstyle41" value="<%=DS_ustyleV41%>" />
                                                            </td>
                                                        </tr>   
                                                        
                                                        
                                                
                                                                
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                                                
                                                    </tbody>
                                                    
                                                </table>
                                        
                                           </th>
                                           <th>
                                           
                                                <table width="100%" cellspacing="0" cellpadding="0">
                        
                                                    
                                                    <!-- +++++++  内容   begin +++++++  -->
                                                    
                                                    <tbody class="mainCt">
                                                    
                                                        
                                                        <thead class="setHead">
                                                            <tr>
                                                               <th  class="setTitle"></th>
                                                               <th colspan="2" class="titleInfo">链接颜色设置</th>
                                                            </tr> 
                                                        </thead>
                                                        
                                                        <tr>
                                                            <td class="title">页面通用链接：</td>
                                                            <td colspan="2" class="lt">
                                                              
                                                              默认<a href="javascript:void(0)" class="colorSelect" id="color-6" style="background:<%=DS_ustyleV6%>"></a>&nbsp;&nbsp;
                                                              悬停<a href="javascript:void(0)" class="colorSelect" id="color-7" style="background:<%=DS_ustyleV7%>"></a>
                                                              <input type="hidden" id="ct-6" name="dsstyle6" value="<%=DS_ustyleV6%>" />
                                                              <input type="hidden" id="ct-7" name="dsstyle7" value="<%=DS_ustyleV7%>" />
                                                            </td>
                                                        </tr>  
                                                        
                                                        <tr>
                                                            <td class="title">LOGO链接：</td>
                                                            <td colspan="2" class="lt">
                                                              
                                                              默认<a href="javascript:void(0)" class="colorSelect" id="color-8" style="background:<%=DS_ustyleV8%>"></a>&nbsp;&nbsp;
                                                              悬停<a href="javascript:void(0)" class="colorSelect" id="color-9" style="background:<%=DS_ustyleV9%>"></a>
                                                              <input type="hidden" id="ct-8" name="dsstyle8" value="<%=DS_ustyleV8%>" />
                                                              <input type="hidden" id="ct-9" name="dsstyle9" value="<%=DS_ustyleV9%>" />
                                                            </td>
                                                        </tr>       
                                                        
                                                        <tr>
                                                            <td class="title">底部链接：</td>
                                                            <td colspan="2" class="lt">
                                                              
                                                              默认<a href="javascript:void(0)" class="colorSelect" id="color-10" style="background:<%=DS_ustyleV10%>"></a>&nbsp;&nbsp;
                                                              悬停<a href="javascript:void(0)" class="colorSelect" id="color-11" style="background:<%=DS_ustyleV11%>"></a>
                                                              <input type="hidden" id="ct-10" name="dsstyle10" value="<%=DS_ustyleV10%>" />
                                                              <input type="hidden" id="ct-11" name="dsstyle11" value="<%=DS_ustyleV11%>" />
                                                            </td>
                                                        </tr>   
                                                        
                                                        <tr>
                                                            <td class="title">导航链接：</td>
                                                            <td colspan="2" class="lt">
                                                              
                                                              默认<a href="javascript:void(0)" class="colorSelect" id="color-12" style="background:<%=DS_ustyleV12%>"></a>&nbsp;&nbsp;
                                                              悬停<a href="javascript:void(0)" class="colorSelect" id="color-13" style="background:<%=DS_ustyleV13%>"></a>
                                                              <input type="hidden" id="ct-12" name="dsstyle12" value="<%=DS_ustyleV12%>" />
                                                              <input type="hidden" id="ct-13" name="dsstyle13" value="<%=DS_ustyleV13%>" />
                                                            </td>
                                                        </tr>   
                                                        
                                                        <tr>
                                                            <td class="title">栏目列表标题链接：</td>
                                                            <td colspan="2" class="lt">
                                                              
                                                              默认<a href="javascript:void(0)" class="colorSelect" id="color-14" style="background:<%=DS_ustyleV14%>"></a>&nbsp;&nbsp;
                                                              悬停<a href="javascript:void(0)" class="colorSelect" id="color-15" style="background:<%=DS_ustyleV15%>"></a>
                                                              <input type="hidden" id="ct-14" name="dsstyle14" value="<%=DS_ustyleV14%>" />
                                                              <input type="hidden" id="ct-15" name="dsstyle15" value="<%=DS_ustyleV15%>" />
                                                            </td>
                                                        </tr>   
                                                        
                                                        <tr>
                                                            <td class="title">侧边栏内容列表链接：</td>
                                                            <td colspan="2" class="lt">
                                                              
                                                              默认<a href="javascript:void(0)" class="colorSelect" id="color-16" style="background:<%=DS_ustyleV16%>"></a>&nbsp;&nbsp;
                                                              悬停<a href="javascript:void(0)" class="colorSelect" id="color-17" style="background:<%=DS_ustyleV17%>"></a>
                                                              <input type="hidden" id="ct-16" name="dsstyle16" value="<%=DS_ustyleV16%>" />
                                                              <input type="hidden" id="ct-17" name="dsstyle17" value="<%=DS_ustyleV17%>" />
                                                            </td>
                                                        </tr>   
                                                        <tr>
                                                            <td class="title">侧边栏分类列表链接：</td>
                                                            <td colspan="2" class="lt">
                                                              
                                                              默认<a href="javascript:void(0)" class="colorSelect" id="color-18" style="background:<%=DS_ustyleV18%>"></a>&nbsp;&nbsp;
                                                              悬停<a href="javascript:void(0)" class="colorSelect" id="color-19" style="background:<%=DS_ustyleV19%>"></a>
                                                              <input type="hidden" id="ct-18" name="dsstyle18" value="<%=DS_ustyleV18%>" />
                                                              <input type="hidden" id="ct-19" name="dsstyle19" value="<%=DS_ustyleV19%>" />
                                                            </td>
                                                        </tr>   
                                                        <tr>
                                                            <td class="title">侧边栏更多按钮链接：</td>
                                                            <td colspan="2" class="lt">
                                                              
                                                              默认<a href="javascript:void(0)" class="colorSelect" id="color-20" style="background:<%=DS_ustyleV20%>"></a>&nbsp;&nbsp;
                                                              悬停<a href="javascript:void(0)" class="colorSelect" id="color-21" style="background:<%=DS_ustyleV21%>"></a>
                                                              <input type="hidden" id="ct-20" name="dsstyle20" value="<%=DS_ustyleV20%>" />
                                                              <input type="hidden" id="ct-21" name="dsstyle21" value="<%=DS_ustyleV21%>" />
                                                            </td>
                                                        </tr>   
                                                        <tr>
                                                            <td class="title">面包屑导航链接：</td>
                                                            <td colspan="2" class="lt">
                                                              
                                                              默认<a href="javascript:void(0)" class="colorSelect" id="color-22" style="background:<%=DS_ustyleV22%>"></a>&nbsp;&nbsp;
                                                              悬停<a href="javascript:void(0)" class="colorSelect" id="color-23" style="background:<%=DS_ustyleV23%>"></a>
                                                              <input type="hidden" id="ct-22" name="dsstyle22" value="<%=DS_ustyleV22%>" />
                                                              <input type="hidden" id="ct-23" name="dsstyle23" value="<%=DS_ustyleV23%>" />
                                                            </td>
                                                        </tr>   
                                                        <tr>
                                                            <td class="title">首页通知公告链接：</td>
                                                            <td colspan="2" class="lt">
                                                              
                                                              默认<a href="javascript:void(0)" class="colorSelect" id="color-24" style="background:<%=DS_ustyleV24%>"></a>&nbsp;&nbsp;
                                                              悬停<a href="javascript:void(0)" class="colorSelect" id="color-25" style="background:<%=DS_ustyleV25%>"></a>
                                                              <input type="hidden" id="ct-24" name="dsstyle24" value="<%=DS_ustyleV24%>" />
                                                              <input type="hidden" id="ct-25" name="dsstyle25" value="<%=DS_ustyleV25%>" />
                                                            </td>
                                                        </tr>  
                                                        <tr>
                                                            <td class="title">友情链接列表：</td>
                                                            <td colspan="2" class="lt">
                                                              
                                                              默认<a href="javascript:void(0)" class="colorSelect" id="color-26" style="background:<%=DS_ustyleV26%>"></a>&nbsp;&nbsp;
                                                              悬停<a href="javascript:void(0)" class="colorSelect" id="color-27" style="background:<%=DS_ustyleV27%>"></a>
                                                              <input type="hidden" id="ct-26" name="dsstyle26" value="<%=DS_ustyleV26%>" />
                                                              <input type="hidden" id="ct-27" name="dsstyle27" value="<%=DS_ustyleV27%>" />
                                                            </td>
                                                        </tr>   
                                                        <tr>
                                                            <td class="title">文章内容页标签链接：</td>
                                                            <td colspan="2" class="lt">
                                                              
                                                              默认<a href="javascript:void(0)" class="colorSelect" id="color-28" style="background:<%=DS_ustyleV28%>"></a>&nbsp;&nbsp;
                                                              悬停<a href="javascript:void(0)" class="colorSelect" id="color-29" style="background:<%=DS_ustyleV29%>"></a>
                                                              <input type="hidden" id="ct-28" name="dsstyle28" value="<%=DS_ustyleV28%>" />
                                                              <input type="hidden" id="ct-29" name="dsstyle29" value="<%=DS_ustyleV29%>" />
                                                            </td>
                                                        </tr>   
                                                                
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                                                
                                                    </tbody>
                                                    
                                                </table>

                                           
                                           </th>
                                    </tr>
                                    
                                </thead>
                          

                            </table>
                            
                        </form>	
    
                    </div>
                    
                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


