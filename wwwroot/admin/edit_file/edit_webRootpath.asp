<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<!--#include file="../../phone/menu.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<%
if request.QueryString("type") = "p" then
	response.Write "<style>a.b2{background:#000; color:#fff}</style>"
else
    response.Write "<style>a.b1{background:#000; color:#fff}</style>"
	
end if


%>

<script>

<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=数据库备份/还原", "link1=<%=MY_sitePath%>admin/data_manage/restore_backup.asp", "title2=标签库", "link2=<%=MY_sitePath%>admin/edit_file/edit_thesaurus.asp","title3=文件批量字符替换", "link3=<%=MY_sitePath%>admin/edit_file/edit_batchReplaceStr.asp","title4=上传文件管理", "link4=<%=MY_sitePath%>admin/data_manage/attachments.asp","title5=IP权限", "link5=<%=MY_sitePath%>admin/shield/IP_filter.asp","title6=网站根目录修改", "link6=<%=MY_sitePath%>admin/edit_file/edit_webRootpath.asp");

	   }
	);

	
	
});


<!-- form ui[js]  end -->

//表单判断
function checkForm(){
	if(document.formM.rootPath.value==""){
		ChkFormTip_page("请不要填写空白",0);
		return false;
	}

	//ok
	loadingCreate();
	return true;
		
}

</script>


<style>
.navBar{display:none}
</style>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 高级功能 | 网站根目录修改
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
            
                <!-- 后台提示 -->
                
                <div class="notification information png_bg content-alert">
                    <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                    
                        <span class="backStage_star">点击按钮后将修改网站所有<strong><span class="backStage_star">安装时自动更新过</span></strong>的根目录地址，请根据您网站的正确根目录进行设置，请慎重操作！<br></span>
                        系统安装后自动更新的根目录变量所在5个文件：config/setValue.asp, cat_js/shortCut.js, cat_js/template.js, cat_js/commConfig.js, template/comm.nav.js
                        
                        <br>
                        <hr>
                        
                        <strong>正确的根目录：</strong><br>如果您是把本程序安装在二级或者三级目录，则指定为"/目录名1/"或者"/目录名1/目录名2/"，依次类推，<span class="backStage_star">头尾的斜杠不能去掉，如果没有放在其他目录，则直接填写单个斜杠/即可，请不要包含http://类似的网址</span><br>
                        比如你把安装包wwwroot内的所有文件放到网站的 newweb/目录下，安装后网站默认首页为 http://yoursite.com/newweb/default.html,则您网站的正确根目录就为<span class="backStage_star">/newweb/</span>
                    
                        
                    </div>
                </div>
                
                

                                    <form name="formM" action="action.asp?action=editRootPath" method="post" onSubmit="javascript:return checkForm();">
                                           
                                                <table width="100%" class="setting" cellspacing="0">
                                                    
                                                    <!-- 标题 -->                        
                                                    <thead class="setHead-class">
                                                        <tr>
                                                               <th  class="setTitle-class"></th>
                                                               <th colspan="2"></th>
                                                        </tr>
                                                        
                                                    </thead>
                                                    
                                                 
                                                    <!-- 底部 -->
                                                    <tfoot>
                                                    
                                                        <tr>
                                                            <td colspan="3"  class="ui-element">         
                                                                <div class="btnBox-iframe">
                                                                    <input type="submit" value="保存修改" class="ui-btn" id="needCreateLoadingBtn">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                    
                                                    
                                                    <tbody>
                                                       <!-- +++++++  内容   begin +++++++  -->
                            
                                                        <tr class="ui-element">
                                                            <td colspan="3" >
                                                            
                                                               <strong>您网站当前的根目录为：</strong><span class="backStage_star"><%=MY_sitePath%></span>,如果您不想把网站放置在任何二级或三级等目录，请直接用单个斜杠/<br>
                                                               
                                                             
                                                              
                                                                                            
                                                                <input name="rootPath" id="rootPath" type="text"  size="25" value="<%=MY_sitePath%>" class="help" data-info="<span class=backStage_star>请慎重操作，请在您<span style=font-size:14px;font-weight:bold;color:#333>转移网站之前</span>进行此操作，操作后网站不可用，需要在新的目录登录后台</span>"  />
                                                                
   
                                                              
                                                            </td>
                                                        </tr>  
                                                        
                                                      
                                                        
                                                  
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                                                
                                                    </tbody>
                                                    
                                                </table>
                   
                   
                                    </form>  

                    
                
              
              </div>      
         </div>       

  
<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


