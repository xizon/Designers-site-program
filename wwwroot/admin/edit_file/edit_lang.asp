<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<!-- 代码高亮   begin -->
    <link rel="stylesheet" href="../../plugins/editorHighlighter/lib/codemirror.css">
    <script src="../../plugins/editorHighlighter/lib/codemirror.js"></script>
    <script src="../../plugins/editorHighlighter/mode/css.js"></script>
    <script src="../../plugins/editorHighlighter/mode/xml.js"></script>
    <style type="text/css">.CodeMirror { border:4px solid #E1E1E1}</style>
<!-- 代码高亮  end  -->

<script>

function tipAction(){
	
	MySite.Cookie.set('tempTip_L','tempTip_L');
	$('#tipA').html('设置成功！');
	
}


$(function(){
	
		if (MySite.Cookie.get('tempTip_L') != "tempTip_L" ){
			if (getQueryStringRegExp('thisState')!="Succeed"){
			floatWin('温馨提示','<div class=tipsFloatWin>（1）如果您修改了文字包，需进行过以下任一操作，才能及时看到效果<br><strong>&nbsp;&nbsp;a)手动操作静态生成</strong>;<br><strong>&nbsp;&nbsp;b)到达自动生成时间并访问页面</strong>;<br><strong>&nbsp;&nbsp;c)内容发布;</strong><br>（2）浏览器存在缓存，需要刷新浏览器才能看到效果<p style=text-align:right id=tipA><a href=javascript: onclick=tipAction()>不再提示</a>&nbsp;&nbsp;</p></div>',460,0,100,0,0,1);
			}
		}
		
		
		
	  
	//for IE6
	if ($.browser.msie && $.browser.version < 7) {

	}else{
		
		  //初始化编辑器
		  var editor = CodeMirror.fromTextArea(document.getElementById("htmlEditorInit-vb"), {
			mode: {name: "css", alignCDATA: true},
			lineNumbers: true,
			//width: '100%',
			//height: '100%',
			textWrapping: false,
			autoMatchParens: true
		  });
	}
	  

	  
	$(document).ready(function() {
	
		 $(".CodeMirror-scroll,.CodeMirror").animate({height: window.screen.height - 335 + "px"},{queue:false,duration:560});
	
	}); 
});


<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=自定义背景/文字颜色", "link1=<%=MY_sitePath%>admin/edit_file/edit_commStyle.asp", "title2=模板HTML代码", "link2=<%=MY_sitePath%>admin/edit_file/edit_TEMP.asp", "title3=模板CSS样式表", "link3=<%=MY_sitePath%>admin/edit_file/edit_css.asp", "title4=模板文字包", "link4=<%=MY_sitePath%>admin/edit_file/edit_lang.asp");

	   }
	);


});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="edit_TEMP.asp"  class="link">HTML代码</a>
                <a href="edit_css.asp"  class="link">CSS样式表</a>
                <a href="edit_commStyle.asp"  class="link">背景/文字颜色</a>

                
                <!-- 当前位置 -->
                当前位置： 自定义样式/模板 | 模板文字包
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
            
            
          
                    <!-- 后台提示 -->
                    
                    <div class="notification information png_bg content-alert">
                        <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                        <div>
                        1. 修改文字包后，您需要手动操作<a href="../root_temp-edit/index.asp">栏目页面静态生成</a>，才能及时看到效果<br>
                        2. 或者等待系统达到<strong>静态自动更新间隔</strong>时间，访问页面时系统即可更新静态页面，您也可以通过导航<strong>网站设置</strong>，设置此时间<br>
                        3. 您修改文字应严格按照下面的格式，只要修改<strong>双引号</strong>内的字符即可
                        <!--[if lt IE 7 ]> <br><span class="backStage_star">您当前使用IE6浏览器，不支持代码高亮编辑，建议升级您的浏览器或者更换其它浏览器！</span> <![endif]-->
                        </div>
                    </div>
                
                    <!-- 独立内容区 -->
                    <div class="singleContentBox">
                    
                         <div class="backStage_star">前台生成时有部分文字在模板中无法找到（无法通过修改模板自定义），因为这是系统生成或者ajax调用的文字，您可以在这里修改<br><br></div>
    
                        <form method="post" action="action.asp?action=editLangColumn">
                              <textarea name="lang" id="htmlEditorInit-vb" cols="93" rows="25"  style="width:98.5%;"><%response.write replace(showFile("../../config/langColumn_value.asp"),"&","&amp;") %></textarea>
                              
                            
                       <div class="btnBox">
                       
                           <input type="submit" value="保存" class="sub" onclick="loadingCreate();" id="needCreateLoadingBtn">
                           
                       </div> 
                            
                        </form>	
    
                    </div>
                    
                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


