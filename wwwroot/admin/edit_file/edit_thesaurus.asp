<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<script>

<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=数据库备份/还原", "link1=<%=MY_sitePath%>admin/data_manage/restore_backup.asp", "title2=标签库", "link2=<%=MY_sitePath%>admin/edit_file/edit_thesaurus.asp","title3=文件批量字符替换", "link3=<%=MY_sitePath%>admin/edit_file/edit_batchReplaceStr.asp","title4=上传文件管理", "link4=<%=MY_sitePath%>admin/data_manage/attachments.asp","title5=IP权限", "link5=<%=MY_sitePath%>admin/shield/IP_filter.asp","title6=网站根目录修改", "link6=<%=MY_sitePath%>admin/edit_file/edit_webRootpath.asp");

	   }
	);

	
	
});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 高级功能 | 标签库
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          

          
                <!-- 后台提示 -->
                
                <div class="notification information png_bg content-alert">
                    <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        使用"|"隔开
                    </div>
                </div>
          
                <!-- 表单域  -->
                <div id="contentShow">
                
                                    <form action="action.asp?action=editThesaurus_hands" name="formM"   method="post">
                                           
                                                <table width="100%" class="setting" cellspacing="0">
                                                    
                                                    <!-- 标题 -->                        
                                                    <thead class="setHead">
                                                        <tr>
                                                               <th  class="setTitle"></th>
                                                               <th colspan="2"></th>
                                                        </tr>
                                                        
                                                    </thead>
                                                    
                                                 
                                                    <!-- 底部 -->
                                                    <tfoot>
                                                    
                                                        <tr>
                                                            <td colspan="3">         
                                                                <div class="btnBox">
                                                                    <input type="submit" value="保存" class="sub" onclick="loadingCreate();" id="needCreateLoadingBtn">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                    
                                                    
                                                    <tbody>
                                                    
                                                       <!-- +++++++  内容   begin +++++++  -->
                                   
                                                        <tr>
                                                            <td class="title">内容：</td>
                                                            <td colspan="2">
                                                               <textarea name="thesaurus" cols="85" rows="15" ><%response.write showFile("../../config/thesaurus.txt") %></textarea>
                                                              
                                                            </td>
                                                        </tr>  
                                                        
                                                        
                                                  
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                                                
                                                    </tbody>
                                                    
                                                </table>
                   
                   
                                    </form>  


                               
                     </div> 
          
                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->



