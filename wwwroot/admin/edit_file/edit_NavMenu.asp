<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<!--#include file="../root_temp-edit/_navValue_menu.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<%
if request.QueryString("type") = "p" then
	response.Write "<style>a.b2{background:#000; color:#fff}</style>"
else
    response.Write "<style>a.b1{background:#000; color:#fff}</style>"
	
end if


%>

<script>

function restore(){
floatWin('还原全部','<div align=left><form name="formM" action="action.asp?action=restoreMenu" method="post">还原功能将替换您现在设置的菜单选项！<br/><br/>&nbsp;&nbsp;<input name="Submit" type="submit" value="确定还原" class="ui-btn"  /></form></div>',300,80,100,0,0,0);
}
function restore2(){
floatWin('还原默认','<div align=left><form name="formM" action="action.asp?action=restoreMenu_no" method="post">还原功能将替换您现在设置的菜单选项！<br/><br/>&nbsp;&nbsp;<input name="Submit" type="submit"    value="确定还原" class="ui-btn"  /></form></div>',300,80,100,0,0,0);
}

//表单判断
function checkForm(){
	if(document.formM.name0.value==""){
		ChkFormTip_page("首页导航名称不能为空",0);
		return false;
	}
	if(document.formM.url0.value==""){
		ChkFormTip_page("首页导航地址不能为空",0);
		return false;
	}
	
	//ok
	ChkFormTip_page("保存成功",1);
	return true;
		
}


function tipAction(){
	
	MySite.Cookie.set('tempTip_M','tempTip_M');
	$('#tipA').html('设置成功！');
	
}

$(function(){
	
	if (MySite.Cookie.get('tempTip_M') != "tempTip_M" ){
		if (getQueryStringRegExp('thisState')!="Succeed"){
			floatWin('温馨提示','<div class=tipsFloatWin>（1）如果您修改了导航菜单，需进行过以下任一操作，才能及时看到效果<br><strong>&nbsp;&nbsp;a)手动操作静态生成</strong>;<br><strong>&nbsp;&nbsp;b)到达自动生成时间并访问页面</strong>;<br><strong>&nbsp;&nbsp;c)内容发布;</strong><br>（2）浏览器存在缓存，需要刷新浏览器才能看到效果<br clear=all><hr><span class=backStage_star>提示：模板页使用标签<strong>${Nav_all}</strong>输出后台自定义导航时有效</span><p style=text-align:right id=tipA><a href=javascript: onclick=tipAction()>不再提示</a>&nbsp;&nbsp;</p></div>',460,0,100,0,0,1);
		}
	}




	
});


<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=Banner幻灯片", "link1=<%=MY_sitePath%>admin/fullColumnSet/edit_banner.asp", "title2=导航菜单", "link2=<%=MY_sitePath%>admin/edit_file/edit_NavMenu.asp");

	   }
	);

});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 幻灯片/导航菜单 | 导航菜单
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
            
                <!-- 后台提示 -->
                
                <div class="notification information png_bg content-alert">
                    <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                    
                        1. 修改导航菜单后，您需要手动操作<a href="../root_temp-edit/index.asp">栏目页面静态生成</a>，才能及时看到效果<br>
                        2. 或者等待系统达到<strong>静态自动更新间隔</strong>时间，访问页面时系统即可更新静态页面，您也可以通过导航<strong>网站设置</strong>，设置此时间<br>
                        3. 二级子导航最多支持20个<br>
                        4. <strong>名称</strong>留空则表示不显示此主导航<br>
                        5. 模板里使用自定义导航的标签为 <strong>${Nav_all}</strong>
                        
                    </div>
                </div>
                
                
					 <%
                     if callCommInfoChk("${Nav_all}") = false then response.Write "<span class=backStage_star>目前模板里暂时没有<strong>自定义导航</strong>标签，无法看到效果<br>(其它原因：此主题并没有调用通用导航菜单标签，而是直接在模板里设计好导航菜单，如果您需要修改导航，请直接修改html模板文件)</span>"
                     %>
                     
                     
                        <div style="height:30px; padding:15px 0 10px 0">
                            <a href="edit_NavMenu.asp?type=c" class="backBtn b1" style="text-decoration:none">通用页面导航</a>&nbsp;&nbsp;
                            <span onMouseOver="tipShow('.phoneAgo',0)" onMouseOut="tipShow('.phoneAgo',1)">
                                <a href="edit_NavMenu_phone.asp?type=p" class="backBtn b2" style="text-decoration:none">手机版页面导航</a>
                                <span class="phoneAgo" style="visibility:hidden">&nbsp;<a href="../../phone/index" target="_blank">手机版预览</a></span>
                            </span>
                        </div>
    
                
          
                        <form method="post" action="action.asp?action=editMenu" name="formM" target="actionPage" onSubmit="javascript:return checkForm();">
                        <%
                        dim i
                        maxSubMenuNum = 20 '子导航最多限制
                        
                        
                        for i=0 to 13
                        
                        bgColor=""
                        if i mod 2<>0 then bgColor="#F2F2F2"
                        
                        nameOk=mName(i,0)
                        nameUrlOk=mNameUrl(i,0)
                        nameTitleOk=mNameTitle(i,0)
                        
                        %>
                        <div style="background:<%=bgColor%>" class="navMainBox" onMouseOver="tipShow('#subBtnControl<%=i%>',0)" onMouseOut="tipShow('#subBtnControl<%=i%>',1)">
                             名称：<input   name="name<%=i%>" type="text"  size="25"  value="<%=nameOk%>" />&nbsp;&nbsp;
                             地址：<input   name="url<%=i%>" type="text"  size="43"  value="<%=nameUrlOk%>" />&nbsp;&nbsp;
                             Title注释：<input   name="title<%=i%>" type="text"  size="20"  value="<%=nameTitleOk%>" /><br />
                             
                             <div id="subBtnControl<%=i%>" style="visibility:hidden">
                             
                                 <% for v = 1 to maxSubMenuNum 
                            
                                    if v = 1 then subBtnShow = "display:block;" else subBtnShow = "display:none;"
                            
                                    subBtnStyle = ""
                                    subBtnStyle = "style="""&subBtnShow&""""
                                    
                                     
                                     
                                 %>
                                 
                                 
                                     <span id="btn_<%=i%>_<%=v%>" class="subBtn" <%=subBtnStyle%> ><a href="javascript:"class="backBtn" onclick="$('#btn_<%=i%>_<%=v%>').fadeOut(2); $('#s_<%=i%>_<%=v%>,#btn_<%=i%>_<%=v + 1%>').fadeIn(2);" style="text-decoration:none">增加二级导航</a></span>
                                 <% next %>
                             
                             </div>
                             
                             
                             <!--/////////////////二级导航    begin///////////////////////////     -->
                                 <% 
                                 
                                    for s = 1 to maxSubMenuNum
                                    
                                        if s = 1 then subInputTop = "margin-top:15px;" else subInputTop = "margin-top:5px;"
                                        
                                        subInputStyle = ""
                                        subInputStyle = "style="""&subInputTop&""""
                                        
                                                     
                                        nameOk_sub=mName(i,s)
                                        nameUrlOk_sub=mNameUrl(i,s)
                                        nameTitleOk_sub=mNameTitle(i,s)
                                        
                                        subMenuShow = ""
                                        if nameOk_sub = "" then  subMenuShow = "style=""display:none;""" else subMenuShow = "style=""display:block;"""
                                        
                                        
                                     
                                 
                                  %>
                                 
                                         <div class="subNavBox" <%=subInputStyle%> >
                                    
                                             <span id="s_<%=i%>_<%=s%>" <%=subMenuShow%>>
                                             
                                                 名称：<input   name="name_<%=i%>_<%=s%>" type="text"  size="22"  value="<%=nameOk_sub%>" />&nbsp;&nbsp;
                                                 地址：<input   name="url_<%=i%>_<%=s%>" type="text"  size="41"  value="<%=nameUrlOk_sub%>" />&nbsp;&nbsp;
                                                 Title注释：<input   name="title_<%=i%>_<%=s%>" type="text"  size="20"  value="<%=nameTitleOk_sub%>" />
                                             </span>
                                    
                                         </div>
                                 
                                 <% next %>
                                 
                              <!--/////////////////二级导航    end///////////////////////////     -->
                             
                             
                        </div>
                        <%next%>
                        
                                    <div class="btnBox">
                                        <input type="submit" value="保存" class="sub">
                                    </div>  
                                                                
                        </form>	 

                    
                
              
              </div>      
         </div>       

  
<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


