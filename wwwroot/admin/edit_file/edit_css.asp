<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<%
cssURL = request.QueryString("cssURL")	

if cssURL = "" then cssURL = "../../template/main.css"	

cssURLBtnClass = replace(replace(cssURL,".css",""),"../../template/","")

response.Write "<style>.horizontal-class ul li.b-"&cssURLBtnClass&"{background:#fff;}</style>"

Set FSO=Server.CreateObject("Scripting.FileSystemObject")
Set AttachmentFolder= FSO.GetFolder(Server.MapPath("..\..\template")) 
Dim AttachmentFileList
Set AttachmentFileList=AttachmentFolder.Files
Dim AttachmentFileName,AttachmentFileEvery

%>


<!-- 代码高亮   begin -->
    <link rel="stylesheet" href="../../plugins/editorHighlighter/lib/codemirror.css">
    <script src="../../plugins/editorHighlighter/lib/codemirror.js"></script>
    <script src="../../plugins/editorHighlighter/mode/css.js"></script>
    <script src="../../plugins/editorHighlighter/mode/xml.js"></script>
    <style type="text/css">.CodeMirror { border:4px solid #E1E1E1}</style>
<!-- 代码高亮  end  -->

<script>



$(function(){
	
	
	//for IE6
	if ($.browser.msie && $.browser.version < 7) {

	}else{
		
		  //初始化编辑器
		  var editor = CodeMirror.fromTextArea(document.getElementById("htmlEditorInit-css"), {
			mode: {name: "css", alignCDATA: true},
			lineNumbers: true,
			//width: '100%',
			//height: '100%',
			textWrapping: false,
			autoMatchParens: true
		  });
	}
	  

	  
	$(document).ready(function() {
	
		 $(".CodeMirror-scroll,.CodeMirror").animate({height: window.screen.height - 335 + "px"},{queue:false,duration:560});
	
	}); 
});


<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=自定义背景/文字颜色", "link1=<%=MY_sitePath%>admin/edit_file/edit_commStyle.asp", "title2=模板HTML代码", "link2=<%=MY_sitePath%>admin/edit_file/edit_TEMP.asp", "title3=模板CSS样式表", "link3=<%=MY_sitePath%>admin/edit_file/edit_css.asp", "title4=模板文字包", "link4=<%=MY_sitePath%>admin/edit_file/edit_lang.asp");

	   }
	);


});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="edit_TEMP.asp"  class="link">HTML代码</a>
                <a href="edit_commStyle.asp"  class="link">背景/文字颜色</a>
                <a href="edit_lang.asp"  class="link">模板文字包</a>
                
                <!-- 当前位置 -->
                当前位置： 自定义样式/模板 | 模板CSS样式表
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
            
                    <!--[if lt IE 7 ]>
                    
                        <div class="notification information png_bg content-alert">
                            <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                            <div>
                            <br><span class="backStage_star">您当前使用IE6浏览器，不支持代码高亮编辑，建议升级您的浏览器或者更换其它浏览器！</span>
                            </div>
                        </div>
                        
                      <![endif]-->
                    
                    <!-- 独立内容区 -->
                    <div class="singleContentBox">
                         
					 <%
                     if instr(cssURL,"temp-userStyle.css") > 0 then response.Write "<div class=backStage_star><strong>temp-userStyle.css</strong>是关联主题主要CSS文件的，它关联到后台直接设置 <strong>[自定义背景/文字颜色]</strong> 的功能</div>"
                     %>
                    
                           <div class="horizontal-class">
                              <ul>
									<%
                                    For Each AttachmentFileEvery IN AttachmentFileList
                                        AttachmentFileName="..\..\template\"&AttachmentFileEvery.Name
                                        
                                        if instr(AttachmentFileEvery.Name,".css") > 0 then response.write "<li class=""b-"&replace(AttachmentFileEvery.Name,".css","")&"""><a href=""?cssURL=../../template/"&AttachmentFileEvery.Name&""">"&AttachmentFileEvery.Name&"</a></li>"&vbcrlf
          
                                        
                                    Next
                                    %>  
                            
                               </ul>
                            </div> 
                    
                        
    
                        <form method="post" action="action.asp?action=editCss&mypath=<%=cssURL%>" >
                              <textarea name="okNameCss" id="htmlEditorInit-css" cols="93" rows="25"  style="width:98.5%;"><% response.write replace(replace(showFile(cssURL), ">", "&gt;"), "<", "&lt;") %></textarea>
                              
                            
                       <div class="btnBox">
                       
                           <input type="submit" value="保存" class="sub" onclick="loadingCreate();" id="needCreateLoadingBtn">
                           
                       </div> 
                            
                        </form>	
    
                    </div>
                    
                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


