<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<script>

<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=文章评论", "link1=<%=MY_sitePath%>admin/article/article_comment.asp", "title2=访客留言", "link2=<%=MY_sitePath%>admin/message/message_edit.asp","title3=关键词过滤", "link3=<%=MY_sitePath%>admin/shield/keywords.asp");

	   }
	);

	
	
});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 评论留言管理 | 关键词过滤
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          

          
                <!-- 后台提示 -->
                
                <div class="notification information png_bg content-alert">
                    <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        使用"|"隔开,开头和结尾不能用"|"
                    </div>
                </div>
          
                <!-- 表单域  -->
                <div id="contentShow">
                
                                    <form action="action.asp?action=ok" name="formM"   method="post" target="actionPage" onSubmit="javascript:return ChkFormTip_page('保存成功',1);" >
                                           
                                                <table width="100%" class="setting" cellspacing="0">
                                                    
                                                    <!-- 标题 -->                        
                                                    <thead class="setHead">
                                                        <tr>
                                                               <th  class="setTitle"></th>
                                                               <th colspan="2"></th>
                                                        </tr>
                                                        
                                                    </thead>
                                                    
                                                 
                                                    <!-- 底部 -->
                                                    <tfoot>
                                                    
                                                        <tr>
                                                            <td colspan="3">         
                                                                <div class="btnBox">
                                                                    <input type="submit" value="保存" class="sub">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                    
                                                    
                                                    <tbody>
                                                    
                                                       <!-- +++++++  内容   begin +++++++  -->

                                   
                                                        <tr>
                                                            <td class="title">关键词：</td>
                                                            <td colspan="2" class="ui-element">
																<%		
                                                                sql="select * from [keyword_controls]"
                                                                set rs=db.execute(sql)
                                                                %>	
                                                            
                                                               <textarea name="shield_words" cols="85" rows="10" ><%=rs("shield_words")%></textarea>
                                                              
																<%		
                                                                rs.close
                                                                set rs=nothing
                                                                %>
                                                              
                                                            </td>
                                                        </tr>  
                                                        
                                                        
                                                  
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                                                
                                                    </tbody>
                                                    
                                                </table>
                   
                   
                                    </form>  


                               
                     </div> 
          
                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


