<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/iframe_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
searchGoEdit = request.QueryString("searchGoEdit")
action = request.QueryString("action")
ID = request.QueryString("ID")
%>
<style>@import "../_manage_css/iframeStyle.css";.jqTransformSelectWrapper ul{height: auto; overflow:hidden; overflow-y:auto}</style>


<%if action = "links" then%>

<%
set rs =server.createobject("adodb.recordset")
sql="select * from [Link] where Linkid = "&ID&"" 
rs.open sql,db,1,1

%>
<script>
//表单判断
function checkForm(){
	exp=/[^0-9()-]/g;
	if(document.formM.LinkTitle.value==""){
		ChkFormStatus_iframe("网站名称不能为空",0);
		return false;
	}
	if(document.formM.URL.value==""  || document.formM.URL.value=="http://"){
		ChkFormStatus_iframe("地址不能为空",0);
		return false;
	}
	if(document.formM.ClassName.value==""){
		ChkFormStatus_iframe("网站类型不能为空",0);
		return false;
	}
	if(document.formM.index.value==""){
		ChkFormStatus_iframe("排序不能为空",0);
		return false;
	}
	if(document.formM.index.value != "" && document.formM.index.value.search(exp)  !=  -1){	
		ChkFormStatus_iframe("排序必须为数字",0);
		return false;
	}	
	
	
	//ok
	ChkFormStatus_iframe('保存成功',2);
	return true;
		
}

</script> 
    <form name="formM"  action="action.asp?searchGoEdit=<%=searchGoEdit%>&action=update&ID=<%=ID%>" method="post" target="iframeAction" onSubmit="javascript:return checkForm();">
    
                    <table width="100%" class="setting" cellspacing="0">
                        
                        <!-- 标题 -->                        
                        <thead class="setHead-class">
                            <tr>
                                   <th  class="setTitle-class"></th>
                                   <th colspan="2"></th>
                            </tr>
                            
                        </thead>
                        
                     
                        <!-- 底部 -->
                        <tfoot>
                        
                            <tr>
                                <td colspan="3"  class="ui-element">         
                                    <div class="btnBox-iframe">
                                        <input name="Submit" type="submit" value="保存" class="ui-btn"/> <span id="tip_iframe"></span>
                                  </div>
                                </td>
                            </tr>
                        </tfoot>
                        
                        <!-- +++++++  内容   begin +++++++  -->
                        <tbody>
    
                            <tr>
                                <td class="title">网站名称：</td>
                                <td colspan="2" class="ui-element">
                                <input name="LinkTitle" type="text" value="<%=rs("LinkTitle")%>"  size="12"  />
                               
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="title">类型：</td>
                                <td colspan="2" class="ui-element">
                                    <select name="ClassName">
                                                      <option value="<%=rs("ClassName")%>" selected="selected"><%=rs("ClassName")%></option>
                                                      <option value="个人网站/博客">个人网站/博客</option>
                                                      <option value="企业团队">企业团队</option>
                                                      <option value="媒体-出版社-机构">媒体-出版社-机构</option>
                                                      <option value="专业服务机构">专业服务机构</option>        
                                                      <option value="其他类型">其他类型</option>
                        
                                        </select>
       
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="title">地址：</td>
                                <td colspan="2" class="ui-element">
                                <input name="URL"  type="text" size="15" value="<%=rs("URL")%>"/>
                               
                               
                                </td>
                            </tr>
                            
                            <tr onMouseOver="tipShow('.tipsFloatWin-upload-min',0)" onMouseOut="tipShow('.tipsFloatWin-upload-min',1)">
                                <td class="title">LOGO：</td>
                                <td colspan="2" class="ui-element">
                                 <input name="logo" id="objdefault"  type="text" size="18" value="<%=rs("logo")%>"/>
                                 <br clear="all">
                                 <div class="tipsFloatWin-upload-min" style="width:340px"><iframe src=../upload/upload_imgID.html?imgID=default class=backStage_uploadFrame allowTransparency=true height="50"  frameborder=0 scrolling=No></iframe><br>推荐尺寸：<span class=backStage_star>88×31</span>,支持外链图片</div>
    
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="title">首页显示：</td>
                                <td colspan="2">
                                    <div class="on_off-box">
                                            <span  class="on_off">                 
											   <%if rs("homeShow")=1 then%>	 
                                               <input type="checkbox" name="homeShow" value="1" checked="checked" />
                                               <%else%>
                                               <input type="checkbox" name="homeShow"  value="1" />
                                              <%end if%>
                                             </span>
                                      </div>
                               
                               
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="title">显示<a href="javascript:void(0)" class="help" data-info="屏蔽此项后首页显示<strong>最终无效</strong>"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                <td colspan="2">
                                    <div class="on_off-box">
                                            <span  class="on_off">                 
											   <%if rs("view")=1 then%>	 
                                               <input type="checkbox" name="view" value="1" checked="checked"  />
                                               <%else%>
                                               <input type="checkbox" name="view"  value="1" />
                                              <%end if%>
                                             </span>
                                             
                                      </div>
                               
                                </td>
                            </tr>
                            
     
                            <tr>
                                <td class="title">排序：</td>
                                <td colspan="2" class="ui-element">
                                <input name="index" type="text" size="5" value="<%=rs("index")%>"  />
                                </td>
                            </tr>
                                           
                            
                              
                           <!-- +++++++  内容   end +++++++  -->
                    
                    
                        </tbody>
                        
                    </table>
                    
                    
    </form>
    
                           
<%
rs.close
set rs=nothing
%> 
<%end if%>

<!--#include file="../_comm/iframe_bottom.asp" -->
