<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<script>

//表单判断
function checkForm(){
	if(document.formM.LinkTitle.value==""){
		ChkFormTip_page("网站名称不能为空",0);
		return false;
	}
	if(document.formM.URL.value=="" || document.formM.URL.value=="http://" ){
		ChkFormTip_page("地址不能为空",0);
		return false;
	}
	if(document.formM.ClassName.value==""){
		ChkFormTip_page("网站类型不能为空",0);
		return false;
	}
	//ok
	loadingCreate();
	return true;
		
}


<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=添加链接", "link1=<%=MY_sitePath%>admin/link/links_add.asp", "title2=管理链接", "link2=<%=MY_sitePath%>admin/link/links_edit.asp","title3=链接审核", "link3=<%=MY_sitePath%>admin/link/userLinks_list.asp");

	   }
	);
	
	
	//外链地址判断
	imgInputChkSrc("#objdefault","#objdefault_imgShow");
	
	
});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="links_edit.asp" class="link">管理链接</a>
                <a href="userLinks_list.asp" class="link">链接审核</a>
                <a href="links_edit-Shielded.asp" class="link">已屏蔽链接</a>
                
                <!-- 当前位置 -->
                当前位置： 友情链接 | 添加链接
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
            
                
                <!-- 后台提示 -->
                
                <div class="notification information png_bg content-alert">
                    <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        
                        模板里首页输出链接的标签为 <strong>${homeLinks}、${homeLinks_txt}(纯文字)</strong><br>
          
                        <%if MY_linksOrderType = 0 then%> 
                        
                        <a class="bS-btn" href="../website/_batchLinksSequence.asp?action=self" target="htmlControl" id="batchBtn" onClick="$('#bS').html('自编排序');$('.bS-btn').fadeOut(500);">调整为<span class="io">自编排序</span></a>
						
						<%else%>
                        
                         <a  class="bS-btn"  href="../website/_batchLinksSequence.asp?action=random" target="htmlControl" id="batchBtn" onClick="$('#bS').html('随机排序');$('.bS-btn').fadeOut(500);">调整为<span class="io">随机排序</span></a>
                        
                        <%end if%>
                        
                        当前状态：<span id="bS" class="backStage_star"><%if MY_linksOrderType = 0 then response.Write "随机排序" else response.Write "自编排序"%></span>
                        <span data-open="0" id="batchBtnStatus"></span>
                        
                        
                    </div>
                </div>
          
                <!-- 表单域  -->
                <div id="contentShow">
                
					 <%
					 if callCommInfoChk("${linkslist}") = false then response.Write "<script>floatWin('温馨提示','<div class=tipsFloatWin>目前模板里暂时没有<strong>内页友情链接</strong>标签，无法看到效果<br>(其它原因：此主题并没有内页友情链接模块)</div>',350,0,100,0,0,0);</script>"
                     %>

                                <form name="formM" action="action.asp?action=add" method="post" onSubmit="javascript:return checkForm();">
                                       
                                            <table width="100%" class="setting" cellspacing="0">
                                                
                                                <!-- 标题 -->                        
                                                <thead class="setHead-class">
                                                    <tr>
                                                           <th  class="setTitle-class"></th>
                                                           <th colspan="2"></th>
                                                    </tr>
                                                    
                                                </thead>
                                                
                                             
                                                <!-- 底部 -->
                                                <tfoot>
                                                
                                                    <tr>
                                                        <td colspan="3">         
                                                            <div class="btnBox">
                                                                <input type="submit" value="添加" class="sub" id="needCreateLoadingBtn">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                                
                                                
                                                <tbody>
                                                
                                                   <!-- +++++++  内容   begin +++++++  -->
                        
                                                    <tr>
                                                        <td class="title">网站名称：<span class="backStage_star">*</span></td>
                                                        <td colspan="2" class="ui-element">
                                                                                        
                                                            <input name="LinkTitle" type="text" size="50">

                                                          
                                                        </td>
                                                    </tr>  
                                                    
                                                    
                                                    
                                                    <tr>
                                                        <td class="title">网站类型：<span class="backStage_star">*</span></td>
                                                        <td colspan="2" class="ui-element">
                                                          <select name="ClassName">           
                                                              <option value="" selected="selected">网站类别</option>
                                                              <option value="个人网站/博客">个人网站/博客</option>
                                                              <option value="企业团队">企业团队</option>
                                                              <option value="媒体-出版社-机构">媒体-出版社-机构</option>
                                                              <option value="专业服务机构">专业服务机构</option>        
                                                              <option value="其他类型">其他类型</option>
                                                        </select>

                                                          
                                                        </td>
                                                    </tr> 
                                                    
                                                    <tr>
                                                        <td class="title">显示<a href="javascript:void(0)" class="help" data-info="屏蔽此项后首页显示<strong>最终无效</strong>"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                                        <td colspan="2">
                                                            <div class="on_off-box">
                                                                    <span  class="on_off">                 
                                                                       <input type="checkbox" name="view" value="1" checked="checked"  />
                                                                     </span>
                                                                     
                                                              </div>
                                                       
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td class="title">首页显示：</td>
                                                        <td colspan="2">
                                                           <div class="on_off-box">
                                                                <span  class="on_off">
                                                                      <input type="checkbox" name="homeShow" value="1" checked="checked" />
                                                                
                                                                </span>
                                                           </div>
                                                        </td>
                                                    </tr>  
                                                    
                                                    
                                                    <tr>
                                                        <td class="title">网站地址：<span class="backStage_star">*</span></td>
                                                        <td colspan="2" class="ui-element">
                                                                                        
                                                           <input name="URL" type="text" size="35" class="focusJS" data-value="http://" value=""/>
                                                          
                                                        </td>
                                                    </tr> 
                                                    
                                                    
                                                    <tr onMouseOver="tipShow('.tipsFloatWin-upload-min',0)" onMouseOut="tipShow('.tipsFloatWin-upload-min',1)">
                                                        <td class="title">网站LOGO：</td>
                                                        <td colspan="2" class="ui-element">
                                                            <input name="obj" id="objdefault" type="text" size="55"  class="help" data-info="您可以本地上传图片，或者直接插入http外链图片网址"/>
                                                             <br clear="all">
                                                             <span id="objdefault_imgShow"></span>
                                                             <div class="tipsFloatWin-upload-min"><iframe src=../upload/upload_imgID.html?imgID=default class=backStage_uploadFrame allowTransparency=true  frameborder=0 scrolling=No></iframe><br>推荐尺寸：<span class=backStage_star>88×31</span>,支持外链图片</div>
                                                             
                                                                                
            
                                                        </td>
                                                    </tr>     
                                                    
                                                    
                                                    
   
                                                   <!-- +++++++  内容   end +++++++  -->
                                            
                                            
                                                </tbody>
                                                
                                            </table>
               
               
                                </form>  


                               
                     </div> 
                  
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->




