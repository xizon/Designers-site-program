<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<script>
<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=添加链接", "link1=<%=MY_sitePath%>admin/link/links_add.asp", "title2=管理链接", "link2=<%=MY_sitePath%>admin/link/links_edit.asp","title3=链接审核", "link3=<%=MY_sitePath%>admin/link/userLinks_list.asp");

	   }
	);
	
});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="links_add.asp" class="link">添加链接</a>
                <a href="links_edit.asp" class="link">管理链接</a>
                <a href="links_edit-Shielded.asp" class="link">已屏蔽链接</a>
         
                
                <!-- 当前位置 -->
                当前位置： 友情链接 | 链接审核
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
                <!-- 后台提示 -->
                
                <div class="notification information png_bg content-alert">
                    <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        1.如果有LOGO的请审核后拷贝LOGO地址到链接列表中并保存修改<br>
                        2.审核通过默认不添加LOGO
                    </div>
                </div>
                
                
                <!-- 表单域  -->
                <div id="contentShow">
                      
                          <!-- 列表 -->
                        
                                                <table width="100%" class="list" cellspacing="1">
                                                    
                                                    <!-- 标题 -->
                                                    <thead class="setTitle">
                                                        <tr>
                                                           <!-- 有多少竖列 -->
                                                           <th>ID</th>
                                                           <th>网站名称</th>
                                                           <th>站长QQ</th>
                                                           <th>Email</th>
                                                           <th>类型</th> 
                                                           <th>地址</th>
                                                           <th>提交时间</th>
                                                           <th>操作</th>
                                                        </tr>
                                                        
                                                   </thead>

                                                    
                                                    <tbody>
                                                    
                                                       <!-- +++++++  内容   begin +++++++  -->
                                                                                                            
														<% 
 
                                                         if request.querystring("pageno") ="" then
                                                                        pageno=1
                                                                    else
                                                                    pageno=cint(request.querystring("pageno") )
                                                                    end if
                                                                    '---------------------------一部分
                                                            set rs=server.CreateObject("adodb.recordset")
                                                            strsql="select * from [links_verify] order by ID desc"	     
                                                            rs.open strsql,db,1,1
                                                            k = 1
                                                                 '--------------------------------开始分页
                                                            
                                                        %>
                                                        <!--#include file="../function/page_first.asp" -->
                                                    
                                                        <!--////////////////循环开始 ////////////////-->
                                                        <tr id="del<%=rs("ID")%>">
                                                            <td><%=k%></td>
                                                            <td><%=rs("title")%></td>
                                                            <td><%=rs("qq")%></td>
                                                            <td><%=rs("email")%></td>
                                                            <td><%=rs("type")%></td>
                                                            <td><a href="<%=rs("url")%>" target="_blank"><img src="../../plugins/d-s-management-ui/img/view.gif"></a></td>
                                                            <td><%=rs("Subdate")%></td>
                                                            <td>
                                                                 <!--icon -->
                                                                 <a onclick="javascript:floatWin('温馨提示','<div align=center><span id=del_Btn<%=rs("ID")%>><a href=action.asp?action=deluserLink&ID=<%=rs("ID")%> target=delFrame class=backBtn onclick=javascript:$(\'#del<%=rs("ID")%>,#del_Btn<%=rs("ID")%>\').fadeOut(2);$(\'#delOk<%=rs("ID")%>\').fadeIn(500);>确认删除?</a></span><span id=delOk<%=rs("ID")%> style=display:none>√删除成功！</span></div>',350,50,150,0,0,1);" target="delFrame" href="javascript:"><img src="../../plugins/d-s-management-ui/img/cross.png" title="删除" /></a>
                                                                 
                                                                 <a title="通过审核" href="action.asp?action=Linkok&name=<%=rs("title")%>&url=<%=rs("url")%>&type=<%=rs("type")%>"  ><img src="../../plugins/d-s-management-ui/img/tick_circle.png" align="通过审核"></a>
                                                           
                                                            </td>


                                                        </tr>
                                                        
                                                        <!--////////////////循环结束 ////////////////-->
                                                        
														<%
														 k = k + 1
                                                         rs.movenext
                                                         loop
                                                         %>  
                                                         
                                                         <iframe name="delFrame" style="display:none"></iframe>
                                                         
                                                         <!-- 无数据 -->
                                                         <%if rs.bof and rs.eof then %>
                                                         
                                                            <tr>
                                                                <td class="title" colspan="8">
                                                                 暂时还没有数据！
                                                                </td>
                                                            </tr>
                                                         
                                                         <%end if%>
														 
                                                
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                        
                                                
                                                    </tbody>
                                                    
                                                 
                                                    <!-- 底部 -->
                                                    <tfoot>
                                                    
                                                        <tr>
                                                            <td colspan="8">
                                                               
                                                               <!--#include file="../function/page_second.asp" --> 
                                                
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                    
                                                    
                                                    
                                                </table>
                                            
            
                               
                     </div> 
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->



