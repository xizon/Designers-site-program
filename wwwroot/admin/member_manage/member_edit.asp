<%memPage = 1%>
<!--#include file="../../member/function/conn.asp" -->
<!--#include file="../../member/function/function.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_mem.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<!--#include file="../admin_login/inc_getSupUser.asp" -->

<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<script>
<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=注册用户管理", "link1=<%=MY_sitePath%>admin/member_manage/member_edit.asp", "title2=投稿管理", "link2=<%=MY_sitePath%>admin/member_manage/memResource_list.asp","title3=建议反馈", "link3=<%=MY_sitePath%>admin/member_manage/msg_list.asp","title4=用户权限设置", "link4=<%=MY_sitePath%>admin/member_manage/config.asp","title5=用户数据库管理", "link5=<%=MY_sitePath%>admin/member_manage/database_member.asp");

	   }
	);
	
});

function editFrame(ID){
	ShowIFrameEdit('会员管理','member_manage/editFrame.asp?action=member&ID=' + ID,150,300,320,150,100,0);
	
}


<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 会员中心 | 注册用户管理
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
            
                <!-- 后台提示 -->
                
                <div class="notification information png_bg content-alert">
                    <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                    
                        1.如果您把会员权限设置成[<strong>超级管理员</strong>]，那此帐号将拥有直接进入超级管理员面板的权限，此操作适合同一网站多个人员管理！<br>
                        2.设置成其他类型则只是起到身份标识的作用，并无特殊操作权限。
                        
                    </div>
                </div>
          
                  <!-- 搜索 -->
                  <!--#include file="searchBoard.asp" -->
 
                                      
                
                <!-- 表单域  -->
                <div id="contentShow">
                      
                          <!-- 列表 -->
                        

                                                <table width="100%" class="list" cellspacing="1">
                                                    
                                                    <!-- 标题 -->
                                                    <thead class="setTitle">
                                                        <tr>
                                                           <!-- 有多少竖列 -->
                                                           <th>ID</th>
                                                           <th>会员名</th>
                                                           <th>网址</th>
                                                           <th>权限</th> 
                                                           <th>Email</th>
                                                           <th>注册类型</th>
                                                           <th>注册时间</th>
                                                           <th>操作</th>
                                                           
                                                        </tr>
                                                        
                                                   </thead>

                                                    
                                                    <tbody>
                                                    
                                                       <!-- +++++++  内容   begin +++++++  -->
														  <%
                                                         if request.querystring("pageno") ="" then
                                                                        pageno=1
                                                                    else
                                                                    pageno=cint(request.querystring("pageno") )
                                                                    end if
                                                                    '---------------------------一部分
                                                                    
                                                                    dim rs,strsql,j
                                                                    j=0
                                                            '----------显示记录
                                                            set rs=server.CreateObject("adodb.recordset")
                                                            strsql="select * from [user] order by ID desc"	     
                                                            rs.open strsql,conn,1,1
															k = 1
                                                        %> 
                                                        <!--#include file="../function/page_first.asp" -->
                                                        <%j=j+1%>	
                                                        <%
														level=rs("level")
					
														if rs("uname") = Application("CommSupUserName") then response.Write "<script>$(document).ready(function(){$('#delBtn"&rs("ID")&",#delBtn2"&rs("ID")&"').fadeOut(500);});</script>"
														
														
														if rs("uname") = Application("CommSupUserName") then showName = "<span class=backStage_star>"&rs("uname")&"</span>" else showName = rs("uname")
					
	
														%>
                                                    
                                                        <!--////////////////循环开始 ////////////////-->
                                                        <tr id="del<%=j%>" title="header=[昵称：<%=rs("nickname")%>] body=[登录次数：<%=rs("login_num")%>次<br>发布资源：<%=rs("resource_count")%>个<br>QQ：<%=rs("qq")%><br>最后登陆：<%=rs("login_last")%><br>来自：<%=rs("province")%>-<%=rs("city")%>]">
                                                            <td><%=rs("ID")%></td>
                                                            <td class="title"><%=showName%></td>
                                                            <td>
                                                            <%if rs("uname") <> Application("CommSupUserName") then%>
                                                            <a href="<%=rs("homepage")%>" title="浏览相关网址" target="_blank"><img src="../../plugins/d-s-management-ui/img/view.gif"></a>
                                                            <%end if%>
                                                            </td>
                                                            <td>
                                                            <%
                                                                    
                                                                    if level=0 then levelShow="普通会员"
                                                                    if level=1 then levelShow="VIP会员"
                                                                    if level=2 then levelShow="商务客户"
                                                                    if level=3 then levelShow="二线管理员"
                                                                    if level=4 then levelShow="<span class=backStage_star>超级管理员</span>"
																	if level=4 and rs("uname") = Application("CommSupUserName") then levelShow="<span class=backStage_star>系统超级管理员</span>"
                                                             %>
                                                
														    	<%=levelShow%>
                                                  

                                                            
                                                           
                                                            </td>  
                                                            
                                                            <td><%=rs("email")%></td>
                                                            <td>
																  <%
                                                                  if business=0 then businessShow="个人注册"
                                                                  if business=1 then businessShow="企业注册"
																  if rs("uname") <> Application("CommSupUserName") then response.Write businessShow
                                                                  %>

                                                            
                                                            </td>
                                                            
                                                            <td><%=rs("l_date")%></td>
                                                           
                                                            
                                                            <td>
                                                                 <!--icon -->
                                                                 
                                                                 <a id="delBtn2<%=rs("ID")%>" href="javascript:" data-open="0" onclick="editFrame('<%=rs("ID")%>');" title="编辑"><img src="../../plugins/d-s-management-ui/img/hammer_screwdriver.png"></a> 

                                                                 <a onclick="javascript:floatWin('温馨提示','<div align=center><span id=del_Btn<%=j%>><a href=action.asp?action=deluser&ID=<%=rs("ID")%> target=delFrame class=backBtn onclick=javascript:$(\'#del<%=j%>,#del_Btn<%=j%>\').fadeOut(2);$(\'#delOk<%=j%>\').fadeIn(500);>确认删除?</a></span><span id=delOk<%=j%> style=display:none>√删除成功！</span></div>',350,50,150,0,0,1);" target="delFrame" href="javascript:" id="delBtn<%=rs("ID")%>"><img src="../../plugins/d-s-management-ui/img/cross.png" title="删除" /></a>
                                                                 
                                                                 <input type="hidden" value="<%=rs("ID")%>" name="ID<%=j%>" />
                                                                 
                                                           
                                                            </td>


                                                        </tr>
                                                        
                                                        <!--////////////////循环结束 ////////////////-->
                                                        
														<%
														 k = k + 1
                                                         rs.movenext
                                                         loop
                                                         %>  
                                                         
                                                         <iframe name="delFrame" style="display:none"></iframe>
                                                         
                                                         <!-- 无数据 -->
                                                         <%if rs.bof and rs.eof then %>
                                                         
                                                            <tr>
                                                                <td class="title" colspan="8">
                                                                 暂时还没有数据！
                                                                </td>
                                                            </tr>
                                                         
                                                         <%end if%>
														 
                                                
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                        
                                                
                                                    </tbody>
                                                    
                                                 
                                                    <!-- 底部 -->
                                                    <tfoot>
                                                    
                                                        <tr>
                                                            <td colspan="8">
                                                               
                                                               <!--#include file="../function/page_second.asp" --> 
                                                               
                   
                                                
                                                            </td>

                                                        </tr>
                                                    </tfoot>
                                                    
                                                    
                                                    
                                                </table>
                
            
                               
                     </div> 
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->




