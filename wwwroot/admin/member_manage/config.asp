<%memPage = 1%>
<!--#include file="../../member/function/conn.asp" -->
<!--#include file="../../member/function/function.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_mem.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<%

	'----------显示记录
	set rs =server.createobject("adodb.recordset")
	sql="select * from [mem_forbit]"
	rs.open sql,conn,1,1

%>


<script>


<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=注册用户管理", "link1=<%=MY_sitePath%>admin/member_manage/member_edit.asp", "title2=投稿管理", "link2=<%=MY_sitePath%>admin/member_manage/memResource_list.asp","title3=建议反馈", "link3=<%=MY_sitePath%>admin/member_manage/msg_list.asp","title4=用户权限设置", "link4=<%=MY_sitePath%>admin/member_manage/config.asp","title5=用户数据库管理", "link5=<%=MY_sitePath%>admin/member_manage/database_member.asp");

	   }
	);

	
});

$(function(){
	

	$("#slider-size").slider({ 
	    value: <%=rs("upload_size")%>,
		step: 50, 
		max: 100000,
		min: 50,
		range: "min",
		slide: function(event, ui){ 
			$("#upload_size").val(ui.value); 
			$(".txtShow").html(ui.value + "KB");
		} 
	
	}); 


		
		

});



<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 会员中心 | 用户权限设置
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          

          
                <!-- 表单域  -->
                <div id="contentShow">

                                    <form  action="action.asp?action=setconfig"  name="formM"   method="post" target="actionPage"  onSubmit="javascript:return ChkFormTip_page('保存成功',1);">
                                           
                                                <table width="100%" class="setting" cellspacing="0">
                                                    
                                                    <!-- 标题 -->                        
                                                    <thead class="setHead">
                                                        <tr>
                                                               <th  class="setTitle"></th>
                                                               <th colspan="2"></th>
                                                        </tr>
                                                        
                                                    </thead>
                                                    
                                                 
                                                    <!-- 底部 -->
                                                    <tfoot>
                                                    
                                                        <tr>
                                                            <td colspan="3">         
                                                                <div class="btnBox">
                                                                    <input type="submit" value="保存" class="sub">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                    
                                                    
                                                    <tbody>
                                                    
                                                       <!-- +++++++  内容   begin +++++++  -->
                                   
                                                        <tr>
                                                            <td class="title">注册功能：</td>
                                                            <td colspan="2">
                                                            
                                                               <div class="on_off-box">
                                                                    <span  class="on_off">
                                                                        <%if rs("reg_allow")="0" then%>
                                                                        
                                                                            <input type="checkbox" name="reg_allow" value="0" checked="checked"/>
                                                                        
                                                                        <%else%>
                                                                        
                                                                            <input type="checkbox" name="reg_allow" value="0"/>
                                                                        
                                                                        <%end if%>
                                                                        
                                                                    
                                                                    </span>
                                                               </div>
                                                              
                                                            </td>
                                                        </tr>  
                                                        
                                                       
                                                        
                                                        <tr>
                                                            <td class="title">找回密码：</td>
                                                            <td colspan="2">
                                                            
                                                               <div class="on_off-box">
                                                                    <span  class="on_off">
                                                                        <%if rs("seekpw_allow")="0" then%>
                                                                        
                                                                            <input type="checkbox"  name="seekpw_allow" value="0" checked="checked"/>
                                                                        
                                                                        <%else%>
                                                                        
                                                                            <input type="checkbox"  name="seekpw_allow" value="0"/>
                                                                        
                                                                        <%end if%>
                                                                        
                                                                    
                                                                    </span>
                                                               </div>
                                                              
                                                            </td>
                                                        </tr>  
                                                        
                                                        
                                                        <tr>
                                                            <td class="title">资料管理：</td>
                                                            <td colspan="2">
                                                            
                                                               <div class="on_off-box">
                                                                    <span  class="on_off">
                                                                        <%if rs("manage_allow")="0" then%>
                                                                        
                                                                            <input type="checkbox"  name="manage_allow" value="0" checked="checked"/>
                                                                        
                                                                        <%else%>
                                                                        
                                                                            <input type="checkbox"  name="manage_allow" value="0"/>
                                                                        
                                                                        <%end if%>
                                                                        
                                                                    
                                                                    </span>
                                                               </div>
                                                              
                                                            </td>
                                                        </tr>  
                                                        
                                                        <tr>
                                                            <td class="title">评论留言<a href="javascript:void(0)" class="help" data-info="注册才可以发表留言评论"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                                            <td colspan="2">
                                                            
                                                               <div class="on_off-box">
                                                                    <span  class="on_off">
                                                                        <%if rs("writemsg_allow")="0" then%>
                                                                        
                                                                            <input type="checkbox"  name="writemsg_allow" value="0" checked="checked"/>
                                                                        
                                                                        <%else%>
                                                                        
                                                                            <input type="checkbox"  name="writemsg_allow" value="0"/>
                                                                        
                                                                        <%end if%>
                                                                        
                                                                        
                                                                        
                                                                    
                                                                    </span>
                                                               </div>
                                                              
                                                            </td>
                                                        </tr>  
                                                        
                                                        <tr>
                                                            <td class="title">文件上传：</td>
                                                            <td colspan="2">
                                                            
                                                               <div class="on_off-box">
                                                                    <span  class="on_off">
                                                                        <%if rs("upload_allow")="0" then%>
                                                                        
                                                                            <input type="checkbox"  name="upload_allow" value="0" checked="checked"/>
                                                                        
                                                                        <%else%>
                                                                        
                                                                            <input type="checkbox"  name="upload_allow" value="0"/>
                                                                        
                                                                        <%end if%>
                                                                        
                                                                    
                                                                    </span>
                                                               </div>
                                                              
                                                            </td>
                                                        </tr>  
                                                        
                                                        
                                                        <tr>
                                                            <td class="title">允许上传类型<a href="javascript:void(0)" class="help" data-info="使用|隔开"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                                            <td colspan="2" class="ui-element">
                                                            
                                                               <input name="upload_type" type="text" value="<%=rs("upload_type")%>" size="70" />
                                                              
                                                            </td>
                                                        </tr>   
                                                        
                                                                        
                                                        <tr>
                                                            <td class="title">允许上传大小<a href="javascript:void(0)" class="help" data-info="单位：KB"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：</td>
                                                            <td width="205">
                                                                <input name="upload_size" id="upload_size" type="hidden" value="<%=rs("upload_size")%>"/> 
                                                                <span class="txtShow"><%=rs("upload_size")%>KB</span>
                                                                
                                                            </td>
                                                            <td><div class="sliderStyle" id="slider-size"></div></td>
                                                        </tr> 
                                                        
                                                        
                                                        
                                                       <input type="hidden" value="1" name="down_allow">              
                                                        
                                                  
                                                       <!-- +++++++  内容   end +++++++  -->
                                                
                                                
                                                    </tbody>
                                                    
                                                </table>
                   
                   
                                    </form>  


                               
                     </div> 
          
                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


