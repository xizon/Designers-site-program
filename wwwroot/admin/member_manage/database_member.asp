<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<!--#include file="../../member/function/data.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<style>.jqTransformSelectWrapper ul{height: 86px; overflow:hidden; overflow-y:auto}</style>
<%
'使用中的数据库
MY_dataBase_useNow = replace(MY_dataBaseMemName,MY_sitePath&"member/data/","")
'数据库路径
MY_dataBase_folderPath = "..\..\member\data\"

'=====================压缩参数=========================
Function CompactDB(compressdataPath, boolIs97)
Dim fso, Engine, strDBPath,JET_3X
strDBPath = left(compressdataPath,instrrev(compressdataPath,"\"))
Set fso = CreateObject("Scripting.FileSystemObject")
If fso.FileExists(compressdataPath) Then
'数据库原大小
set ofile=fso.GetFile(compressdataPath)
oldsize=ofile.size
set ofile=nothing
Set Engine = CreateObject("JRO.JetEngine")
If boolIs97 = "True" Then

	Engine.CompactDatabase "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & compressdataPath, _
	"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & strDBPath & "temp.mdb;" _
	& "Jet OLEDB:Engine Type=" & JET_3X
set ofile=fso.GetFile(compressdataPath)
newsize=ofile.size
set ofile=nothing
Else	
	Engine.CompactDatabase "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & compressdataPath,"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & strDBPath & "temp.mdb"
End If

fso.CopyFile strDBPath & "temp.mdb",compressdataPath
fso.DeleteFile(strDBPath & "temp.mdb")

'压缩后大小
set ofile2=fso.GetFile(compressdataPath)
newsize=ofile2.size
set ofile2=nothing

Set Engine = nothing

 CompactDB = "数据库, " & compressdataPath & ", 已经压缩成功!(压缩前："&oldsize/1000&" KB,压缩后："&newsize/1000&" KB)" & vbCrLf

Else
CompactDB = "数据库名称或路径不正确. 请重试!" & vbCrLf
End If
Set fso = nothing
End Function

'=====================备份数据库=========================
Function CopyDB(dbPath, bkpath)
Set fso = CreateObject("Scripting.FileSystemObject")
If fso.FileExists(dbPath) Then
	fso.CopyFile dbpath,bkpath
	
	CopyDB = "数据库, " & dbpath & "备份文件 #"&year(now)&"-"&month(now)&"-"&day(now)&"=No."&hour(now)&".asa 已经备份成功!" & vbCrLf
Else
	CopyDB = "数据库名称或路径不正确. 请重试!" & vbCrLf
End If
Set fso = nothing
End Function

'=====================恢复数据库=========================
Function RestoreDB(dbPath, bkdataPath)

Set fso = CreateObject("Scripting.FileSystemObject")

If fso.FileExists(bkdataPath) Then
	fso.CopyFile bkdataPath,dbpath
	
	
	RestoreDB = "数据库, " & bkdataPath & ", 已经恢复成功!<p>恢复成功，请不要刷新本页，您必须点这里安全退出后从新登陆才能继续管理：<a href=""../admin_login/?logout=ok"" onclick=""clearCookies();"" target=""_top""><b>退出后台</b></a></p>" & vbCrLf
	  
Else
	RestoreDB = "数据库名称或路径不正确. 请重试!" & vbCrLf
End If
Set fso = nothing

End Function

'=====================删除数据库=========================
Function DelDB(deldataPath)


Set fso = CreateObject("Scripting.FileSystemObject")
if fso.FileExists(deldataPath) then 
		fso.DeleteFile deldataPath 
   DelDB = "数据库, " & deldataPath & ", 已经删除!" & vbCrLf
Else
	 DelDB = "数据库名称或路径不正确. 请重试!" & vbCrLf
End If

Set fso = nothing

End Function

'-------------------------------
dbpath = server.mappath(MY_dataBaseMemName)
bkpath = server.mappath(""&MY_dataBase_folderPath&"#"&year(now)&"-"&month(now)&"-"&day(now)&"=No."&hour(now)&".asa")

act=request("act")
if act="compress" then
compressdataPath=server.mappath(request.form("compresspathDynamic"))
datamsg=(CompactDB(compressdataPath,0))	
end if
if act="copy" then
datamsg=CopyDB(dbPath, bkpath)
end if

if act="restore" then
bkdataPath=server.mappath(request.form("bkpathDynamic"))
datamsg=RestoreDB(dbPath, bkdataPath)
end if

if act="del" then
deldataPath=server.mappath(request.form("delpathDynamic"))
datamsg=DelDB(deldataPath)
end if


Set FSO=Server.CreateObject("Scripting.FileSystemObject")
Set AttachmentFolder= FSO.GetFolder(Server.MapPath(""&MY_dataBase_folderPath&"")) '获取已经存在的数据库
Dim AttachmentFileList
Set AttachmentFileList=AttachmentFolder.Files
Dim AttachmentFileName,AttachmentFileEvery	


%>   
<script>
//表单判断
function checkForm1(){
	if(document.formM1.bkpathDynamic.value=="none"){
		ChkFormTip_page("请选择要恢复的数据库",0);
		return false;
	}

	//ok
	ChkFormTip_page("操作成功",1);
	return true;
		
}


function checkForm2(){

	if(document.formM3.delpathDynamic.value=="none"){
		ChkFormTip_page("请选择要删除的数据库",0);
		return false;
	}
	if(document.formM3.delpathDynamic.value=="<%=MY_dataBaseName%>"){
		ChkFormTip_page("禁止删除正在使用的数据库",0);
		return false;
	}
	//ok
	ChkFormTip_page("操作成功",1);
	return true;
		
}

<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=注册用户管理", "link1=<%=MY_sitePath%>admin/member_manage/member_edit.asp", "title2=投稿管理", "link2=<%=MY_sitePath%>admin/member_manage/memResource_list.asp","title3=建议反馈", "link3=<%=MY_sitePath%>admin/member_manage/msg_list.asp","title4=用户权限设置", "link4=<%=MY_sitePath%>admin/member_manage/config.asp","title5=用户数据库管理", "link5=<%=MY_sitePath%>admin/member_manage/database_member.asp");

	   }
	);
	
	
});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 会员中心 | 用户数据库管理
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
            
                <!-- 后台提示 -->
                
                <div class="notification information png_bg content-alert">
                    <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        数据库在同一天同一小时段内备份覆盖<br>
                        <strong><%response.Write datamsg%></strong>
                    </div>
                </div>
            
    
                 <!-- 表单域  -->
                <div id="contentShow">
                
                


                                    <form action="database_member.asp?act=copy" method="post">
                                           
                                                     <table width="100%" class="setting" cellspacing="0">
                                                        
                                                        <!-- 标题 -->                        
                                                        <thead class="setHead-class">
                                                            <tr>
                                                                   <th  class="setTitle-class"></th>
                                                                   <th colspan="2"></th>
                                                            </tr>
                                                            
                                                        </thead>
       
                                                        <tbody>
                                                        
                                                           <!-- +++++++  内容   begin +++++++  -->
                                
                                                            <tr>
                                                                <td colspan="3" class="ui-element">                          
                                                                    <div class="btnBox-iframe">
                                                                        <input type="submit" value="备份数据库" class="ui-btn">
                                                                    </div>
                                                                </td>
                                                            </tr>  
                                                     
           
                                                           <!-- +++++++  内容   end +++++++  -->
                                                    
                                                    
                                                        </tbody>
                                                        
                                                    </table>
                   
                   
                                    </form>  
                                    
                                    
                                    <form  action="database_member.asp?act=compress" name="formM2"   method="post">
                                           
                                                     <table width="100%" class="setting" cellspacing="0">
                                                        
                                                        <!-- 标题 -->                        
                                                        <thead class="setHead-class">
                                                            <tr>
                                                                   <th  class="setTitle-class"></th>
                                                                   <th colspan="2"></th>
                                                            </tr>
                                                            
                                                        </thead>
                                                        
                                                        
                                                        
                                                        <tbody>
                                                        
                                                            <!-- +++++++  内容   begin +++++++  -->
                                
                                                            <tr height="115">
                                                                <td class="ui-element">
                                                                <input  type="hidden" name="" value="<%=MY_dataBaseMemName%>">
                                                                      <select name="compresspathDynamic">
                                                                        <option selected="selected" value="none">〓压缩数据库路径〓</option>
                                                                        <%
                                                                                For Each AttachmentFileEvery IN AttachmentFileList
                                                                                    AttachmentFileName=""&MY_dataBase_folderPath&""&AttachmentFileEvery.Name
                                                                                
                                                                                     if instr(AttachmentFileName,MY_dataBase_useNow)=0 then Response.Write "<option value="""&AttachmentFileName&""">"&AttachmentFileEvery.Name&"</option>" else Response.Write ""
                                                                                Next
                                                                       %>
                                                                      </select>
                                                                </td>
                                                                <td colspan="2" class="ui-element">
                                                                    <div class="btnBox-iframe">
                                                                        <input type="submit" value="压缩数据库" class="ui-btn">
                                                                    </div>
                                                                      
                                                                </td>
                                                            </tr>  
                                                     
           
                                                           <!-- +++++++  内容   end +++++++  -->
                                                    
                                                    
                                                        </tbody>
                                                        
                                                    </table>

                   
                   
                                    </form> 
                                    
                                    
                                    <form  action="database_member.asp?act=restore" name="formM1"   method="post" onSubmit="javascript:return checkForm1();">
                                           
                                                     <table width="100%" class="setting" cellspacing="0">
                                                        
                                                        <!-- 标题 -->                        
                                                        <thead class="setHead-class">
                                                            <tr>
                                                                   <th  class="setTitle-class"></th>
                                                                   <th colspan="2"></th>
                                                            </tr>
                                                            
                                                        </thead>
                                                        
                                                    
                                                        
                                                        <tbody>
                                                        
                                                            <!-- +++++++  内容   begin +++++++  -->
                                
                                                            <tr height="115">
                                                                <td class="ui-element">
                                                                      <select name="bkpathDynamic">
                                                                        <option selected="selected" value="none">〓恢复数据库路径〓</option>
                                                                        <%
                                                                                For Each AttachmentFileEvery IN AttachmentFileList
                                                                                    AttachmentFileName=""&MY_dataBase_folderPath&""&AttachmentFileEvery.Name
                                                                
                                                                                    if instr(AttachmentFileName,MY_dataBase_useNow)=0 then Response.Write "<option value="""&AttachmentFileName&""">"&AttachmentFileEvery.Name&"</option>" else Response.Write ""
                                                                                Next
                                                                       %>
                                                                      </select>
                                                                </td>
                                                                <td colspan="2" class="ui-element">

                                                                    <div class="btnBox-iframe">
                                                                        <input type="submit" value="恢复数据库" onClick="return confirm('确定要恢复数据库？')" class="ui-btn">
                                                                    </div>
                                                                      
                                                                </td>
                                                            </tr>  
                                                     
           
                                                           <!-- +++++++  内容   end +++++++  -->
                                                    
                                                    
                                                        </tbody>
                                                        
                                                        
                                                    </table>

                   
                   
                                    </form>  
                                    
                                    
                                    <form  action="database_member.asp?act=del" name="formM3"   method="post" onSubmit="javascript:return checkForm2();">
                                           
                                                     <table width="100%" class="setting" cellspacing="0">
                                                        
                                                        <!-- 标题 -->                        
                                                        <thead class="setHead-class">
                                                            <tr>
                                                                   <th  class="setTitle-class"></th>
                                                                   <th colspan="2"></th>
                                                            </tr>
                                                            
                                                        </thead>
                                                        
     
                                                        
                                                        <tbody>
                                                        
                                                            <!-- +++++++  内容   begin +++++++  -->
                                
                                                            <tr height="115">
                                                                <td class="ui-element">
                                                                      <select name="delpathDynamic">
                                                                        <option selected="selected" value="none">〓删除数据库路径〓</option>
                                                                        <%
                                                                            For Each AttachmentFileEvery IN AttachmentFileList
                                                                                AttachmentFileName=""&MY_dataBase_folderPath&""&AttachmentFileEvery.Name
																				
																				if instr(AttachmentFileName,MY_dataBase_useNow)=0 then Response.Write "<option value="""&AttachmentFileName&""">"&AttachmentFileEvery.Name&"</option>" else Response.Write ""
					
                                                                            Next
                                                                   %>
                                                                      </select>
                                                                </td>
                                                                <td colspan="2" class="ui-element">
                                                                    <div class="btnBox-iframe">
                                                                        <input type="submit" value="删除数据库" onclick="return confirm('确定要删除数据库？')"  class="ui-btn">
                                                                    </div>

                                                                </td>
                                                            </tr>  
                                                     
           
                                                           <!-- +++++++  内容   end +++++++  -->
                                                    
                                                    
                                                        </tbody>
                                                        
                                                        
                                                    </table>

                   
                   
                                    </form>   
                                    


                               
                     </div> 
                     
                     
                    <!-- 独立内容区 -->
                    <div class="singleContentBox">
                        <%response.Write datamsg%>
                    </div>
                
                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->



