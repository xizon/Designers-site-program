<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
' ==================================================
'图片水印
' ==================================================
function imgWaterMark(imgpath)

if MY_checkJpegComponent=0 then

	'路径
	LocalFile=imgpath 
	TargetFile=imgpath
	Dim Jpeg 
	Set Jpeg = Server.CreateObject("Persits.Jpeg") 
	If -2147221005=Err then 
	Response.write "<div align=left>没有这个组件，请安装!</div>" '检查是否安装AspJpeg组件 
	Response.End() 
	End If 
	Jpeg.Open Server.MapPath(LocalFile) '打开图片 
	
	Dim aa 
	aa=Jpeg.Binary '将原始数据赋给aa 
	'=========加文字水印================= 
	Jpeg.Canvas.Font.Color = &Hfffffff '水印文字颜色 
	Jpeg.Canvas.Font.Family = Arial '字体 
	Jpeg.Canvas.Font.Bold = True '是否加粗 
    Jpeg.Canvas.Font.Size = cint(MY_waterSize) '字体大小 
	Jpeg.Canvas.Font.ShadowColor = &H000000 '阴影色彩 
	Jpeg.Canvas.Font.ShadowYOffset = 1 
	Jpeg.Canvas.Font.ShadowXOffset = 1 
	Jpeg.Canvas.Brush.Solid = True 

	
	randomize
	myX=rnd(Jpeg.OriginalWidth)*cint(MY_waterX)
	myY=rnd(Jpeg.OriginalHeight)*cint(MY_waterY)
	
	Jpeg.Canvas.PrintText myX,myY,MY_waterMark '水印位置及文字 
	bb=Jpeg.Binary '将文字水印处理后的值赋给bb，这时，文字水印没有不透明度 
	'============调整文字透明度================ 
	Set MyJpeg = Server.CreateObject("Persits.Jpeg") 
	MyJpeg.OpenBinary aa 
	Set final = Server.CreateObject("Persits.Jpeg") 
	final.OpenBinary bb 
	MyJpeg.DrawImage 0,0, final, 0.5 '透明度 
	cc=MyJpeg.Binary '将最终结果赋值给cc,这时也可以生成目标图片了 
	'response.BinaryWrite cc '将二进输出给浏览器 
	MyJpeg.Save Server.MapPath(TargetFile) 
	set aa=nothing 
	set bb=nothing 
	set cc=nothing 
	Jpeg.close 
	MyJpeg.Close 
	final.Close 
	
end if

end function 


' ==================================================
'生成缩略图
'用法：imgSmallCreate("??.jpg")
'设置参数： 
'    s_OriginalPath:        原图片路径 例:images/image1.gif 
'    s_BuildBasePath:    生成图片的基路径,不论是否以"/"结尾均可 例:images或images/ 
'    n_MaxWidth:            生成图片最大宽度 
'                        如果在前台显示的缩略图是 100*100,这里 n_MaxWidth=100,n_MaxHeight=100. 
'    n_MaxHeight:        生成图片最大高度 
' ==================================================
function imgSmallCreate(imgpath)
if MY_checkJpegComponent=0 then
	Dim sOriginalPath 
	sOriginalPath = imgpath 
	'原图片路径一般上传完毕后获取，或者从数据库获取 
	Dim sReturnInfo, sSmallPath '函数返回信息, 缩略图路径 
	sReturnInfo = BuildSmallPic(sOriginalPath, ""&MY_sitePath&""&MY_uploadSmallImgPath&"/", cint(MY_AbbreviativeImg_list_W), cint(MY_AbbreviativeImg_list_H))
	'Response.Write "返回信息:" & sReturnInfo & "" 
	If InStr(sReturnInfo, "Error_") <= 0 Then 
		sSmallPath = sReturnInfo '返回信息就是  
		'将sSmallPath写入数据库 
		' 
	Else 
	 
		Select Case sReturnInfo 
		Case "Error_01" 
			Response.Write "<div align=left><span color='black'>创建AspJpeg组件失败,没有正确安装注册该组件</span>" & "</div>" 
		Case "Error_02" 
			'Response.Write "<div align=left><b><img src="""&MY_sitePath&"images/error.gif"" />详细错误:</b><span color='black'>"&replace(imgpath,""&MY_secCheckSiteLink&"/","")&"不存在.</span>" & "</div>" 
			Exit Function
		Case "Error_03"     
			'Response.Write "<div align=left><span color='black'>缩略图存盘失败.可能原因:a)缩略图保存基地址不存在;b)对目录没有写权限;磁盘空间不足</span>" & "</div>" 
		Case "Error_Other" 
			Response.Write "<div align=left><span color='black'>未知错误</span>" & "</div>" 
		End Select 
		Response.End 
	End If 
	
	'Response.Write "原文件名："&sOriginalPath&"<br/>" 
	'Response.Write "缩略图文件名："&sSmallPath&"<br/> "
	'Response.Write "原图片：<img src="&sOriginalPath&" border=0><br/><br/>" 
	'Response.Write "缩略图：<img src="&sSmallPath&" border=0>"

end if

end function 


Function BuildSmallPic(s_OriginalPath, s_BuildBasePath, n_MaxWidth, n_MaxHeight) 
		Err.Clear 
		On Error Resume Next 
		 
		'检查组件是否已经注册 
		Dim AspJpeg 
		Set AspJpeg = Server.Createobject("Persits.Jpeg") 
		If Err.Number <> 0 Then 
			Err.Clear 
			BuildSmallPic = "Error_01" 
			Exit Function 
		End If 
		'检查原图片是否存在 
		Dim s_MapOriginalPath 
		s_MapOriginalPath = Server.MapPath(s_OriginalPath) 
		AspJpeg.Open s_MapOriginalPath '打开原图片 
		If Err.Number <> 0 Then 
			Err.Clear 
			BuildSmallPic = "Error_02" 
			Exit Function 
		End If 
		'按比例取得缩略图宽度和高度 
		Dim n_OriginalWidth, n_OriginalHeight '原图片宽度、高度 
		Dim n_BuildWidth, n_BuildHeight '缩略图宽度、高度 
		Dim div1, div2 
		Dim n1, n2 
		n_OriginalWidth = AspJpeg.Width 
		n_OriginalHeight = AspJpeg.Height 
		div1 = n_OriginalWidth / n_OriginalHeight 
		div2 = n_OriginalHeight / n_OriginalWidth 
		n1 = 0 
		n2 = 0 
	
	
	
	
		If n_OriginalWidth > n_MaxWidth Then 
			n1 = n_OriginalWidth / n_MaxWidth 
		Else 
			n_BuildWidth = n_OriginalWidth 
		End If 
		If n_OriginalHeight > n_MaxHeight Then 
			n2 = n_OriginalHeight / n_MaxHeight 
		Else 
			n_BuildHeight = n_OriginalHeight 
		End If 
	
	if MY_abbreviativeImgSize = 1 then	
	
	'自动缩放生成
		If n1 <> 0 Or n2 <> 0 Then 
			If n1 > n2 Then 
				n_BuildWidth = n_MaxWidth 
				n_BuildHeight = n_MaxWidth * div2 
			Else 
				n_BuildWidth = n_MaxHeight * div1 
				n_BuildHeight = n_MaxHeight 
			End If 
		End If 
		
	elseif MY_abbreviativeImgSize = 0 then	
	
	'指定缩放生成(固定宽度)
	
				n_BuildWidth = n_MaxWidth 
				n_BuildHeight = n_MaxWidth * div2 
	
	 
		AspJpeg.Width = n_BuildWidth 
		AspJpeg.Height = n_BuildHeight 
		
	end if
	
	
	
	'--将缩略图存盘开始-- 
		Dim pos, s_OriginalFileName, s_OriginalFileExt '位置、原文件名、原文件扩展名 
		pos = InStrRev(s_OriginalPath, "/") + 1 
		s_OriginalFileName = Mid(s_OriginalPath, pos) 
		pos = InStrRev(s_OriginalFileName, ".") 
		s_OriginalFileExt = Mid(s_OriginalFileName, pos) 
		Dim s_MapBuildBasePath, s_MapBuildPath, s_BuildFileName '缩略图绝对路径、缩略图文件名 
		Dim s_NameFlag '小图片文件名结尾标识 例: 如果大图片文件名是"image1.gif",头部标识是"s_",那么小图片文件名就是"s_image.gif" 
		If Right(s_BuildBasePath, 1) <> "/" Then s_BuildBasePath = s_BuildBasePath & "/" 
		s_MapBuildBasePath = Server.MapPath(s_BuildBasePath) 
		s_NameFlag = "s_" '可以自定义,只要能区别大小图片即可 
		s_BuildFileName = s_NameFlag & Replace(s_OriginalFileName, s_OriginalFileExt, "") & s_OriginalFileExt 
		s_MapBuildPath = s_MapBuildBasePath & "\" & s_BuildFileName 
		
		AspJpeg.Save s_MapBuildPath '保存 
	
	
	
		If Err.Number <> 0 Then 
			Err.Clear 
			BuildSmallPic = "Error_03" 
			Exit Function 
		End If 
		'--将缩略图存盘结束-- 
		'注销实例 
		Set AspJpeg = Nothing 
		If Err.Number <> 0 Then 
			BuildSmallPic = "Error_Other" 
			Err.Clear 
		End If 
		BuildSmallPic = s_BuildBasePath & s_BuildFileName 
End Function 



' ==================================================
function imgSmallCreate_s(imgpath)
if MY_checkJpegComponent=0 then
	Dim sOriginalPath 
	sOriginalPath = imgpath 
	'原图片路径一般上传完毕后获取，或者从数据库获取 
	Dim sReturnInfo, sSmallPath '函数返回信息, 缩略图路径 
	sReturnInfo = BuildSmallPic_s(sOriginalPath, ""&MY_sitePath&""&MY_uploadSmallImgPath&"/", cint(MY_AbbreviativeImg_small_W), cint(MY_AbbreviativeImg_small_H))
	'Response.Write "返回信息:" & sReturnInfo & "" 
	If InStr(sReturnInfo, "Error_") <= 0 Then 
		sSmallPath = sReturnInfo '返回信息就是  
		'将sSmallPath写入数据库 
		' 
	Else 
	 
		Select Case sReturnInfo 
		Case "Error_01" 
			Response.Write "<div align=left><span color='black'>创建AspJpeg组件失败,没有正确安装注册该组件</span>" & "</div>" 
		Case "Error_02" 
			'Response.Write "<div align=left><b><img src="""&MY_sitePath&"images/error.gif"" />详细错误:</b><span color='black'>"&replace(imgpath,""&MY_secCheckSiteLink&"/","")&"不存在.</span>" & "</div>" 
			Exit Function
		Case "Error_03"     
			'Response.Write "<div align=left><span color='black'>缩略图存盘失败.可能原因:a)缩略图保存基地址不存在;b)对目录没有写权限;磁盘空间不足</span>" & "</div>" 
		Case "Error_Other" 
			Response.Write "<div align=left><span color='black'>未知错误</span>" & "</div>" 
		End Select 
		Response.End 
	End If 
	
	'Response.Write "原文件名："&sOriginalPath&"<br/>" 
	'Response.Write "缩略图文件名："&sSmallPath&"<br/> "
	'Response.Write "原图片：<img src="&sOriginalPath&" border=0><br/><br/>" 
	'Response.Write "缩略图：<img src="&sSmallPath&" border=0>"

end if
end function 


Function BuildSmallPic_s(s_OriginalPath, s_BuildBasePath, n_MaxWidth, n_MaxHeight) 
		Err.Clear 
		On Error Resume Next 
		 
		'检查组件是否已经注册 
		Dim AspJpeg 
		Set AspJpeg = Server.Createobject("Persits.Jpeg") 
		If Err.Number <> 0 Then 
			Err.Clear 
			BuildSmallPic_s = "Error_01" 
			Exit Function 
		End If 
		'检查原图片是否存在 
		Dim s_MapOriginalPath 
		s_MapOriginalPath = Server.MapPath(s_OriginalPath) 
		AspJpeg.Open s_MapOriginalPath '打开原图片 
		If Err.Number <> 0 Then 
			Err.Clear 
			BuildSmallPic_s = "Error_02" 
			Exit Function 
		End If 
		'按比例取得缩略图宽度和高度 
		Dim n_OriginalWidth, n_OriginalHeight '原图片宽度、高度 
		Dim n_BuildWidth, n_BuildHeight '缩略图宽度、高度 
		Dim div1, div2 
		Dim n1, n2 
		n_OriginalWidth = AspJpeg.Width 
		n_OriginalHeight = AspJpeg.Height 
		div1 = n_OriginalWidth / n_OriginalHeight 
		div2 = n_OriginalHeight / n_OriginalWidth 
		n1 = 0 
		n2 = 0 
		
		
		
		If n_OriginalWidth > n_MaxWidth Then 
			n1 = n_OriginalWidth / n_MaxWidth 
		Else 
			n_BuildWidth = n_OriginalWidth 
		End If 
		If n_OriginalHeight > n_MaxHeight Then 
			n2 = n_OriginalHeight / n_MaxHeight 
		Else 
			n_BuildHeight = n_OriginalHeight 
		End If 
	
	if MY_abbreviativeImgSize = 1 then	
	
	'自动缩放生成
		If n1 <> 0 Or n2 <> 0 Then 
			If n1 > n2 Then 
				n_BuildWidth = n_MaxWidth 
				n_BuildHeight = n_MaxWidth * div2 
			Else 
				n_BuildWidth = n_MaxHeight * div1 
				n_BuildHeight = n_MaxHeight 
			End If 
		End If 
		
	elseif MY_abbreviativeImgSize = 0 then	
	
	'指定缩放生成(固定宽度)
	
				n_BuildWidth = n_MaxWidth 
				n_BuildHeight = n_MaxWidth * div2 
	
	 
		AspJpeg.Width = n_BuildWidth 
		AspJpeg.Height = n_BuildHeight 
		
	end if
	
	
	'--将缩略图存盘开始-- 
		Dim pos, s_OriginalFileName, s_OriginalFileExt '位置、原文件名、原文件扩展名 
		pos = InStrRev(s_OriginalPath, "/") + 1 
		s_OriginalFileName = Mid(s_OriginalPath, pos) 
		pos = InStrRev(s_OriginalFileName, ".") 
		s_OriginalFileExt = Mid(s_OriginalFileName, pos) 
		Dim s_MapBuildBasePath, s_MapBuildPath, s_BuildFileName '缩略图绝对路径、缩略图文件名 
		Dim s_NameFlag '小图片文件名结尾标识 例: 如果大图片文件名是"image1.gif",头部标识是"s_",那么小图片文件名就是"s_image.gif" 
		If Right(s_BuildBasePath, 1) <> "/" Then s_BuildBasePath = s_BuildBasePath & "/" 
		s_MapBuildBasePath = Server.MapPath(s_BuildBasePath) 
		s_NameFlag = "ss_" '可以自定义,只要能区别大小图片即可 
		s_BuildFileName = s_NameFlag & Replace(s_OriginalFileName, s_OriginalFileExt, "") & s_OriginalFileExt 
		s_MapBuildPath = s_MapBuildBasePath & "\" & s_BuildFileName 
		
		AspJpeg.Save s_MapBuildPath '保存 
	
	
	
		If Err.Number <> 0 Then 
			Err.Clear 
			BuildSmallPic_s = "Error_03" 
			Exit Function 
		End If 
		'--将缩略图存盘结束-- 
		'注销实例 
		Set AspJpeg = Nothing 
		If Err.Number <> 0 Then 
			BuildSmallPic_s = "Error_Other" 
			Err.Clear 
		End If 
		BuildSmallPic_s = s_BuildBasePath & s_BuildFileName 
	End Function 
	
	
	' ==================================================
function imgSmallCreate_subject(imgpath)
if MY_checkJpegComponent=0 then
	Dim sOriginalPath 
	sOriginalPath = imgpath 
	'原图片路径一般上传完毕后获取，或者从数据库获取 
	Dim sReturnInfo, sSmallPath '函数返回信息, 缩略图路径 
	sReturnInfo = BuildSmallPic_subject(sOriginalPath, ""&MY_sitePath&""&MY_uploadSmallImgPath&"/", cint(MY_AbbreviativeImg_listSecond_W), cint(MY_AbbreviativeImg_listSecond_H))
	'Response.Write "返回信息:" & sReturnInfo & "" 
	If InStr(sReturnInfo, "Error_") <= 0 Then 
		sSmallPath = sReturnInfo '返回信息就是  
		'将sSmallPath写入数据库 
		' 
	Else 
	
		Select Case sReturnInfo 
		Case "Error_01" 
			Response.Write "<div align=left><span color='black'>创建AspJpeg组件失败,没有正确安装注册该组件</span>" & "</div>" 
		Case "Error_02" 
			'Response.Write "<div align=left><b><img src="""&MY_sitePath&"images/error.gif"" />详细错误:</b><span color='black'>"&replace(imgpath,""&MY_secCheckSiteLink&"/","")&"不存在.</span>" & "</div>" 
			Exit Function
		Case "Error_03"     
			'Response.Write "<div align=left><span color='black'>缩略图存盘失败.可能原因:a)缩略图保存基地址不存在;b)对目录没有写权限;磁盘空间不足</span>" & "</div>" 
		Case "Error_Other" 
			Response.Write "<div align=left><span color='black'>未知错误</span>" & "</div>" 
		End Select 
		Response.End 
	End If 
	
	'Response.Write "原文件名："&sOriginalPath&"<br/>" 
	'Response.Write "缩略图文件名："&sSmallPath&"<br/> "
	'Response.Write "原图片：<img src="&sOriginalPath&" border=0><br/><br/>" 
	'Response.Write "缩略图：<img src="&sSmallPath&" border=0>"

end if
end function 


Function BuildSmallPic_subject(s_OriginalPath, s_BuildBasePath, n_MaxWidth, n_MaxHeight) 
		Err.Clear 
		On Error Resume Next 
		 
		'检查组件是否已经注册 
		Dim AspJpeg 
		Set AspJpeg = Server.Createobject("Persits.Jpeg") 
		If Err.Number <> 0 Then 
			Err.Clear 
			BuildSmallPic_subject = "Error_01" 
			Exit Function 
		End If 
		'检查原图片是否存在 
		Dim s_MapOriginalPath 
		s_MapOriginalPath = Server.MapPath(s_OriginalPath) 
		AspJpeg.Open s_MapOriginalPath '打开原图片 
		If Err.Number <> 0 Then 
			Err.Clear 
			BuildSmallPic_subject = "Error_02" 
			Exit Function 
		End If 
		'按比例取得缩略图宽度和高度 
		Dim n_OriginalWidth, n_OriginalHeight '原图片宽度、高度 
		Dim n_BuildWidth, n_BuildHeight '缩略图宽度、高度 
		Dim div1, div2 
		Dim n1, n2 
		n_OriginalWidth = AspJpeg.Width 
		n_OriginalHeight = AspJpeg.Height 
		div1 = n_OriginalWidth / n_OriginalHeight 
		div2 = n_OriginalHeight / n_OriginalWidth 
		n1 = 0 
		n2 = 0 
		
		
		
		If n_OriginalWidth > n_MaxWidth Then 
			n1 = n_OriginalWidth / n_MaxWidth 
		Else 
			n_BuildWidth = n_OriginalWidth 
		End If 
		If n_OriginalHeight > n_MaxHeight Then 
			n2 = n_OriginalHeight / n_MaxHeight 
		Else 
			n_BuildHeight = n_OriginalHeight 
		End If 
	
	if MY_abbreviativeImgSize = 1 then	
	
	'自动缩放生成
		If n1 <> 0 Or n2 <> 0 Then 
			If n1 > n2 Then 
				n_BuildWidth = n_MaxWidth 
				n_BuildHeight = n_MaxWidth * div2 
			Else 
				n_BuildWidth = n_MaxHeight * div1 
				n_BuildHeight = n_MaxHeight 
			End If 
		End If 
		
	elseif MY_abbreviativeImgSize = 0 then	
	
	'指定缩放生成(固定宽度)
	
				n_BuildWidth = n_MaxWidth 
				n_BuildHeight = n_MaxWidth * div2 
	
	 
		AspJpeg.Width = n_BuildWidth 
		AspJpeg.Height = n_BuildHeight 
		
	end if
	
	
	
	'--将缩略图存盘开始-- 
		Dim pos, s_OriginalFileName, s_OriginalFileExt '位置、原文件名、原文件扩展名 
		pos = InStrRev(s_OriginalPath, "/") + 1 
		s_OriginalFileName = Mid(s_OriginalPath, pos) 
		pos = InStrRev(s_OriginalFileName, ".") 
		s_OriginalFileExt = Mid(s_OriginalFileName, pos) 
		Dim s_MapBuildBasePath, s_MapBuildPath, s_BuildFileName '缩略图绝对路径、缩略图文件名 
		Dim s_NameFlag '小图片文件名结尾标识 例: 如果大图片文件名是"image1.gif",头部标识是"s_",那么小图片文件名就是"s_image.gif" 
		If Right(s_BuildBasePath, 1) <> "/" Then s_BuildBasePath = s_BuildBasePath & "/" 
		s_MapBuildBasePath = Server.MapPath(s_BuildBasePath) 
		s_NameFlag = "sub_" '可以自定义,只要能区别大小图片即可 
		s_BuildFileName = s_NameFlag & Replace(s_OriginalFileName, s_OriginalFileExt, "") & s_OriginalFileExt 
		s_MapBuildPath = s_MapBuildBasePath & "\" & s_BuildFileName 
		
		AspJpeg.Save s_MapBuildPath '保存 
	
	
	
		If Err.Number <> 0 Then 
			Err.Clear 
			BuildSmallPic_subject = "Error_03" 
			Exit Function 
		End If 
		'--将缩略图存盘结束-- 
		'注销实例 
		Set AspJpeg = Nothing 
		If Err.Number <> 0 Then 
			BuildSmallPic_subject = "Error_Other" 
			Err.Clear 
		End If 
		BuildSmallPic_subject = s_BuildBasePath & s_BuildFileName 
End Function 

%>
