<%

'=========================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=========================================


' ============================================
'添加更新模板xml配置文件时的本地插件
' ============================================

Function addLocalPlugins(appID,appName,appCode,appURL)

	set rsChk =server.createobject("adodb.recordset")
	sqlChk="select * from [App] where appID='"&appID&"'"
	rsChk.open sqlChk,db,1,1 
	
	'存在性验证
	if not rsChk.EOF then
		'已存在

	else
		'执行添加操作
		
		GetNum_A = db.execute("select count(1) from [App]")(0)

		set rs =server.createobject("adodb.recordset")
		sql="select * from [App]"
		rs.open sql,db,1,2
		rs.addnew
		
		rs("appID")=appID
		rs("appName")=appName
		rs("appCode")=appCode
		rs("appURL")=appURL
		rs("index")=GetNum_A + 1
		

		rs.update
		rs.close
		set rs=nothing
		
		
	end if

	
	rsChk.close
	set rsChk=nothing

End Function

' ==================================================
'取出分页总数
' ==================================================
Function getDataPageTotal()
	 
	titleLimitLen_art_ListNum = getTempLablePara_siteConfig("内容输出列表数量(文章列表)")
	titleLimitLen_works_ListNum = getTempLablePara_siteConfig("内容输出列表数量(案例作品列表)")
	titleLimitLen_msg_ListNum = getTempLablePara_siteConfig("内容输出列表数量(留言)")
	titleLimitLen_mood_ListNum = getTempLablePara_siteConfig("内容输出列表数量(微博)")
	 
	'文章
	sql="select * from [article]"
	set rs=server.createObject("ADODB.Recordset")
	rs.open sql,db,1,1
	
	num1=titleLimitLen_art_ListNum'每页显示记录数
	rs.pagesize=num1
	totalpage1=rs.pagecount
	rs.close
	set rs=nothing
	
	'案例
	sql="select * from [SuccessWork] where private=0"
	set rs=server.createObject("ADODB.Recordset")
	rs.open sql,db,1,1
	
	num2=titleLimitLen_works_ListNum'每页显示记录数
	rs.pagesize=num2
	totalpage2=rs.pagecount
	rs.close
	set rs=nothing
	
	'留言
	sql="select * from [message]"
	set rs=server.createObject("ADODB.Recordset")
	rs.open sql,db,1,1
	
	num3=titleLimitLen_msg_ListNum
	rs.pagesize=num3
	totalpage3=rs.pagecount
	rs.close
	set rs=nothing
	
	'微博
	sql="select * from [mood]"
	set rs=server.createObject("ADODB.Recordset")
	rs.open sql,db,1,1
	
	num4=titleLimitLen_mood_ListNum
	rs.pagesize=num4
	totalpage4=rs.pagecount
	rs.close
	set rs=nothing
	
	pageArr=""&totalpage1&","&totalpage2&","&totalpage3&","&totalpage4&""
    getDataPageTotal = getArrMax(pageArr)

End Function

' ==================================================
'邮件发送函数
' ==================================================
Function SendEmail(frommail,fromname,tomail,subject,content,mailserver,ServerPass)
'SendEmail(发送人Email,发送者,收件人Email,邮件标题,邮件内容,smtp服务器地址,邮件服务器验证密码)
    On Error Resume Next
    Set JMail = server.CreateObject("JMail.Message") '创建Jmail对象
    JMail.Logging = True '是否使用日志
    JMail.charset = "gb2312" '邮件编码，缺省为"US-ASCII"，最好加上，免得出现乱码
    JMail.ContentType = "text/html" '邮件的格式，text/plain为纯文本型
    JMail.From = frommail '发送邮件地址，最好写邮件服务器验证的邮箱账号
    JMail.FromName = fromname '发送者，可以随便填写
    JMail.AddRecipient tomail '要发送到的邮件地址
    JMail.Subject = subject '邮件的标题
    JMail.Priority = 3 '邮件的优先级
    JMail.Body = content '邮件的内容

    JMail.MailServerUserName = frommail '邮件服务器验证账号
    JMail.MailServerPassword = ServerPass '邮件服务器验证密码
    JMail.Send(mailserver) '通过邮件服务器smtp发送邮件

    Set JMail = Nothing '关闭对象

    If Err.Number<>0 Then '错误处理
        SendEmail = False
    Else
        SendEmail = True
    End If
End Function

' ==================================================
'数据库添加修改操作
' ==================================================
Function DBQuest(table,DBArray,Action)
	 dim AddCount,TempDB,i,v
	 if Action<>"insert" or Action<>"update" then Action="insert"
	 if Action="insert" then v=2 else v=3
	 if not IsArray(DBArray) then
	   DBQuest=-1
	   exit Function
	 else
	   Set TempDB=Server.CreateObject("ADODB.RecordSet")
	   On Error Resume Next
	   TempDB.Open table,Conn,1,v
	   if err then
		DBQuest=-2
		exit Function
	   end if
	   if Action="insert" then TempDB.addNew
	   AddCount=UBound(DBArray,1)
	   for i=0 to AddCount
		TempDB(DBArray(i)(0))=DBArray(i)(1)
	   next
	   TempDB.update
	   TempDB.close
	   set TempDB=nothing
	   DBQuest=0
	 end if
End Function


%>
