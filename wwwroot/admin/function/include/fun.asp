<%
'=========================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=========================================

' ==================================================
'判断是否存在字段
' ==================================================
Function apiStrNull(str)
    if len(str) >= 1 then
	    sn = 1
	else
	    sn = 0
	end if
	apiStrNull = sn

End Function

' ==================================================
'取出数组最大值
' ==================================================
Function getArrMax(str)
	arr=split(str,",")
	max=arr(0)
	for ii=0 to ubound(arr)
	    if cint(arr(ii))>cint(max) then max=arr(ii)
	next
	getArrMax=max
End Function

' ==================================================
'查询字符串数
' ==================================================
Function getCount(s,subs)   
	count = 0   
	while(instr(s,subs)>0)   
		count = count + 1   
		s = right(s,len(s) - instr(s,subs))   
	Wend   
	getCount = count   
End Function   


' ==================================================
'去掉最右边的某字符
' ==================================================
Function clearRightString(stro,str)
    dim strNum
	strNum = len(str)
	if right(stro,strNum)=str then  stro = left(stro,len(stro)-strNum)
	clearRightString = stro
End Function


' ============================================
'代码转化成js输出函数
' ============================================
Function jsConversWrite(Str,ID)
	If Not IsNull(Str) Then
	
	     'URL非法修正
		for kl = 1 to 100
			Str = replace(Str,"width="""&kl&"%""","style=""width:auto""")
		next
		jsConversWrite = "$(""#"&ID&""").html(decodeURIComponent("&CHR(39)&""&replace(replace(replace(Str,CHR(13),""),CHR(10),""),""&CHR(39)&"","\"&CHR(39)&"")&""&CHR(39)&"));"
	End If
End Function

' ============================================
'获得定制版信息
' ============================================
Function getDSVersion()
	'///////////////如果是customize 定制版，则停止共享版的云服务
	set xmlDom2D = server.CreateObject("MSXML2.DOMDocument")
	xmlDom2D.async = false
	setXmlPath2D = ""&MY_sitePath&"template/tempConfig.xml"
	upCloudActionFilepath = ""&MY_sitePath&"admin/_app.asp"
	
	
	if not xmlDom2D.Load(Server.MapPath(setXmlPath2D))  then 
		'response.write("Load wrong!")
	else
	
	
		'读取模板信息
		set tempInfo2D = xmlDom2D.getElementsByTagName("DSsetting")  
		'类型 
		tempVersion = tempInfo2D(0).selectSingleNode("version").Text
		if tempVersion = "customize" then 
		    
		    getDSVersion = 1
		else
		    getDSVersion = 0
			
		end if
	
	
	
	end if
End Function


' ============================================
'生成对应ID
' ============================================
Function createBase64ID(ByVal asContents) 

    Const sBASE_64_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789" 
	Dim lnPosition 
	Dim lsResult 
	Dim Char1 
	Dim Char2 
	Dim Char3 
	Dim Char4 
	Dim Byte1 
	Dim Byte2 
	Dim Byte3 
	Dim SaveBits1 
	Dim SaveBits2 
	Dim lsGroupBinary 
	Dim lsGroup64 
	
	If Len(asContents) Mod 3 > 0 Then asContents = asContents & String(3 - (Len(asContents) Mod 3), " ") 
	lsResult = "" 
	
	For lnPosition = 1 To Len(asContents) Step 3 
	lsGroup64 = "" 
	lsGroupBinary = Mid(asContents, lnPosition, 3) 
	
	Byte1 = Asc(Mid(lsGroupBinary, 1, 1)): SaveBits1 = Byte1 And 3 
	Byte2 = Asc(Mid(lsGroupBinary, 2, 1)): SaveBits2 = Byte2 And 15 
	Byte3 = Asc(Mid(lsGroupBinary, 3, 1)) 
	
	Char1 = Mid(sBASE_64_CHARACTERS, ((Byte1 And 252) \ 4) + 1, 1) 
	Char2 = Mid(sBASE_64_CHARACTERS, (((Byte2 And 240) \ 16) Or (SaveBits1 * 16) And &HFF) + 1, 1) 
	Char3 = Mid(sBASE_64_CHARACTERS, (((Byte3 And 192) \ 64) Or (SaveBits2 * 4) And &HFF) + 1, 1) 
	Char4 = Mid(sBASE_64_CHARACTERS, (Byte3 And 63) + 1, 1) 
	lsGroup64 = Char1 & Char2 & Char3 & Char4 
	
	lsResult = lsResult + lsGroup64 
	Next 
	
	createBase64ID = mid(lsResult,1,12)
	
	
End Function 


' ============================================
'移位
' ============================================
Function UnEncode(Code)
But = CInt(30)
Code = Replace(Replace(Code, "％", Chr(37)), "＂", Chr(34)) 
    For i = 1 To Len(Code)
        If Mid(Code, i, 1) <> "╋" Then
            If Asc(Mid(Code, i, 1)) < 32 Or Asc(Mid(Code, i, 1)) > 126 Then
               TempCode = TempCode & Chr(Asc(Mid(Code, i, 1)))
            Else
               PkCode = Asc(Mid(Code, i, 1)) - But
               If PkCode > 126 Then
                  PkCode = PkCode -95
               ElseIf PkCode < 32 Then
                  PkCode = PkCode + 95
               End If
               TempCode = TempCode & Chr(PkCode)
            End If
        Else
            TempCode = TempCode & vbCrLf
        End If
    Next
    UnEncode = TempCode
End Function


' ============================================
'防止外部提交
'作用：提交表单只能用本站程序，不能借用其他外部程序
'使用方法：if not ChkPost() then
'          response.Write "<script>alert('请注意,您被禁止此操作 !');〈/script>"
'          response.end
'          end if

' ============================================
Function ChkPost() 
  dim server_v1,server_v2
  chkpost=false
  server_v1=Cstr(Request.ServerVariables("HTTP_REFERER"))
'读取上一页地址,并赋值给server_v1(如:http://localhost/htdocs/aa.asp)
  server_v2=Cstr(Request.ServerVariables("SERVER_NAME"))
'读取当前页URL中的主机名,并赋值给server_v2(如:localhost)
  If Mid(server_v1,8,Len(server_v2))<>server_v2 then
'取server_v1第8个字符后server_v2那么长的字符串和server_v2比较是不是一样
    chkpost=False
  else

   chkpost=True
  end If
End Function 

Function chk_transfer()
    if not ChkPost() then
    getshow "请注意,您被禁止此操作 !",1,""
    end if
End Function


'================================================
'函数名：letterTrans
'作  用：英文大小写忽略
'================================================
Function letterTrans(ByVal keywords)

        keywords=replace(keywords,"A","a") 
		keywords=replace(keywords,"B","b")
		keywords=replace(keywords,"C","c") 
		keywords=replace(keywords,"D","d") 
		keywords=replace(keywords,"E","e") 
		keywords=replace(keywords,"F","f") 
		keywords=replace(keywords,"G","g") 
		keywords=replace(keywords,"H","h") 
		keywords=replace(keywords,"I","i") 
		keywords=replace(keywords,"J","j") 
		keywords=replace(keywords,"K","k") 
		keywords=replace(keywords,"L","l") 
		keywords=replace(keywords,"M","m") 
		keywords=replace(keywords,"N","n") 
		keywords=replace(keywords,"O","o") 
		keywords=replace(keywords,"P","p") 
		keywords=replace(keywords,"Q","q") 
		keywords=replace(keywords,"R","r") 
		keywords=replace(keywords,"S","s") 
		keywords=replace(keywords,"T","t") 
		keywords=replace(keywords,"U","u") 
		keywords=replace(keywords,"V","v") 
		keywords=replace(keywords,"W","w") 
		keywords=replace(keywords,"X","x") 
		keywords=replace(keywords,"Y","y") 
		keywords=replace(keywords,"Z","z") 	
		
		letterTrans = keywords
		
End Function

' ============================================
'防XSS注入函数-跨站脚本(妖妖改进版，防止转义执行)
'用法：Checkxss(request.QueryString("？？"))
' ============================================
Function Checkxss(byVal ChkStr)
    Dim Str
    Str = ChkStr
    If IsNull(Str) Then
        CheckStr = ""
        Exit Function
    End If
    Str = Replace(Str, "&", "&amp;")
    Str = Replace(Str, "'", "&acute;")
    Str = Replace(Str, """", "&quot;")
    Str = Replace(Str, "<", "&lt;")
    Str = Replace(Str, ">", "&gt;")
    Str = Replace(Str, "/", "&#47;")
    Str = Replace(Str, "*", "&#42;")
	Str = Replace(Str, ")", "&#41;")
	Str = Replace(Str, "(", "&#40;")
	Str = Replace(Str, "%", "&#37;")
	Str = Replace(Str, "|", "&#5;")
    Str = Replace(Str, "/", "&#47;")
    Str = Replace(Str, "*", "&#42;")
    Str = Replace(Str, "\", "&#92;")
    Dim re
    Set re = New RegExp
    re.IgnoreCase = True
    re.Global = True
    re.Pattern = "(w)(here)"
    Str = re.Replace(Str, "$1h&#101;re")
    re.Pattern = "(s)(elect)"
    Str = re.Replace(Str, "$1el&#101;ct")
    re.Pattern = "(i)(nsert)"
    Str = re.Replace(Str, "$1ns&#101;rt")
    re.Pattern = "(c)(reate)"
    Str = re.Replace(Str, "$1r&#101;ate")
    re.Pattern = "(d)(rop)"
    Str = re.Replace(Str, "$1ro&#112;")
    re.Pattern = "(a)(lter)"
    Str = re.Replace(Str, "$1lt&#101;r")
    re.Pattern = "(d)(elete)"
    Str = re.Replace(Str, "$1el&#101;te")
    re.Pattern = "(u)(pdate)"
    Str = re.Replace(Str, "$1p&#100;ate")
    re.Pattern = "(s)(or)"
    Str = re.Replace(Str, "$1")
        re.Pattern = "( )"
    Str = re.Replace(Str, "$1")
        '----------------------------------
        re.Pattern = "(java)(script)"
    Str = re.Replace(Str, "$1scri&#112;t")
        re.Pattern = "(j)(script)"
    Str = re.Replace(Str, "$1scri&#112;t")
        re.Pattern = "(vb)(script)"
    Str = re.Replace(Str, "$1scri&#112;t")
        '----------------------------------
        If Instr(Str, "expression") > 0 Then
                Str = Replace(Str, "expression", "e&#173;xpression", 1, -1, 0) '防止xss注入
        End If
    Set re = Nothing
    Checkxss = Str
End Function

' ============================================
'验证码
' ============================================
Function TestCaptcha(byval valSession, byval valCaptcha)
	dim tmpSession
	valSession = Trim(valSession)
	valCaptcha = Trim(valCaptcha)
	if (valSession = vbNullString) or (valCaptcha = vbNullString) then
		TestCaptcha = false
	else
		tmpSession = valSession
		valSession = Trim(Session(valSession))
		Session(tmpSession) = vbNullString
		if valSession = vbNullString then
			TestCaptcha = false
		else
			valCaptcha = Replace(valCaptcha,"i","I")
			if StrComp(valSession,valCaptcha,1) = 0 then
				TestCaptcha = true
			else
				TestCaptcha = false
			end if
		end if		
	end if
End Function

' ============================================
'邮箱格式检测
' ============================================
Function IsValidEmail(email)
 IsValidEmail=true
 Dim Rep
 Set Rep = new RegExp
 rep.pattern="^([\.a-zA-Z0-9_-]){2,10}@([a-zA-Z0-9_-]){2,10}(\.([a-zA-Z0-9]){2,}){1,4}$"
 pass=rep.Test(email)
 Set Rep=Nothing
 If not pass Then IsValidEmail=false
End function



' ============================================
'字体随机大小
' ============================================
Function FontRandom_Size(str)

	randomize
	thisRanNum=int(str*rnd)
	
	select case thisRanNum
	
	 case "0"
	 thisFontSize = 9
	
	 case "1"
	 thisFontSize = 10
	
	 case "2"
	 thisFontSize = 11
	
	 case "3"
	 thisFontSize = 12
	
	 case "4"
	 thisFontSize = 13
	
	 case "5"
	 thisFontSize = 14
	
	 case "6"
	 thisFontSize = 15
	
	 case "7"
	 thisFontSize = 16
	
	 case "8"
	 thisFontSize = 17
	
	 case "9"
	 thisFontSize = 18
	
	 
	 case else
	 thisFontSize = 12
	
	
	end select
	
	FontRandom_Size = thisFontSize
	
End Function


' ============================================
'字体随机粗细
' ============================================
Function FontRandom_Weight(str)
    
	randomize
	thisRanNum=int(str*rnd)

	select case thisRanNum
	
	 case "0"
	 thisFontWeight = 100
	 case "1"
	 thisFontWeight = 200
	 case "2"
	 thisFontWeight = 300
	 case "3"
	 thisFontWeight = 400
	 case "4"
	 thisFontWeight = 500
	 case "5"
	 thisFontWeight = 600
	 case "6"
	 thisFontWeight = 700
	 case "7"
	 thisFontWeight = 800
	 case "8"
	 thisFontWeight = 900
	 case "9"
	 thisFontWeight = 300
	 
	 case else
	 thisFontWeight = 300
	
	end select
	
	FontRandom_Weight = thisFontWeight
	
End Function



' ============================================
'日期转换
' ============================================
Function dateMonthGoEnglish(str)
   
	select case str
	
	 case "1"
	 thisMonth = "JAN"
	 case "2"
	 thisMonth = "FEB"
	 case "3"
	 thisMonth = "MAR"
	 case "4"
	 thisMonth = "APR"
	 case "5"
	 thisMonth = "MAY"
	 case "6"
	 thisMonth = "JUN"
	 case "7"
	 thisMonth = "JUL"
	 case "8"
	 thisMonth = "AUG"
	 case "9"
	 thisMonth = "SEP"
	 case "10"
	 thisMonth = "OCT"
	 case "11"
	 thisMonth = "NOV"
	 case "12"
	 thisMonth = "DEC"
	 	 
	
	end select
	
	dateMonthGoEnglish = thisMonth
	
End Function

' ==================================================
'转换HTML代码--写入数据库的内容
' ==================================================
Function HTMLEncode(Str)
	If Not IsNull(Str) Then
		Str = Replace(Str, ">", "&gt;")
		Str = Replace(Str, "<", "&lt;")		
	    Str = Replace(Str, CHR(9), "&#160;&#160;&#160;&#160;")'Chr(13)回车，Chr(10)换行
	    Str = Replace(Str, CHR(32), "&nbsp;")'Chr(32)一个空格'Chr(9)在记事本中按一个Tab键的效果
	    Str = Replace(Str, CHR(39), "&#39;") '单引号
    	Str = Replace(Str, CHR(34), "&quot;")'双引号
		Str = Replace(Str, CHR(13), "")
	    Str = Replace(Str, CHR(10), "<br/>")
		Str = Replace(Str, "%", "％")
		Str = Replace(Str, "·", "●")

		HTMLEncode = Str
	End If
End Function


' ==================================================
'反转换HTML代码--输出数据库的内容到页面
' ==================================================
Function HTMLDecode(Str) 
	If Not IsNull(Str) Then
		Str = Replace(Str, "&gt;", ">")
		Str = Replace(Str, "&lt;", "<")
		Str = Replace(Str, "&#160;&#160;&#160;&#160;", CHR(9))
	    Str = Replace(Str, "&nbsp;", CHR(32))
		Str = Replace(Str, "&#39;", CHR(39))
		Str = Replace(Str, "&quot;", CHR(34))
		Str = Replace(Str, "&#47;", "/")
		Str = Replace(Str, "", CHR(13))
		Str = Replace(Str, "<br/>", chr(13)&chr(10))
		Str = Replace(Str, "％", "%")
		Str = Replace(Str, "●", "·")
		
		HTMLDecode = Str
	End If
End Function


' ==================================================
'字符切割
'用法：gotTopic(变量名,字节数)
' ==================================================
Function gotTopic(str,strlen)
	dim l,t,c, i
	l=len(str)
	t=0
	for i=1 to l
	c=Abs(Asc(Mid(str,i,1)))
	if c>255 then
	t=t+2
	else
	t=t+1
	end if
	if t>=strlen then
	gotTopic=left(str,i)&"..."
	exit for
	else
	gotTopic=str
	end if
	next
End Function


' ==================================================
'组件可用性判断
' ==================================================
Function CheckObjInstalled(strClassString)
	On Error Resume Next
	Dim Temp
	Err = 0
	Dim TmpObj
	Set TmpObj = Server.CreateObject(strClassString)
	Temp = Err
	IF Temp = 0 OR Temp = -2147221477 Then
		CheckObjInstalled=true
	ElseIF Temp = 1 OR Temp = -2147221005 Then
		CheckObjInstalled=false
	End IF
	Err.Clear
	Set TmpObj = Nothing
	Err = 0
End Function


' ============================================
'函数功能：模板配置解析
' ==================================================
Function tempInfoParse(Code)
But = CInt(-50)
Code = Replace(Replace(Code, "％", Chr(37)), "＂", Chr(34)) 
    For i = 1 To Len(Code)
        If Mid(Code, i, 1) <> "╋" Then
            If Asc(Mid(Code, i, 1)) < 32 Or Asc(Mid(Code, i, 1)) > 126 Then
               TempCode = TempCode & Chr(Asc(Mid(Code, i, 1)))
            Else
               PkCode = Asc(Mid(Code, i, 1)) - But
               If PkCode > 126 Then
                  PkCode = PkCode -95
               ElseIf PkCode < 32 Then
                  PkCode = PkCode + 95
               End If
               TempCode = TempCode & Chr(PkCode)
            End If
        Else
            TempCode = TempCode & vbCrLf
        End If
    Next
    tempInfoParse = TempCode
End Function


' ============================================
'函数功能：判断模板是否调用了通用信息
' ============================================
Function callCommInfoChk(str)
    thisCallTmp = showFile(DesignerSite_tempPath_common)
	if instr(thisCallTmp,str) > 0 then
	    callis = true
	else
	    callis = false
	end if
	
	callCommInfoChk = callis

End Function


' ==================================================
'载入模板总依赖标签配置
' ==================================================
Function getTempLablePara_siteConfig(str)

	DSWebMainConfigPath = MY_sitePath&"admin/root_temp-edit/_createConfig.asp"
	DSWebMainConfig = showFile(DSWebMainConfigPath)
	
	if str = "标题字符数截取(侧边栏)" then tGVShow = "tmpProperty01"
	if str = "标题字符数截取(友情链接内页)" then tGVShow = "tmpProperty02"
	if str = "标题字符数截取(最新案例作品-通用)" then tGVShow = "tmpProperty03"
	if str = "标题字符数截取(置顶推荐文章-通用)" then tGVShow = "tmpProperty04"
	if str = "标题字符数截取(最新推荐文章-通用)" then tGVShow = "tmpProperty05"
	if str = "标题字符数截取(对应类别调用文章-通用)" then tGVShow = "tmpProperty06"
	if str = "标题字符数截取(对应类别调用作品-通用)" then tGVShow = "tmpProperty07"
	if str = "标题字符数截取(首页文章列表)" then tGVShow = "tmpProperty08"
	if str = "标题字符数截取(通知公告)" then tGVShow = "tmpProperty09"
	if str = "标题字符数截取(留言者姓名)" then tGVShow = "tmpProperty10"
	if str = "标题字符数截取(文章列表)" then tGVShow = "tmpProperty11"
	if str = "标题字符数截取(案例作品列表)" then tGVShow = "tmpProperty12"
	if str = "内容输出列表数量(侧边栏)" then tGVShow = "tmpProperty13"
	if str = "内容输出列表数量(侧边栏评论)" then tGVShow = "tmpProperty14"
	if str = "内容输出列表数量(友情链接内页)" then tGVShow = "tmpProperty15"
	if str = "内容输出列表数量(首页置顶文章)" then tGVShow = "tmpProperty16"
	if str = "内容输出列表数量(首页文章)" then tGVShow = "tmpProperty17"
	if str = "内容输出列表数量(首页案例作品)" then tGVShow = "tmpProperty18"
	if str = "内容输出列表数量(通知公告)" then tGVShow = "tmpProperty19"
	if str = "内容输出列表数量(最新案例作品-通用)" then tGVShow = "tmpProperty20"
	if str = "内容输出列表数量(置顶推荐文章-通用)" then tGVShow = "tmpProperty21"
	if str = "内容输出列表数量(最新推荐文章-通用)" then tGVShow = "tmpProperty22"
	if str = "内容输出列表数量(对应类别调用文章-通用)" then tGVShow = "tmpProperty23"
	if str = "内容输出列表数量(对应类别调用作品-通用)" then tGVShow = "tmpProperty24"
	if str = "内容输出列表数量(留言)" then tGVShow = "tmpProperty25"
	if str = "内容输出列表数量(微博)" then tGVShow = "tmpProperty26"
	if str = "内容输出列表数量(文章列表)" then tGVShow = "tmpProperty27"
	if str = "内容输出列表数量(案例作品列表)" then tGVShow = "tmpProperty28"
	if str = "内容输出列表数量(相关文章列表)" then tGVShow = "tmpProperty29"
	if str = "内容输出列表数量(手机版栏目列表)" then tGVShow = "tmpProperty30"


	getTempLablePara_siteConfig = cint(getDSLableAttr(DSWebMainConfig,tGVShow,"value"))

End Function


' ============================================
'函数功能：模板侧边栏调用配置
' ============================================
Function getTempLablePara_siderBar_list(str,m,c)
    
 if getSideBar(str) = "0" then 
     getTempLablePara_siderBar_list = dependentLabelFun(Template_Code_trip,"sideBar:article,search,about,message",1,"","","")
 elseif getSideBar(str) = "1" then
     getTempLablePara_siderBar_list = dependentLabelFun(Template_Code_trip,"sideBar:article,search,about,message",0,"","","")
 else
     getTempLablePara_siderBar_list = dependentLabelFun(Template_Code_trip,"sideBar:article,search,about,message",m,"","","")'系统默认
 end if
 
End Function

Function getTempLablePara_siderBar_home(str,m,c)
    
 if getSideBar(str) = "0" then 
     getTempLablePara_siderBar_home = dependentLabelFun(Template_Code_trip,"sideBar:home",1,"","","")
 elseif getSideBar(str) = "1" then
     getTempLablePara_siderBar_home = dependentLabelFun(Template_Code_trip,"sideBar:home",0,"","","")
 else
     getTempLablePara_siderBar_home = dependentLabelFun(Template_Code_trip,"sideBar:home",m,"","","")'系统默认
 end if 
 

	

End Function


Function getTempLablePara_siderBar_content(str,m,c)
    
 if getSideBar(str) = "0" then 
     getTempLablePara_siderBar_content = dependentLabelFun(Template_Code_trip,"sideBar:content.comm",1,"","","")
 elseif getSideBar(str) = "1" then
     getTempLablePara_siderBar_content = dependentLabelFun(Template_Code_trip,"sideBar:content.comm",0,"","","")
 else
     getTempLablePara_siderBar_content = dependentLabelFun(Template_Code_trip,"sideBar:content.comm",m,"","","")'系统默认
 end if 
 
	

End Function

Function getSideBar(str)
    
 	Template_Code_New=showFile(DesignerSite_tempPath_common)
	getSideBar = getDSLableAttr(Template_Code_New,str,"value")
 
End Function


' ============================================
'函数功能：只调用栏目独立HTML代码
' ============================================
Function callPageAllHTML(Tstr,tm)

	 Template_Code_New=showFile(DesignerSite_tempPath_common)
	 Template_Code_trip_New = makeTempLabelString(Template_Code_New,"{#"&Tstr&"}","{/"&Tstr&"}")
	 getAll = getDSLableAttr(Template_Code_trip_New,tm,"value")
	 if getAll = "true" then 
		 '判断是否使用独立HTML代码
		 Template_Code_trip_New=replace(Template_Code_trip_New,"『","(")
	     Template_Code_trip_New=replace(Template_Code_trip_New,"』",")")
		 Template_Code_trip_New = replace(Template_Code_trip_New,"<DSSetting.完全独立页面代码.不调用其它任何依赖标签的内容 value=""true"" />","")
		 Template_Code_trip_New = replace(Template_Code_trip_New,"<DSSetting.完全独立页面代码.不调用其它任何依赖标签的内容 value=""true""/>","")
		 Template_Code = Template_Code_trip_New 
		 Template_Code_diary = Template_Code_trip_New
		 Template_Code_page = Template_Code_trip_New
	 end if 
	 


End Function




' ============================================
'函数功能：FSO操作函数
' ==================================================
'//功能：创建文件夹
Function createFolder(filePath) 
 	set fs=server.CreateObject("Scripting.FileSystemObject") 
	if not  fs.FolderExists(server.mappath(filePath)) then
          fs.createfolder(server.mappath(filePath))
    end if
	 Set fs = Nothing
End Function 

'//功能：文件删除
Function delfile(filePath) 
 	set fs=server.CreateObject("Scripting.FileSystemObject") 
	 if fs.FileExists(server.mappath(filePath)) then
		fs.DeleteFile(server.mappath(filePath))
	 end if
	 Set fs = Nothing
End Function 

'//功能：文件夹删除
Function delFolder(filePath) 
 	set fs=server.CreateObject("Scripting.FileSystemObject") 
	 if fs.FolderExists(server.mappath(filePath)) then
		fs.DeleteFolder(server.mappath(filePath))
	 end if
	 Set fs = Nothing
End Function 

'//功能：读出文件内容
Function showFile(filePath) 
 	set fs=server.CreateObject("Scripting.FileSystemObject") 
     set ts=fs.opentextfile(server.mappath(filePath))
		showFile=ts.readall
		
		'判断是否读出的是模板文件
		If InStr(showFile,"<!-- Template OK -->")>0 Then
		
		    showFile=replace(replace(replace(showFile,"../../",MY_sitePath),"../",MY_sitePath),"file="&CHR(34)&""&MY_sitePath&"","file="&CHR(34)&"../")

			
		end if
		
		
	 Set fs = Nothing
End Function 


'//功能：得到文件的修改时间
Function getFileModifiedDate(filePath)
	Set fso = Server.CreateObject("Scripting.FileSystemObject")
	filePath=server.Mappath(filePath)
	Set fsofile=fso.getfile(filePath)
	filePathre=fsofile.DateLastModified '文件时间
	filePathre2=chr(9)&"<span color=green>"&fsofile.type&"</span>" '类型
	filePathre3=chr(9)&round(fsofile.size*1000/1024/1000,2)&" KB" '大小
	set fso=nothing
	getFileModifiedDate=CStr(replace(replace(replace(filePathre,CHR(32),"-"),"/","-"),":","-"))
End Function

'//功能：写文件
Function CreateFile(filePath,FsoContent) 
		set fs=server.CreateObject("Scripting.fileSystemObject")
		set ts=fs.createtextfile(server.MapPath(filePath))
		ts.writeline FsoContent
		ts.close
		Set fs = Nothing
End Function 

'//功能：显示此目录下的所有文件
Function ShowFileList(folderspec)
    set fso=server.CreateObject("Scripting.FileSystemObject") 
    Dim f, f1, fc, s
    Set f = fso.GetFolder(server.mappath(folderspec))
    Set fc = f.Files
      For Each f1 in fc
           s = s & f1.name 
           s = s & "|"
      Next
    ShowFileList = s
    'response.Write  ShowFileList
End Function


'//功能：修改文件名
Function EditFileName(SourceFile,DestinationFile)
		set fso=server.CreateObject("Scripting.FileSystemObject") 
		Dim MyFile
		Set MyFile = fso.GetFile(server.mappath(SourceFile))
		MyFile.Name = DestinationFile
		
End Function

'//功能：修改文件夹名
Function EditFolderName(nowfld,newfld)

		nowfld=server.mappath(nowfld)
		newfld=server.mappath(newfld)
		
		Set fso = CreateObject("Scripting.FileSystemObject")
		if not fso.FolderExists(nowfld) then
			response.write("需要修改的文件夹路径不正确或文件夹名称输入错误")
		else
			fso.CopyFolder nowfld,newfld
			'fso.DeleteFolder(nowfld)
		end if
		set fso=nothing

End Function

'//功能：更换模板文件夹
'//用法：TempExchangeFolderName "要更换的模板文件夹名","默认支持的模板文件夹名"
Function TempExchangeFolderName(nowfld,newfld)


	EditFolderName ""&nowfld&"/",""&newfld&"(2)/"
	EditFolderName ""&newfld&"/",""&nowfld&"/"
	EditFolderName ""&newfld&"(2)/",""&newfld&"/"

End Function


'//功能：文件复制
Function CopyFile(SourceFile,DestinationFile)
		set fso=server.CreateObject("Scripting.FileSystemObject") 
		Dim MyFile
		Set MyFile = fso.GetFile(server.mappath(SourceFile))
		MyFile.Copy (server.mappath(DestinationFile))
End Function

'//功能：文件移动
Function MoveFile(SourceFile,DestinationFile) 
		set fso=server.CreateObject("Scripting.FileSystemObject") 
		path1 = server.mappath(SourceFile)
		path2 = server.mappath(DestinationFile)
		fso.MoveFile path1,path2
End Function

'//功能：文件夹复制
Function CopyFolder(FolderName,FolderPath) 
	sFolder=server.mappath(FolderName) 
	oFolder=server.mappath(FolderPath) 
	set fso=server.createobject("scripting.filesystemobject") 
	if fso.folderexists(server.mappath(FolderName)) Then'检查原文件夹是否存在 
	if fso.folderexists(server.mappath(FolderPath)) Then'检查目标文件夹是否存在 
	fso.copyfolder sFolder,oFolder 
	Else '目标文件夹如果不存在就创建 
	CreateNewFolder = Server.Mappath(FolderPath) 
	fso.CreateFolder(CreateNewFolder) 
	fso.copyfolder sFolder,oFolder 
	End if 
	
	Else 
	'错误，文件夹不存在
	End If 
	set fso=nothing 
End Function 


'//功能：判断文件数量
Function FileCount(SourceFile) 
		set fso=server.CreateObject("Scripting.FileSystemObject") 
		path = server.mappath(SourceFile)
		filescount= fso.GetFolder(path).Files.Count'计算文件数量
		FileCount = filescount
End Function

'//功能：文件夹移动
Function MoveFolder(SourceFolder,DestinationFolder)
		set fso=server.CreateObject("Scripting.FileSystemObject") 
		path1 = server.mappath(SourceFolder)
		path2 = server.mappath(DestinationFolder)
		fso.MoveFolder path1,path2
End Function

'//功能：判断文件是否存在
Function FileExistsStatus(FileName)
    set fso=server.CreateObject("Scripting.FileSystemObject")  
    Dim msg,thisState
	thisState = -1
    If (fso.FileExists(server.mappath(FileName))) Then
       msg = ""
	   thisState = 1
    Else
       msg = "文件不存在" 
	   thisState = -1
    End If
    'response.Write msg
	FileExistsStatus = thisState
End Function

'//功能：判断文件夹是否存在
Function FolderExistsStatus(FileName)
    set fso=server.CreateObject("Scripting.FileSystemObject")  
    Dim msg,thisState
	thisState = -1
    If (fso.FolderExists(server.mappath(FileName))) Then
       msg = ""
	   thisState = 1
    Else
       msg = "文件不存在" 
	   thisState = -1
    End If
    'response.Write msg
	FolderExistsStatus = thisState
End Function

' ==================================================
''函数:UBoundStrToArr
''作用:检测原字符串转换为数组的最大下标值
''参数:cCheckStr(需要检测的字符串)
''      cUBoundArr(生成数组的最大下标值)
''      cSpaceStr(间隔字符串)
''返回:数组的最大下标值
''思路 1.检测右侧是否存在间隔字符串，如果存在则需要将数组最大下标值减一
''     2.如果不存在则直接返回原数组最大下标
''   3.在使用Split函数对其进行分隔的时候产生的数组最大下标值是不同的，往往在不注意的时候会在最右侧多写一个间隔字符。这样在输出的时候就会多遍历一个数据，为了避免这种情况的发生，就要对字符串进行检测
' ==================================================
Function UBoundStrToArr(ByVal cCheckStr,ByVal cUBoundArr,ByVal cSpaceStr)
    If Instr(cCheckStr,cSpaceStr)=0 Then
        UBoundStrToArr=cUBoundArr
        Exit Function
    End If
    Dim TempSpaceStr,UBoundValue
    TempSpaceStr=Mid(cCheckStr,Len(cCheckStr)-Len(cSpaceStr)+1) ''获取字符串右侧间隔字符
    If TempSpaceStr=cSpaceStr Then ''如果字符串最右侧存在间隔字符,则下标值需要-1
        UBoundValue=cUBoundArr-1
    Else
        UBoundValue=cUBoundArr
    End If
    UBoundStrToArr=UBoundValue
End Function



'================================================
'函数名：URLDecode
'作  用：URL解码，UTF-8转GB编码
'用  法: URLDecode(value)
'================================================
Function URLDecode(ByVal urlcode)
 
	Dim start,final,length,char,i,butf8,pass
	Dim leftstr,rightstr,finalstr
	Dim b0,b1,bx,blength,position,u,utf8
	On Error Resume Next

	b0 = Array(192,224,240,248,252,254)
	urlcode = Replace(urlcode,"+"," ")
	pass = 0
	utf8 = -1

	length = Len(urlcode) : start = InStr(urlcode,"%") : final = InStrRev(urlcode,"%")
	If start = 0 Or length < 3 Then URLDecode = urlcode : Exit Function
	leftstr = Left(urlcode,start - 1) : rightstr = Right(urlcode,length - 2 - final)

	For i = start To final
		char = Mid(urlcode,i,1)
		If char = "%" Then
			bx = URLDecode_Hex(Mid(urlcode,i + 1,2))
			If bx > 31 And bx < 128 Then
				i = i + 2
				finalstr = finalstr & ChrW(bx)
			ElseIf bx > 127 Then
				i = i + 2
				If utf8 < 0 Then
					butf8 = 1 : blength = -1 : b1 = bx
					For position = 4 To 0 Step -1
						If b1 >= b0(position) And b1 < b0(position + 1) Then
							blength = position
							Exit For
						End If
					Next
					If blength > -1 Then
						For position = 0 To blength
							b1 = URLDecode_Hex(Mid(urlcode,i + position * 3 + 2,2))
							If b1 < 128 Or b1 > 191 Then butf8 = 0 : Exit For
						Next
					Else
						butf8 = 0
					End If
					If butf8 = 1 And blength = 0 Then butf8 = -2
					If butf8 > -1 And utf8 = -2 Then i = start - 1 : finalstr = "" : pass = 1
					utf8 = butf8
				End If
				If pass = 0 Then
					If utf8 = 1 Then
						b1 = bx : u = 0 : blength = -1
						For position = 4 To 0 Step -1
							If b1 >= b0(position) And b1 < b0(position + 1) Then
								blength = position
								b1 = (b1 xOr b0(position)) * 64 ^ (position + 1)
								Exit For
							End If
						Next
						If blength > -1 Then
							For position = 0 To blength
								bx = URLDecode_Hex(Mid(urlcode,i + 2,2)) : i = i + 3
								If bx < 128 Or bx > 191 Then u = 0 : Exit For
								u = u + (bx And 63) * 64 ^ (blength - position)
							Next
							If u > 0 Then finalstr = finalstr & ChrW(b1 + u)
						End If
					Else
						b1 = bx * &h100 : u = 0
						bx = URLDecode_Hex(Mid(urlcode,i + 2,2))
						If bx > 0 Then
							u = b1 + bx
							i = i + 3
						Else
							If Left(urlcode,1) = "%" Then
								u = b1 + Asc(Mid(urlcode,i + 3,1))
								i = i + 2
							Else
								u = b1 + Asc(Mid(urlcode,i + 1,1))
								i = i + 1
							End If
						End If
						finalstr = finalstr & Chr(u)
					End If
				Else
					pass = 0
				End If
			End If
		Else
			finalstr = finalstr & char
		End If
	Next
	URLDecode = leftstr & finalstr & rightstr
 
End Function


' ==================================================
'GB转UTF-8编码
' ==================================================
Function toUTF8(szInput)
	Dim wch, uch, szRet
	Dim x
	Dim nAsc, nAsc2, nAsc3
	'如果输入参数为空，则退出函数
	If szInput = "" Then
	toUTF8 = szInput
	Exit Function
	End If
	'开始转换
	For x = 1 To Len(szInput)
	'利用mid函数分拆GB编码文字
	wch = Mid(szInput, x, 1)
	'利用ascW函数返回每一个GB编码文字的Unicode字符代码
	'注：asc函数返回的是ANSI 字符代码，注意区别
	nAsc = AscW(wch)
	If nAsc < 0 Then nAsc = nAsc + 65536
	
	If (nAsc And &HFF80) = 0 Then
	szRet = szRet & wch
	Else
	If (nAsc And &HF000) = 0 Then
	uch = "%" & Hex(((nAsc \ 2 ^ 6)) Or &HC0) & Hex(nAsc And &H3F Or &H80)
	szRet = szRet & uch
	Else
	'GB编码文字的Unicode字符代码在0800 - FFFF之间采用三字节模版
	uch = "%" & Hex((nAsc \ 2 ^ 12) Or &HE0) & "%" & _
	Hex((nAsc \ 2 ^ 6) And &H3F Or &H80) & "%" & _
	Hex(nAsc And &H3F Or &H80)
	szRet = szRet & uch
	End If
	End If
	Next
	
	toUTF8 = szRet
End Function



' ==================================================
'函数名：keyword_tag
'作 用：将字符串里的关键词标记
'参 数：str ------ 字符串
' keyword ------ 标记关键词（之间用空格分开）
'返 回：字符串（html格式）
' ==================================================
Function keyword_tag(byval str,byval keyword)
	 dim keywords,str01,str02,i
	   '全角－－》半角　空格
	 keyword=replace(keyword,"　"," ")
	 str01=replace(str,keyword,"<mark>"&keyword&"</mark>") 
	   keywords=split(keyword," ")
	 if ubound(keywords)>0 then
	   str02=str
	 for i=0 to ubound(keywords)
	 str02=replace(str02,keywords(i),"<mark>"&keywords(i)&"</mark>") 
	 next
	   keyword_tag=str02
	 else
	 keyword_tag=str01
	 end if
End Function

' ==================================================
'切割内容 - 按行分割
' ==================================================
Function SplitLines(byVal Content,byVal ContentNums) 
	Dim ts,i,l
	ContentNums=int(ContentNums)
	If IsNull(Content) Then Exit Function
	i=1
	ts = 0
	For i=1 to Len(Content)
      l=Lcase(Mid(Content,i,5))
      	If l="<br/>" Then
         	ts=ts+1
      	End If
      l=Lcase(Mid(Content,i,4))
      	If l="<br>" Then
         	ts=ts+1
      	End If
      l=Lcase(Mid(Content,i,3))
      	If l="<p>" Then
         	ts=ts+1
      	End If
	If ts>ContentNums Then Exit For 
	Next
	If ts>ContentNums Then
    	Content=Left(Content,i-1)
	End If
	SplitLines=Content
End Function

' ==================================================
'切割内容 - 按字符分割
' ==================================================
Function CutStr(byVal Str,byVal StrLen)
	Dim l,t,c,i
	If IsNull(Str) Then CutStr="":Exit Function
	l=Len(str)
	StrLen=int(StrLen)
	t=0
	For i=1 To l
		c=Asc(Mid(str,i,1))
		If c<0 Or c>255 Then t=t+2 Else t=t+1
		IF t>=StrLen Then
			CutStr=left(Str,i)&"..."
			Exit For
		Else
			CutStr=Str
		End If
	Next
End Function

' ==================================================
'图片展示链接
' ==================================================
Function ShowImageURL(str) 
	ShowImageStr=replace(replace(replace(replace(str,"../../"&MY_sec_uploadImgPath&"/?src=",""),""&MY_secCheckSiteLink&"/",""),"../../",""),""&MY_sec_uploadImgPath&"/?src=","")
	ShowImageURL=ShowImageStr
End Function 


' ==================================================
'清除HTML标签
' ==================================================
Function Chk_Length(str) 
	dim re 
	Set re=new RegExp 
	re.IgnoreCase =true 
	re.Global=True 
	re.Pattern="(\<.[^\<]*\>)" 
	str=re.replace(str," ") 
	re.Pattern="(\<\/[^\<]*\>)" 
	str=re.replace(str," ") 
	str=replace(str," ","") 
	str=replace(str," ","") 
	Chk_Length=str 
	set re=nothing 
End Function 


Function stripHTML(htmlStr)
	Dim regEx
	SET regEx = New Regexp
	regEx.IgnoreCase = True
	regEx.Global = True
	regEx.Pattern = "<.+?>"
	htmlStr = regEx.Replace(htmlStr,"")
	htmlStr = Replace(htmlStr, "<","&lt;")
	htmlStr = Replace(htmlStr, ">","&gt;")
	htmlStr = Replace(htmlStr,chr(10),"")
	htmlStr = Replace(htmlStr,chr(13),"")
	stripHTML = htmlStr
	SET regEx = Nothing
End Function

' ==================================================
'清除重复数组元素
' ==================================================
Function MoveR(Rstr,Separator)
	 Dim i,SpStr
	 SpStr = Split(Rstr,Separator)
	 For i = 0 To Ubound(Spstr)
	  If I = 0 then
	   MoveR = MoveR & SpStr(i) & Separator
	  Else
	   If instr(MoveR,SpStr(i))=0 and i=Ubound(Spstr) Then
		MoveR = MoveR & SpStr(i)
	   Elseif instr(MoveR,SpStr(i))=0 Then
		MoveR = MoveR & SpStr(i) & Separator
	   End If
	  End If
	 Next
End Function

' ==================================================
'获取客户端IP
' ==================================================
Function getIP() 
	 dim strIP,IP_Ary,strIP_list
	 strIP_list=Replace(Request.ServerVariables("HTTP_X_FORWARDED_FOR"),"'","")
	 
	 If InStr(strIP_list,",")<>0 Then
		IP_Ary = Split(strIP_list,",")
		strIP = IP_Ary(0)
	 Else
		strIP = strIP_list
	 End IF
	 
	 If strIP=Empty Then strIP=Replace(Request.ServerVariables("REMOTE_ADDR"),"'","")
	 getIP=strIP
End Function

' ==================================================
'页面禁止缓存
' ==================================================
Function PageOverdue()
	Response.Buffer = true 
	Response.Expires = -1
	Response.ExpiresAbsolute = Now() - 1 
	Response.Expires = 0 
	Response.CacheControl = "no-cache" 
End Function

' ==================================================
'过滤超链接
' ==================================================
Function checkURL(ByVal ChkStr)
	Dim str:str=ChkStr
	str=Trim(str)
	If IsNull(str) Then
		checkURL = ""
		Exit Function 
	End If
	Dim re
	Set re=new RegExp
	re.IgnoreCase =True
	re.Global=True
	re.Pattern="(d)(ocument\.cookie)"
    Str = re.replace(Str,"$1ocument cookie")
	re.Pattern="(d)(ocument\.write)"
    Str = re.replace(Str,"$1ocument write")
   	re.Pattern="(s)(cript:)"
    Str = re.replace(Str,"$1cri&#112;t ")
   	re.Pattern="(s)(cript)"
    Str = re.replace(Str,"$1cri&#112;t")
   	re.Pattern="(o)(bject)"
    Str = re.replace(Str,"$1bj&#101;ct")
   	re.Pattern="(a)(pplet)"
    Str = re.replace(Str,"$1ppl&#101;t")
   	re.Pattern="(e)(mbed)"
    Str = re.replace(Str,"$1mb&#101;d")
	Set re=Nothing
   	Str = Replace(Str, ">", "&gt;")
	Str = Replace(Str, "<", "&lt;")
	checkURL=Str    
End Function


' ============================================
'函数功能：回车换行自动转化|分界
'用法：
'1.写入数据库前取得数据TrimLockIPList(Request.form("？？"),0)
'2.从数据库取出显示数据TrimLockIPList(Request.form("？？"),1)
' ============================================
Function TrimLockIPList(str,stype)
	str = Trim(str)
	If IsNull(str) Or Len(str) < 2 Then
		TrimLockIPList = ""
		Exit Function
	End If
	str = Replace(str, vbNewline, "|")
	str = Replace(str, Chr(13), "")
	str = Replace(str, Chr(10), "|")
	Dim a,s,i
	a = Split(str, "|")
	For i = 0 To UBound(a)
		If Len(a(i)) > 0 Then
			s = s & a(i) & "|"
		End If
	Next
	If Len(s)>0 Then s = Left(s,Len(s)-1)
	If stype = 1 Then
		TrimLockIPList = Replace(s, "|", vbNewline)
	Else
		TrimLockIPList = s
	End If
End Function

' ============================================
'函数功能：xmlhttp获取源代码
' ============================================
Function getHTTPPage(url)

	dim adxmlhttp
	
	set adxmlhttp = Server.createobject("Microsoft.XMLHTTP")
	adxmlhttp.open "get",url,false
	adxmlhttp.send()
	If adxmlhttp.readystate=4 And adxmlhttp.Status=200 Then
		if adxmlhttp.readystate <> 4 then  exit function
		getHTTPPage = Bytes2bStr2(adxmlhttp.responsebody)
	Else
		getHTTPPage = "Error.网络错误或者路径不正确!"
	End If

	set adxmlhttp = nothing

End function

Function Bytes2bStr2(vin)

	Dim BytesStream,StringReturn
	Set BytesStream = Server.CreateObject("adodb.stream")
	BytesStream.Type = 2
	BytesStream.Open
	BytesStream.WriteText vin
	BytesStream.Position = 0
	BytesStream.Charset = "GB2312"
	BytesStream.Position = 2
	StringReturn =BytesStream.ReadText
	BytesStream.close
	Set BytesStream = Nothing
	Bytes2bStr2 = StringReturn

End function

' ============================================
'函数功能：正则表达式判断
'验证通过返回 True 失败返回 False
' ============================================
Function RegExpData(patrn, strng) 
	Dim regEx
	Set regEx = New RegExp ' 建立正则表达式。 
	regEx.Pattern = patrn ' 设置模式。 
	regEx.IgnoreCase = False ' 设置是否区分大小写。 
	RegExpData= regEx.Test(strng) ' 执行搜索测试。 

End Function
'      ^\d+$       非负整数（正整数 + 0） 
'      ^[0-9]*[1-9][0-9]*$       正整数 
'      ^((-\d+)|(0+))$       非正整数（负整数 + 0） 
'      ^-[0-9]*[1-9][0-9]*$       负整数 
'      ^-?\d+$       整数 
'      ^\d+(\.\d+)?$       非负浮点数（正浮点数 + 0） 
'      ^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$       正浮点数 
'      ^((-\d+(\.\d+)?)|(0+(\.0+)?))$       非正浮点数（负浮点数 + 0） 
'      ^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$       负浮点数 
'      ^(-?\d+)(\.\d+)?$       浮点数 
'      ^[A-Za-z]+$       由26个英文字母组成的字符串 
'      ^[A-Z]+$       由26个英文字母的大写组成的字符串 
'      ^[a-z]+$       由26个英文字母的小写组成的字符串 
'      ^[A-Za-z0-9]+$       由数字和26个英文字母组成的字符串 
'      ^\w+$       由数字、26个英文字母或者下划线组成的字符串 
'      ^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$       email地址 
'      ^[a-zA-z]+://(\w+(-\w+)*)(\.(\w+(-\w+)*))*(\?\S*)?$              url
'       /^(d{2}|d{4})-((0([1-9]{1}))|(1[1|2]))-(([0-2]([1-9]{1}))|(3[0|1]))$/          年-月-日
'       /^((0([1-9]{1}))|(1[1|2]))/(([0-2]([1-9]{1}))|(3[0|1]))/(d{2}|d{4})$/          月/日/年
'      ^([w-.]+)@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.)|(([w-]+.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(]?)$       Emil
'      (d+-)?(d{4}-?d{7}|d{3}-?d{8}|^d{7,8})(-d+)?       电话号码
'      ^(d{1,2}|1dd|2[0-4]d|25[0-5]).(d{1,2}|1dd|2[0-4]d|25[0-5]).(d{1,2}|1dd|2[0-4]d|25[0-5]).(d{1,2}|1dd|2[0-4]d|25[0-5])$       IP地址
'      [\u4e00-\u9fa5]       匹配中文字符的正则表达式
'      [^\x00-\xff]        匹配双字节字符(包括汉字在内)
'      \n[\s| ]*\r        匹配空行的正则表达式
'      /<(.*)>.*<\/\1>|<(.*) \/>/          匹配HTML标记的正则表达式
'      \w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*        匹配Email地址的正则表达式
'      ^[a-zA-z]+://(\\w+(-\\w+)*)(\\.(\\w+(-\\w+)*))*(\\?\\S*)?$         匹配网址URL的正则表达式
'      ^[a-zA-Z][a-zA-Z0-9_]{4,15}$          匹配帐号是否合法(字母开头，允许5-16字节，允许字母数字下划线)
'      (\d{3}-|\d{4}-)?(\d{8}|\d{7})?          匹配国内电话号码
'      ^[1-9]*[1-9][0-9]*$            匹配腾讯QQ号


%>
