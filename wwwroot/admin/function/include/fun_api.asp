<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================

Sub commAPIEcho()

	'（获取管理员信息）
	set rs=server.CreateObject("adodb.recordset")
	strsql="select * from [users]"
	rs.open strsql,db,1,1
	 apiuserName = rs("userName")
	rs.close
	set rs=nothing
	'----------
	'（验证信息提交权限，对应到指定的管理员密匙上）
	if apiSQLKey <> MY_API_SQLKEY then spiSqlChk = 1 else spiSqlChk = 0
	if apiInfoAddKey <> MY_API_InfoAddKEY then spiInfoAddChk = 1 else spiInfoAddChk = 0
	if user <> apiuserName then spiuserChk = 1 else spiuserChk = 0
	
	
	
	'容错输出
	if webidKey <> webIDTrue then apiError(0)
	if spiSqlChk = 1 and openData = "true" then apiError(1)
	if spiSqlChk = 1 and openData = "false" then apiError(2)
	if spiSqlChk = 0 and openData = "false" then apiError(3)
	if spiSqlChk = 0 and openData = "false" then apiError(3)
	
	if instr(columnType,".list") > 0 then
	  if apiStrNull(infoEchoNum) = 0 then apiError(7)
	end if
	
	
	if apiStrNull(apiInfoAddKey) = 1 then
		if spiuserChk = 1 then apiError(8)
		if spiInfoAddChk = 1 and openData = "true" then apiError(4)
		if spiInfoAddChk = 0 and openData = "true" then apiError(5)
		if apiInfoAddKey <> MY_API_InfoAddKEY then apiError(4)
	end if


End Sub


Function apiError(num)

	select case num
	
	 case "0" : response.write "Error000.(网站接口ID不符)" : response.End()
	 case "1" : response.write "Error001.(查询密匙不符)" : response.End()
	 case "2" : response.write "Error002.(查询密匙不符)" : response.End()
	 case "3" : response.write "Error003.(查询请求失败)" : response.End()
	 case "4" : response.write "Error004.(权限密匙不符)" : response.End()
	 case "5" : response.write "Error005.(权限请求失败)" : response.End()
	 case "6" : response.write "Error006.(参数重叠,获取数据失败)" : response.End()
	 case "7" : response.write "Error007.(列表查询未指定输出参数)" : response.End()
	 case "8" : response.write "Error008.(用户ID不符)" : response.End()
	 case "9" : response.write "Error009.(关键词参数过长)" : response.End()
	 case "10" : response.write "Error010.(参数为空值)" : response.End()
	 case "11" : response.write "Error011.(文章类别不存在)" : response.End()
	 case "12" : response.write "Error012.(社会化接口API_KEY验证失败)" : response.End()
	 case else
	 response.write "Error013.(不可预知的错误)" : response.End()

	end select

End Function


'返回值输出
'apiWriteJSON("Property1:'value'","Property2:'value'","","","","","","","","","","","","","","","","","","")
Function apiWriteJSON(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20)
    jsonC = ""
	
	p1=HTMLEncode(p1) : p2=HTMLEncode(p2) : p3=HTMLEncode(p3) : p4=HTMLEncode(p4) : p5=HTMLEncode(p5) : p6=HTMLEncode(p6) : p7=HTMLEncode(p7) : p8=HTMLEncode(p8) : p9=HTMLEncode(p9) : p10=HTMLEncode(p10) : p11=HTMLEncode(p11) : p12=HTMLEncode(p12) : p13=HTMLEncode(p13) : p14=HTMLEncode(p14) : p15=HTMLEncode(p15) : p16=HTMLEncode(p16) : p17=HTMLEncode(p17) : p18=HTMLEncode(p18) : p19=HTMLEncode(p19) : p20=HTMLEncode(p20) 

	dot1 = "," : dot2 = "," : dot3 = "," : dot4 = "," : dot5 = "," : dot6 = "," : dot7 = "," : dot8 = "," : dot9 = "," : dot10 = "," : dot11 = "," : dot12 = "," : dot13 = "," : dot14 = "," : dot15 = "," : dot16 = "," : dot17 = "," : dot18 = "," : dot19 = ","
	
	if p2 = "" then dot1 = "" : if p3 = "" then dot2 = "" : if p4 = "" then dot3 = "" : if p5 = "" then dot4 = "" : if p6 = "" then dot5 = "" : if p7 = "" then dot6 = "" : if p8 = "" then dot7 = "" : if p9 = "" then dot8 = "" : if p10 = "" then dot9 = "" : if p11 = "" then dot10 = "" : if p12 = "" then dot11 = "" : if p13 = "" then dot12 = "" : if p14 = "" then dot13 = "" : if p15 = "" then dot14 = "" : if p16 = "" then dot15 = "" : if p17 = "" then dot16 = "" : if p18 = "" then dot17 = "" : if p19 = "" then dot18 = "" : if p20 = "" then dot19 = ""
	
	jsonC = jsonC&p1&dot1&p2&dot2&p3&dot3&p4&dot4&p5&dot5&p6&dot6&p7&dot7&p8&dot8&p9&dot9&p10&dot10&p11&dot11&p12&dot12&p13&dot13&p14&dot14&p15&dot15&p16&dot16&p17&dot17&p18&dot18&p19&dot19&p20
	
	jsonC = clearRightString(replace(jsonC,",,",""),",")
	jsonC = replace(jsonC,"{#}",chr(34))
	

	apiWriteJSON = jsonC

End Function

%>