<!--#include file="../dsMainFunction.asp" -->
<!--#include file="../imgAction.asp" -->
<!--#include file="../imgGetWH.asp" -->
<!--#include file="../../checkFun_noSup.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%

cutMode=request.QueryString("mode")
pathNow=request.QueryString("PhotoUrl")

'初始化文章、案例作品剪裁判断
response.Cookies("cAArticle") = 0
response.Cookies("cAWorks") = 0

'载入模板配置文档
set xmlDom = server.CreateObject("MSXML2.DOMDocument")
xmlDom.async = false
setXmlPath = ""&MY_sitePath&"template/tempConfig.xml"
if not xmlDom.Load(Server.MapPath(setXmlPath))  then 
    'response.write("Load wrong!")
else

    '读取节点
	set setTempPara = xmlDom.getElementsByTagName("config") 
	
	setTempValue1 = 200
	setTempValue2 = 200
	 
	if cutMode = "article" then
		'-----文章
		set setnode = setTempPara(0).selectSingleNode("setting_article")
		setTempValue1 = setnode.GetAttribute("ImgClippingWidth")
		setTempValue2 = setnode.GetAttribute("ImgClippingHeight")
	end if	
	
	if cutMode = "works" then
		'-----作品
		set setnode = setTempPara(0).selectSingleNode("setting_works")
		setTempValue1 = setnode.GetAttribute("ImgClippingWidth")
		setTempValue2 = setnode.GetAttribute("ImgClippingHeight")
	end if	
	
	
		
end if


picW = getImgWH(pathNow,"w")
picH = getImgWH(pathNow,"h")


urlOrigin=pathNow
imgSmallCreate(urlOrigin)
imgSmallCreate_subject(urlOrigin)
			

%>

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title></title>
<script src="../../../cat_js/jquery.min.js"></script>
<script src="../../../cat_js/jquery.mousewheel.js"></script>
<script src="../../../cat_js/jquery.jscrollpane.min.js"></script>
<script src="jquery.Jcrop.js"></script>
<link rel="stylesheet" href="jquery.Jcrop.css" type="text/css" />
</head>
<body oncontextmenu="return false">

<style type="text/css">
html{overflow-x:hidden}
body{margin:0 auto; text-align:left; font-size:12px; color:#333; margin-left:5px;}
#cropbox { border:2px #999 solid}
.btn{background:#ccc; border:1px #666 solid; color:#000; cursor:pointer}
#controlImg{ position:relative; height:30px;}
#controlImg a{color:#f00}
#ImgBox {width:626px; height:295px; overflow:hidden}
#ImgBox .img{ width:<%=cint(request.cookies("newImgW"))%>px; height:<%=cint(request.cookies("newImgH"))%>px;}
#loadingBox{ margin-top:50px; position:absolute; font-family:Georgia, "Times New Roman", Times, serif; font-style:italic; font-size:14px;}

/*jScrollPane*/
.jspContainer{overflow:hidden;position:relative;}
.jspPane{position:absolute; z-index:9999}
.jspVerticalBar{position:absolute; z-index:9999; top:0;right:0;width:5px;height:100%;background:none;}
.jspHorizontalBar{position:absolute; z-index:9999; bottom:0;left:0;width:100%;height:5px;background:none;}
.jspVerticalBar *,.jspHorizontalBar *{margin:0;padding:0;}
.jspCap{display:none;}
.jspHorizontalBar .jspCap{float:left;}
.jspTrack{background:#ccc;position:relative;}
.jspDrag{background:#000;position:relative;top:0;left:0;cursor:pointer;}
.jspDrag:hover{ background:#333}
.jspHorizontalBar .jspTrack,.jspHorizontalBar .jspDrag{float:left;height:100%;}
.jspArrow{background:#50506d;text-indent:-20000px;display:block;cursor:pointer;}
.jspArrow.jspDisabled{cursor:default;background:#80808d;}
.jspVerticalBar .jspArrow{height:5px;}
.jspHorizontalBar .jspArrow{width:5px;float:left;height:100%;}
.jspVerticalBar .jspArrow:focus{outline:none;}
.jspCorner{display:none}
/* Yuk! CSS Hack for IE6 3 pixel bug:( */
* html .jspCorner{margin:0 -3px 0 0;}

</style>

<script>

$(document).ready(function(){

  $('#cropbox').Jcrop({
	onChange:   showCoords,
	onSelect:   showCoords,
	onRelease:  clearCoords
  },function(){
	jcrop_api = this;
	jcrop_api.animateTo([0,0,<%=cint(setTempValue1)%>,<%=cint(setTempValue2)%>]);
	//scroll
	$("#ImgBox").jScrollPane();
	
  });
  
  
});

setInterval(function(){
	
	var clW = $('#w').val();
	if (clW!= ""){
		$("#loadingBox").fadeOut(500);
		$("#ImgBox .img").fadeIn(500);
	}
	
},2000);



function showCoords(c)
{
  $('#x1').val(c.x);
  $('#y1').val(c.y);
  $('#x2').val(c.x2);
  $('#y2').val(c.y2);
  $('#w').val(c.w);
  $('#h').val(c.h);
};

function clearCoords()
{
  $('#x1,#y1,#x2,#y2,#w,#h').val('');
};
	
function resizeImage(img,w,h) {   
     //remove default attribute   
     img.removeAttribute("width");   
     img.removeAttribute("height");   
     var pic;   
     //if is ie   
     if(window.ActiveXObject) {   
         var pic=new Image();   
         pic.src=img.src;   
     } else pic=img;   
     if(pic && pic.width && pic.height && w) {   
         if(!h) h=w;   
         if(pic.width>w||pic.height>h) {   
           var scale=pic.width/pic.height
		   fit=scale>=w/h;
             if(window.ActiveXObject) img=img.style;   
                 img[fit?'width':'height']=fit?w:h;   
             //if is ie6   
             if(window.ActiveXObject)   
                 img[fit?'height':'width']=(fit?w:h)*(fit?1/scale:scale);   
         }   
     }
}


</script>
        
<div id="loadingBox"><img src="../../../images/wbox/loading.gif" align="absmiddle">&nbsp;Loading image...</div>
<div id="controlImg">
    <form id="coords"  action="ImgCutSave.asp?pathNow=<%=pathNow%>&mode=<%=cutMode%>" method="post">

       <input name="" type="submit" class="btn" value="→ 剪裁此图片" onClick="Create()" />&nbsp;&nbsp;
        <a href="../../website/siteConfig.asp#setting-img" target="_parent">设置裁剪大小,剪裁功能开关</a>&nbsp;&nbsp;原图宽度:<%=cint(request.cookies("newImgW"))%> 原图高度:<%=cint(request.cookies("newImgH"))%>

          <input type="hidden" id="x1" name="x1" />
          <input type="hidden" id="y1" name="y1" />
          <input type="hidden" id="x2" name="x2" />
          <input type="hidden" id="y2" name="y2" />
          <input type="hidden" id="w" name="w" />
          <input type="hidden" id="h" name="h" />  
          <input type="hidden" name="Pw"  value="<%=picW%>"/>
          <input type="hidden" name="Ph" value="<%=picH%>" />  
    </form>
</div>

<div id="ImgBox">
<!--图片剪裁区-->
    <div class="img">
        <img src="<%=pathNow%>"  id="cropbox"/>
    </div>
</div>



</body>
</html>
