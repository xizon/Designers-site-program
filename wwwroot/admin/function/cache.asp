<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
' ============================================
'没位道缓存函数
'用法：
'生成缓存文件
'HtmlCode = getHTTPPage(thisFileUrlCacheOrigin) 
'CreateFile FileUrlCacheName,HtmlCode
' ============================================

Dim thisFileUrl,thisFileUrlCache,FileUrlCacheName,thisFileUrlCacheOrigin,save_t,cacheAutoUpOverdue,cacheForbitTime,HtmlCode

'获取文件虚拟路径
thisFileUrl=replace(replace(""&Request.ServerVariables("Server_Name")&""&Request.ServerVariables("SCRIPT_NAME")&"",".","-"),"/","-")
thisFileUrlCache=toUTF8(thisFileUrl)
thisFileUrlCacheOrigin="http://"&Request.ServerVariables("Server_Name")&""&Request.ServerVariables("SCRIPT_NAME")&""

'缓存路径
createFolder(MY_cachePath)
'生成缓存文件名
FileUrlCacheName=""&MY_cachePath&""&thisFileUrlCache&".txt"

function DyCache()

'缓存时间检测--------start
save_t = ""&MY_sitePath&"cacheNow.txt"
set fs=server.createobject("scripting.filesystemobject")
time_file=server.mappath(save_t)
set txt=fs.opentextfile(time_file,1,true)
if not txt.atendofstream then'只读打开，避免出错
times=txt.ReadLine
Else 
content_t = NOW()
CreateFile save_t,content_t
end If

cacheForbitTime=datediff("s",times,NOW())
cacheAutoUpOverdue=MY_AutoUpTime * 86400      '过期时间(天*秒)
If  cacheForbitTime> cacheAutoUpOverdue Then
'---------

Set MyFileObject=Server.CreateObject("Scripting.FileSystemObject")

foldername=server.mappath(MY_cachePath)
Set MyFolder=MyFileObject.GetFolder(foldername)
i=0
'循环显示其中文件夹
For Each thing in MyFolder.subfolders
  if MyFileObject.folderExists(thing) then            '判断folder文件夹是否存在
     fileCon=thing
     MyFileObject.deletefolder thing                         '删除folder文件夹 
  end if 
i=i+1
Next
'循环显示其中文件
i=0
For Each thing in MyFolder.Files
 if MyFileObject.FileExists(thing) then            '判断folder文件夹是否存在 
    fileCon=thing
     MyFileObject.DeleteFile thing                         '删除folder文件夹 
  end if 
i=i+1
Next

'---------
'更新
content_t = NOW()
CreateFile save_t,content_t

End If

'缓存时间检测--------end

end function

%>