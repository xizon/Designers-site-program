<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<style>
.labelName a.closeb{background:none; border:none; visibility:hidden}

</style>

<script>

function tipAction(){
	
	MySite.Cookie.set('tempTip_LB','tempTip_LB');
	$('#tipA').html('设置成功！');
	
}

var newWidth = window.screen.width - 315;

function editFrame(name,title){
    floatWin('&nbsp;'+ title,'<iframe name="frameLabel" src="edit_form.asp?labelSymbol=' + name + '"  frameborder="0" scrolling="no" height="440" width="' + (newWidth - 5) + '"></iframe>',newWidth,450,20,0,0,1);
	//for IE6
	if ($.browser.msie && $.browser.version < 7) {
		setTimeout('document.frames("frameLabel").location.reload();',500);
	}

}


$(function(){
	
	if (MySite.Cookie.get('tempTip_LB') != "tempTip_LB" ){
		if (getQueryStringRegExp('thisState')!="Succeed"){
		floatWin('温馨提示','<div class=tipsFloatWin>（1）如果您修改了自定义标签内容，需进行过以下任一操作，才能及时看到效果<br><strong>&nbsp;&nbsp;a)手动操作静态生成</strong>;<br><strong>&nbsp;&nbsp;b)到达自动生成时间并访问页面</strong>;<br><strong>&nbsp;&nbsp;c)内容发布;</strong><br>（2）浏览器存在缓存，需要刷新浏览器才能看到效果<p style=text-align:right id=tipA><a href=javascript: onclick=tipAction()>不再提示</a>&nbsp;&nbsp;</p></div>',460,0,100,0,0,1);
		}
	}
	
	
	  
});


<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=自定义标签", "link1=<%=MY_sitePath%>admin/self-definition/edit_label.asp", "title2=自定义页面", "link2=<%=MY_sitePath%>admin/newTmplate/edit_temp.asp?edit=no");

	   }
	);


});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="add_label.asp" class="link">新增标签</a>
                
                <!-- 当前位置 -->
                当前位置： 自定义标签/页面 | 管理标签
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
          
                    <!-- 后台提示 -->
                    
                    <div class="notification information png_bg content-alert">
                        <a href="javascript:" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                        <div>
                        
                        1. 修改自定义标签内容后，您需要手动操作<a href="../root_temp-edit/index.asp">栏目页面静态生成</a>，才能及时看到效果<br>
                        2. 或者等待系统达到<strong>静态自动更新间隔</strong>时间，访问页面时系统即可更新静态页面，您也可以通过导航<strong>网站设置</strong>，设置此时间<br>
                        
                        </div>
                    </div>
                
                    <!-- 独立内容区 -->
                    <div class="singleContentBox">
    
                                
								<%
								ts = "您还没有添加任何自定义标签！"
								
                                set rs=server.createobject("adodb.recordset")
                                sql="select * from [define_label] order by ID desc"
                                rs.open sql,db,1,1
								
                                %>
						
                                    <div class="labelName" id="labelName">
                                        <ul>
                                            <%
                                            do while not rs.eof and not rs.bof
                                            
                                            thisL = rs("labelSymbol")
                                            thisN = rs("lableName")
                                            thisID = MD5(thisL,1)
											
                                            
                                            if len(thisL) > 1 then response.write "<li onMouseOver=""tipShow('#"&thisID&"',0)"" onMouseOut=""tipShow('#"&thisID&"',1)""><a href=""javascript:"" onClick=""editFrame('"&thisL&"','"&thisN&"');""><strong>"&thisN&"</strong></a><span class=""s"">标签："&thisL&"</span>&nbsp;<a id="""&thisID&""" class=closeb onClick=""return window.confirm('确认要删除吗！');"" href=""action.asp?action=del&labelSymbol="&thisL&"""><img border=""0"" title=""删除当前标签"" src=""../../plugins/editInfo_img/4.gif""/></a></li>"&vbcrlf : ts = ""
                                            
                                            rs.movenext
                                            loop
                                            %>
    
                                        </ul>
                                    </div>
                                    
                                <%

								rs.close
								set rs=nothing
								
								response.Write ts
								%>
                                
                                
    
                    </div>
                    
                
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


