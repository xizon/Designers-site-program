<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>

<!-- //=====================================================HTML编辑器  begin  -->
<script>
var Path_uploadFUnFile = "../../plugins/HTMLEdit/_ds_fun/upload.asp";
var Path_saveDraft = "../../plugins/HTMLEdit/_ds_fun/AutoSave_show.asp";  
var Path_saveFunFile = "../../plugins/HTMLEdit/_ds_fun/AutoSaver.asp";
var Path_chkDangerImgFunFile = "../../plugins/HTMLEdit/_ds_fun/chk_dangerImg.asp";
var Path_saveFormItemID = "#lableContent";
</script>
<script type="text/javascript" charset="utf-8" src="../../plugins/HTMLEdit/xheditor.js"></script>
<script type="text/javascript">
$(pageInit);
function pageInit(){
	$.extend(xheditor.settings,{shortcuts:{'ctrl+enter':submitForm}});
	$(Path_saveFormItemID).xheditor({upImgUrl:Path_uploadFUnFile,upImgExt:"jpg,jpeg,gif,png",upFlashUrl:Path_uploadFUnFile,upFlashExt:"swf",upMediaUrl:Path_uploadFUnFile,upMediaExt:"wmv,avi,wma,mp3,mid"});
}
function submitForm(){$('#formM').submit();}

</script>
<!-- //=====================================================HTML编辑器  end  -->



<script>


//表单判断
function checkForm(){
	if(document.formM.labelSymbol.value=="" || document.formM.labelSymbol.value=="${newlabel_}" ){
		ChkFormTip_page("标签不能为空",0);
		return false;
	}
	if(document.formM.labelSymbol.value.indexOf(" ") >= 0){
		ChkFormTip_page("标签不能包含空格",0);
		return false;
	}
	if(document.formM.lableName.value==""){
		ChkFormTip_page("标签名称不能为空",0);
		return false;
	}
	if(/.*[\u4e00-\u9fa5]+.*$/.test(document.formM.labelSymbol.value)){
		ChkFormTip_page("标签标识符不能使用中文",0);
		return false;
	}
	
	//ok
	loadingCreate();
	return true;
		
}

<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=自定义标签", "link1=<%=MY_sitePath%>admin/self-definition/edit_label.asp", "title2=自定义页面", "link2=<%=MY_sitePath%>admin/newTmplate/edit_temp.asp?edit=no");


	   }
	);
	
	
});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="edit_label.asp" class="link">管理标签</a>
                
                <!-- 当前位置 -->
                当前位置： 自定义标签/页面 | 新增标签
            
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
          
          
          <!--//////////////////////////////////////////////增加主要内容标签/////////////////////////////////////////////////////////-->


                <!-- 后台提示 -->
                
                <div class="notification information png_bg content-alert">
                  <a href="#" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        1.您可以增加类似 <strong>${labelName}</strong> 的自定义标签和名称，并把新增标签放置到模板（template文件夹）文件中的任意位置<br>
                        2.新增标签后，您可以使用自定义标签编辑器修改标签的内容，静态生成后前台页面就会显示您增加的标签的内容<strong>(前提是模板文件里有${}标签)</strong><br>
                        3.您可以点击编辑器<img align="absmiddle" src="icon.png">按钮，使用<strong>源代码模式</strong>进行编辑<br>
                        4.修改自定义标签后，您需要手动操作<a href="../root_temp-edit/index.asp">栏目页面静态生成</a>，才能及时看到效果

                    </div>
                </div>
          
            <!-- 表单域  -->
                <div id="contentShow">
                
                            <form name="formM" action="action.asp?action=add"  method="post" onSubmit="javascript:return checkForm();">
                                   
                                        <table width="100%" class="setting" cellspacing="0">
                                            
                                            <!-- 标题 -->                        
                                            <thead class="setHead">
                                                <tr>
                                                       <th  class="setTitle"></th>
                                                       <th colspan="2"></th>
                                                </tr>
                                                
                                            </thead>
                                            
                                         
                                            <!-- 底部 -->
                                            <tfoot>
                                            
                                                <tr>
                                                    <td colspan="3">         
                                                        <div class="btnBox">
                                                            <input type="submit" value="添加标签" class="sub" id="needCreateLoadingBtn">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                            
                                            
                                            <tbody>
                                            
                                               <!-- +++++++  内容   begin +++++++  -->
                    
                                                 <tr>
                                                    <td class="title">标签<a href="javascript:void(0)" class="help" data-info="网站普通标签,用来替换模板里的标签内容,可以在后台<strong>[自定义标签]</strong>中修改"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：<span class="backStage_star">*</span></td>
                                                    <td colspan="2" class="ui-element">
                                                    
                                                        <input   name="labelSymbol" type="text"  size="18" value="${newlabel_}" />
                                                        <br clear="all">
                                                        为了不与网站原有标签重复冲突，强烈建议标签加个前缀例如：${newlabel_about}

                                                    </td>
                                                </tr>  
                                                
                                                 <tr>
                                                    <td class="title">名称：<span class="backStage_star">*</span></td>
                                                    <td colspan="2" class="ui-element">
                                                    
                                                        <input   name="lableName" type="text"  size="18"  value="" class="help" data-info="如果名称中包含 <strong>源代码</strong> 3个字，则管理此标签时会自动切换到源代码模式编辑" />
                                                        <br clear="all">
                                                        例如：业务信息说明[首页]，这样写就知道您增加的标签是属于哪个模板，在哪个模板中是显示哪方面的信息

                                                    </td>
                                                </tr>  
                                                
                                                
                                                 <tr>
                                                    <td class="title">标签详细内容：</td>
                                                   <td colspan="2">
                                                    
                                                         <!-- //=====================================================HTML编辑器  begin -->
                                                     <textarea name="lableContent" id="lableContent" rows="25" cols="80" style="width: 95%; height:220px;"></textarea>
          
                                                        
                                                        <!-- //=====================================================HTML编辑器  end -->

                                                   </td>
                                                </tr> 
                                                

                                              <!-- +++++++  内容   end +++++++  -->
                                        
                                        
                                            </tbody>
                                            
                                        </table>
           
           
                            </form>  
             


                               
                     </div> 
                     


          
                
              
          </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


