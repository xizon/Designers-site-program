<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
'===========================
'cookies
'DateDiff(interval,   date1,   date2[,   firstdayofweek[,   firstweekofyear]])   
'h 小时  m 月  s   秒
'===========================


Response.Cookies("errorNumManageNow")="0"
Response.Cookies("errorNumManageNow").Expires=DateAdd("h",12,now()) 
Application("CommSupUserName")=userName
Application("CommSupUserPass")=userPass
   
if edtSaveDate="" or loginMode="shortcut" or loginMode="iframeLog" then 
  Response.Cookies("cat")=userName   '不保留
  Response.Cookies("catpass")=userPass
  Response.Cookies("catName")=""
  Response.Cookies(thisChkCookieIP)=MD5(randCookieChk,4)
  Response.Cookies("mem_login"&userName)=userName
  Response.Cookies("mem_loginPass_"&userName)=userPass
  Response.Cookies(thisChkCookieIP&userName)=MD5(randCookieChk,4)
  
end if 
if edtSaveDate="1" or loginMode="shortcut" or loginMode="iframeLog" then 
  Response.Cookies("cat")=userName   '保留一天
  Response.Cookies("cat").Expires=DateAdd("h",24,Now()) 
  Response.Cookies("catName").Expires=DateAdd("h",24,Now())
  Response.Cookies("mem_login"&userName)=userName  
  Response.Cookies("mem_login"&userName).Expires=DateAdd("h",24,Now())
  Response.Cookies("mem_loginPass_"&userName)=userPass
  Response.Cookies("mem_loginPass_"&userName).Expires=DateAdd("h",24,Now())
  Response.Cookies(thisChkCookieIP&userName)=MD5(randCookieChk,4)
  Response.Cookies(thisChkCookieIP&userName).Expires=DateAdd("h",24,Now()) 
  Response.Cookies("catpass")=userPass
  Response.Cookies("catpass").Expires=DateAdd("h",24,Now())   
  Response.Cookies(thisChkCookieIP)=MD5(randCookieChk,4)
  Response.Cookies(thisChkCookieIP).Expires=DateAdd("h",24,Now()) 

end if
if edtSaveDate="30" or loginMode="shortcut" or loginMode="iframeLog" then 
  Response.Cookies("cat")=userName   '保留一个月
  Response.Cookies("cat").Expires=DateAdd("m",1,Now())
  Response.Cookies("catName").Expires=DateAdd("m",1,Now())
  Response.Cookies("mem_login"&userName)=userName  
  Response.Cookies("mem_login"&userName).Expires=DateAdd("m",1,Now())
  Response.Cookies("mem_loginPass_"&userName)=userPass
  Response.Cookies("mem_loginPass_"&userName).Expires=DateAdd("m",1,Now())
  Response.Cookies(thisChkCookieIP&userName)=MD5(randCookieChk,4)
  Response.Cookies(thisChkCookieIP&userName).Expires=DateAdd("m",1,Now()) 
  Response.Cookies("catpass")=userPass
  Response.Cookies("catpass").Expires=DateAdd("m",1,Now())
  Response.Cookies(thisChkCookieIP)=MD5(randCookieChk,4)
  Response.Cookies(thisChkCookieIP).Expires=DateAdd("m",1,Now())
end if
if edtSaveDate="365" or loginMode="shortcut" or loginMode="iframeLog" then 
  Response.Cookies("cat")=userName   '保留一年
  Response.Cookies("cat").Expires=DateAdd("m",12,now()) 
  Response.Cookies("catName").Expires=DateAdd("m",12,now())   
  Response.Cookies("mem_login"&userName)=userName  
  Response.Cookies("mem_login"&userName).Expires=DateAdd("m",12,now()) 
  Response.Cookies("mem_loginPass_"&userName)=userPass
  Response.Cookies("mem_loginPass_"&userName).Expires=DateAdd("m",12,now())
  Response.Cookies(thisChkCookieIP&userName)=MD5(randCookieChk,4)
  Response.Cookies(thisChkCookieIP&userName).Expires=DateAdd("m",12,Now())   
  Response.Cookies("catpass")=userPass
  Response.Cookies("catpass").Expires=DateAdd("m",12,now()) 
  Response.Cookies(thisChkCookieIP)=MD5(randCookieChk,4)
  Response.Cookies(thisChkCookieIP).Expires=DateAdd("m",12,now()) 
end if  
%>