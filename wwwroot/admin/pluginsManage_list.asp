<!--#include file="function/dsMainFunction.asp" -->
<!--#include file="checkFun_mem.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
<meta name="generator" content="Designers ' Site 全站静态系统" /> 
<title>Designer's site Program 后台管理面板</title>
<script src="../cat_js/template.js"  type="text/javascript"></script>
<script src="../cat_js/jquery.min.js"  type="text/javascript"></script>
<script src="../cat_js/jquery.wbox.js"            type="text/javascript"></script>
<script src="../cat_js/manageComm.js"  type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="_manage_css/manage.css" />

<!-- form ui[js&css]  begin -->
<script type="text/javascript" src="../plugins/d-s-management-ui/jquery.ui.custom.min.js" ></script>
<script type="text/javascript" src="../plugins/d-s-management-ui/d-s.ui.comm.js" ></script>
<link rel="stylesheet" type="text/css" href="../plugins/d-s-management-ui/jquery.ui.custom.css" />
  <!-- form ui[js&css]  end  -->
        
<!--[if lt IE 7 ]>
    <script src="../cat_js/png.js"  type="text/javascript"></script>
    <script>
        DD_belatedPNG.fix('img, a, span, h3, h1, li, form, input,section, #topStatus-info, #userGuide_mask, #userGuide-1,#userGuide-2,#userGuide-3,#userGuide-4,#userGuide-5,#userGuide-6,#userGuide-7,#userGuide-8');
        document.execCommand("BackgroundImageCache", false, true); 
    </script>
<![endif]-->


</head>

<body>

<!--#include file="_ds_updateReferences.asp"-->
<div id="pluginsMenuListBox">
    <ul class="pluginsMenuList">
        <%
        set rs_rplugin =server.createobject("adodb.recordset")
        sql_rplugin="select * from App order by ID asc" 
        rs_rplugin.open sql_rplugin,db,1,1 
        if rs_rplugin.bof and rs_rplugin.eof then
        
            response.Write "暂无..."&vbcrlf
            
                
        else
            do while not rs_rplugin.eof and not rs_rplugin.bof
            
        
            response.Write "<li><a href='"&rs_rplugin("appURL")&"' id='"&rs_rplugin("appID")&"' target='mainPage'>"&rs_rplugin("appName")&"</a></li>"&vbcrlf
            
            rs_rplugin.movenext
            loop	
        
    
        
        end if
    
        rs_rplugin.close
        set rs_rplugin=nothing
        
        %>

        <!--#include file="plugins/cloud/dir.asp" -->
    </ul>
</div>
<script>
$(document).ready(function(){
	$(".columnBtn",window.parent.document).css("visibility","hidden");
	$(".columnBtn",window.parent.document).stop().animate({ marginLeft: (GetWidth() + 200) + "px" }, {queue:false,duration:10});
	setTimeout(function(){
		$(".columnBtn",window.parent.document).stop().animate({ marginLeft: "-170px" }, {queue:false,duration:10});
	},1500);

});
</script>
<style>

/*插件导航*/
body{background:#fff; }
#pluginsMenuListBox{height:180px; overflow-y:auto}
.pluginsMenuList ul{margin:10px; list-style:none;}
.pluginsMenuList li{list-style:none; display:block; float:left; margin-right:30px; margin-bottom:7px; text-align:center; width:100px; overflow:hidden; text-indent:0}
.pluginsMenuList li a{background:#666; border:1px #CCC solid; padding:3px 6px 3px 4px; text-shadow:none; text-decoration:none; color:#fff; display:block; width:90px; height:22px; line-height:22px; margin-top:10px; text-indent:0}
.pluginsMenuList li a:hover{ color:#fff; background:#000; text-decoration:none}	

</style>
</body>
</html>