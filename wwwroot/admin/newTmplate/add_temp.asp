<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../checkFun_noSup.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
step=request.querystring("step")
newUrl=request.querystring("newUrl")
thisURL=request.querystring("thisURL")
tempPath=request.querystring("tempPath")
%>

<!-- //=====================================================HTML编辑器  begin  -->
<script>
var Path_uploadFUnFile = "../../plugins/HTMLEdit/_ds_fun/upload.asp";
var Path_saveDraft = "../../plugins/HTMLEdit/_ds_fun/AutoSave_show.asp";  
var Path_saveFunFile = "../../plugins/HTMLEdit/_ds_fun/AutoSaver.asp";
var Path_chkDangerImgFunFile = "../../plugins/HTMLEdit/_ds_fun/chk_dangerImg.asp";
var Path_saveFormItemID = "#lableContent";
</script>
<script type="text/javascript" charset="utf-8" src="../../plugins/HTMLEdit/xheditor.js"></script>
<script type="text/javascript">
$(pageInit);
function pageInit(){
	$.extend(xheditor.settings,{shortcuts:{'ctrl+enter':submitForm}});
	$(Path_saveFormItemID).xheditor({upImgUrl:Path_uploadFUnFile,upImgExt:"jpg,jpeg,gif,png",upFlashUrl:Path_uploadFUnFile,upFlashExt:"swf",upMediaUrl:Path_uploadFUnFile,upMediaExt:"wmv,avi,wma,mp3,mid"});
}
function submitForm(){$('#formM').submit();}

</script>
<!-- //=====================================================HTML编辑器  end  -->


<!-- 代码高亮   begin -->
    <link rel="stylesheet" href="../../plugins/editorHighlighter/lib/codemirror.css">
    <script src="../../plugins/editorHighlighter/lib/codemirror.js"></script>
    <script src="../../plugins/editorHighlighter/mode/xml.js"></script>
        <style type="text/css">.CodeMirror { border:4px solid #E1E1E1}</style>
<!-- 代码高亮  end  -->

<script>



<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=自定义标签", "link1=<%=MY_sitePath%>admin/self-definition/edit_label.asp", "title2=自定义页面", "link2=<%=MY_sitePath%>admin/newTmplate/edit_temp.asp?edit=no");


	   }
	);
	
	
});

<!-- form ui[js]  end -->

</script>

<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="edit_temp.asp?edit=no" class="link">管理页面</a>
                
                <!-- 当前位置 -->
                当前位置： 自定义标签/页面 | 新增页面
            
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
          
          
          
          <!--//////////////////////////////////////////////增加主要内容标签/////////////////////////////////////////////////////////-->
          
          <%if step = "addLabel" then%>
          
				<script>
                //表单判断
                function checkForm(){
                    if(document.formM.labelSymbol.value=="" || document.formM.labelSymbol.value=="${newlabel_}" ){
                        ChkFormTip_page("标签不能为空",0);
                        return false;
                    }
					if(document.formM.labelSymbol.value.indexOf(" ") >= 0){
						ChkFormTip_page("标签不能包含空格",0);
						return false;
					}
                    if(document.formM.lableName.value==""){
                        ChkFormTip_page("标签名称不能为空",0);
                        return false;
                    }
                    if(/.*[\u4e00-\u9fa5]+.*$/.test(document.formM.labelSymbol.value)){
                        ChkFormTip_page("标识符不能使用中文",0);
                        return false;
                    }
                    
                    //ok
                    loadingCreate();
                    return true;
                        
                }
                </script>
          
          
                <!-- 后台提示 -->
                
                <div class="notification information png_bg content-alert">
                  <a href="#" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                      <strong class="guideTitle">【第一步】增添您想要新建模板里的一个 [主要内容自定义标签] , 格式: ${labelName} </strong><br>
                        1)把这个标签放到您新建的模板的HTML代码中，您就可以在<strong>[自定义标签]</strong>中进行管理<br>
                        2)这个新建标签的内容就是您想在新建页面里<strong>可以后台修改的主要内容</strong><br>
                        <hr>
                        您还可以直接在template/的模板文件里使用依赖标签 <strong>{#body:columnShow.CustomPage[序号]} 页面内容 {/body:columnShow.CustomPage[序号]}</strong>，[序号]即1,2,3,4,5,6... <a href="<%=MY_sitePath%>皮肤主题自定义API教程文档.html" target="_blank">具体用法可以参看这里</a><br>
                        PS:使用这种方法创建独立页面您也可以使用"自定义标签"插入到模板里来进行内容管理
                                                
                    </div>
                </div>
          
            <!-- 表单域  -->
                <div id="contentShow">
                
                                        <form name="formM" action="action.asp?action=addLabel"  method="post" onSubmit="javascript:return checkForm();">
                                               
                                                    <table width="100%" class="setting" cellspacing="0">
                                                        
                                                        <!-- 标题 -->                        
                                                        <thead class="setHead">
                                                            <tr>
                                                                   <th  class="setTitle"></th>
                                                                   <th colspan="2"></th>
                                                            </tr>
                                                            
                                                        </thead>
                                                        
                                                     
                                                        <!-- 底部 -->
                                                        <tfoot>
                                                        
                                                            <tr>
                                                                <td colspan="3">         
                                                                    <div class="btnBox">
                                                                        <input type="submit" value="继续" class="sub" id="needCreateLoadingBtn">
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tfoot>
                                                        
                                                        
                                                        <tbody>
                                                        
                                                           <!-- +++++++  内容   begin +++++++  -->
                                
                                                             <tr>
                                                                <td class="title">标签<a href="javascript:void(0)" class="help" data-info="网站普通标签,用来替换模板里的标签内容,可以在后台<strong>[自定义标签]</strong>中修改"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：<span class="backStage_star">*</span></td>
                                                                <td colspan="2" class="ui-element">
                                                                
                                                                    <input   name="labelSymbol" type="text"  size="18" value="${newlabel_}" />
                                                                    <br clear="all">
                                                                    为了不与网站原有标签重复冲突，强烈建议标签加个前缀例如：${newlabel_about}

                                                                </td>
                                                            </tr>  
                                                            
                                                             <tr>
                                                                <td class="title">名称：<span class="backStage_star">*</span></td>
                                                                <td colspan="2" class="ui-element">
                                                                
                                                                    <input   name="lableName" type="text"  size="18"  value="" />
                                                                    <br clear="all">
                                                                    例如：业务信息说明[首页]，这样写就知道您增加的标签是属于哪个模板，在哪个模板中是显示哪方面的信息

                                                                </td>
                                                            </tr>  
                                                            
                                                            
                                                             <tr>
                                                                <td class="title">标签详细内容：</td>
                                                               <td colspan="2">
                                                                
                                                                     <!-- //=====================================================HTML编辑器  begin -->
                                                                 <textarea name="lableContent" id="lableContent" rows="25" cols="80" style="width: 95%; height:220px;"></textarea>

                                                                    
                                                                    
                                                                    <!-- //=====================================================HTML编辑器  end -->

                                                               </td>
                                                            </tr> 
                                                            
           
                                                          <!-- +++++++  内容   end +++++++  -->
                                                    
                                                    
                                                        </tbody>
                                                        
                                                    </table>
                       
                       
                                        </form>  
             


                               
                     </div> 
                     
                     
                     <%end if%>
                     
                     <!--/////////////////////////////////////////增加地址//////////////////////////////////////////////////////////////-->
                     
                     
					  <%if step = "addUrl"  then%>
                      
                            <script>
                            //表单判断
                            function checkForm(){
                                if(document.formM.url.value==""){
                                    ChkFormTip_page("页面地址不能为空",0);
                                    return false;
                                }
								if(document.formM.url.value.indexOf(" ") >= 0){
									ChkFormTip_page("页面地址不能包含空格",0);
									return false;
								}
                                if(document.formM.title.value==""){
                                    ChkFormTip_page("模板的名称不能为空",0);
                                    return false;
                                }
                                if(/.*[\u4e00-\u9fa5]+.*$/.test(document.formM.url.value)){
                                    ChkFormTip_page("页面地址不能使用中文",0);
                                    return false;
                                }
                                
                                //ok
                                loadingCreate();
                                return true;
                                    
                            }
                            </script>
                      
                      
                            <!-- 表单域  -->
                            <div id="contentShow">
                            
<form name="formM" action="action.asp?action=addUrl" method="post" onSubmit="javascript:return checkForm();">
                                                           
                                                                <table width="100%" class="setting" cellspacing="0">
                                                                    
                                                                    <!-- 标题 -->                        
                                                                    <thead class="setHead">
                                                                        <tr>
                                                                               <th  class="setTitle"></th>
                                                                               <th colspan="2"></th>
                                                                        </tr>
                                                                        
                                                                    </thead>
                                                                    
                                                                 
                                                                    <!-- 底部 -->
                                                                    <tfoot>
                                                                    
                                                                        <tr>
                                                                            <td colspan="3">         
                                                                                <div class="btnBox">
                                                                                    <input type="submit" value="继续" class="sub" id="needCreateLoadingBtn">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tfoot>
                                                                    
                                                                    
                                                                    <tbody>
                                                                    
                                                                       <!-- +++++++  内容   begin +++++++  -->
                                            
                                                                         <tr>
                                                                            <td class="title">页面地址：<span class="backStage_star">*</span></td>
                                                                            <td colspan="2">
                                                                            
                                                                              <span style="font-size:14px; font-weight:bold;"> <%=MY_secCheckSiteLink%>/<input name="url"  value=""  type="text"  size="15">.html</span>
            
                                                                           </td>
                                                                        </tr>  
                                                                        
                                                                         <tr>
                                                                            <td class="title">新建的模板名称：<span class="backStage_star">*</span></td>
                                                                            <td colspan="2">
                                                                            
                                                                                <input name="title"  value=""  type="text"  size="22">
                                                                                <br clear="all">
                                                                                建议使用自己易懂的文字或英文,不要加特殊符号和其他符号息
            
                                                                           </td>
                                                                        </tr>  
                                                                        
                                                                   
                       
                                                                      <!-- +++++++  内容   end +++++++  -->
                                                                
                                                                
                                                                    </tbody>
                                                                    
                                                                </table>
                                   
                                   
                                                    </form>
                         
            
            
                                           
                     </div> 
                     
                     
                     <%end if%>
                     
                     
                     <!--////////////////////////////////////////////////增加模板///////////////////////////////////////////////////////-->
                     
					  <%if step = "addTemp"  then%>
                      
                            <script>
                            //表单判断
                            function checkForm(){
                                //ok
                                loadingCreate();
                                return true;
                                    
                            }
                            </script>
                      
                      
                            <!-- 后台提示 -->
                            
                            <div class="notification information png_bg content-alert">
                                <a href="#" class="close"><img src="../../plugins/d-s-management-ui/img/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                <div>
                                    <strong class="guideTitle">【最后一步】编写新建页面模板的源代码</strong><br>
                                    1)您可以把刚刚创建的标签 <strong><%=request.Cookies("labelSymbol")%></strong> 加入到源代码里<br>
                                    2)您也可以拷贝template里的某个类似模板的源代码，或者编写新的HTML源代码<br>
                                    3)您也可以插入<strong>通用栏目信息标签</strong>到源码里使用,详细标签<a href="<%=MY_sitePath%>皮肤主题自定义API教程文档.html" target="_blank">请参看这里</a><br>
                                    4)新建模板页面对依赖标签无效,,形如 {#body:article} {/body:article} 类型的标签就属于依赖标签
                                    <!--[if lt IE 7 ]> <br><span class="backStage_star">您当前使用IE6浏览器，不支持代码高亮编辑，建议升级您的浏览器或者更换其它浏览器！</span> <![endif]-->
                                                            
                                </div>
                            </div>
                                  
                            <!-- 表单域  -->
                            <div id="contentShow">
                            
<form name="formM" action="action.asp?action=addTemp&thisURL=<%=request.Cookies("thisNewPageUrl")%>&tempPath=<%=tempPath%>" method="post" onSubmit="javascript:return checkForm();">
                                                           
                                                                <table width="100%" class="setting" cellspacing="0">
                                                                    
                                                                    <!-- 标题 -->                        
                                                                    <thead class="setHead">
                                                                        <tr>
                                                                               <th  class="setTitle"></th>
                                                                               <th colspan="2"></th>
                                                                        </tr>
                                                                        
                                                                    </thead>
                                                                    
                                                                 
                                                                    <!-- 底部 -->
                                                                    <tfoot>
                                                                    
                                                                        <tr>
                                                                            <td colspan="3">         
                                                                                <div class="btnBox">
                                                                                    <input type="submit" value="提交完成" class="sub" id="needCreateLoadingBtn">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tfoot>
                                                                    
                                                                    
                                                                    <tbody>
                                                                    
                                                                       <!-- +++++++  内容   begin +++++++  -->
                                            
                                                                         <tr>
                                                                           
                                                                           <td colspan="3">
                                                                            
                                                                             <textarea name="tmpCode" id="tmpCode"  cols="93" rows="25" style="width:98.5%;"></textarea>
            
                                                                            </td>
                                                                        </tr>  
                                                                        
                       
                                                                      <!-- +++++++  内容   end +++++++  -->
                                                                
                                                                
                                                                    </tbody>
                                                                    
                                                                </table>
                                   
                                   
                                                    </form>
                         
                         
													<script>
													
													
														$(function(){
															
																													
															//for IE6
															if ($.browser.msie && $.browser.version < 7) {
														
															}else{
																
																  var editor = CodeMirror.fromTextArea(document.getElementById("tmpCode"), {
																	mode: {name: "xml", alignCDATA: true},
																	lineNumbers: true,
																	//width: '100%',
																	//height: '100%',
																	textWrapping: false,
																	autoMatchParens: true
																  });
															}
														

															$(document).ready(function() {
															
																 $(".CodeMirror-scroll,.CodeMirror").animate({height: window.screen.height - 335 + "px"},{queue:false,duration:560});
															
															}); 
															
														
															
														});
                                                    </script>
                         
            
            
                                           
                     </div> 
                     
                     
                     <%end if%>

                     
                     <!--////////////////////////////////////////完成添加///////////////////////////////////////////////////////////////-->
                     
                     
					<%if step = "ok"  then%>
                    
                    <!-- 独立内容区 -->
                    <div class="singleContentBox">
                    
                        <p class="clear"></p><p class="clear"></p>
                        
                        <iframe src="../root_temp-edit/ClearCache.asp" style="display:none"></iframe>
                        <iframe src="../root_temp-edit/newTemplate_show.asp" style="display:none"></iframe>
                            
                        <img src="../../plugins/d-s-management-ui/img/status-y.GIF">&nbsp;操作完毕，您已经成功添加 <a href="<%=thisURL%>" target="_blank"><%=thisURL%></a> ！&nbsp;5-10秒后页面生成后可浏览
                    
                    </div>  
                    
                    <%end if%>
                     
   

          
                
              
          </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->


