<!--#include file="../function/dsMainFunction.asp" -->
<!--#include file="../root_temp-edit/set_config.asp" -->
<!--#include file="../root_temp-edit/_commInfo.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================

'是否生成列表页
if callCommInfoChk("{#loopListContentShow:message/}") = true then


	'列表页通用信息js调用判断
	sideBarCommInfoChk = 1
	
	'缓存内容页面
	createFolder("../_contentCache/")
	thisFilePath = "../_contentCache/list_msg.txt"
	
	if FileExistsStatus(thisFilePath) = 1 then 
	
	
	response.Write DY_LANG_103
	
	
	else
	
	
	titleLimitLen = getTempLablePara_siteConfig("标题字符数截取(留言者姓名)")
	titleLimitLen_msg_ListNum = getTempLablePara_siteConfig("内容输出列表数量(留言)")
	
	
	HTMLSERVE_HEAD()
	createFolder("../../message")
	classlistName="message"
		
	'------分页循环
	sql="select * from [message]"
	set rs=server.createObject("ADODB.Recordset")
	rs.open sql,db,1,1
	
	num=titleLimitLen_msg_ListNum
	rs.pagesize=num
	totalpage=rs.pagecount
	rs.close
	set rs=nothing
	
	
	'指定更新页数
	if MY_batchPageCreateHtmlStatus = 0 then 
		UpPageNum = 1
		'分页页面生成采用倒序定位
		thisPageBegin = 1
		if thisPageBegin > totalpage - 1 then thisPageBegin = totalpage - 1
		betweenPageBegin = thisPageBegin : totalpageShowCreate=totalpage - (betweenPageBegin-1)'定位参数
	
		
	else
		UpPageNum = cint(MY_betweenPageEnd) : if UpPageNum > totalpage then UpPageNum = totalpage
		'分页页面生成采用倒序定位
		thisPageBegin = cint(MY_betweenPageBegin)
		if thisPageBegin > totalpage - 1 then thisPageBegin = totalpage - 1
		betweenPageBegin = thisPageBegin : totalpageShowCreate=totalpage - (betweenPageBegin-1)'定位参数
	
		
	end if
	
	'////获取并判断动态模板 begin
	thisTempCodePath=""&MY_sitePath&"admin/_temp/msg_"&tempModifiedDate&".html"
	if FileExistsStatus(thisTempCodePath) = -1 then '不存在
	'开始执行依赖标签功能
	'==================
			
			
			Template_Code_batch=showFile(DesignerSite_tempPath_common)
			'=============================模板依赖标签不存在判断   begin
			
			'模板主注释，参数不用变更，默认全部清除
			Template_Code_trip = dependentLabelFun(Template_Code_batch,"note:template",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"note:commCreateInfo",0,"","","")
			
			
			'【必选其一】各栏目标题依赖标签,必选其一设置为参数 1
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:commonUse.title",0,"","","") '通用标题
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:content.article",0,"","","")	 '内容页-文章 
	
	
			'列表页
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.article",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.works",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.about",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.mood",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.search",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.links",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"title:columnShow.message",1,"","","")
			'【必选其一】各栏目meta描述依赖标签,必选其一设置为参数 1 
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:comm.desc",1,"","","") '通用网站描述
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:content.articleDesc",0,"","","")'内容页-文章 
	
			
			'【必选其一】各栏目meta关键词依赖标签,必选其一设置为参数 1 
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:commonUse.keywords",1,"","","")'其他通用页
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:home",0,"","","")'首页
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"meta:content",0,"","","")'内容页关键词
			
			
			'RSS订阅，可分配在任何页面
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"rss:commonUse.article",0,"","","") 
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"rss:commonUse.article.class",0,"","","") 
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"atom:commonUse.article",0,"","","") 
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"atom:commonUse.article.class",0,"","","")
			
			'banner轮换，可分配在任何页面
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.banner",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"head:commonUse.banner",0,"","","")
			
			'搜索框，订阅图标，置顶推荐文章，最新案例作品，规定数量随机tags输出，可分配在任何页面
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.searchBoard",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.articleFeed",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.article.top",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.works.list",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:commonUse.article.tags.random",0,"","","")
			
			
			'内容页SEO可选link
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"head:content.linkRel",0,"","","")
			
			
			
			'侧边栏
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"sideBar:home",0,"","","")  '首页
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"sideBar:content.comm",0,"","","") '内容页
			Template_Code_trip = getTempLablePara_siderBar_list("留言列表侧边栏",1,"body:columnShow.message")
			Template_Code_trip = dependentLabelFun_para(Template_Code_trip,"留言列表侧边栏",0,"body:columnShow.message")
			
			'各栏目的列表页面
			
			'文章
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.article",0,"","","//") '栏目必选
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article.class",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.article.tags",0,"","","")  
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.article",0,"/*","*/","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.article.class",0,"/*","*/","")
			
			
			'作品案例
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.worksIe6Js",0,"","","") '栏目必选
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.works",0,"","","//")   '栏目必选
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.works",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.works.class",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.works",0,"/*","*/","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.works.class",0,"/*","*/","")
			
			
		
			'微博
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.mood",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:content.js.mood",0,"/*","*/","")
			
	
			'关于我们
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.about",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.about",0,"/*","*/","")
			
			
			'搜索
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.search",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.searchResult",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.search",0,"/*","*/","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.searchResult",0,"/*","*/","")
			
			'友情链接
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.links",0,"","","")	  
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.links",0,"/*","*/","")
			
			'留言
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.message",1,"","","//")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.message",1,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.message",1,"/*","*/","")
			
			'首页
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:columnShow.home",0,"","","//")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:columnShow.home",0,"","","")
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:columnShow.home",0,"/*","*/","")
			
			
			'内容页
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.js.comm.form",1,"","","")	 '栏目必选  	
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.show.commJs",0,"","","")'栏目必选   	
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"body:content.show.article",0,"","","")	 						 
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"autoUpdateTemp:content.js.article",0,"/*","*/","") 
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:content.js.comm",0,"","","//")		 
			Template_Code_trip = dependentLabelFun(Template_Code_trip,"js:content.article",0,"","","//")
			
			
			%>
			<!--#include file="../root_temp-edit/_clearLabel_comm.asp" -->
			<%
	  
	
			'==================
			Template_Code_Now = Template_Code_trip		
			CreateFile thisTempCodePath,Template_Code_Now
			
	 
	end if
	'=============================模板依赖标签不存在判断   end
	
	
	
	sql2="select * from [message]"
	set rs2=server.createObject("ADODB.Recordset")
	rs2.open sql2,db,1,1
	
	'判断是否有留言
	if not rs2.EOF then
		noMsg = 0
	else
		noMsg = 1 '不存在
		UpPageNum = 1
		
	end if
	
	rs2.close
	set rs2=nothing
	
	
	
	
	for j=betweenPageBegin to UpPageNum
	
	Template_Code=showFile(thisTempCodePath)
	'只调用栏目独立HTML代码判断
	callPageAllHTML "body:columnShow.message","完全独立页面代码.不调用其它任何依赖标签的内容"
	
	'------循环开始
	sql="select * from [message]  order by messageid desc"
	set rs=server.createObject("ADODB.Recordset")
	rs.open sql,db,1,1
	
	
	whichpage=j 
	rs.pagesize=num
	listnum=rs.RecordCount
	totalpage=rs.pagecount
	rs.absolutepage=whichpage
	howmanyrecs=0
	
	listcontent=""  '这里对下次标签内容的清空，重要！
	listcontent_msg=""
	
	do while not rs.eof and howmanyrecs<rs.pagesize
	
	'++++++++++++++++++++++++++++++++++++++++++++++++内容页
		
		
	
		url=replace(replace(replace(HTMLDecode(rs("url")),"http",""),"https",""),"://","")
		Subdate=rs("Subdate")
		UserName=replace(rs("UserName"),chr(32),"")
		UserName_show=gotTopic(UserName,titleLimitLen)
		content=rs("comment")
		reply=rs("reply")
		stateMsg=rs("state")
		UIP=rs("UIP")
		thisPic=rs("pic")
		view=rs("view")
		shield=""&DY_LANG_45&""&vbcrlf	
		hiddenMsg=""&DY_LANG_46&""&vbcrlf
		commID=rs("messageid")
	
		  if instr(UIP,".") > 0 then
			  UIPN = split(UIP, ".") 
			  UIPShow = UIPN(0)& "."&UIPN(1)& ".*.* "
		  else
			  UIPShow = UIP
		  end if
		
		
		if reply<>"" then replyMSG="<hr /><div class=""t1"" align=""left""><span class=""t2"">"&name_Now&""&DY_LANG_44&"</span>"+reply+"</div>"  else replyMSG=""
		
		
		commNum = "<div class=""commNum"">"&replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(commID,"1","<span class=""comm_num1""></span>"),"2","<span class=""comm_num2""></span>"),"3","<span class=""comm_num3""></span>"),"4","<span class=""comm_num4""></span>"),"5","<span class=""comm_num5""></span>"),"6","<span class=""comm_num6""></span>"),"7","<span class=""comm_num7""></span>"),"8","<span class=""comm_num8""></span>"),"9","<span class=""comm_num9""></span>"),"0","<span class=""comm_num0""></span>")&"</div>"
	
		
		delControl="<h6 id=""m"&commID&"""><script>msgShowHtmlEdit('"&commID&"','msg');</script></h6>"&vbcrlf		
		
		
		
		if view="0" and stateMsg<>"1" then
		
			vShow = ""&content&""&replyMSG&""&commNum&""&delControl&""
		
		
		elseif view="1" and stateMsg<>"1" then
		
		
			vShow = "<span class=""t1"">"&shield&"</span>"&commNum&""&delControl&""
		
		
		elseif view="0" and stateMsg="1" then 
		
			vShow = ""&hiddenMsg&""&replyMSG&""&commNum&""&delControl&""
	
		else 
		
			vShow = "<span class=""t1"">"&shield&"</span>"&commNum&""&delControl&""
		
		end if
		
		
		
			listcontent_msg=listcontent_msg&loopTempLabelString("loopList:message","{#loopVar:message.commID/}",""&commID&"","{#loopVar:message.url/}",""&url&"","{#loopVar:message.UserName/}",""&UserName&"","{#loopVar:message.DY_LANG_48/}",""&DY_LANG_48&"","{#loopVar:message.thisPic/}",""&thisPic&"","{#loopVar:message.DY_LANG_47/}",""&DY_LANG_47&"","{#loopVar:message.subdate/}",""&subdate&"","{#loopVar:message.UIP/}",""&UIPShow&"","{#loopVar:message.vShow/}",""&vShow&"","","","","","","","","","","","","","","","","","","","","","","",0)
	
	'++++++++++++++++++++++++++++++++++++++++++++++++
	
	rs.movenext
	howmanyrecs=howmanyrecs+1
	loop
	
	rs.close
	set rs=nothing
	
	
	
	'///////////////////////分页输出
	AbsPage=""
	
	urlPath="../"&classlistName&"/"&classlistName&""
	%>
	<!--#include file="../root_temp-edit/_list_page.asp" -->
	<!--#include file="../root_temp-edit/comm_getSideData.asp" -->
	<%
	
	
	Template_Code=replace(Template_Code,"{#loopListContentShow:message/}",listcontent_msg&listcontent)
	
	filename="../../"&classlistName&"/"&classlistName&"_"&totalpageShowCreate&".html"
	'Seo
	thisSeoPath=replace(replace(filename,"../../",MY_sitePath),"../",MY_sitePath)
	Template_Code=replace(Template_Code,"${thisPageSEOURL}",thisSeoPath)
	
	totalpageShowCreate=totalpageShowCreate-1
	
	'----------有留言信息
	if noMsg = 0 then
		
		
		if totalpageShowCreate > 0 or totalpageShowCreate = 0 then  CreateFile filename,Template_Code&createData&DS_CopyrightVerificationInfo
		
		
	end if
	
	
	
	
	
	'判断有无内容
	if totalpage<1 then 
	response.Write "<span class=""OKinfo"">×出现错误→此栏目没有内容...</span><br>"
	end if
	
	next
		
	
	'----------有数据
	if noMsg = 0 then
		
		
		CopyFile "../../"&classlistName&"/"&classlistName&"_"&totalpage&".html","../../"&classlistName&"/"&classlistName&"_default.html"
		Template_Code_default=replace(showFile("../../"&classlistName&"/"&classlistName&"_default.html"),classlistName&"/"&classlistName&"_"&totalpage&".html",classlistName&"/"&classlistName&"_default.html")
		CreateFile "../../"&classlistName&"/"&classlistName&"_default.html",Template_Code_default
			
		
	end if
	
	
	'----------没有数据
	if noMsg = 1 then
	
		Template_Code_no=replace(Template_Code,"<span id=""pageShow""></span>", DY_LANG_102)
		
		CreateFile "../../"&classlistName&"/"&classlistName&"_default.html",Template_Code_no&createData
	
	end if
	
	
	
	'页面缓存
	CreateFile thisFilePath,now()
	
			
	HTMLSERVE_BOTTOM()
	
	
	end if
	
	
else
    response.Redirect "message_action_nolist.asp"

end if
%>
