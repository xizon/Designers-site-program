<!--#include file="../admin/function/dsMainFunction.asp" -->
<!--#include file="../admin/root_temp-edit/_commInfo.asp" -->
<!--#include file="../admin/function/include/fun_api.asp" -->
<!--#include file="../admin/function/Md5.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'    DS系统API接口，返回json数据类型
'
'=====================================================================

Const DS_APP_VERSION = "1.0" '系统API版本(使用后台升级功能后API将会同步升级)
webIDTrue = Application("CommWebID") '获取API接口网站唯一识别ID

'对应表相关的数据查询
'----------
'（通用）
API_KEY = request.QueryString("API_KEY") '当您使用社会化登录，社会化评论接入到网站同步网站时，就需要API-KEY
apiSQLKey = request.QueryString("apiSQLKey") '信息密匙(自主指定)
apiInfoAddKey = request.QueryString("apiInfoAddKey") '信息添加密匙(自主指定)
webidKey = Checkxss(request.QueryString("webidKey")) '获取API接口网站唯一识别ID
openData = Checkxss(request.QueryString("openData")) '是否查询数据
columnType = Checkxss(request.QueryString("columnType"))'栏目表
user = Checkxss(request.QueryString("user"))'用户
appAction = request.QueryString("appAction")'app对接处理
appid = request.QueryString("appid")'app对接id
result = request.QueryString("result")'app对接返回值
WEB_APIKEY = MD5(MY_API_SQLKEY,5)&MD5(MY_API_InfoAddKEY,5)


'----------
'（限输出一条）
infoTitle = request.QueryString("infoTitle") '标题
infoID = Checkxss(request.QueryString("infoID")) 'ID
'----------
infoClass = request.QueryString("infoClass") '筛选类别
infoKeyword = Checkxss(request.QueryString("infoKeyword")) '关键词同类查询
infoEchoNum = Checkxss(request.QueryString("infoEchoNum")) '输出数量
infoScanType = Checkxss(request.QueryString("infoScanType")) '浏览类型选择
'----------
'（栏目参数接收传值,最多接受10个参数）
cp1 = HTMLEncode(request.QueryString("cp1"))
cp2 = HTMLEncode(request.QueryString("cp2"))
cp3 = HTMLEncode(request.QueryString("cp3"))
cp4 = HTMLEncode(request.QueryString("cp4"))
cp5 = HTMLEncode(request.QueryString("cp5"))
cp6 = HTMLEncode(request.QueryString("cp6"))
cp7 = HTMLEncode(request.QueryString("cp7"))
cp8 = HTMLEncode(request.QueryString("cp8"))
cp9 = HTMLEncode(request.QueryString("cp9"))
cp10 = HTMLEncode(request.QueryString("cp10"))
'----------

'----------app处理返回
if appAction = "true" and appid = "a" and result = "ok"  then response.Write session("resultNow") : response.End()
if appAction = "true" and appid = "a" and result = "error10"  then apiError(10)
if appAction = "true" and appid = "a" and result = "error11"  then apiError(11)
if appAction = "true" and appid = "l" and result = "ok" and API_KEY = WEB_APIKEY  then response.Write session("resultNowSocialLogin") : response.End()
if appAction = "true" and appid = "l" and  API_KEY <> WEB_APIKEY  then apiError(12)
if appAction = "true" and appid = "l" and result = "error12"  then apiError(12)
if appAction = "true" and appid = "m" and result = "ok" and API_KEY = WEB_APIKEY  then response.Write session("resultNowSocialContact") : response.End()
if appAction = "true" and appid = "m" and  API_KEY <> WEB_APIKEY  then apiError(12)
if appAction = "true" and appid = "m" and result = "error12"  then apiError(12)

'通用判断
commAPIEcho()


' ============================================================================
'API类型： 社会化登录同步接口
' ============================================================================
If spiSqlChk = 0 and openData = "true" and columnType = "sociallogin" and  API_KEY = WEB_APIKEY then

	   '接口同步
	   response.Write "正在转入同步登录接口..."
	   Response.AddHeader "refresh","1;URL="&MY_secCheckSiteLink&"/admin/admin_login/login_API.asp?API_ADD=1&API_KEY="&API_KEY&"&user="&user&""

End If


' ============================================================================
'API类型： 社会化留言同步录入接口
' ============================================================================
If spiSqlChk = 0 and openData = "true" and columnType = "socialcontact" and  API_KEY = WEB_APIKEY then

	   '接口同步
	   response.Write "正在转入同步录入接口..."
	   Response.AddHeader "refresh","1;URL="&MY_secCheckSiteLink&"/admin/message/message_API.asp?API_ADD=1&API_KEY="&API_KEY&"&user="&user&"&cp1="&server.URLEncode(cp1)&"&cp4="&server.URLEncode(cp4)&""

End If

' ============================================================================
'API类型： 查询
'栏目：    指定文章
' ============================================================================

If spiSqlChk = 0 and openData = "true" and columnType = "article" then

    set rs=server.CreateObject("adodb.recordset")
	 
    if apiStrNull(infoID) = 1 then strsql="select * from [Article] where Articleid = "&infoID
	if apiStrNull(infoTitle) = 1 then strsql="select * from [Article] where ArticleTitle  like '%"&infoTitle&"%' "

    rs.open strsql,db,1,1

    '-----------------
	
	ID=rs("Articleid")
	Subdate=rs("Subdate")
	classname=rs("ClassName")
	Title=replace(rs("ArticleTitle"),chr(32),"")
	hits=rs("hits")
	files=replace(rs("files"),"../","")
	mainImg=rs("mainImg")
	renum=rs("renum")
    Summary=replace(replace(HTMLDecode(rs("Summary")),"<p>",""),"</p>","<br />")
	mypath=replace(rs("FilePath"),"/default.html","")
	SubdateShow=year(Subdate)&"-"&month(Subdate)&"-"&day(Subdate)
	postID=createBase64ID(replace(replace(mypath,MY_sitePath,""),classlistName,""))
	
	'主题图片
	SmallOrginUrl=replace(replace(mainImg,"../",""),""&MY_sec_uploadImgPath&"/?src=","")
	sSmallPath=""&MY_secCheckSiteLink&"/"&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")	
	
	'分类获取
	sql_class="select * from [ArticleClass] where ClassName='"&ClassName&"'"
	set rs_class=server.createObject("ADODB.Recordset")
	rs_class.open sql_class,db,1,1
	ClassID=rs_class("ArticleClassID")
	ClassLink=""&MY_secCheckSiteLink&"/classlist/"&MY_createFolder_art&""&ClassID&"_default.html"
	rs_class.close
	set rs_class=nothing
	
	
	'取出被顶踩信息
	set rs_Dig=server.createobject("adodb.recordset")
	sql_Dig="Select * From [vote] Where titleID="&ID&""
	rs_Dig.open sql_Dig,db,1,1
	if rs_Dig.eof then
	my_good="0"
	my_bad="0" 
	else 
	my_good=rs_Dig("art_good")
	my_bad=rs_Dig("art_bad")
	end if
	
	rs_Dig.Close
	Set rs_Dig = Nothing
	  
	'------------------
	

	
	thisACode = apiWriteJSON("{#}title{#}:{#}"&Title&"{#}","{#}classname{#}:{#}"&classname&"{#}","{#}hits{#}:{#}"&hits&"{#}","{#}comment{#}:{#}"&renum&"{#}","{#}classLink{#}:{#}"&ClassLink&"{#}","{#}preImg{#}:{#}"&sSmallPath&"{#}","{#}date{#}:{#}"&Subdate&"{#}","{#}postID{#}:{#}"&postID&"{#}","{#}goodNum{#}:{#}"&my_good&"{#}","{#}badNum{#}:{#}"&my_bad&"{#}","{#}url{#}:{#}"&replace(mypath,MY_sitePath&MY_createFolder_art,MY_secCheckSiteLink&"/"&MY_createFolder_art&"")&"{#}","{#}intro{#}:{#}"&Summary&"{#}","{#}author{#}:{#}"&name_Now&"{#}","","","","","","","")
	jsonCode = "{"&thisACode&"}"&vbcrlf

    if rs.bof and rs.eof then response.Write "No content." : jsonCode = ""
	
	response.Write jsonCode
	
	rs.close
	set rs=nothing
	
End If

' ============================================================================
'API类型： 查询
'栏目：    部分文章列表
' ============================================================================
If spiSqlChk = 0 and openData = "true" and columnType = "article.list" then

    set rs=server.CreateObject("adodb.recordset")
	
	'----------排序类型判断
	'随机选择
	if apiStrNull(infoScanType) = 1 and infoScanType = "random" then sqlOrder = " order by Rnd(ArticleID-timer()) desc"
	'顺序输出
    if apiStrNull(infoScanType) = 1 and infoScanType = "asc" then sqlOrder = " order by  Articleid asc"
	'倒序输出
    if apiStrNull(infoScanType) = 1 and infoScanType = "desc" then sqlOrder = " order by  Articleid desc"
	'浏览最多
	if apiStrNull(infoScanType) = 1 and infoScanType = "hot" then sqlOrder = "  order by hits desc, Subdate desc"	
	'评论最多
	if apiStrNull(infoScanType) = 1 and infoScanType = "comment" then sqlOrder = "  order by renum desc, Subdate desc"	
	
	'----------归类类型判断
    '类别判断
    if apiStrNull(infoClass) = 1 then strsql="select top "&infoEchoNum&"  * from [Article]  where ClassName='"& infoClass &"' "&sqlOrder&""
	'置顶
	if apiStrNull(infoClass) = 1 and infoClass = "top" then strsql="select top "&infoEchoNum&"  * from [Article]   where top=1 "&sqlOrder&""
	'最近月输出
	if apiStrNull(infoClass) = 1 and infoClass = "month" then strsql="select top "&infoEchoNum&"  * from [Article]  where DateDiff('h',Subdate,now())<=24*30 "&sqlOrder&""
	'最近周输出
	if apiStrNull(infoClass) = 1 and infoClass = "week" then strsql="select top "&infoEchoNum&"  * from [Article]  where DateDiff('h',Subdate,now())<=24*7 "&sqlOrder&""
	'关键词同类列表
	if apiStrNull(infoKeyword) = 1 then strsql="select top "&infoEchoNum&"  * from [Article]  where ArticleTitle like '%" & infoKeyword & "%' "&sqlOrder&""
	
    if apiStrNull(infoKeyword) = 0 and apiStrNull(infoClass) = 0 then strsql="select top "&infoEchoNum&"  * from [Article] "&sqlOrder&""
			

    showACode = ""
    thisACode = ""
	jsonCode1 = "["
    
    
 
    rs.open strsql,db,1,1
	
	If Err Then
		err.Clear
		apiError(6)
	End If
	
	if  rs.eof and rs.bof then
	    thisACode = "No content."
	else
	    
		do while not rs.eof and not rs.bof
	
		'-----------------
		
		ID=rs("Articleid")
		Subdate=rs("Subdate")
		classname=rs("ClassName")
		Title=replace(rs("ArticleTitle"),chr(32),"")
		hits=rs("hits")
		files=replace(rs("files"),"../","")
		mainImg=rs("mainImg")
		renum=rs("renum")
		Summary=replace(replace(HTMLDecode(rs("Summary")),"<p>",""),"</p>","<br />")
		mypath=replace(rs("FilePath"),"/default.html","")
		SubdateShow=year(Subdate)&"-"&month(Subdate)&"-"&day(Subdate)
		postID=createBase64ID(replace(replace(mypath,MY_sitePath,""),classlistName,""))
		
		'主题图片
		SmallOrginUrl=replace(replace(mainImg,"../",""),""&MY_sec_uploadImgPath&"/?src=","")
		sSmallPath=""&MY_secCheckSiteLink&"/"&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")	
		
		'分类获取
		sql_class="select * from [ArticleClass] where ClassName='"&ClassName&"'"
		set rs_class=server.createObject("ADODB.Recordset")
		rs_class.open sql_class,db,1,1
		ClassID=rs_class("ArticleClassID")
		ClassLink=""&MY_secCheckSiteLink&"/classlist/"&MY_createFolder_art&""&ClassID&"_default.html"
		rs_class.close
		set rs_class=nothing
		
		
		'取出被顶踩信息
		set rs_Dig=server.createobject("adodb.recordset")
		sql_Dig="Select * From [vote] Where titleID="&ID&""
		rs_Dig.open sql_Dig,db,1,1
		if rs_Dig.eof then
		my_good="0"
		my_bad="0" 
		else 
		my_good=rs_Dig("art_good")
		my_bad=rs_Dig("art_bad")
		end if
		
		rs_Dig.Close
		Set rs_Dig = Nothing
		  
		'------------------
		
	    thisContent = apiWriteJSON("{#}title{#}:{#}"&Title&"{#}","{#}classname{#}:{#}"&classname&"{#}","{#}hits{#}:{#}"&hits&"{#}","{#}comment{#}:{#}"&renum&"{#}","{#}classLink{#}:{#}"&ClassLink&"{#}","{#}preImg{#}:{#}"&sSmallPath&"{#}","{#}date{#}:{#}"&Subdate&"{#}","{#}postID{#}:{#}"&postID&"{#}","{#}goodNum{#}:{#}"&my_good&"{#}","{#}badNum{#}:{#}"&my_bad&"{#}","{#}url{#}:{#}"&replace(mypath,MY_sitePath&MY_createFolder_art,MY_secCheckSiteLink&"/"&MY_createFolder_art&"")&"{#}","{#}intro{#}:{#}"&Summary&"{#}","{#}author{#}:{#}"&name_Now&"{#}","","","","","","","")
		
		thisACode = thisACode&"{"&thisContent&"},"

		rs.movenext
		loop
	
	end if	
	rs.close
	set rs=nothing
	
	jsonCode2 = "]"
	
	
	thisACode = clearRightString(thisACode,"},")
	thisACode = thisACode&"}"
	showACode = jsonCode1&thisACode&jsonCode2&vbcrlf
	showACode = replace(showACode,"},{","},"&vbcrlf&"{")
	
	if instr(showACode,"{title:"&chr(34)&""&chr(34)&",") > 0 then showACode = "No content."

	response.Write showACode

	
End If



' ============================================================================
'API类型： 查询
'栏目：    指定案例作品
' ============================================================================
If spiSqlChk = 0 and openData = "true" and columnType = "works" then

    set rs=server.CreateObject("adodb.recordset")
	 
    if apiStrNull(infoID) = 1 then strsql="select * from [SuccessWork] where ID = "&infoID
	if apiStrNull(infoTitle) = 1 then strsql="select * from [SuccessWork] where title  like '%"&infoTitle&"%' "

	 
    rs.open strsql,db,1,1

    '-----------------
	
	ID=rs("ID")
	Title=replace(rs("title"),chr(32),"")
	url=rs("url")
	ClassName=rs("ClassName")
    content=replace(replace(HTMLDecode(rs("worksIntro")),"<p>",""),"</p>","<br />")
	Image=replace(rs("preURL"),"../","")
	Subdate=rs("Subdate")
	siteURL=rs("siteURL")
	theBigImage=rs("hdImg_d")
	
	postID=createBase64ID(replace(replace(url,MY_sitePath,""),classlistName,""))
	
	if instr(siteURL,"javascript:")>0 then siteURL = ""

	SmallOrginUrl=replace(replace(rs("preURL"),"../",""),""&MY_sec_uploadImgPath&"/?src=","")
	sSmallPath=""&MY_secCheckSiteLink&"/"&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")	
	
    '分类获取
	sql_class="select * from [SuccessWorkClass] where ClassName='"&ClassName&"'"
	set rs_class=server.createObject("ADODB.Recordset")
	rs_class.open sql_class,db,1,1
	ClassID=rs_class("SuccessWorkClassID")
	ClassLink=""&MY_secCheckSiteLink&"/classlist/"&MY_createFolder_work&""&ClassID&"_default.html"
	rs_class.close
	set rs_class=nothing

	'------------------

	thisACode = apiWriteJSON("{#}title{#}:{#}"&Title&"{#}","{#}classname{#}:{#}"&classname&"{#}","{#}hdImage{#}:{#}"&replace(theBigImage,MY_sitePath&"security",MY_secCheckSiteLink&"/security")&"{#}","{#}preURL{#}:{#}"&siteURL&"{#}","{#}classLink{#}:{#}"&ClassLink&"{#}","{#}preImg{#}:{#}"&sSmallPath&"{#}","{#}date{#}:{#}"&Subdate&"{#}","{#}postID{#}:{#}"&postID&"{#}","{#}url{#}:{#}"&replace(url,MY_sitePath&MY_createFolder_work,MY_secCheckSiteLink&"/"&MY_createFolder_work&"")&"{#}","{#}intro{#}:{#}"&content&"{#}","{#}author{#}:{#}"&name_Now&"{#}","","","","","","","","","")
	
	jsonCode = "{"&thisACode&"}"&vbcrlf
	
    if rs.bof and rs.eof then response.Write "No content." : jsonCode = ""
	
	response.Write jsonCode
	
	rs.close
	set rs=nothing
	

	
End If

' ============================================================================
'API类型： 查询
'栏目：    部分案例作品列表
' ============================================================================
If spiSqlChk = 0 and openData = "true" and columnType = "works.list" then

    set rs=server.CreateObject("adodb.recordset")
	
	'----------排序类型判断
	'随机选择
	if apiStrNull(infoScanType) = 1 and infoScanType = "random" then sqlOrder = " order by Rnd(ID-timer()) desc"
	'顺序输出
    if apiStrNull(infoScanType) = 1 and infoScanType = "asc" then sqlOrder = " order by  ID asc"
	'倒序输出
    if apiStrNull(infoScanType) = 1 and infoScanType = "desc" then sqlOrder = " order by  ID desc"
	'浏览最多
	if apiStrNull(infoScanType) = 1 and infoScanType = "hot" then sqlOrder = "  order by hits desc, Subdate desc"	
	
	'----------归类类型判断
    '类别判断
    if apiStrNull(infoClass) = 1 then strsql="select top "&infoEchoNum&"  * from [SuccessWork]  where ClassName='"& infoClass &"' "&sqlOrder&""
	'最近月输出
	if apiStrNull(infoClass) = 1 and infoClass = "month" then strsql="select top "&infoEchoNum&"  * from [SuccessWork]  where DateDiff('h',Subdate,now())<=24*30 "&sqlOrder&""
	'最近周输出
	if apiStrNull(infoClass) = 1 and infoClass = "week" then strsql="select top "&infoEchoNum&"  * from [SuccessWork]  where DateDiff('h',Subdate,now())<=24*7 "&sqlOrder&""
	'关键词同类列表
	if apiStrNull(infoKeyword) = 1 then strsql="select top "&infoEchoNum&"  * from [SuccessWork]  where title like '%" & infoKeyword & "%' "&sqlOrder&""
	
	if apiStrNull(infoKeyword) = 0 and apiStrNull(infoClass) = 0 then strsql="select top "&infoEchoNum&"  * from [SuccessWork] "&sqlOrder&""
	

    showACode = ""
    thisACode = ""
	jsonCode1 = "["

    rs.open strsql,db,1,1
	
	If Err Then
		err.Clear
		apiError(6)
	End If
	
	if  rs.eof and rs.bof then
	    thisACode = "No content."
	else
	
		do while not rs.eof and not rs.bof
	
		'-----------------
		
		ID=rs("ID")
		Title=replace(rs("title"),chr(32),"")
		url=rs("url")
		ClassName=rs("ClassName")
		content=replace(replace(HTMLDecode(rs("worksIntro")),"<p>",""),"</p>","<br />")
		Image=replace(rs("preURL"),"../","")
		Subdate=rs("Subdate")
		siteURL=rs("siteURL")
		theBigImage=rs("hdImg_d")
		
		postID=createBase64ID(replace(replace(url,MY_sitePath,""),classlistName,""))
		
		if instr(siteURL,"javascript:")>0 then siteURL = ""
	
		SmallOrginUrl=replace(replace(rs("preURL"),"../",""),""&MY_sec_uploadImgPath&"/?src=","")
		sSmallPath=""&MY_secCheckSiteLink&"/"&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")	
		
		'分类获取
		sql_class="select * from [SuccessWorkClass] where ClassName='"&ClassName&"'"
		set rs_class=server.createObject("ADODB.Recordset")
		rs_class.open sql_class,db,1,1
		ClassID=rs_class("SuccessWorkClassID")
		ClassLink=""&MY_secCheckSiteLink&"/classlist/"&MY_createFolder_work&""&ClassID&"_default.html"
		rs_class.close
		set rs_class=nothing

		  
		'------------------
		
	    thisContent = apiWriteJSON("{#}title{#}:{#}"&Title&"{#}","{#}classname{#}:{#}"&classname&"{#}","{#}hdImage{#}:{#}"&replace(theBigImage,MY_sitePath&"security",MY_secCheckSiteLink&"/security")&"{#}","{#}preURL{#}:{#}"&siteURL&"{#}","{#}classLink{#}:{#}"&ClassLink&"{#}","{#}preImg{#}:{#}"&sSmallPath&"{#}","{#}date{#}:{#}"&Subdate&"{#}","{#}postID{#}:{#}"&postID&"{#}","{#}url{#}:{#}"&replace(url,MY_sitePath&MY_createFolder_work,MY_secCheckSiteLink&"/"&MY_createFolder_work&"")&"{#}","{#}intro{#}:{#}"&content&"{#}","{#}author{#}:{#}"&name_Now&"{#}","","","","","","","","","")
		
		thisACode = thisACode&"{"&thisContent&"},"	

		rs.movenext
		loop
	
	end if	
	rs.close
	set rs=nothing
	
	jsonCode2 = "]"
	
	thisACode = clearRightString(thisACode,"},")
	thisACode = thisACode&"}"
	showACode = jsonCode1&thisACode&jsonCode2&vbcrlf
	showACode = replace(showACode,"},{","},"&vbcrlf&"{")
	
	if instr(showACode,"{title:"&chr(34)&""&chr(34)&",") > 0 then showACode = "No content."

	response.Write showACode

	
End If


' ============================================================================
'API类型： 查询
'栏目：    部分微博列表
' ============================================================================
If spiSqlChk = 0 and openData = "true" and columnType = "mblog.list" then

    set rs=server.CreateObject("adodb.recordset")
	
	'----------排序类型判断
	'随机选择
	if apiStrNull(infoScanType) = 1 and infoScanType = "random" then sqlOrder = " order by Rnd(ID-timer()) desc"
	
	'----------归类类型判断
	'最近月输出
	if apiStrNull(infoClass) = 1 and infoClass = "month" then strsql="select top "&infoEchoNum&"  * from [mood]  where DateDiff('h',Subdate,now())<=24*30 "&sqlOrder&""
	'最近周输出
	if apiStrNull(infoClass) = 1 and infoClass = "week" then strsql="select top "&infoEchoNum&"  * from [mood]  where DateDiff('h',Subdate,now())<=24*7 "&sqlOrder&""
	
	if apiStrNull(infoClass) = 0  then strsql="select top "&infoEchoNum&"  * from [mood] "&sqlOrder&""


    showACode = ""
    thisACode = ""
	jsonCode1 = "["

    rs.open strsql,db,1,1
	
	If Err Then
		err.Clear
		apiError(6)
	End If
	
	if  rs.eof and rs.bof then
	    thisACode = "No content."
	else
	
		do while not rs.eof and not rs.bof
	
		'-----------------
		
		Subdate=rs("Subdate")
		content=rs("content")
		face=rs("face")
		thisPic=rs("pic")
		view=rs("view")
		commID=rs("ID")
		
		
		'表情转换
		faceShow=""
		
		%>
		<!--#include file="../admin/root_temp-edit/mood_face.asp" -->
		<%	
	
		if view="1" then 
			faceShow="" 
			content=""&DY_LANG_91&""
		end if
		
		
		ImageThis=ShowImageURL(thisPic) 
		parm=""&MY_sitePath&""&MY_sec_uploadImgPath&"/?src="
		ShowImage=""&MY_secCheckSiteLink&"/clintInfo/img.html?parm="&parm&"&path="&replace(ImageThis,MY_sitePath,"")&""
		'微博图片
		
		SmallOrginUrl=replace(replace(thisPic,"../",""),""&MY_sec_uploadImgPath&"/?src=","")
		if MY_checkJpegComponent="0" then
		   sSmallPath=""&MY_secCheckSiteLink&"/"&MY_sec_uploadSmallImgPath&"/?src=sub_"&replace(SmallOrginUrl,MY_sitePath,"")
		else
		   sSmallPath=""&MY_secCheckSiteLink&"/"&MY_sec_uploadImgPath&"/?src="&replace(SmallOrginUrl,MY_sitePath,"")	
		end if	
		
		
		'----------图片外链支持
		httpImgPath = thisPic
		if instr(httpImgPath,"http://")>0 then
		   ShowImage=httpImgPath
		   sSmallPath=httpImgPath
		end if
					 
		
		if thisPic <> ""  and  thisPic <> "none" then 
		moodPicShow = "<a href="""&ShowImage&"""target=""_blank""><img src="""&sSmallPath&""" onerror=""this.src='"&MY_sitePath&"images/nonpreview.gif'""></a>"
		else
		moodPicShow = ""
		end if


		  
		'------------------
		
	    thisContent = apiWriteJSON("{#}content{#}:{#}"&content&"{#}","{#}face{#}:{#}"&faceShow&"{#}","{#}author{#}:{#}"&name_Now&"{#}","{#}date{#}:{#}"&Subdate&"{#}","{#}postID{#}:{#}"&commID&"{#}","{#}preImg{#}:{#}"&sSmallPath&"{#}","{#}imgHerf{#}:{#}"&moodPicShow&"{#}","","","","","","","","","","","","","")
		
		thisACode = thisACode&"{"&thisContent&"},"	

		rs.movenext
		loop
	
	end if	
	rs.close
	set rs=nothing
	
	jsonCode2 = "]"
	
	thisACode = clearRightString(thisACode,"},")
	thisACode = thisACode&"}"
	showACode = jsonCode1&thisACode&jsonCode2&vbcrlf
	showACode = replace(showACode,"},{","},"&vbcrlf&"{")
	
	if instr(showACode,"{title:"&chr(34)&""&chr(34)&",") > 0 then showACode = "No content."

	response.Write showACode

	
End If

' ============================================================================
'API类型： 数据添加
'栏目：    文章
' ============================================================================
If spiInfoAddChk = 0  and columnType = "article" and openData <> "true"  then
    
	   '接口同步添加文章
	   response.Write "正在转入同步录入接口..."
	   Response.AddHeader "refresh","1;URL="&MY_secCheckSiteLink&"/admin/article/article_add_API.asp?API_ADD=1&cp1="&server.URLEncode(cp1)&"&cp2="&server.URLEncode(cp2)&"&cp3="&server.URLEncode(cp3)&"&cp4="&server.URLEncode(cp4)&""
	   
End If



%>