<%
'//功能：文件夹删除
Function delFolder(filePath) 
 	set fs=server.CreateObject("Scripting.FileSystemObject") 
	 if fs.FolderExists(server.mappath(filePath)) then
		fs.DeleteFolder(server.mappath(filePath))
	 end if
	 Set fs = Nothing
End Function 
'//功能：修改文件夹名
Function EditFolderName(nowfld,newfld)

		nowfld=server.mappath(nowfld)
		newfld=server.mappath(newfld)
		
		Set fso = CreateObject("Scripting.FileSystemObject")
		if not fso.FolderExists(nowfld) then
			response.write("需要修改的文件夹路径不正确或文件夹名称输入错误")
		else
			fso.CopyFolder nowfld,newfld
			fso.DeleteFolder(nowfld)
		end if
		set fso=nothing

End Function
%>