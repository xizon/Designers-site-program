function ResizeImage(obj, MaxW, MaxH) {
        if (obj != null) imageObject = obj;
        var state = imageObject.readyState;
        if (state != 'complete') {
            setTimeout("ResizeImage(null," + MaxW + "," + MaxH + ")", 50);
            return;
        }
        var oldImage = new Image();
        oldImage.src = imageObject.src;
        var dW = oldImage.width;
        var dH = oldImage.height;
        if (dW > MaxW || dH > MaxH) { a = dW / MaxW; b = dH / MaxH; if (b > a) a = b; dW = dW / a; dH = dH / a; }
        if (dW > 0 && dH > 0) { imageObject.width = dW; imageObject.height = dH; }
}
 