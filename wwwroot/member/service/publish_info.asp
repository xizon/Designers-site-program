<!--#include file="../function/conn.asp" -->
<!--#include file="../function/function.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
myname=Request.QueryString("name")
%>
<!--#include file="../checkFun.asp" -->
<script>
<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=在线投稿", "link1=<%=MY_sitePath%>member/service/publish_info.asp?name=<%=myname%>", "title2=列表管理", "link2=<%=MY_sitePath%>member/service/publish_info_list.asp?name=<%=myname%>");

	   }
	);
	
});

<!-- form ui[js]  end -->


$(document).ready(function() {
	var limitNum = 50;
	$('#resources_title').inputlimiter({
		limit: limitNum,
		boxId: 'limitingtext',
		boxAttach: false
	});	

	document.getElementById("limitingtext").innerHTML = "0/" + limitNum;

	var limitNum2 = 5000;
	$('#resources_info').inputlimiter({
		limit: limitNum2,
		boxId: 'limitingtext2',
		boxAttach: false
	});	

	document.getElementById("limitingtext2").innerHTML = "0/" + limitNum2;


});


//表单判断
function checkForm(){
	if(document.formM.resources_title.value==""){
		ChkFormTip_page("标题不能为空",0);
		return false;
	}
	if(document.formM.resources_info.value==""){
		ChkFormTip_page("描述内容不能为空",0);
		return false;
	}
	//ok
	ChkFormTip_page("正在处理中",2);
	$('#needCreateLoadingBtn').fadeOut(500);
	return true;
		
}


</script>


<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                <a href="publish_info_list.asp?name=<%=myname%>" class="link">列表管理</a>
                
                <!-- 当前位置 -->
                当前位置： 投稿区 | 在线投稿
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
            
            
                  <div id="contentShow">
                
                   <form  name="formM" action="action.asp?action=add&name=<%=myname%>" method="post" onSubmit="javascript:return checkForm();">
  
                                <table width="100%" class="setting" cellspacing="0">
                                    
                                    <!-- 标题 -->                        
                                    <thead class="setHead">
                                        <tr>
                                               <th  class="setTitle"></th>
                                               <th colspan="2"></th>
                                        </tr>
                                        
                                    </thead>
                                    
                                 
                                    <!-- 底部 -->
                                    <tfoot>
                                    
                                        <tr>
                                            <td colspan="3">         
                                                <div class="btnBox">
                                                    <input type="submit" value="发表" class="sub" id="needCreateLoadingBtn">
                                                    <!--<input type="reset" value="重置" class="reset">-->
                                              </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                    
                                    <!-- +++++++  内容   begin +++++++  -->
                                    <tbody>
            
                                        <tr>
                                            <td class="title">标题：<span class="backStage_star">*</span></td>
                                            <td colspan="2" class="ui-element">
                                                <input type="text" name="resources_title" id="resources_title"  size="40" >&nbsp;&nbsp;<span id="limitingtext"></span>
                                            </td>
                                        </tr>  

                                        
                                        <tr style="display:none">
                                            <td class="title">相关链接：</td>
                                            <td colspan="2" class="ui-element">
                                               <input name="resources_URL" id="resources_URL" type="text" size="35" class="focusJS" data-value="http://" value=""/>
                                            </td>
                                        </tr>
                                         
                              
                                                   
                                        <tr>
                                            <td class="title">描述内容<a href="javascript:void(0)" class="help" data-info="请遵守相关法律法规，不要提交恶意性言论"><img src="../../plugins/d-s-management-ui/img/help.PNG" alt=""></a>：<span class="backStage_star">*</span></td>
                                            <td colspan="2" class="ui-element">
                                                <textarea  cols="120" rows="6" name="resources_info" id="resources_info"></textarea>&nbsp;&nbsp;<span id="limitingtext2"></span>
                                            </td>
                                        </tr>       
        
                                          
                                       <!-- +++++++  内容   end +++++++  -->
                                
                                
                                    </tbody>
                                    
                                </table>
   
                    </form>  
                            
               
                               
                  </div> 
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->
