<!--#include file="../function/conn.asp" -->
<!--#include file="../function/function.asp" -->
<!--#include file="../_comm/page_head.asp" -->
<!--#include file="../_comm/page_bodyComm.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
myname=Request.QueryString("name")
%>
<!--#include file="../checkFun.asp" -->

<script>
<!-- form ui[js]  begin -->
$(document).ready(function(){

	//columnSwitch
	$("#column-btn-back").click(
	   function() {
			columnNavShow("mode_column=1", "title1=欢迎界面", "link1=<%=MY_sitePath%>member/website/statistics.asp?name=<%=myname%>", "title2=修改资料", "link2=<%=MY_sitePath%>member/edit_file/edit.asp?name=<%=myname%>", "title3=修改密码", "link3=<%=MY_sitePath%>member/edit_file/edit_ps.asp?name=<%=myname%>");

	   }
	);
	
});

<!-- form ui[js]  end -->

//退出
function clearCookies(){
	MySite.Cookie.set('cat','');
	MySite.Cookie.set('catpass','');
	MySite.Cookie.set('manageOK','');
	MySite.Cookie.set('memScan','');
	MySite.Cookie.set('onlineChk<%=myname%>','');
}

//表单判断
function checkForm(){
	
	//密码判断
	if (document.formM.oldpass.value==""){
		ChkFormTip_page("密码不能为空",0);
		return false;	
	}
	if (document.formM.oldpass.value!="" && document.formM.oldpass.value.length < 6){
		ChkFormTip_page("密码不得少于6个字符",0);
		return false;	
	}
	if (document.formM.upass.value =="" && document.formM.oldpass.value!=""){
		ChkFormTip_page("新密码不能为空",0);
		return false;	
	}
	if (document.formM.upass.value!="" && document.formM.upass.value.length > 24 && document.formM.oldpass.value!=""){
		ChkFormTip_page("密码不得大于24个字符",0);
		return false;
	}
	if (document.formM.upass.value != document.formM.reuserPass.value && document.formM.oldpass.value!=""){
		ChkFormTip_page("两次密码输入不相同",0);
		return false;	
	}


	//ok
	ChkFormTip_page("保存成功",1);
	return true;
		
}	



</script>


<!--========================================主内容区  begin ===================================== -->  

        <!-- 状态栏  -->
        <div id="headStatus">
            <div class="nav">
                <a href="javascript:" class="link" id="column-btn-back" style="display:none">返回</a>
                
                <!-- 当前位置 -->
                当前位置： 资料管理 | 修改密码
            
            </div>
            
            
            
        </div>
        
        
        <div id="contentShowBox">
            <div id="contentShowMain">
            
                  <div id="contentShow">
                  
                                <form name="formM" action="action.asp?action=okPass&name=<%=myname%>" method="post" onSubmit="javascript:return checkForm();"  target="actionPage">
        
                                <table width="100%" class="setting" cellspacing="0">
                                    
                                    <!-- 标题 -->                        
                                    <thead class="setHead">
                                        <tr>
                                               <th  class="setTitle"></th>
                                               <th colspan="2"></th>
                                        </tr>
                                        
                                    </thead>
                                    
                                    
                                  <!-- 底部 -->
                                    <tfoot>
                                    
                                        <tr>
                                            <td colspan="3">
                                               <div class="btnBox">
                                               
                                                   <input type="submit" value="保存" class="sub">
                                                   
                                               </div>
                                
                                            </td>
                                        </tr>
                                    </tfoot>
                                    
                                    
                                    
                                    <!-- +++++++  内容   begin +++++++  -->
                                    
                                    <tbody>
                                        
  

                                        <thead class="setHead">
                                            <tr>
                                               <th  class="setTitle"></th>
                                               <th colspan="2" class="titleInfo">密码设置</th>
                                            </tr> 
                                        </thead> 
                                    
                                    
                                        <tr>
                                            <td class="title">旧密码：</td>
                                            <td colspan="2" class="ui-element">
                                                <input name="oldpass"type="password" size="35"  />
                                            </td>
                                        </tr> 
                                        
                                        
                                        <tr>
                                            <td class="title">新密码：</td>
                                            <td colspan="2" class="ui-element">
                                                <input name="upass" type="password" size="35" />
                                            </td>
                                        </tr> 
                                        
                                        <tr>
                                            <td class="title">重复新密码：</td>
                                            <td colspan="2" class="ui-element">
                                                <input name="reuserPass" type="password" size="35"/>
                                            </td>
                                        </tr> 
                                        
                                        <tr id="passEditState">
                                            <td colspan="3">
                                                <span></span>
                                            </td>
                                        </tr> 
                                        
                                          
                                       <!-- +++++++  内容   end +++++++  -->
                                
                                
                                    </tbody>
                                    
                                </table>
                                
                                
                              </form>
                      
      
                  </div> 
              
              </div>      
         </div>       


<!--========================================主内容区  end ===================================== -->  


<!--#include file="../_comm/page_bottom.asp" -->
