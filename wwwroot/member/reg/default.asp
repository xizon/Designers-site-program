<!--#include file="../function/conn.asp" -->
<!--#include file="../function/function.asp" -->
<!--#include file="../../admin/function/MD5.asp" -->
<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%

PageOverdue()

'功能限制
	set rs =server.createobject("adodb.recordset")
	sql="select * from [mem_forbit]" 
	rs.open sql,conn,1,1
	reg_IP=rs("reg_IP")
	reg_allow=rs("reg_allow")	
	rs.close
	set rs=nothing	
	
If getIP()=reg_IP then 
	geterror(4)
End If

If reg_allow=1 then 
	geterror(1)
End If


%>
<!doctype html>
<html dir="ltr" lang="zh-CN" xml:lang="zh-CN" class="bgNone">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>新用户注册</title>
<link rel="stylesheet" type="text/css" href="<%=MY_sitePath%>images/skins/style.css" />
<script src="<%=MY_sitePath%>cat_js/template.js"            type="text/javascript"></script>
<script src="<%=MY_sitePath%>cat_js/manageComm.js"  type="text/javascript"></script>
<script src="<%=MY_sitePath%>cat_js/Province-City.js"  type="text/javascript"></script>
<script src="<%=MY_sitePath%>cat_js/jquery.min.js"            type="text/javascript"></script>
<script src="<%=MY_sitePath%>cat_js/jquery.wbox.js"            type="text/javascript"></script>
<style>
body{line-height:45px; font-size:12px;}
#UserFrame form table {text-align: left;}
.reg-table{margin-left:15px}
#UserFrame form table td strong{padding-left:10px}
.agree{color:#333; line-height:20px;}
.avatar{border:1px #666 inset; width:30px; height:30px;}

.even{}  /* 偶数行样式*/
.odd{}  /* 奇数行样式*/
.selected{ background:#E6E6E6}
.selected input{background:#fff}
.selected .inputSubBtn{background:#1E1E1E}
.fc{color:#f00}

/*表单提示框架样式*/
.tipsFloatWin{padding:15px 0 0 20px}

#tip {display:block; width:95%;}
#tip div{padding:2px 5px 2px 5px; background:#FCE8DC; border:1px solid #F60; color:#F30; line-height:15px; height:15px; border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px; -o-webkit-border-radius: 3px; margin-top:5px;}

.inputSubBtn{ color:#fff; font-family:"微软雅黑"; padding:4px 10px 3px 10px; width:140px; height:35px; line-height:30px; cursor:pointer; overflow:hidden; border-radius: 7px; -moz-border-radius: 7px; -webkit-border-radius: 7px; -o-webkit-border-radius: 7px; background:#1E1E1E}

input,select{padding:6px; margin:3px; background-image:url(../../plugins/d-s-management-ui/img/manage/inputBG.GIF); border:1px #CCC solid}

</style>
<script>
function checkForm(){
	
        exp=/[^0-9()-]/g; 
		exp2=/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
		exp3=/^[\u4e00-\u9fa5\da-zA-Z\-\_]+$/;
		

		
		if(document.formM.user.value=="")
		{
			$("#tip").html("<div class=tipsFloatWin>×&nbsp;用户名不能为空！</div>").slideDown(500);
			setTimeout('$("#tip").slideUp(500);',1000);
			return false;
		}else if (document.formM.user.value.length < 4)
		{ 
			$("#tip").html("<div class=tipsFloatWin>×&nbsp;用户名不能少于4个字符！</div>").slideDown(500);
			setTimeout('$("#tip").slideUp(500);',1000);
			return false;
		}else if (document.formM.user.value.length >20)
		{
			$("#tip").html("<div class=tipsFloatWin>×&nbsp;用户名不能大于20个字符！</div>").slideDown(500);
			setTimeout('$("#tip").slideUp(500);',1000);
			return false;
		}else if (document.formM.user.value.search(exp3) == -1)
		{
			$("#tip").html("<div class=tipsFloatWin>×&nbsp;用户名不能含有特殊符号！</div>").slideDown(500);
			setTimeout('$("#tip").slideUp(500);',1000);
			return false;
		}
		
		
		if (document.formM.email.value!="" && document.formM.email.value.search(exp2) == -1)
		{
			$("#tip").html("<div class=tipsFloatWin>×&nbsp;电子邮箱格式不正确！</div>").slideDown(500);
			setTimeout('$("#tip").slideUp(500);',1000);
			return false;	
		}

		if (document.formM.email.value=="" || document.formM.email.value=="yourmail@host.com" )
		{
			$("#tip").html("<div class=tipsFloatWin>×&nbsp;电子邮箱必须填写！</div>").slideDown(500);
			setTimeout('$("#tip").slideUp(500);',1000);
			return false;
		}
				
		

		if(document.formM.pass1.value=="")
		{
			$("#tip").html("<div class=tipsFloatWin>×&nbsp;密码不能为空！</div>").slideDown(500);
			setTimeout('$("#tip").slideUp(500);',1000);
			return false;
		}else if (document.formM.pass1.value.length < 6)
		{
			$("#tip").html("<div class=tipsFloatWin>×&nbsp;密码不得少于6个字符！</div>").slideDown(500);
			setTimeout('$("#tip").slideUp(500);',1000);
			return false;
		}else if (document.formM.pass1.value.length > 20)
		{
			$("#tip").html("<div class=tipsFloatWin>×&nbsp;密码不得大于20个字符！</div>").slideDown(500);
			setTimeout('$("#tip").slideUp(500);',1000);
			return false;
		}else if (document.formM.pass1.value != document.formM.pass2.value)
		{
			$("#tip").html("<div class=tipsFloatWin>×&nbsp;两次密码输入不相同！</div>").slideDown(500);
			setTimeout('$("#tip").slideUp(500);',1000);
			return false;
		}
		
		
		return true;
		}
		
$(function() {
    var $trs = $("#trs>tr");  //选择所有行
    $trs.filter(":odd").addClass("odd");  //给奇数行添加odd样式
    $trs.filter(":even").addClass("even");//给偶数行添加odd样式

    //点击行,添加变色样式
    $trs.mouseover(function(e){
        $(this).addClass("selected")
        .siblings().removeClass("selected");
    })

})
</script>
</head>
<body id="UserFrame" class="iframeFloatL">
<script type="text/javascript">
$(document).ready(function() {
	
	//ajax判断地址
	$("#user").keyup(function(){
	  var thisData=$("#user").val();
	  $.post("../checkuser.asp?user=" + thisData,{suggest:thisData},function(result){
		$("#divCheckuser").html(result);
		if(result.indexOf("已存在") > 0){
			$(".inputSubBtn").fadeOut(500);
		}else{
			$(".inputSubBtn").fadeIn(500);
		}
		
	  });
	});	
	
	
	
});

</script>
<%if request.querystring("thisReg")<>"true"  then%>
<form name="formM" method="post" action="?action=add&thisReg=true" onSubmit="javascript:return checkForm();">
  <table cellpadding="5" cellspacing="0" width="100%" class="reg-table">
  <tbody id="trs">
    <tr>
      <td width="90"><strong>用户名：</strong></td>
      <td colspan="2"><div align="left">
          <input type="text"  class="input_text" name="user" id="user" maxlength="32" size="18" title="只能包含英文字母、数字和下划线，最大32位(注册成功后不得更改)" />
          <input type="button"  style="display:none" value="检测帐号" class="input"  onClick="checkuser(this.form,true)" />
          <span class="backStage_star"> *</span>&nbsp;&nbsp;不公开,您可以<a href="javascript:" onclick="$('#other').fadeIn(400);">[添加个性昵称]</a> </div>
          
          
          </td>
    </tr>
    <tr id="tipUser">
      <td></td>
      <td colspan="2"><div align="left"><div id="divCheckuser" class="fc"></div></div></td>
    </tr>
     <tr>
      <td><strong>头像：</strong></td>
      <td colspan="2"><div align="left">
        <select name="avatars" onchange="document.images.avatarpic.src = this[this.selectedIndex].value;">
        <option value="<%=MY_sitePath%>images/avatar/b1.jpg">Avatar0</option>
        <option value="<%=MY_sitePath%>images/avatar/b2.jpg" selected="selected">Avatar1</option>
        <option value="<%=MY_sitePath%>images/avatar/b3.jpg">Avatar2</option>
        <option value="<%=MY_sitePath%>images/avatar/g1.jpg">Avatar3</option>
        <option value="<%=MY_sitePath%>images/avatar/g2.jpg">Avatar4</option>
        <option value="<%=MY_sitePath%>images/avatar/g3.jpg">Avatar5</option>
        </select>
        <img src="<%=MY_sitePath%>images/avatar/b2.jpg" class="avatar" name="avatarpic" align="absmiddle" > 
         性别： <select name="sex">
            <option value="帅哥">帅哥</option>
            <option value="美女">美女</option>
            <option value="保密" selected="selected">保密</option>
          </select>
        
          </div></td>
    </tr>   

    <tr>
      <td><strong>注册类型：</strong></td>
      <td colspan="2"><div align="left"><input name="business" type="radio" value="0" checked="checked" />个人注册&nbsp;&nbsp;<input name="business" type="radio" value="1"/>企业注册  （注册后不可修改）</div></td>
    </tr>
    <tr style="display:none">
      <td><strong>所在地区：</strong></td>
      <td><div align="left"><select id="Province" name="Province" onChange="return cat_select();"> 
<option selected="selected" value="">-请选择-</option> 
<option value="1">北京</option> 
<option value="2">上海</option> 
<option value="3">天津</option> 
<option value="4">重庆</option> 
<option value="5">河北</option> 
<option value="6">山西</option> 
<option value="7">内蒙古</option> 
<option value="8">辽宁</option> 
<option value="9">吉林</option> 
<option value="10">黑龙江</option> 
<option value="11">江苏</option> 
<option value="12">浙江</option> 
<option value="13">安徽</option> 
<option value="14">福建</option> 
<option value="15">江西</option> 
<option value="16">山东</option> 
<option value="17">河南</option> 
<option value="18">湖北</option> 
<option value="19">湖南</option> 
<option value="20">广东</option> 
<option value="21">广西</option> 
<option value="22">海南</option> 
<option value="23">四川</option> 
<option value="24">贵州</option> 
<option value="25">云南</option> 
<option value="26">西藏</option> 
<option value="27">陕西</option> 
<option value="28">甘肃</option> 
<option value="29">宁夏</option> 
<option value="30">青海</option> 
<option value="31">新疆</option> 
<option value="32">香港</option> 
<option value="33">澳门</option> 
<option value="34">台湾</option> 
<option value="35">Foreign Country</option> 
</select> 
<input name="Province1" type="text" id="Province1" size="10" style="display:none" maxlength="30" readonly /> </div>
 </td>
      <td><div align="left"><select id="City" onChange="return cat_result();"> 
<option selected="selected">-请选择-</option> 
</select> 
<input name="City1" type="text" id="City1" size="10" maxlength="30"  style="display:none" readonly /></div></td>
    </tr>
    <input type="hidden"  name="question" value="" />
    <input type="hidden"  name="key" value="" />
    <tr>
      <td><strong>电子邮箱：</strong></td>
      <td colspan="2"><div align="left">
          <input name="email" type="text" class="input_text focusJS" size="32" maxlength="64" data-value="yourmail@host.com" value=""/>
          <span class="backStage_star"> *</span>本站为您保密</div></td>
    </tr>
    <tr>
      <td><strong>密码：</strong></td>
      <td colspan="2"><div align="left">
          <input type="password" class="input_text" onclick="document.getElementById('pawStrength').style.display='block';"  name="pass1" maxlength="32" size="32" onkeyup='javascript:EvalPwdStrength(document.forms[0],this.value);' onmouseout='javascript:EvalPwdStrength(document.forms[0],this.value);' onblur='javascript:EvalPwdStrength(document.forms[0],this.value);'/>
          <span class="backStage_star"> *</span></div>
        <div align="left" id="pawStrength" style="display:none">
          <%Response.Write ShowPwdStrength%>
        </div></td>
    </tr>

    <tr>
      <td><strong>确认密码：</strong></td>
      <td colspan="2"><div align="left">
          <input type="password" class="input_text" name="pass2" maxlength="32" size="32" />
          <span class="backStage_star"> *</span></div></td>
    </tr>
    <tr style="display:none">
      <td><strong>验证码：</strong></td>
      <td width="499"><div align="left">
          <input type="text" data-value="点击显示"  onclick="document.getElementById('chkShow').style.display='block';" value="" class="input_text focusJS" name="checkcode" size="10" />
        </div></td>
      <td width="588"><div align="left" id="chkShow" style="display:none">
           <img id="imgCaptcha" src="<%=MY_sitePath%>plugins/check_code" onclick="RefreshImage('imgCaptcha')" style="cursor:pointer" title="点击更换验证码" />
        </div></td>
    </tr>
    <tr id="other" style="display:none">
      <td><strong>昵称：</strong></td>
      <td colspan="2"><div align="left">
          <input name="nick" type="text" class="input_text" value="佚名用户" size="32" maxlength="64" />
          <span class="backStage_star"> *</span></div></td>
    </tr>
    <input name="qq" type="hidden" />
    <input name="home" value="http://" type="hidden" />
    <tr>
      <td colspan="3"><input name="submit" type="submit" class="inputSubBtn"  value="提交注册"  /><span id="tip"></span>
        </td>
    </tr>
    <tr style="display:none">
      <td colspan="3" style="text-align:left;"><input name="agreeBtn" type="checkbox" checked="checked" value="yes"/>&nbsp;我已经阅读并同意了注册协议</td>
    </tr>
    <tr style="display:none">
      <td colspan="3" style="text-align:left;"><textarea cols="70" rows="7" class="agree"><%loginRules()%>
</textarea></td>
    </tr>
   </tbody>
  </table>
</form>

<%end if%>
<%


dim checkcode
checkcode = trim(request.form("checkcode"))

' ============================================
'用户注册
' ============================================
if request.querystring("action")="add"  then

  
'////////////////////////////////验证完毕,写入数据


	uname=HTMLEncode(request.form("user"))
	upass=MD5(request.form("pass1"),cint(MY_MD5Seed_login))
	nickname=HTMLEncode(request.form("nick"))
	homepage=HTMLEncode(request.form("home"))
	qq=HTMLEncode(request.form("qq"))
	question="未设置"
	key="未设置"
	email=HTMLEncode(request.form("email"))
	avatars=request.form("avatars")
	sex=request.form("sex")
	province="未设置"
	city="未设置"
	business=request.form("business")
	 
	 
	set rsChk =server.createobject("adodb.recordset")
	sqlChk="select * from [user] where uname='"&uname&"'"
	rsChk.open sqlChk,conn,1,1 
	
	'存在性验证
	if not rsChk.bof and not rsChk.eof then
		'已存在

	else
		'执行添加操作

			set rsLog =server.createobject("adodb.recordset")
			sqlLog="select * from [user]" 
			rsLog.open sqlLog,conn,1,2
		
			money="10"		
			IP=getIP()
			
			
			if nickname="" then nickname="无"
			if qq="" then qq="无"
			if homepage="" then homepage="无"
		
					
			rsLog.addnew  '以下是添加进数据库的内容
			rsLog("uname")=uname
			rsLog("upass")=upass
			rsLog("nickname")=nickname
			rsLog("homepage")=homepage
			rsLog("email")=email
			rsLog("qq")=qq
			rsLog("question")=question
			rsLog("key")=key
			rsLog("money")=money
			rsLog("IP")=IP
			rsLog("sex")=sex
			rsLog("picture")=avatars
			rsLog("province")=province
			rsLog("city")=city
			rsLog("business")=business
			rsLog.update
			rsLog.close
			set rsLog=nothing
	
	
		
	end if

	
	rsChk.close
	set rsChk=nothing 
	 
	 
	

	'////////////////////////输出登陆框
	Response.Write "<form method=""post"" target=""_blank"" action="""&MY_sitePath&"admin/admin_login/chk_login.asp?action=verify&mode=shortcut""><div align=""left"">"& vbcrlf
	Response.Write "<table width=""100%"" border=""0"" cellspacing=""8"" cellpadding=""0"">"& vbcrlf
	Response.Write "<tr>"& vbcrlf
	Response.Write "<td colspan=""3"" height=""17""><div align=""left""><img src="&MY_sitePath&"images/right.gif border=0/>&nbsp;&nbsp;注册成功,您目前的身份是：普通会员 </div></td>"& vbcrlf
	Response.Write "</tr>"& vbcrlf
	Response.Write "<tr>"& vbcrlf
	Response.Write "<td colspan=""3"" height=""12""><div align=""left""> &nbsp;&nbsp;帐&nbsp;&nbsp;号："& vbcrlf
	Response.Write "<input name=""userName"" type=""text"" style=""width:200px""  class=""input_text"" size=""13"" maxlength=""30"" value="""&request.form("user")&""" />"& vbcrlf
	Response.Write "</div></td>"& vbcrlf
	Response.Write "</tr>"& vbcrlf
	Response.Write "<tr>"& vbcrlf
	Response.Write "<td colspan=""3""> <div align=""left""> &nbsp;&nbsp;密&nbsp;&nbsp;码："& vbcrlf
	Response.Write "<input name=""userPass""  type=""password"" style=""width:200px""  class=""input_text"" value="""&request.form("pass1")&""" size=""15"">"& vbcrlf
	Response.Write "</div></td>"& vbcrlf
	Response.Write "</tr>"& vbcrlf
	Response.Write "<tr>"& vbcrlf
	Response.Write "<td width=""66%""> <div align=""left"">  &nbsp;&nbsp;记&nbsp;住?："& vbcrlf
	Response.Write "<input name=""edtSaveDate"" type=""radio"" value=""""/>不保留"& vbcrlf
	Response.Write "<input name=""edtSaveDate"" type=""radio"" value=""365"" checked/>记住密码"& vbcrlf
	Response.Write "</div></td>"& vbcrlf
	Response.Write "</tr>"& vbcrlf
	Response.Write "</table>"& vbcrlf
	Response.Write " &nbsp;&nbsp;"& vbcrlf
	Response.Write "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name=""Submit"" type=""submit"" id=""btnPost""  value=""登陆"" class=""inputSubBtn""  />&nbsp;&nbsp;"& vbcrlf
	Response.Write "</div>"& vbcrlf
	Response.Write "</form>"& vbcrlf

end if
%>

<script src="../../cat_js/commFooterFun.js"></script>
</body>
</html>
