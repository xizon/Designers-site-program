<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%

' ============================================
'数据库操作类
' ============================================
Class myclass

'-----------“添加”记录方法

		public 	function AddRecord(table,fieldhe,website)
			fie=split(fieldhe,",")
			fieldnum=ubound(fie)
			set addRs=server.createobject("adodb.recordset")
			strsql="select * from ["& table &"]"
			addRs.open strsql,conn,1,2
			addRs.addnew
			for i=0 to fieldnum
				addRs(fie(i))=request.Form(fie(i))					
			next
			addRs("Index")=	GetNum(table)+1
			addRs.update			
			response.Redirect(website)
		end function
'用法
'   dim **
'	set **=new myclass	
'   addRs=**.addRecord("表名","字段1,字段2,字段3","转向地址") 

		
'-----------“删除”记录方法

		public function DelRecord(table,condition,guestid,website)
			dim strsql		
			strsql="delete from ["& table &"] where "& condition &"=" & guestid
			conn.execute(strsql)
			response.Redirect(website)
		end function
'用法
'   dim **
'	set **=new myclass	
'   DelRs=**.DelRecord("表名","ID",获得的参数ID,"转向地址")		
	
		
'----------------求表的记录总数函数

		public function GetNum(stable)
   			GetNum=conn.execute("select count(1) from ["&stable&"]")(0)
		end function
			
'用法
'   dim **
'	set **=new myclass	
'   num=cat.GetNum("表名")	
'------------------关闭对象[局部过程]  
		Private Sub Class_Terminate()
		
			
			rs.Close
			Set rs=Nothing
			conn.Close
			Set conn=Nothing
		End Sub
end class


' ============================================
'会员错误信息
' ============================================
function geterror(i)
	select case i
	case "1"
	
	Response.Redirect ""&MY_sitePath&"clintInfo/error.html?client=error_MemE1"

	case "2"
	Response.Redirect ""&MY_sitePath&"clintInfo/error.html?client=error_MemE2"

	case "3"
	Response.Redirect ""&MY_sitePath&"clintInfo/error.html?client=error_MemE3"

	case "4"
	Response.Redirect ""&MY_sitePath&"clintInfo/error.html?client=error_MemE4"

	case else
	Response.Redirect ""&MY_sitePath&"clintInfo/error.html?client=error_MemE5"
	end select
		
end function


' ============================================
'密码强度
' ============================================
Function ShowPwdStrength()
    Dim strStrength
    strStrength = strStrength & "<script language='JavaScript' src='"&MY_sitePath&"member/reg/PwdStrength.js'></script>" & vbCrLf
    strStrength = strStrength & "<script language='JavaScript'>" & vbCrLf
    strStrength = strStrength & "<!--" & vbCrLf
    strStrength = strStrength & "window.onerror = ignoreError;" & vbCrLf
    strStrength = strStrength & "function ignoreError(){return true;}" & vbCrLf
    strStrength = strStrength & "function EvalPwdStrength(oF,sP){" & vbCrLf
    strStrength = strStrength & "  PadPasswd(oF,sP.length*2);" & vbCrLf
    strStrength = strStrength & "  if(ClientSideStrongPassword(sP,gSimilarityMap,gDictionary)){DispPwdStrength(3,'cssStrong');}" & vbCrLf
    strStrength = strStrength & "  else if(ClientSideMediumPassword(sP,gSimilarityMap,gDictionary)){DispPwdStrength(2,'cssMedium');}" & vbCrLf
    strStrength = strStrength & "  else if(ClientSideWeakPassword(sP,gSimilarityMap,gDictionary)){DispPwdStrength(1,'cssWeak');}" & vbCrLf
    strStrength = strStrength & "  else{DispPwdStrength(0,'cssPWD');}" & vbCrLf
    strStrength = strStrength & "}" & vbCrLf
    strStrength = strStrength & "function PadPasswd(oF,lPwd){" & vbCrLf
    strStrength = strStrength & "  if(typeof oF.PwdPad=='object'){var sPad='IfYouAreReadingThisYouHaveTooMuchFreeTime';var lPad=sPad.length-lPwd;oF.PwdPad.value=sPad.substr(0,(lPad<0)?0:lPad);}" & vbCrLf
    strStrength = strStrength & "}" & vbCrLf
    strStrength = strStrength & "function DispPwdStrength(iN,sHL){" & vbCrLf
    strStrength = strStrength & "  if(iN>3){ iN=3;}for(var i=0;i<4;i++){ var sHCR='cssPWD';if(i<=iN){ sHCR=sHL;}if(i>0){ GEId('idSM'+i).className=sHCR;}GEId('idSMT'+i).style.display=((i==iN)?'inline':'none');}" & vbCrLf
    strStrength = strStrength & "}" & vbCrLf
    strStrength = strStrength & "function GEId(sID){return document.getElementById(sID);}" & vbCrLf
    strStrength = strStrength & "//-->" & vbCrLf
    strStrength = strStrength & "</script>" & vbCrLf
    strStrength = strStrength & "<style>" & vbCrLf
    strStrength = strStrength & "input{FONT-FAMILY:宋体;FONT-SIZE: 9pt;}" & vbCrLf
    strStrength = strStrength & ".cssPWD{background-color:#EBEBEB;border-right:solid 1px #BEBEBE;border-bottom:solid 1px #BEBEBE; height:10px; width:50px; display:block; float:left}" & vbCrLf
    strStrength = strStrength & ".cssWeak{background-color:#FF4545;border-right:solid 1px #BB2B2B;border-bottom:solid 1px #BB2B2B;height:10px; width:50px; display:block; float:left}" & vbCrLf
    strStrength = strStrength & ".cssMedium{background-color:#FFD35E;border-right:solid 1px #E9AE10;border-bottom:solid 1px #E9AE10;height:10px; width:50px; display:block; float:left}" & vbCrLf
    strStrength = strStrength & ".cssStrong{background-color:#3ABB1C;border-right:solid 1px #267A12;border-bottom:solid 1px #267A12;height:10px; width:50px; display:block; float:left}" & vbCrLf
    strStrength = strStrength & ".cssPWT{width:250px; clear:both}" & vbCrLf
    strStrength = strStrength & "</style>" & vbCrLf
    strStrength = strStrength & "<table cellpadding='0' cellspacing='0' class='cssPWT' style='height:16px; width:200px; display:inline'><tr valign='bottom'><td id='idSM1' width='33%' class='cssPWD' align='center'><span style='font-size:1px'>&nbsp;</span><span id='idSMT1' style='display:none;'></span></td><td id='idSM2' width='34%' class='cssPWD' align='center' style='border-left:solid 1px #fff'><span style='font-size:1px'>&nbsp;</span><span id='idSMT0' style='display:inline;font-weight:normal;color:#666'></span><span id='idSMT2' style='display:none;'></span></td><td id='idSM3' width='33%' class='cssPWD' align='center' style='border-left:solid 1px #fff'><span style='font-size:1px'>&nbsp;</span><span id='idSMT3' style='display:none;'></span></td></tr></table>"
    ShowPwdStrength = strStrength
End Function

%>
<!--#include file="../../admin/function/include/fun.asp" -->
