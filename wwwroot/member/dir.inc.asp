<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<%
myname=Request.Cookies("cat")
%>

<style>

/*nav*/
#mainNav{ position:fixed; _position:absolute; top:97px; right:18px; z-index:99995}
a.nav-open{background:url(../../plugins/d-s-management-ui/img/manage/ui-main-member.png) no-repeat -524px -276px; width:40px; height:36px; display:block; position:absolute; top:35%; right:-50px; z-index:99995}
a.nav-close{ display:block; width:60px; height:30px; position:absolute; top:110px; right:20px; z-index:99995;}
#mainNav .show{margin:0; padding:0; list-style:none; background:url(../../plugins/d-s-management-ui/img/manage/ui-main-member.png) no-repeat -633px 0; _background:url(../../plugins/d-s-management-ui/img/manage/menu-bg-ie6-member.gif) no-repeat 0 0; height:445px; width:135px; overflow:hidden; padding:30px 0 0 5px;}
#mainNav .show li{ height:23px; line-height:23px; width:120px; text-indent:20px; margin-left:9px; background:url(../../plugins/d-s-management-ui/img/manage/menu-icon.PNG) no-repeat;}
#mainNav .show li.line{ color:#98272d; background:url(../../plugins/d-s-management-ui/img/manage/line.PNG) repeat-x; height:25px; text-indent:2px}
#mainNav .show li.publish{background-position:0 -14px;}
#mainNav .show li.msg{background-position:0 -44px;}
#mainNav .show li.links{background-position:0 -75px;}
#mainNav .show li.n-slide{background-position:0 -108px;}
#mainNav .show li.setting{background-position:0 -142px;}
#mainNav .show li.html-css{background-position:0 -176px;}
#mainNav .show li.newCus{background-position:0 -213px;}
#mainNav .show li.create{background-position:0 -251px;}
#mainNav .show li.member{background-position:0 -290px;}
#mainNav .show li.highFun{background-position:0 -332px;}
#mainNav .show li.plugins{background-position:0 -370px;}
#mainNav .show li.cloudSkin{background-position:0 -408px;}
#mainNav .show li.list{background-position:0 -442px;}

#mainNav .show li a { color:#807b76; text-decoration:none;}
#mainNav .show li a:hover{color:#000; text-shadow:1px 1px 2px #fff;}
</style>

    <!--menu-->
    <div id="mainNav">
       <ul class="show">
           
           <li class="line">个人中心</li>
           
           <li class="member"><a href="javascript:void(0)" onClick='columnNavShow("mode_index=1", "title1=欢迎界面", "link1=<%=MY_sitePath%>member/website/statistics.asp?name=<%=myname%>", "title2=修改资料", "link2=<%=MY_sitePath%>member/edit_file/edit.asp?name=<%=myname%>", "title3=修改密码", "link3=<%=MY_sitePath%>member/edit_file/edit_ps.asp?name=<%=myname%>");' >资料管理</a></li>
           <li class="plugins"><a href="javascript:void(0)" onClick='columnNavShow("mode_index=1", "title1=网站收藏", "link1=<%=MY_sitePath%>member/collect/webCollect_edit.asp?name=<%=myname%>");' >我的应用</a></li>
           
           <li class="line">互动中心</li>
           
           <li class="publish"><a href="javascript:void(0)" onClick='columnNavShow("mode_index=1", "title1=在线投稿", "link1=<%=MY_sitePath%>member/service/publish_info.asp?name=<%=myname%>", "title2=列表管理", "link2=<%=MY_sitePath%>member/service/publish_info_list.asp?name=<%=myname%>");' >投稿区</a></li>
           
           <li class="msg"><a href="javascript:void(0)" onClick='columnNavShow("mode_index=1", "title1=意见反馈", "link1=<%=MY_sitePath%>member/opinion/write.asp?name=<%=myname%>", "title2=列表管理", "link2=<%=MY_sitePath%>member/opinion/write_list.asp?name=<%=myname%>");' >反馈区</a></li>
           

                            
       </ul>
    
    </div>
   <a class="nav-open" href="javascript:void(0)" title="Open"></a>
   <a class="nav-close" href="javascript:void(0)" title="Close"></a>
