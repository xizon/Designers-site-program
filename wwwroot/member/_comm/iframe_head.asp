<%
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> 
<title></title>
<script src="<%=MY_sitePath%>cat_js/template.js"  type="text/javascript"></script>
<script src="<%=MY_sitePath%>cat_js/jquery.min.js"  type="text/javascript"></script>
<script src="<%=MY_sitePath%>cat_js/manageComm.js"  type="text/javascript"></script>
<script src="<%=MY_sitePath%>cat_js/jquery.wbox.js"            type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<%=MY_sitePath%>admin/_manage_css/commUI.css" />


<!-- form ui[js&css]  begin -->
<script type="text/javascript" src="<%=MY_sitePath%>plugins/d-s-management-ui/jquery.ui.custom.min.js" ></script>
<script type="text/javascript" src="<%=MY_sitePath%>plugins/d-s-management-ui/d-s.ui.comm.js" ></script>
<link rel="stylesheet" type="text/css" href="<%=MY_sitePath%>plugins/d-s-management-ui/jquery.ui.custom.css" />
  <!-- form ui[js&css]  end  -->
  
<script>

$(function(){
	
    //formEffect
	$(".ui-element").jqTransform();
	$('.on_off :checkbox').iphoneStyle();
	
	
	//help
	$(".help").powerFloat({
		targetMode: "remind",
		targetAttr: "data-info",
		position: "1-4"
	});	

});

</script>

</head>
<body oncontextmenu="return false">

<iframe name="iframeAction" style="display:none"></iframe>
