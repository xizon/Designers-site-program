<!--#include file="langColumn_value.asp" -->
<%
'=====================================================================
'  请勿修改此文件
'=====================================================================
%>
<%


'===========================侧边栏标题，独立调用内容标题(案例作品列表)
DY_LANG_1 = langValue2

'===========================侧边栏标题，独立调用内容标题(文章列表)
DY_LANG_2 = langValue1

'===========================侧边栏标题，独立调用内容标题(热文排行列表)
DY_LANG_3 = langValue35
DY_LANG_4 = langValue36
DY_LANG_5 = langValue37


'===========================【待定】
DY_LANG_6 = ""
DY_LANG_7 = ""
DY_LANG_8 = ""
DY_LANG_9 = ""

'===========================侧边栏(分类)
DY_LANG_10 = langValue3
DY_LANG_11 = ""
DY_LANG_12 = ""
DY_LANG_13 = ""
DY_LANG_104 = langValue4

'===========================侧边栏(其他)
DY_LANG_14 = langValue5
DY_LANG_15 = langValue6
DY_LANG_16 = langValue7
DY_LANG_17 = ""
DY_LANG_18 = langValue8
DY_LANG_19 = ""
DY_LANG_20 = ""
DY_LANG_21 = langValue9
DY_LANG_22 = langValue10
DY_LANG_23 = langValue11
DY_LANG_24 = ""
DY_LANG_25 = ""
DY_LANG_26 = ""
DY_LANG_27 = langValue12
DY_LANG_28 = langValue13
DY_LANG_29 = langValue14
DY_LANG_30 = langValue15
DY_LANG_31 = langValue16


'===========================侧边栏(统计)
DY_LANG_32 = langValue17
DY_LANG_33 = ""
DY_LANG_34 = ""
DY_LANG_35 = langValue18
DY_LANG_36 = ""
DY_LANG_37 = langValue19
DY_LANG_38 = ""
DY_LANG_39 = langValue20
DY_LANG_40 = langValue21
DY_LANG_41 = langValue22
DY_LANG_42 = langValue23


'===========================明细内容页-评论、留言

DY_LANG_43 = langValue24
DY_LANG_44 = langValue25
DY_LANG_45 = langValue26
DY_LANG_46 = langValue27
DY_LANG_47 = langValue28
DY_LANG_48 = langValue29
DY_LANG_49 = ""



'===========================首页公告
DY_LANG_50 = langValue31
DY_LANG_51 = langValue32


'===========================部分栏目通用文字
DY_LANG_52 = langValue33
DY_LANG_53 = langValue34


'===========================【待定】
DY_LANG_54 = ""
DY_LANG_55 = ""

'===========================【待定】
DY_LANG_56 = ""
DY_LANG_57 = ""
DY_LANG_58 = ""
DY_LANG_59 = ""
DY_LANG_60 = ""


'===========================分页

DY_LANG_61 = langValue40
DY_LANG_62 = langValue41
DY_LANG_63 = langValue42
DY_LANG_64 = langValue43

'===========================【待定】
DY_LANG_65 = ""


'===========================功能权限
DY_LANG_66 = langValue30

'===========================【待定】
DY_LANG_67 = ""
DY_LANG_68 = ""

'===========================邮件发送标题，内容
DY_LANG_69 = ""
DY_LANG_70 = ""
DY_LANG_71 = ""
DY_LANG_72 = ""


'===========================搜索
DY_LANG_73 = langValue38

'===========================【待定】
DY_LANG_74 = ""
DY_LANG_75 = ""
DY_LANG_76 = ""
DY_LANG_77 = ""


'===========================后台网站动态提示提示窗口
DY_LANG_78 = ""
DY_LANG_79 = langValue46
DY_LANG_80 = langValue47
DY_LANG_81 = langValue48
DY_LANG_82 = ""
DY_LANG_83 = langValue49
DY_LANG_84 = ""
DY_LANG_85 = ""
DY_LANG_86 = ""
DY_LANG_87 = langValue50
DY_LANG_88 = langValue51
DY_LANG_89 = langValue52
DY_LANG_90 = langValue53


'===========================站内微博
DY_LANG_91 = langValue54
DY_LANG_92 = langValue55
DY_LANG_93 = langValue56
DY_LANG_94 = langValue57


'===========================【待定】
DY_LANG_95 = ""
DY_LANG_96 = ""
DY_LANG_97 = ""
DY_LANG_98 = ""
DY_LANG_99 = ""
DY_LANG_100 = ""

'===========================【待定】
DY_LANG_101 = ""


'===========================没有列表数据提示
DY_LANG_102 = langValue39


'===========================静态缓存提示
DY_LANG_103 = langValue58


'===========================会员
DY_LANG_Office_1 = langValue44
DY_LANG_Office_2 = langValue45

%>
