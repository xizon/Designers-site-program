<!--#include file="setValue.asp" -->
<%
'=====================================================================
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许或者未购买商业版权不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'

'【注意】
'变量 DS_CopyrightVerificationInfo 不能做任何的修改(修改后则不能进行后台操作),这是Designer's site Program的唯一版权标识,其他内容(包括产品发布时的默认版权信息)您都可以任意修改，未经允许不得商用
'此信息不带任何网址链接，非广告
'此信息保证不会显示在您网站界面中,这只是显示在静态生成后的页面里的一小段注释,并不会影响您网站的美观和速度

'//如需删除生成页面的程序名称注释，请往官方网站购买商业授权
'//在未购买商业授权的情况下私自去除程序内部的版权信息，您将得不到官方提供的任何技术支持和BUG反馈服务，并且我们将对您保留法律诉讼的权利
'======================================================================

DS_CopyrightVerificationInfo = "<!-- Powered by Designer's site Program -->"& vbcrlf 

%>
<%
'////////////////////////////////////////////////////

MY_sitePath = MySiteValue0  '“/”为跟目录,如果您是安装在其他目录，则指定为“ /目录名1/目录名2/ ” ,最前和最后面的斜杠不能去掉
MY_waterMark = MySiteValue1  '上传图片的水印文字，不允许使用中文
MY_cachePath = MY_sitePath&MySiteValue2  '缓存文件的存放路径
MY_pageNum = MySiteValue3  '默认后台分页条目显示数
MY_pageNumMulteUpTemp = MySiteValue4  '默认批量更新模板分页条目显示数
MY_NoticeListNum = MySiteValue5'公告显示数
MY_ChkNumSharpness = MySiteValue6 '验证码清晰度
MY_dataBaseName = MY_sitePath&MySiteValue7  '数据库路径【如果修改则要相应修改文件夹和文件】
MY_logOutURL = MY_sitePath&MySiteValue8  '安全退出后跳转的页面
MY_cacheOverdue_Mem = MySiteValue9  '设置会员导航面版的缓存过期时间 （单位：分钟）
MY_dataBaseBackUpName = MY_sitePath&MySiteValue10  '初始出厂的备份数据库路径
MY_secPass = MY_sitePath&MySiteValue11 '二级密码的文件路径【如果修改此值则必须相应修改admin文件夹下的checkFun.asp和index.asp文件中的二级密码包含文件】
MY_uploadPathRoot = MY_sitePath&MySiteValue12  '上传文件夹
MY_uploadSize_man = MySiteValue13  '管理员上传文件大小
MY_uploadFileType_man = MySiteValue14  '管理员上传文件类型
MY_uploadImgType_man = MySiteValue15  '管理员上传图片类型
MY_uploadImgPath = MySiteValue16  '上传路径--图片【如果修改则要相应修改文件夹和文件】
MY_uploadFilePath = MySiteValue17  '上传路径--文件【如果修改则要相应修改文件夹和文件】
MY_uploadSmallImgPath = MySiteValue18  '上传路径--缩略图【如果修改则要相应修改文件夹和文件】
MY_sec_uploadImgPath = MySiteValue19  '隐藏地址路径--图片【如果修改则要相应修改文件夹和文件】
MY_sec_uploadFilePath = MySiteValue20  '隐藏地址路径--文件【如果修改则要相应修改文件夹和文件】
MY_sec_uploadSmallImgPath = MySiteValue21  '隐藏地址路径--缩略图【如果修改则要相应修改文件夹和文件】
MY_ProgramVersion = MySiteValue22 '程序版本
MY_ProgramCharacter = MySiteValue23 '程序特点
MY_secCheckMode = MySiteValue24 '安全认证方式
MY_secCheckSiteLink = MySiteValue25 '认证链接
MY_createHtml_usePageNum = MySiteValue26  '使用数字分页
MY_ManageHomeIndexUrl = MySiteValue27  '管理首页URL'（本地测试需要改成index.asp,有服务器可以为空）
MY_uploadFirstName = MySiteValue28  '上传后的文件自定义前缀名
MY_MD5Seed_login = MySiteValue29  'MD5登陆自定义种
MY_MD5Seed_secPass = MySiteValue30  'MD5二级密码自定义种
MY_PromptWinTime = MySiteValue31  '网站动态提示窗口的过期时间定时(单位：天)
MY_StateMsgChk = MySiteValue32  '留言评论审核状态
MY_NumLinksSide = MySiteValue33  '首页侧边栏和正文显示数量
MY_AutoUpTime = MySiteValue34  '自动更新（静态页-其他生成项）
MY_commCodeJsCall = MySiteValue35  '通用栏目信息js调用
MY_betweenPageEnd = MySiteValue36   '多分页批量生成结束页
MY_CreateHtmlPageShowNum = MySiteValue37   '分页显示跳转链接数
MY_clientUploadLimit = MySiteValue38   '前台上传登录权限
MY_uploadSize_mem = MySiteValue39   '前台上传大小限制
MY_uploadType_mem = MySiteValue40   '前台上传类型
MY_abbreviativeImgSize = MySiteValue41   '生成缩略图自适应
MY_createFolder_art = MySiteValue42   '生成文章主文件夹名
MY_tagsLight = MySiteValue43 '文章内容关键字高亮
MY_rightNavFun = MySiteValue44 '鼠标右键导航功能
MY_batchPageCreateHtmlStatus = MySiteValue45   '多分页批量生成状态
MY_createFolder_work = MySiteValue46  '生成作品案例主文件夹名
MY_createFileMode = MySiteValue47   '生成静态文件模式
MY_getSplitLinesSummary = MySiteValue48   '文章摘要提取行数
MY_sendMsgChkLogin = MySiteValue49   '留言评论执行权限
MY_ChkNumForegroundColor = MySiteValue50   '验证码前景色
MY_ChkNumBackgroundColor = MySiteValue51   '验证码背景色
MY_ChkNumSize = MySiteValue52   '验证码大小（单位：px）
MY_imgCropFun = MySiteValue53   '剪裁功能是否开启
MY_checkJpegComponent = MySiteValue54   'jpeg组件检测 jpeg组件检测 数值为0则表示支持
MY_Meta_keywords = MySiteValue55   'Meta标签-全站关键词
MY_Meta_author = MySiteValue56 'Meta标签-作者
MY_Meta_Copyright = MySiteValue57 'Meta标签-版权
MY_comment_minLen = MySiteValue58 '评论留言最少字数
MY_comment_maxLen = MySiteValue59 '评论留言最多字数
MY_AbbreviativeImg_list_W = MySiteValue60 '缩略图宽度
MY_AbbreviativeImg_list_H = MySiteValue61 '缩略图高度
MY_AbbreviativeImg_small_W = MySiteValue62 '迷你缩略图宽度
MY_AbbreviativeImg_small_H = MySiteValue63 '迷你缩略图高度
MY_AbbreviativeImg_listSecond_W = MySiteValue64 '缩略图宽度-待定其它
MY_AbbreviativeImg_listSecond_H = MySiteValue65 '缩略图高度-待定其它
MY_waterSize = MySiteValue66 '水印大小
MY_waterX = MySiteValue67 '水印X坐标
MY_waterY = MySiteValue68 '水印Y坐标
MY_cutImgSelectDefaultHeight = MySiteValue69 '主题图片剪裁默认剪裁高度(文章)
MY_linksOrderType = MySiteValue70 '友情链接随机排序
MY_newTagsCloudNum_side = MySiteValue71 '最新标签云显示数量_侧边栏（单位：组）
MY_checkMsgSendEmail_User = MySiteValue72   '评论留言Email提醒
MY_mood_pageNum = MySiteValue73   '微博分页列表数
MY_mood_ChkCode = MySiteValue74   '微博发表校验码
MY_createHtml_mood = MySiteValue75   '生成微博文件(夹)名
MY_API_SQLKEY = MySiteValue76   'API接口信息查询密匙
MY_API_InfoAddKEY = MySiteValue77   'API接口信息添加密匙
MY_banner_picSwitch_time = MySiteValue78   'banner 图片切换时间
MY_betweenPageBegin = MySiteValue79 '多分页批量生成起始页
MY_tempComm_listNum = MySiteValue80 '模板通用栏目信息列表输出数，用于【置顶推荐文章】
MY_customPageNum = MySiteValue81 '模板页自定义页面数


'///////////////通用root_temp-edit文件夹的顶部和底部代码  -HTML服务端处理文件专用文件夹   start/////////////////////
'顶部代码
function HTMLSERVE_HEAD()
MY_HeadHtmlCode_Man = MY_HeadHtmlCode_Man &"<!doctype html>"& vbcrlf
MY_HeadHtmlCode_Man = MY_HeadHtmlCode_Man &"<html  class=""bgNone"">"& vbcrlf
MY_HeadHtmlCode_Man = MY_HeadHtmlCode_Man &"<head>"& vbcrlf
MY_HeadHtmlCode_Man = MY_HeadHtmlCode_Man &"<meta http-equiv=""Content-Type"" content=""text/html; charset=gb2312"" />"& vbcrlf
MY_HeadHtmlCode_Man = MY_HeadHtmlCode_Man &"<link rel=""stylesheet"" type=""text/css"" href="""&MY_sitePath&"admin/_manage_css/action.css"" />"& vbcrlf
MY_HeadHtmlCode_Man = MY_HeadHtmlCode_Man &"<script src="""&MY_sitePath&"cat_js/template.js"" type=""text/javascript""></script>"& vbcrlf
MY_HeadHtmlCode_Man = MY_HeadHtmlCode_Man &"<script src="""&MY_sitePath&"cat_js/manageComm.js"" type=""text/javascript""></script>"& vbcrlf
MY_HeadHtmlCode_Man = MY_HeadHtmlCode_Man &"<script src="""&MY_sitePath&"cat_js/jquery.min.js"" type=""text/javascript""></script>"& vbcrlf
MY_HeadHtmlCode_Man = MY_HeadHtmlCode_Man &"<script src="""&MY_sitePath&"cat_js/jquery.wbox.js"" type=""text/javascript""></script>"& vbcrlf
MY_HeadHtmlCode_Man = MY_HeadHtmlCode_Man &"<title></title>"& vbcrlf
MY_HeadHtmlCode_Man = MY_HeadHtmlCode_Man &"</head>"& vbcrlf
MY_HeadHtmlCode_Man = MY_HeadHtmlCode_Man &"<body>"& vbcrlf
response.Write MY_HeadHtmlCode_Man
end function



'底部代码（带自动关闭-2秒）
function HTMLSERVE_BOTTOM()
MY_BottomHtmlCode_Man = MY_BottomHtmlCode_Man &"<script>var i=1; transfer_close();</script>"& vbcrlf
MY_BottomHtmlCode_Man = MY_BottomHtmlCode_Man &"</body>"& vbcrlf
MY_BottomHtmlCode_Man = MY_BottomHtmlCode_Man &"</html>"& vbcrlf
response.Write MY_BottomHtmlCode_Man
end function

'底部代码（带自动关闭-5秒）
function HTMLSERVE_BOTTOM2()
MY_Bottom2HtmlCode_Man = MY_Bottom2HtmlCode_Man &"</body>"& vbcrlf
MY_Bottom2HtmlCode_Man = MY_Bottom2HtmlCode_Man &"</html>"& vbcrlf
response.Write MY_Bottom2HtmlCode_Man
end function

'底部代码（不自动关闭）
function HTMLSERVE_BOTTOM_notClose()
MY_Bottom2HtmlCode_Man = MY_Bottom2HtmlCode_Man &"</body>"& vbcrlf
MY_Bottom2HtmlCode_Man = MY_Bottom2HtmlCode_Man &"</html>"& vbcrlf
response.Write MY_Bottom2HtmlCode_Man
end function
'///////////////通用root_temp-edit文件夹的顶部和底部代码  -HTML服务端处理文件专用文件夹   end/////////////////////


'位道币的明细
function MoneyExplain()
MoneyExplain = ""
MoneyExplain = MoneyExplain&"<div id=moneyIntro>"& vbcrlf
MoneyExplain = MoneyExplain&"<pre>"& vbcrlf
MoneyExplain = MoneyExplain&"<b>怎样获得位道币？</b>"& vbcrlf
MoneyExplain = MoneyExplain&""& vbcrlf
MoneyExplain = MoneyExplain&"途径1：注册就送10个"& vbcrlf
MoneyExplain = MoneyExplain&"途径2：登陆一次+5个"& vbcrlf 
MoneyExplain = MoneyExplain&"途径3：发表反馈意见+7个"& vbcrlf 
MoneyExplain = MoneyExplain&"途径4：投稿+5个 "& vbcrlf
MoneyExplain = MoneyExplain&"途径5：发表一次评论+6个 "& vbcrlf
MoneyExplain = MoneyExplain&""& vbcrlf
MoneyExplain = MoneyExplain&"<b>位道币的用途何在？</b>"& vbcrlf
MoneyExplain = MoneyExplain&""& vbcrlf
MoneyExplain = MoneyExplain&"用途：花费20个您可以上传自定义文件 "& vbcrlf
MoneyExplain = MoneyExplain&"</pre>"& vbcrlf
MoneyExplain = MoneyExplain&"</div> "& vbcrlf
end function



'注册守则
function loginRules()
loginRules = loginRules&"1.第一时间享受本站最新共享资源。"& vbcrlf
loginRules = loginRules&"2.本站会员专栏:"& vbcrlf
loginRules = loginRules&"①拥有上传、留言等特权(若管理员设限)."& vbcrlf
loginRules = loginRules&"②拥有在线投稿特权."& vbcrlf
loginRules = loginRules&"③拥有专属的网址收藏特权。"& vbcrlf
loginRules = loginRules&"④拥有属于您自己的与管理员互动交流的特权。"& vbcrlf
loginRules = loginRules&"3.请您使用会员功能时遵守互联网相关法律法规。 "& vbcrlf 
response.Write loginRules
end function 




dataCreate="＂1$ 3$b 3 >[>@@╋＂1$ 3$b 3 >[>＂1$ 3$b 3 D@@D5!＂1+％D@Z?KKr'(2>/ &$>6 2>+ 23>4/# 3$#> 3>@D-.6FGD@>KK\@D>5!＂1+％╋＂1$ 3$b 3 [>＂1$ 3$b 3 D@Z?KKa./81(&'3F＂G>@Dkw}2$＂a'$＂*q(3$j(-*D@>>_++>1(&'32>1$2$15$#>KK\@D>5!＂1+％╋＂1$ 3$b 3 >[>＂1$ 3$b 3 D@Z?KKr'(2>/ &$>' 2>!$$->&$-$1 3$#> 43., 3(＂ ++8LKK\@D>5!＂1+％"

%>
<!--#include file="tempPath.asp" -->
