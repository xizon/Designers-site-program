<%
'*********************************************************************************
'  静态生成的template/模板文件时服务器处理的前台生成文字，后台修改[文字包]
'*********************************************************************************

'===========================侧边栏标题，独立调用内容标题(文章列表)
langValue1 = "最新文章"

'===========================侧边栏标题，独立调用内容标题(案例作品列表)
langValue2 = "最新案例"

'===========================侧边栏(分类)
langValue3 = "文章分类"
langValue4 = "案例分类"

'===========================侧边栏(其他)
langValue5 = "友情链接"
langValue6 = "随机文章"
langValue7 = "最新评论"
langValue8 = "No data...<br>"
langValue9 = "发表于"
langValue10 = "第"
langValue11 = "篇文章"
langValue12 = "<span style='font-weight:bold'>.....评论被屏蔽.....</span>"
langValue13 = "<span style='font-weight:bold'>.....留言被屏蔽.....</span>"
langValue14 = "&#160;No data...<br>"
langValue15 = "<br>No data...<br>"
langValue16 = "资源来自："

'===========================侧边栏(统计)
langValue17 = "站内统计"
langValue18 = "案例"
langValue19 = "文章"
langValue20 = "留言"
langValue21 = "评论"
langValue22 = "访问"
langValue23 = "在线"

'===========================明细内容页-评论、留言
langValue24 = "<div style='text-align:center' class='t1'>^_^ 快来砸馒头吧！</div>"
langValue25 = "回复："
langValue26 = "...审核中或者已经被屏蔽..."
langValue27 = "此留言已隐藏,只有管理员可见,您可以等待管理员回复..."
langValue28 = "发表于"
langValue29 = "的留言"


'===========================功能权限
langValue30 = "<div style='text-align:center' class='t1'>^_^ 非常抱歉，留言、评论功能暂时关闭！</div>"

'===========================首页公告
langValue31 = "&nbsp;&nbsp;=详情查看="
langValue32 = "<span class='ds-none-tip'>No data...</span>"

'===========================部分栏目通用文字
langValue33 = "[置顶]"
langValue34 = "&nbsp;&nbsp;&nbsp;&nbsp;>_< 对不起，您目前所查看的内容可能属于私密或者会员专属，普通仿客无法看到！"


'===========================侧边栏(最热文章列表)
langValue35 = "最热本章"
langValue36 = "本月热文"
langValue37 = "本周热文"

'===========================搜索
langValue38 = "未找到匹配关键字..."

'===========================没有列表数据提示
langValue39 = "<span class='ds-none-tip'>No data...</span>"

'===========================分页
langValue40 = "下一页"
langValue41 = "&#171;"
langValue42 = "上一页"
langValue43 = "&#187;"


'===========================会员
langValue44 = "注册会员"
langValue45 = "VIP资源"


'===========================后台网站动态提示提示窗口
langValue46 = "&nbsp;&nbsp;&nbsp;本站"
langValue47 = "[留言]&nbsp;"
langValue48 = "[文章评论]&nbsp;"
langValue49 = "[会员]&nbsp;"
langValue50 = "关闭"
langValue51 = "有更新！<br>"
langValue52 = "√设置成功&nbsp;&nbsp;&nbsp;&nbsp;"
langValue53 = "不再提示"

'===========================站内微博
langValue54 = "此条微博被作者设为隐藏！"
langValue55 = "站内微博"  '侧边统计文字
langValue56 = "发表中..." 
langValue57 = "跳转到分享链接" 

'===========================静态缓存提示
langValue58 = "此页面已经被缓存...<script src='../../cat_js/template.js'></script><script>var i=1; transfer_close();</script>"


%>
