//Develop: http://www.c945.com   QQ:546007561 (兼容各浏览器)
//================================================


//*************************计时器
var AutoSaveTime=15000;//自动保存时间间隔，15秒
var AutoSaveTimer;// 计时器对象
SetAutoSave();//设置一次自动保存状态


//*************************ajax请求

var xmlHttp;
if (window.XMLHttpRequest)    // code for IE7+, Firefox, Chrome, Opera, Safari
{
	xmlHttp=new XMLHttpRequest();
}
else    // code for IE6, IE5
{
	xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlHttp.onreadystatechange=function()
{
	if (xmlHttp.readyState==4 && xmlHttp.status==200)
	{
		//document.getElementById("saveStateNow").innerHTML=xmlHttp.responseText;
	}
}


function stateChanged() { 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	 { 
	     //document.getElementById("saveStateNow").innerHTML=xmlHttp.responseText 
	 } 
}

function GetXmlHttpObject(){
	var xmlHttp=null;
	try{
		 // Firefox, Opera 8.0+, Safari
		 xmlHttp=new XMLHttpRequest();
		 }
		catch (e)
		 {
		 // Internet Explorer
		 try
		  {
		  xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		  }
		 catch (e)
		  {
		  xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
	 }
	return xmlHttp;
}


function AutoSave() {
	
	//------------获取表单的ID值------------start
	var FormContent;
	var content=$(Path_saveFormItemID).val();
	FormContent=content;

	if ( content == "" ) {
		
		//保存状态
		$('#saveStateNow').fadeIn(2);
		document.getElementById("saveStateNow").innerHTML = "无内容..";
		setTimeout("$('#saveStateNow').fadeOut(500);",500);	
		
	}else{
		
		//保存状态
		$('#saveStateNow').fadeIn(2);
		document.getElementById("saveStateNow").innerHTML = "保存成功!";
		setTimeout("$('#saveStateNow').fadeOut(500);",500);	
			
	}
	
	
	
	//--------------------要传送的变量-------------start
	
	//POST方法传参数避免URL LONG
	xmlHttp.open("POST",Path_saveFunFile,true);
	xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlHttp.send("param=" + escape(FormContent));
	
	xmlHttp.onreadystatechange=stateChanged;   //输入时动态传参




}


//*************************自动保存
function SetAutoSave() {

    // 是否自动保存？

    if(document.getElementById("Draft_AutoSave").checked==true){
	

        // 是，设置计时器
         
        AutoSaveTimer=setInterval("AutoSave()",AutoSaveTime);

		
	}

    else{

        // 否，清除计时器

        clearInterval(AutoSaveTimer);
		
	}

}

//*************************查看草稿

function FileView(){
	window.open(Path_saveDraft, '', 'height=600, width=800, top=0, left=0, toolbar=no, menubar=no, scrollbars=yes, resizable=yes,location=no, status=no')
}

//*************************还原草稿

function getContent(){

	xmlHttp=GetXmlHttpObject()
	if (xmlHttp==null)
	  {
	  alert ("Browser does not support HTTP Request")
	  return
	  } 
	
	xmlHttp.onreadystatechange=stateChanged_getContent 
	xmlHttp.open("GET",Path_saveDraft,true)
	xmlHttp.send(null);
	
	
	//恢复状态
	$('#saveStateNow').fadeIn(2);
	document.getElementById("saveStateNow").innerHTML = "操作成功!";
	setTimeout("$('#saveStateNow').fadeOut(500);",500);		
	

} 

function stateChanged_getContent() { 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	 { 

		 $(Path_saveFormItemID).val(xmlHttp.responseText);
		 
	 } 
}




function FileRestore(){
	if (window.confirm('确定要恢复上一次保存吗?')){

		getContent();
		

		}
		

}



//控制编辑器的尺寸

$(function(){
	$('#increase').click(function(){
      
	  if( $(Path_saveFormItemID).height() < 650 ){
		 $(Path_saveFormItemID).animate({ height : "+=100" },500).css({"overflow":"hidden"});

	  }
	  $(this).blur();
	  return false;
	})
	$('#reduce').click(function(){
		
		if( $(Path_saveFormItemID).height() > 350 ){
			$(Path_saveFormItemID).animate({ height : "-=100" },500).css({"overflow":"hidden"});

		}
	  $(this).blur();
	  return false;
	})
})
