/*
'=====================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=====================================================================
 */
 

function GetWidth() {
	var myWidth = document.documentElement.clientWidth;
	return myWidth;
}
function GetHeight() {
	var myHeight = document.documentElement.clientHeight;
	return myHeight;
}

var switchDis = $("#mainPage",window.parent.document).width();
var columnBoxW = 600;


//main effect			  
$(function(){
	

		
		//floatFrame
		$("#floatFrame").css("height", (GetHeight() - 27) + "px");
		
		//none-plugins
		$("#columnBtnBox-plugins .none").css({"margin-left": "100px"});

		
		//btn
		$(document).ready(function(){
			$(".sub").hover(function() {
					$(this).css("background-position","0 -53px");
				}, function() {
					$(this).css("background-position","0 -3px");
			});
			
	
			$(".reset").hover(function() {
					$(this).css("background-position","0 -155px");
				}, function() {
					$(this).css("background-position","0 -105px");
			});
			$(".ui-slider-handle").hover(function() {
					$(this).css("background-position","0 -38px");
				}, function() {
					$(this).css("background-position","0 0");
			});	
			
			$("#warpper").css("height", (GetHeight() - 50) + "px");
			$(".autoSaveWrapper").css("width", (GetWidth() - 200) + "px");
			
			
			//nav
			$(".nav-close").click(function() {
					$("#mainNav").animate({ "right": "-150px" }, {queue:false,duration:500});
					$(this).animate({ "right": "-150px" }, {queue:false,duration:500});
					$(".nav-open").animate({ "right": "0" }, {queue:false,duration:500});
					$("body").contents().find("#warpper").animate({ "padding-right": "0" }, {queue:false,duration:500});
					
			});
			$(".nav-open").click(function() {
					$("#mainNav").animate({ "right": "18px" }, {queue:false,duration:500});
					$(this).animate({ "right": "-50px" }, {queue:false,duration:500});
					$(".nav-close").animate({ "right": "18px" }, {queue:false,duration:500});
					$("body").contents().find("#warpper").animate({ "padding-right": "130px" }, {queue:false,duration:500});
			});
			
			$(".nav-close",window.parent.document).click(function() {
					$("#warpper").animate({ "padding-right": "0" }, {queue:false,duration:500});
					$("#floatFrame").animate({ "right": "-130px" }, {queue:false,duration:500});
					
					
			});
			$(".nav-open",window.parent.document).click(function() {
					$("#warpper").animate({ "padding-right": "130px" }, {queue:false,duration:500});
					$("#floatFrame").animate({ "right": "0" }, {queue:false,duration:500});
					
			});
			
		});
		
	
		
		//removeloading
		$(document).ready(function(){
			$("#loadingT").fadeOut(500);
			$("#floatFrame").fadeIn(500);
		});
		
		

		
		//mouse listener
		$("body.mainContent").mousemove(function(e){
			if($(e.target).parents('#adminMenuBox',window.parent.document)[0]==undefined){
				$("#topStatus-info",window.parent.document).slideUp(500);  
			}
		});

		
		//登录信息提示
		setTimeout('$("#guideTxt").fadeIn(1000)',2000);
		setTimeout('$("#guideTxt").fadeOut(1000)',7000);	
		

	
	$(window).scroll(function(){
		
		
		if ($(window).scrollTop()>5){
			$("#headStatus").addClass("topShadow");
		}
		else
		{
			$("#headStatus").removeClass("topShadow");
		}
		
		
		//for IE6
		if ($.browser.msie && $.browser.version < 7) {
			var docWidth = document.documentElement.clientWidth;
			var docHeight = document.documentElement.clientHeight;
			var topDis = parseInt(document.documentElement.scrollTop,10)+ (-2);
			$(".btnBox").animate({ top: topDis + "px" }, {queue:false,duration:500});
			var topDis2 = parseInt(document.documentElement.scrollTop,10)+33;
			$("#floatFrame").animate({ top: topDis2 + "px" }, {queue:false,duration:500});
			var topDis3 = parseInt(document.documentElement.scrollTop,10)+0;
			$("#headStyle,.tip,#headStatus").animate({ top: topDis3 + "px" }, {queue:false,duration:500});
			$("#headStatus").css("width",GetWidth() + "px");
			var topDis4 = parseInt(document.documentElement.scrollTop,10)+10;
			$("#topStatus-info").animate({ top: topDis4 + "px" }, {queue:false,duration:500});
			var topDis5 = parseInt(document.documentElement.scrollTop,10)+62;
			$("#iframeSwitchBox").animate({ top: topDis5 + "px" }, {queue:false,duration:500});
			var topDis6 = parseInt(document.documentElement.scrollTop,10)+97;
			$("#mainNav").animate({ top: topDis6 + "px" }, {queue:false,duration:500});
			var topDis7 = parseInt(document.documentElement.scrollTop,10)+70;
			$(".customRightmenuBtn").animate({ top: topDis7 + "px" }, {queue:false,duration:500});
			var topDis8 = parseInt(document.documentElement.scrollTop,10)+55;
			$("#tip_iframe").animate({ top: topDis8 + "px" }, {queue:false,duration:500});
			
			
			
			
			

		}
		
		

	});

	//table
	
	$('tbody tr:even').addClass("alt-row");
	
	//close
	$(".close").click(
		function () {
			$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
				$(this).slideUp(400);
			});
			return false;
		}
	);
	
	$(".closeTopMenu").click(
		function () {
			$("#topStatus-info").fadeOut(500);
		}
	);

});


//初始化
$(document).ready(function(){
	
		//for IE6
		if ($.browser.msie && $.browser.version < 7) {
			$("#headStatus").css("width",GetWidth() + "px");

		}
		
		$("#columnBtnBox",window.parent.document).css({"margin-left":GetWidth() + 200 + "px"});
		$("#columnBtnBox-plugins").css({"margin-left":GetWidth() + 200 + "px"});
		
		

	
	
	
});


//载入页面
function columnNavShow(arg){

    //添加数组元素
	var Ep = [];
	for(i = 0; i < arguments.length; i ++)
	{
	Ep[i] = arguments[i].split(' ').join('').split('=');
	for (var j = Ep[i].length-1; j > 1; j --){
	Ep[i][j-1]+="="+Ep[i].pop();
	}
	
	switch (Ep[i][0])
	{
	case 'title1' : var title1 = Ep[i][1] ; break ;
	case 'title2' : var title2 = Ep[i][1] ; break ;
	case 'title3' : var title3 = Ep[i][1] ; break ;
	case 'title4' : var title4 = Ep[i][1] ; break ;
	case 'title5' : var title5 = Ep[i][1] ; break ;
	case 'title6' : var title6 = Ep[i][1] ; break ;
	case 'title7' : var title7 = Ep[i][1] ; break ;
	case 'title8' : var title8 = Ep[i][1] ; break ;
	case 'title9' : var title9 = Ep[i][1] ; break ;
	case 'title10' : var title10 = Ep[i][1] ; break ;
	case 'title11' : var title11 = Ep[i][1] ; break ;
	case 'title12' : var title12 = Ep[i][1] ; break ;
	case 'title13' : var title13 = Ep[i][1] ; break ;
	case 'title14' : var title14 = Ep[i][1] ; break ;
	case 'title15' : var title15 = Ep[i][1] ; break ;
	case 'link1' : var link1 = Ep[i][1] ; break ;
	case 'link2' : var link2 = Ep[i][1] ; break ;
	case 'link3' : var link3 = Ep[i][1] ; break ;
	case 'link4' : var link4 = Ep[i][1] ; break ;
	case 'link5' : var link5 = Ep[i][1] ; break ;
	case 'link6' : var link6 = Ep[i][1] ; break ;
	case 'link7' : var link7 = Ep[i][1] ; break ;
	case 'link8' : var link8 = Ep[i][1] ; break ;
	case 'link9' : var link9 = Ep[i][1] ; break ;
	case 'link10' : var link10 = Ep[i][1] ; break ;
	case 'link11' : var link11 = Ep[i][1] ; break ;
	case 'link12' : var link12 = Ep[i][1] ; break ;
	case 'link13' : var link13 = Ep[i][1] ; break ;
	case 'link14' : var link14 = Ep[i][1] ; break ;
	case 'link15' : var link15 = Ep[i][1] ; break ;
	case 'mode_index' : var mode_index = Ep[i][1] ; break ;
	case 'mode_column' : var mode_column = Ep[i][1] ; break ;
	case 'mode_plugins' : var mode_plugins = Ep[i][1] ; break ;
	default :;
	}
	}
	
	var chtml = "";
	chtml += '<ul class="columnBtn indexBox">';
	if(title1) {chtml += '<li><a href="' + link1 + '" target="mainPage">' + title1 + '</a></li>';}
	if(title2) {chtml += '<li><a href="' + link2 + '" target="mainPage">' + title2 + '</a></li>';}
	if(title3) {chtml += '<li><a href="' + link3 + '" target="mainPage">' + title3 + '</a></li>';}
	if(title4) {chtml += '<li><a href="' + link4 + '" target="mainPage">' + title4 + '</a></li>';}
	if(title5) {chtml += '<li><a href="' + link5 + '" target="mainPage">' + title5 + '</a></li>';}
	if(title6) {chtml += '<li><a href="' + link6 + '" target="mainPage">' + title6 + '</a></li>';}
	if(title7) {chtml += '<li><a href="' + link7 + '" target="mainPage">' + title7 + '</a></li>';}
	if(title8) {chtml += '<li><a href="' + link8 + '" target="mainPage">' + title8 + '</a></li>';}
	if(title9) {chtml += '<li><a href="' + link9 + '" target="mainPage">' + title9 + '</a></li>';}
	if(title10) {chtml += '<li><a href="' + link10 + '" target="mainPage">' + title10 + '</a></li>';}
	if(title11) {chtml += '<li><a href="' + link11 + '" target="mainPage">' + title11 + '</a></li>';}
	if(title12) {chtml += '<li><a href="' + link12 + '" target="mainPage">' + title12 + '</a></li>';}
	if(title13) {chtml += '<li><a href="' + link13 + '" target="mainPage">' + title13 + '</a></li>';}
	if(title14) {chtml += '<li><a href="' + link14 + '" target="mainPage">' + title14 + '</a></li>';}
	if(title15) {chtml += '<li><a href="' + link15 + '" target="mainPage">' + title15 + '</a></li>';}
	chtml += '</ul>';
	
	
	if (mode_column == 1){
        $("#columnBtnBox-plugins",window.parent.document).stop().animate({ marginLeft: (GetWidth() + 200) + "px" }, {queue:false,duration:500});
		setTimeout(function(){
			$("#columnBtnBox",window.parent.document).css({"margin-left":GetWidth() + 200 + "px"});
			$("#columnBtnBox",window.parent.document).html(chtml);
			$("#columnBtnBox",window.parent.document).animate({ marginLeft: (GetWidth() -columnBoxW) / 2 + "px" }, {queue:false,duration:500});
		},500);
		
	
		
		$("#iframeMask",window.parent.document).stop().animate({ marginLeft: "0px" }, {queue:false,duration:500});
	}
	
	if (mode_index == 1){

		$("#columnBtnBox-plugins").stop().animate({ marginLeft: (GetWidth() + 200) + "px" }, {queue:false,duration:500});
		setTimeout(function(){
			$("#columnBtnBox").css({"margin-left":GetWidth() + 200 + "px"});
			$("#columnBtnBox").html(chtml);
			$("#columnBtnBox").animate({ marginLeft: (GetWidth() -columnBoxW) / 2 + "px" }, {queue:false,duration:500});
		},500);
	
		
		$("#iframeMask").stop().animate({ marginLeft: "0px" }, {queue:false,duration:500});
	
		
	}
	
	if (mode_plugins == 2){
		
		$("#columnBtnBox",window.parent.document).stop().animate({ marginLeft: (GetWidth() + 200) + "px" }, {queue:false,duration:500});
		setTimeout(function(){
			$("#iframeMask",window.parent.document).stop().animate({ marginLeft: "0px" }, {queue:false,duration:500});
		},800);
		setTimeout(function(){
			$("#columnBtnBox-plugins",window.parent.document).stop().animate({ marginLeft: (GetWidth() -columnBoxW) / 2 + "px" }, {queue:false,duration:500});
		},1300);
		
	
		
	}
	
	
	
	
	

	
}
function pluginShow(){
    	var iH = 200; 
	var iW = 500;
	parent.floatWin('插件管理','<iframe name="frameEdit" allowTransparency"=true" frameborder="0"  scrolling="no" height="' + iH + '" width="' + iW + '" hspace="0" vspace="0" src="/demo/manage/admin/pluginsManage_list.asp"></iframe>',iW + 20,0,150,0,0,0);	
	//for IE6
	if ($.browser.msie && $.browser.version < 7) {
		setTimeout('document.frames("frameEdit").location.reload();',500);
	}	
	
}




//================================================
// 浮动位置矫正
//================================================
function absolutePositionCheck(thisID){
	
	$(thisID).css("left", i_getLeftPos() + "px");
	
	
	i_addEvent(window,'scroll',i_handleScroll);
	i_addEvent(window,'resize',i_handleResize);
	
	
	function i_handleScroll() {
		var node =  document.getElementById(thisID);
		if (!node) {
			return;
		}
		
	}
	
	function i_handleResize() {
		var node =  document.getElementById(thisID);
		if (!node) {
			return;
		}
		i_handleScroll();
	}
	
	
	
	function i_getLeftPos() {
		var tw = $(thisID).width();
		var rp = i_getClientWidth()/2 - (tw/2);
		return rp > 0 ? rp : 0;
		
	}

}

//----------------------------------通用取值
function i_addEvent(node,type,l) {
	if (node.addEventListener) {
		node.addEventListener(type,l,false);
	} else {
		node.attachEvent('on' + type,l);
	}
}

/**
 * body后才能调用，因此不能在head里的js调用
 * @returns {Boolean}
 */
 
 

function i_getClientWidth() {
	return i_getDocEle().clientWidth;
}

function i_getDocEle() {
	return document.compatMode == 'CSS1Compat' ? document.documentElement : document.body;
}




