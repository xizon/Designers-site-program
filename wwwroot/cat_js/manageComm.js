/*
'=================================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=================================================================================
*/

function include(path) 
{ 
      var scripts = document.getElementsByTagName("script"); 
      if(!scripts) return;
      var jsPath = scripts[0].src;      
      jsPath=jsPath.substring(0,jsPath.lastIndexOf('/')+1);
      var sobj = document.createElement('script'); 
      sobj.type = "text/javascript"; 
      sobj.src = jsPath+path; 
      var headobj = document.getElementsByTagName('head')[0]; 
      headobj.appendChild(sobj); 
}

include('commConfig.js');

var MyrootPath;


//================================================
//后台信息提示
//================================================

$(document).ready(function(){
	
	if (getQueryStringRegExp('thisState')=="Succeed") 
	{
		ChkFormTip_page('操作成功',1);
	}
	
	if (getQueryStringRegExp('thisState')=="secPassChk") 
	{
	floatWin('权限错误','<div class=tipsFloatWin>×&nbsp;您所在用户组没有此权限!<br/><br/>您可以<a href=' + MyrootPath + 'admin/data_manage/restore_backup.asp>验证身份</a><br/></div>',350,120,100,0,1,0);
	}

	
	//用户管理提示
	if (getQueryStringRegExp('thisState')=="Error1") 
	{
	floatWin('出现错误','<div class=tipsFloatWin>×&nbsp;密码不能为空!</div>',350,120,100,0,1,0);
	}
	
	if (getQueryStringRegExp('thisState')=="Error1_2") 
	{
	floatWin('出现错误','<div class=tipsFloatWin>×&nbsp;用户名或密码不能为空!</div>',350,120,100,0,1,0);
	}
	
	if (getQueryStringRegExp('thisState')=="Error2") 
	{
	floatWin('出现错误','<div class=tipsFloatWin>×&nbsp;密码至少为6位!</div>',350,120,100,0,1,0);
	}
	
	if (getQueryStringRegExp('thisState')=="Error3") 
	{
	floatWin('出现错误','<div class=tipsFloatWin>×&nbsp;密码最多20位!</div>',350,120,100,0,1,0);
	}
	
	if (getQueryStringRegExp('thisState')=="Error4") 
	{
	floatWin('出现错误','<div class=tipsFloatWin>×&nbsp;两次输入的密码不相同!</div>',350,120,100,0,1,0);
	}
	
	if (getQueryStringRegExp('thisState')=="Error5") 
	{
	floatWin('出现错误','<div class=tipsFloatWin>×&nbsp;您的邮箱格式不正确，不能修改!</div>',350,120,100,0,1,0);
	}
	
	if (getQueryStringRegExp('thisState')=="Error6") 
	{
	floatWin('出现错误','<div class=tipsFloatWin>×&nbsp;您原来的密码输入不正确，不能修改!</div>',350,120,100,0,1,0);
	}
	
	//--------
	
	if (getQueryStringRegExp('thisState')=="IPAddCHk") 
	{
	floatWin('出现错误','<div class=tipsFloatWin>×&nbsp;请输入正确的IP!</div>',350,120,100,0,1,0);
	}

});

//iframe加载
function ShowIFrameEdit(title,url,iH,iW,winW,winH,winTop,scrollC){
	var scrollType;
	if (scrollC == 0) {scrollType="no";}else{scrollType="auto";}
	$("#columnBtnBox",window.parent.document).html("");
	parent.floatWin(title,'<iframe name="frameEdit" allowTransparency"=true" frameborder="0"  scrolling="'+ scrollType + '" height="' + iH + '" width="' + iW + '" hspace="0" vspace="0" src="' + url + '"></iframe>',winW,winH,winTop,0,0,1);
	//for IE6
	if ($.browser.msie && $.browser.version < 7) {
		setTimeout('document.frames("frameEdit").location.reload();',500);
	}	
	
}



//缓存清理
function clearCache(){
floatWin('清空缓存','<div align=left><iframe allowTransparency="true"  frameborder="0"  scrolling="no" width="374" hspace="0" height="150" vspace="0" src="' + MyrootPath + 'admin/root_temp-edit/ClearCache.asp"></iframe></div>',378,177,100,0,1,1);	
}


//处理提示
function loadingCreate(){
	ChkFormTip_page("正在处理中...",2);
	$('#needCreateLoadingBtn').fadeOut(200);
}


//iframeChkForm
function ChkFormStatus_iframe(txt,mode){
	if (mode == 0){
			
		$("#tip_iframe").html("<div class=tipsFloatWin>×&nbsp;" + txt + "!</div>").slideDown(500);
		setTimeout('$("#tip_iframe").slideUp(500);',1500);
	
	}else{
		
		$("#tip_iframe").html("<div class=tipsFloatWin>√&nbsp;" + txt + "!</div>").slideDown(500);
		setTimeout('$("#tip_iframe").slideUp(500);',3000);

	}

}


function existImgshowID(id){
	var s=parent.document.getElementById(id);
	if(s){return true}
	else{return false}
}

function ChkFormTip_page(txt,mode){
	var thisClass;

	if (mode == 0){thisClass = "error"; setTimeout('$(".tip").animate({ marginTop: "-100px" }, {queue:false,duration:300});',3000);}
	if (mode == 1){thisClass = "ok"; setTimeout('$(".tip").animate({ marginTop: "-100px" }, {queue:false,duration:300});',4500);}
	if (mode == 2){thisClass = "wait";}
	
	if (mode == 1){
		setTimeout(function(){
			$(".tip").animate({ marginTop: "0px" }, {queue:false,duration:300});
		},1500);
	}
	if (mode == 0 || mode == 2){
		$(".tip").animate({ marginTop: "0px" }, {queue:false,duration:300});
	}
		
	$(".tip div").html('<span class="' + thisClass + '">' + txt + '!</span>');
	

}

//静态生成框架提示
function ChkFormTip_pageCreateHtml(txt){
	var autoHid = 0;

	$(".tip",window.parent.document).animate({ marginTop: "0px" }, {queue:false,duration:300});
	$(".tip div",window.parent.document).html('<span class="ok">' + txt + '!</span>');
	setTimeout('$(".tip",window.parent.document).animate({ marginTop: "-100px" }, {queue:false,duration:300});',3000);

}

//table滑过信息
function tipShow(id,mode){
	if (mode == 0) {
		//onmouseover
		$(id).css("visibility","visible");
	}else{
		//onmouseout
		$(id).css("visibility","hidden");
		
	}
	
	
}
function editFormShow(id,id2,btnID){
	
	var openChk = $(btnID).attr("data-open");;
	if (openChk != 1) {
		$(id).css("display","block");
		$(id2).css("display","none");
		$(btnID).attr("data-open","1");
	
	} else {
		$(id).css("display","none");
		$(id2).css("display","block");
		$(btnID).attr("data-open","0");
	}
		

	
}

//插入多图
function  addImgInput(ID1,ID2,ID3){
	$(ID1,ID3).css("display","block");
	$(ID2).css("display","none");
}

//添加信息
function switchInfo(id,txt1,txt2){
	var openChk = $(id).attr("data-open");;
	if (openChk != 1) {
		$(id).slideDown(500);
		$(id + " span.io").html(txt1);
		$(id).attr("data-open","1");
	
	} else {
		$(id).slideUp(500);
		$(id + " span.io").html(txt2);
		$(id).attr("data-open","0");
	}

	
}

//批量处理开启关闭
function switchBatchCreate(id,btnID,txt1,txt2,url1,url2){
	var openChk = $(btnID).attr("data-open");;
	if (openChk != 1) {
		$(id + " span").html(txt1);
		$(id).attr("href",url1);
		$(btnID).attr("data-open","1");
	
	} else {
		$(id + " span").html(txt2);
		$(id).attr("href",url2);
		$(btnID).attr("data-open","0");
	}

	
}

//全选表单checkBox
function checkAll(formName)
	{
	for (var i=0;i<formName.myselect.length;i++)
	{
	var e = formName.myselect[i];
	if (e.name != 'selectAll')
	   e.checked = formName.selectAll.checked;
	}
}



//表单赋值
function txtSameGet(id1,id2){
	var valNow = $(id1).val();
	$(id2).val(valNow);

	
}

//动态读取显示图片
function imgInputChkSrc(ID,imgShowID){
	
	
	var $jsChkImgSrc=$(ID);
	$jsChkImgSrc.keyup(function(){
		if($jsChkImgSrc.val().indexOf(".jpg") >= 0 || $jsChkImgSrc.val().indexOf(".jpeg") >= 0 || $jsChkImgSrc.val().indexOf(".png") >= 0 || $jsChkImgSrc.val().indexOf(".gif") >= 0){
			$(imgShowID).html('<img src="'+$jsChkImgSrc.val()+'" style="border:#E3E3E3 solid 2px;margin-bottom:10px;" width="220">');
		}	
		
	});	
		
}
