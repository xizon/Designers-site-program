/*
'=================================================================================
'
'    Designer's site Program 全站静态系统
'    xizon@c945.com
'    www.c945.com
'    技术支持：没位道
'    未经允许不得私自用于任何商业用途
'    产品官方网站：http://www.badpark.net   http://www.c945.com/d-s
'
'=================================================================================
*/
function include(path) 
{ 
      var scripts = document.getElementsByTagName("script"); 
      if(!scripts) return;
      var jsPath = scripts[0].src;      
      jsPath=jsPath.substring(0,jsPath.lastIndexOf('/')+1);
      var sobj = document.createElement('script'); 
      sobj.type = "text/javascript"; 
      sobj.src = jsPath+path; 
      var headobj = document.getElementsByTagName('head')[0]; 
      headobj.appendChild(sobj); 
}

include('commConfig.js');

var MyrootPath;

//提交
$("#btnPost").click(function () {
	$('#frmLoginHtml').submit();
});

$(document).ready(function(){
	$('#frmLoginHtml').attr("action", "chk_login.asp");
});




//表单判断
function checkFormLogin(){
	if(document.formM.userName.value=="" || document.formM.userName.value=="帐 号"){
		$("#divCheckuser").html('&nbsp;用户名不能为空！');	
		return false;
	}
	if(document.formM.userPass.value=="" || document.formM.userPass.value=="登录密码"){
		$("#divCheckuser").html('&nbsp;登录密码不能为空！');	
		return false;
	}
	//ok
    $("#btnPost").fadeOut(500);
	$("#subStatus").html("数据验证中...");
	return true;
		
}

function openRegForm(){
	floatWin('会员注册','<iframe name=frameR allowTransparency=true onload=Javascript:SetCwinHeight() frameborder=0  scrolling=no class=memReg hspace=0 vspace=0 src=../../member/reg></iframe>',620,0,50,0,0,1);
	//for IE6
	if ($.browser.msie && $.browser.version < 7) {
		setTimeout('document.frames("frameR").location.reload();',500);
	}
}

function openLoseForm(){
	floatWin('忘记密码','<iframe name=frameL allowTransparency=true onload=Javascript:SetCwinHeight() frameborder=0  scrolling=no  hspace=0 vspace=0 src=../../member/lost_pw/?mode=input></iframe>',620,210,170,0,0,1);
	//for IE6
	if ($.browser.msie && $.browser.version < 7) {
		setTimeout('document.frames("frameL").location.reload();',500);
	}
}

$(document).ready(function(){  

    var logU = 0;
	var logP = 0;

    //regFrame  
	if (getQueryStringRegExp("mode")=="reg") {
		openRegForm();

	}
	
	//ajax判断登录输入信息
	$("#frmLoginHtml").keyup(function(){
	  var thisData1=$("#userName").val();
	  var thisData2=$("#userPass").val();
	  var thisData3=$("#checkcode").val();
	  $.post("chk_input.asp?user=" + thisData1 + "&pass=" + thisData2 + "&code=" + thisData3,function(result){
		$("#divCheckuser").html(result);
		
	  });
	});	

	//快捷写入
	if(MySite.Cookie.get("cat")!=null && MySite.Cookie.get("cat") != ""){$("#userName").val(MySite.Cookie.get("cat")); logU = 1;};
	if(MySite.Cookie.get("catpass")!=null && MySite.Cookie.get("catpass") != ""){$("#userPass").val(MySite.Cookie.get("catpass")); logP = 1;};
	
	if (logU == 1 && logP == 1 && getQueryStringRegExp("client")!="error_PwdorName"){
		$('#frmLoginHtml').attr("action", "chk_login.asp?action=verify&mode=shortcut");
		$('#frmLoginHtml').submit();
	}
	

		
}) 



